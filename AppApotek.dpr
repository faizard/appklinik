program AppApotek;

uses
  Forms,
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls,
  Dialogs, StdCtrls, sEdit, sLabel, ExtCtrls, sPanel, sButton,

  MenuUtama in 'Module\MenuUtama.pas' {frm_menu_utama},
  U_DM in 'Module\U_DM.pas' {dm: TDataModule},
  U_login in 'Module\U_login.pas' {frm_login},
  u_bantu in 'Bin\u_bantu.pas',
  U_User in 'Module\U_User.pas' {frm_user},
  u_Thread in 'Bin\u_Thread.pas',
  U_Obat in 'Module\U_Obat.pas' {frm_obat},
  U_ObatMasuk in 'Module\U_ObatMasuk.pas' {frm_obat_masuk},
  U_DataObatMasuk in 'Module\U_DataObatMasuk.pas' {frm_data_obat_masuk},
  U_inputJumlahObat in 'Module\U_inputJumlahObat.pas' {frm_input_jlhObat},
  U_ObatKeluar in 'Module\U_ObatKeluar.pas' {frm_barang_terjual},
  U_JlhObatTerjual in 'Module\U_JlhObatTerjual.pas' {frm_jlh_obat_keluar},
  U_BiayaAdmin in 'Module\U_BiayaAdmin.pas' {frm_biaya},
  U_AksesUser in 'Module\U_AksesUser.pas' {frm_akses_user},
  U_Satuan in 'Module\U_Satuan.pas' {frm_satuan},
  U_JenisRacikan in 'Module\U_JenisRacikan.pas' {frm_jenis_racikan},
  U_pembayaran_tagihan in 'Module\U_pembayaran_tagihan.pas' {frm_pembayaran},
  U_Closing in 'Module\U_Closing.pas' {frm_closing},
  U_graph in 'Module\U_graph.pas' {frm_graph},
  creamInput in 'Module\creamInput.pas' {frm_inputCream},
  U_ObatRetur in 'Module\U_ObatRetur.pas' {frm_obat_retur},
  U_data_obat_keluar in 'Module\U_data_obat_keluar.pas' {frm_obat_keluar},
  U_koneksi in 'Module\U_koneksi.pas' {frm_koneksi};

{$R *.res}
  var d: String;
  myStringList : TStringList;

begin
  Application.Initialize;
  Application.CreateForm(Tdm, dm);
   try
      CreateADO;
      myStringList:= TStringlist.create;
      myStringList.LoadFromFile('appklinik.dll');
      SplitText(',',myStringList.DelimitedText,myStringList);

//    my_connection.Connected := True;
    with dm.my_connection do begin
      Server:= myStringList[0];
      Username:= myStringList[1];
      Password:= myStringList[2];
      Database:= myStringList[3];
      Port:= StrToInt( myStringList[4]);
      Connect;
    end;

      Application.CreateForm(Tfrm_menu_utama, frm_menu_utama);
      Application.CreateForm(Tfrm_jlh_obat_keluar, frm_jlh_obat_keluar);

   except

      Application.CreateForm(Tfrm_koneksi, frm_koneksi);
      frm_koneksi.ShowModal;
      frm_koneksi.Hide;
   end;




  Application.Run;
end.
