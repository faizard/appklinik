-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_apotek_praya
DROP DATABASE IF EXISTS `db_apotek_praya`;
CREATE DATABASE IF NOT EXISTS `db_apotek_praya` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_apotek_praya`;

-- Dumping structure for procedure db_apotek_praya.getSatuan
DROP PROCEDURE IF EXISTS `getSatuan`;
DELIMITER //
CREATE DEFINER=`` PROCEDURE `getSatuan`(
	IN `kd` BIGINT

)
BEGIN
SELECT * from (

	SELECT DISTINCT t_obat.satuan,'1' as urut FROM `t_obat` WHERE id = kd
	UNION
	SELECT DISTINCT t_obat.satuan_pack,'2' FROM `t_obat` WHERE id = kd
	
	) as r order by urut;
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_cetak_nota
DROP PROCEDURE IF EXISTS `sp_cetak_nota`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cetak_nota`(
	IN `nomornota` VARCHAR(50)

,
	IN `adaracikan` TINYINT
,
	IN `biayaRacik` INT
)
BEGIN
	if adaracikan = 0 then 
	begin
	SELECT
				ok.no_nota,
				ok.nm_obat as nama_obat,
				ok.jumlah_obat_keluar,
				ok.satuan,
				ok.hrga_jual as harga_satuan,
				SUM( ok.hrga_jual * ok.jumlah_obat_keluar ) AS total_harga,
				ok.uang,
				ok.kembalian,
				ok.biaya_listrik,
				ok.biaya_admin,
				( ok.biaya_listrik + ok.biaya_admin ) AS biayaadmin,
				ok.tot_harga,
				biayaRacik  
			FROM
				v_obat_keluar AS ok 
			WHERE
				ok.no_nota = nomornota 
				AND ok.racikan = 0 
			GROUP BY
				ok.id_obat;
				
	end;
		else
			begin
			 SELECT
				ok.no_nota,
				'OBAT RACIKAN' AS nama_obat,
				SUM( ok.jumlah_obat_keluar ) AS jumlah_obat_keluar,
				"Item" as satuan,
				0 AS harga_satuan,
				SUM( round( ok.hrga_jual / ok.jlhper_raciik ) * ok.jumlah_obat_keluar ) AS total_harga,
				ok.uang,
				ok.kembalian,
				ok.biaya_listrik,
				ok.biaya_admin,
				( ok.biaya_listrik + ok.biaya_admin ) AS biayaadmin,
				ok.tot_harga ,
				biayaRacik 
			FROM
				v_obat_keluar AS ok 
			WHERE
				ok.no_nota = nomornota 
				AND ok.racikan = 1 
			UNION
			SELECT
				ok.no_nota,
				ok.nm_obat,
				ok.jumlah_obat_keluar,
				ok.satuan,
				ok.hrga_jual,
				SUM( ok.hrga_jual * ok.jumlah_obat_keluar ) AS total_harga,
				ok.uang,
				ok.kembalian,
				ok.biaya_listrik,
				ok.biaya_admin,
				( ok.biaya_listrik + ok.biaya_admin ) AS biayaadmin,
				ok.tot_harga ,
				biayaRacik 
			FROM
				v_obat_keluar AS ok 
			WHERE
				ok.no_nota = nomornota 
				AND ok.racikan = 0 
			GROUP BY
				ok.id_obat;

			end;	
	 end if;
	END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat
DROP PROCEDURE IF EXISTS `sp_obat`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat`()
BEGIN
	SELECT t_obat.*, CONCAT(t_obat.quantity,",",t_obat.quantity_racik) AS jumlah FROM t_obat;
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat_expire
DROP PROCEDURE IF EXISTS `sp_obat_expire`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat_expire`()
BEGIN
		SELECT count(id) as jlh from t_obat 
			where DATEDIFF(t_obat.expire_date, CURRENT_DATE()) <= 180;
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat_jatuhtempo
DROP PROCEDURE IF EXISTS `sp_obat_jatuhtempo`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat_jatuhtempo`()
BEGIN
		SELECT count(id_obat) as jlh from t_obat_masuk 
		where DATEDIFF(t_obat_masuk.tgl_jatuh_tempo, CURRENT_DATE()) <= 182 and  (lunas ="0" or lunas is null) ;
END//
DELIMITER ;

-- Dumping structure for table db_apotek_praya.t_biaya_lain
DROP TABLE IF EXISTS `t_biaya_lain`;
CREATE TABLE IF NOT EXISTS `t_biaya_lain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_biaya` varchar(100) DEFAULT NULL,
  `biaya` mediumint(9) DEFAULT NULL,
  `statusenabled` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table db_apotek_praya.t_closing
DROP TABLE IF EXISTS `t_closing`;
CREATE TABLE IF NOT EXISTS `t_closing` (
  `id` int(11) NOT NULL,
  `id_obat` bigint(20) NOT NULL DEFAULT 0,
  `quantity` int(11) NOT NULL,
  `quantity_racik` int(11) NOT NULL,
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `jlhper_raciik` int(11) NOT NULL,
  `tahun` year(4) NOT NULL,
  `priode` int(11) NOT NULL,
  KEY `FK_t_closing_t_user` (`id_user`),
  CONSTRAINT `FK_t_closing_t_user` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table db_apotek_praya.t_dokter
DROP TABLE IF EXISTS `t_dokter`;
CREATE TABLE IF NOT EXISTS `t_dokter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nm_dokter` varchar(200) DEFAULT NULL,
  `no_hp` char(12) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `jk` enum('L','P') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table db_apotek_praya.t_jenis
DROP TABLE IF EXISTS `t_jenis`;
CREATE TABLE IF NOT EXISTS `t_jenis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table db_apotek_praya.t_jenis_racikan
DROP TABLE IF EXISTS `t_jenis_racikan`;
CREATE TABLE IF NOT EXISTS `t_jenis_racikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diskripsi` varchar(50) DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table db_apotek_praya.t_obat
DROP TABLE IF EXISTS `t_obat`;
CREATE TABLE IF NOT EXISTS `t_obat` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nm_obat` varchar(100) DEFAULT NULL,
  `quantity` mediumint(9) NOT NULL,
  `satuan` char(15) NOT NULL,
  `quantity_racik` mediumint(9) NOT NULL,
  `jlhper_raciik` mediumint(9) NOT NULL,
  `satuan_racik` varchar(50) NOT NULL,
  `expire_date` date NOT NULL,
  `tgl_jatuh_tempo` date DEFAULT NULL,
  `hrgper_racik` int(11) NOT NULL,
  `hrga_jual` mediumint(9) NOT NULL,
  `jenis` enum('Cream','Obat') NOT NULL,
  `jlhper_pack` mediumint(9) NOT NULL,
  `satuan_pack` varchar(100) NOT NULL,
  `hrgper_pack` int(11) NOT NULL,
  `ket` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000001 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table db_apotek_praya.t_obat_keluar
DROP TABLE IF EXISTS `t_obat_keluar`;
CREATE TABLE IF NOT EXISTS `t_obat_keluar` (
  `norec` bigint(20) NOT NULL AUTO_INCREMENT,
  `no_nota` varchar(50) DEFAULT '0',
  `nama_pengunjung` varchar(100) DEFAULT NULL,
  `id_pasien` bigint(20) DEFAULT NULL,
  `tgl_obat_keluar` datetime NOT NULL,
  `id_obat` bigint(20) NOT NULL,
  `jumlah_obat_keluar` smallint(6) NOT NULL,
  `biaya_listrik` mediumint(9) NOT NULL DEFAULT 0,
  `biaya_admin` mediumint(9) NOT NULL DEFAULT 0,
  `nm_dokter` varchar(150) NOT NULL,
  `tot_harga` int(11) NOT NULL,
  `uang` int(11) NOT NULL DEFAULT 0,
  `kembalian` int(11) NOT NULL DEFAULT 0,
  `insert_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `racikan` char(1) NOT NULL,
  `id_jenis_racikan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`norec`),
  KEY `FK_t_obat_keluar_t_obat` (`id_obat`),
  KEY `FK_t_obat_keluar_t_user` (`id_user`),
  KEY `FK_t_obat_keluar_t_jenis_racikan` (`id_jenis_racikan`),
  CONSTRAINT `FK_t_obat_keluar_t_jenis_racikan` FOREIGN KEY (`id_jenis_racikan`) REFERENCES `t_jenis_racikan` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_keluar_t_obat` FOREIGN KEY (`id_obat`) REFERENCES `t_obat` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_keluar_t_user` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1002441 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table db_apotek_praya.t_obat_masuk
DROP TABLE IF EXISTS `t_obat_masuk`;
CREATE TABLE IF NOT EXISTS `t_obat_masuk` (
  `norec` bigint(20) NOT NULL AUTO_INCREMENT,
  `no_faktur` char(20) NOT NULL,
  `id_suppli` bigint(20) NOT NULL,
  `tgl_fakur` datetime NOT NULL,
  `batch` varchar(50) DEFAULT NULL,
  `id_obat` bigint(20) NOT NULL,
  `expire_date` date NOT NULL,
  `jlh_obat_masuk` mediumint(9) NOT NULL,
  `total_harga` bigint(20) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `discount_harga` float(10,2) NOT NULL,
  `discount` float(10,2) NOT NULL,
  `harga_jual` float NOT NULL,
  `total_harga_beli` bigint(20) NOT NULL,
  `ppn` bigint(20) NOT NULL,
  `total_diskon` bigint(20) NOT NULL,
  `total_harga_iclude_ppn` bigint(20) NOT NULL,
  `tgl_jatuh_tempo` date DEFAULT NULL,
  `lunas` enum('0','1') DEFAULT NULL,
  `tgl_bayar` date DEFAULT NULL,
  `id_user` int(11) NOT NULL DEFAULT 0,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`norec`),
  KEY `FK_t_obat_masuk_t_suppli` (`id_suppli`),
  KEY `FK_t_obat_masuk_t_obat` (`id_obat`),
  KEY `FK_t_obat_masuk_t_user` (`id_user`),
  CONSTRAINT `FK_t_obat_masuk_t_obat` FOREIGN KEY (`id_obat`) REFERENCES `t_obat` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_masuk_t_suppli` FOREIGN KEY (`id_suppli`) REFERENCES `t_supplier` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_masuk_t_user` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table db_apotek_praya.t_pabrik
DROP TABLE IF EXISTS `t_pabrik`;
CREATE TABLE IF NOT EXISTS `t_pabrik` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nm_pabrik` varchar(200) DEFAULT NULL,
  `no_contact` varchar(50) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table db_apotek_praya.t_pasien
DROP TABLE IF EXISTS `t_pasien`;
CREATE TABLE IF NOT EXISTS `t_pasien` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nam_pasien` varchar(100) NOT NULL,
  `alamat` varchar(300) DEFAULT NULL,
  `no_hp` char(12) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `no_rekam_medis` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `no_rekam_medis` (`no_rekam_medis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table db_apotek_praya.t_pot
DROP TABLE IF EXISTS `t_pot`;
CREATE TABLE IF NOT EXISTS `t_pot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namapot` varchar(50) DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table db_apotek_praya.t_puyer
DROP TABLE IF EXISTS `t_puyer`;
CREATE TABLE IF NOT EXISTS `t_puyer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namacetakan` varchar(50) DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table db_apotek_praya.t_satuan
DROP TABLE IF EXISTS `t_satuan`;
CREATE TABLE IF NOT EXISTS `t_satuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `satuan` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table db_apotek_praya.t_supplier
DROP TABLE IF EXISTS `t_supplier`;
CREATE TABLE IF NOT EXISTS `t_supplier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `namapbf` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `no_kontak` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table db_apotek_praya.t_user
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE IF NOT EXISTS `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `status` enum('A','O') DEFAULT NULL,
  `jk` enum('L','P') DEFAULT NULL,
  `no_hp` varchar(12) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for view db_apotek_praya.v_obat
DROP VIEW IF EXISTS `v_obat`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat` (
	`id` BIGINT(20) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity_racik` MEDIUMINT(9) NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`expire_date` DATE NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`hrgper_racik` INT(11) NOT NULL,
	`hrga_jual` MEDIUMINT(9) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrgper_pack` INT(11) NOT NULL,
	`jumlah` VARCHAR(19) NOT NULL COLLATE 'utf8_general_ci',
	`i_expire` INT(7) NULL,
	`i_jatuh_tempo` INT(7) NULL
) ENGINE=MyISAM;

-- Dumping structure for view db_apotek_praya.v_obat_jatuh_tempo
DROP VIEW IF EXISTS `v_obat_jatuh_tempo`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat_jatuh_tempo` (
	`namapbf` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`norec` BIGINT(20) NOT NULL,
	`no_faktur` CHAR(20) NOT NULL COLLATE 'latin1_swedish_ci',
	`id_suppli` BIGINT(20) NOT NULL,
	`tgl_fakur` DATETIME NOT NULL,
	`batch` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`id_obat` BIGINT(20) NOT NULL,
	`expire_date` DATE NOT NULL,
	`jlh_obat_masuk` MEDIUMINT(9) NOT NULL,
	`total_harga` BIGINT(20) NOT NULL,
	`harga_satuan` INT(11) NOT NULL,
	`discount_harga` FLOAT(10,2) NOT NULL,
	`discount` FLOAT(10,2) NOT NULL,
	`harga_jual` FLOAT NOT NULL,
	`total_harga_beli` BIGINT(20) NOT NULL,
	`ppn` BIGINT(20) NOT NULL,
	`total_diskon` BIGINT(20) NOT NULL,
	`total_harga_iclude_ppn` BIGINT(20) NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`lunas` ENUM('0','1') NULL COLLATE 'latin1_swedish_ci',
	`tgl_bayar` DATE NULL,
	`id_user` INT(11) NOT NULL,
	`create_at` TIMESTAMP NOT NULL,
	`username` VARCHAR(200) NULL COLLATE 'latin1_swedish_ci',
	`i_jatuhtempo` INT(7) NULL
) ENGINE=MyISAM;

-- Dumping structure for view db_apotek_praya.v_obat_keluar
DROP VIEW IF EXISTS `v_obat_keluar`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat_keluar` (
	`id` BIGINT(20) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity_racik` MEDIUMINT(9) NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`expire_date` DATE NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`hrgper_racik` INT(11) NOT NULL,
	`hrga_jual` MEDIUMINT(9) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrgper_pack` INT(11) NOT NULL,
	`norec` BIGINT(20) NOT NULL,
	`no_nota` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`nama_pengunjung` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`id_pasien` BIGINT(20) NULL,
	`tgl_obat_keluar` DATETIME NOT NULL,
	`id_obat` BIGINT(20) NOT NULL,
	`jumlah_obat_keluar` SMALLINT(6) NOT NULL,
	`biaya_listrik` MEDIUMINT(9) NOT NULL,
	`biaya_admin` MEDIUMINT(9) NOT NULL,
	`nm_dokter` VARCHAR(150) NOT NULL COLLATE 'latin1_swedish_ci',
	`tot_harga` INT(11) NOT NULL,
	`uang` INT(11) NOT NULL,
	`kembalian` INT(11) NOT NULL,
	`insert_at` TIMESTAMP NOT NULL,
	`racikan` CHAR(1) NOT NULL COLLATE 'latin1_swedish_ci',
	`id_jenis_racikan` INT(11) NOT NULL,
	`id_user` INT(11) NOT NULL,
	`id_jenis_racikan1` INT(11) NOT NULL,
	`diskripsi` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`biaya` INT(11) NULL,
	`username` VARCHAR(200) NULL COLLATE 'latin1_swedish_ci',
	`satuanjual` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- Dumping structure for view db_apotek_praya.v_obat_masuk
DROP VIEW IF EXISTS `v_obat_masuk`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat_masuk` (
	`id` BIGINT(20) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity_racik` MEDIUMINT(9) NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`expire_date` DATE NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`hrgper_racik` INT(11) NOT NULL,
	`hrga_jual` MEDIUMINT(9) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrgper_pack` INT(11) NOT NULL,
	`namapbf` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`no_faktur` CHAR(20) NOT NULL COLLATE 'latin1_swedish_ci',
	`tgl_fakur` DATETIME NOT NULL,
	`tgl_jatuh_tempo_real` DATE NULL,
	`i_jatuhtempo` INT(7) NULL,
	`id_suppli` BIGINT(20) NOT NULL,
	`batch` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`jlh_obat_masuk` MEDIUMINT(9) NOT NULL,
	`total_harga` BIGINT(20) NOT NULL,
	`harga_satuan` INT(11) NOT NULL,
	`discount_harga` FLOAT(10,2) NOT NULL,
	`discount` FLOAT(10,2) NOT NULL,
	`harga_jual` FLOAT NOT NULL,
	`create_at` VARCHAR(21) NULL COLLATE 'utf8mb4_general_ci',
	`total_harga_beli` BIGINT(20) NOT NULL,
	`total_diskon` BIGINT(20) NOT NULL,
	`total_harga_iclude_ppn` BIGINT(20) NOT NULL,
	`ppn` BIGINT(20) NOT NULL,
	`lunas` ENUM('0','1') NULL COLLATE 'latin1_swedish_ci',
	`tgl_bayar` DATE NULL,
	`username` VARCHAR(200) NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- Dumping structure for trigger db_apotek_praya.trg_barang_keluar
DROP TRIGGER IF EXISTS `trg_barang_keluar`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_barang_keluar` AFTER INSERT ON `t_obat_keluar` FOR EACH ROW BEGIN
	declare racikan,jlh_stok_racik,sisa_stok_racik int;
	
	SELECT t_obat_keluar.racikan from t_obat_keluar where t_obat_keluar.norec = new.norec into racikan; 

-- jika bukan racikan

	if racikan ='0' then
	begin
			update t_obat set t_obat.quantity = quantity - NEW.jumlah_obat_keluar
			where t_obat.id = new.id_obat;
	end;		
		else
		-- jika satuan racikan
			begin
				SELECT t_obat.quantity_racik from t_obat where t_obat.id = new.id_obat into jlh_stok_racik;
				set sisa_stok_racik = jlh_stok_racik - new.jumlah_obat_keluar;
				
				if sisa_stok_racik <0 then
				begin
					update t_obat set t_obat.quantity = t_obat.quantity - 1, 
					t_obat.quantity_racik = (t_obat.jlhper_raciik + sisa_stok_racik) where t_obat.id = new.id_obat;
				end;	
					else
						begin
								update t_obat set t_obat.quantity_racik = quantity_racik- new.jumlah_obat_keluar 
								where t_obat.id = new.id_obat;
						end;	
				end if;
				
				
			end;	
			
		
	end if;	
			
			
			
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_apotek_praya.trg_obat_insert
DROP TRIGGER IF EXISTS `trg_obat_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_obat_insert` AFTER INSERT ON `t_obat` FOR EACH ROW BEGIN
	--	update t_obat set t_obat.quantity = quantity-1  where t_obat.id = new.id;
		
	 
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_apotek_praya.trg_obat_masuk
DROP TRIGGER IF EXISTS `trg_obat_masuk`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_obat_masuk` AFTER INSERT ON `t_obat_masuk` FOR EACH ROW BEGIN
		UPDATE t_obat 
		SET t_obat.quantity = quantity + NEW.jlh_obat_masuk,
		t_obat.tgl_jatuh_tempo = new.tgl_jatuh_tempo,
		t_obat.expire_date = new.expire_date 
		WHERE
			t_obat.id = new.id_obat;	
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for view db_apotek_praya.v_obat
DROP VIEW IF EXISTS `v_obat`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat` AS SELECT 
	t_obat.*,
	CONCAT(t_obat.quantity,",",t_obat.quantity_racik) AS jumlah,
	DATEDIFF(t_obat.expire_date, CURRENT_DATE()) AS i_expire,
	DATEDIFF(t_obat.tgl_jatuh_tempo, CURRENT_DATE()) AS i_jatuh_tempo
	from t_obat ;

-- Dumping structure for view db_apotek_praya.v_obat_jatuh_tempo
DROP VIEW IF EXISTS `v_obat_jatuh_tempo`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat_jatuh_tempo`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat_jatuh_tempo` AS -- v_obat_jatuh_tempo


SELECT 
 sup.namapbf,
ob.nm_obat,
ob.satuan,
ob.quantity,
ob.jenis,
 om.*,
 us.username,

 DATEDIFF(om.tgl_jatuh_tempo, CURRENT_DATE()) AS i_jatuhtempo 
 
 
 FROM t_obat_masuk AS om

INNER JOIN t_obat AS ob ON ob.id = om.id_obat
inner join t_user as us on us.id = om.id_user
INNER JOIN t_supplier AS sup ON sup.id = om.id_suppli ;

-- Dumping structure for view db_apotek_praya.v_obat_keluar
DROP VIEW IF EXISTS `v_obat_keluar`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat_keluar`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat_keluar` AS -- v_obat_keluar


SELECT 

t_obat.*,
ok.*,
jn.id as id_jenis_racikan1,
jn.diskripsi,
jn.biaya,
t_user.username,

case when ok.racikan = 1 then t_obat.satuan_racik else t_obat.satuan end as satuanjual

 FROM t_obat_keluar as ok 
		JOIN t_obat ON t_obat.id = ok.id_obat
		JOIN t_user on t_user.id = ok.id_user
		join t_jenis_racikan as jn on jn.id = ok.id_jenis_racikan
	 

/*Menampilkan hnya data obat yg 1 tahun*/
 where
	DATEDIFF(CURRENT_DATE(),ok.tgl_obat_keluar) <360 
	
 ORDER BY ok.insert_at desc ;

-- Dumping structure for view db_apotek_praya.v_obat_masuk
DROP VIEW IF EXISTS `v_obat_masuk`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat_masuk`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat_masuk` AS -- v_obat_masuk

SELECT 

t_obat.*,
t_supplier.namapbf,
t_obat_masuk.no_faktur,
t_obat_masuk.tgl_fakur,
t_obat_masuk.tgl_jatuh_tempo as tgl_jatuh_tempo_real,
DATEDIFF(t_obat_masuk.tgl_jatuh_tempo, CURRENT_DATE()) as i_jatuhtempo,
t_obat_masuk.id_suppli,
t_obat_masuk.batch,
t_obat_masuk.jlh_obat_masuk,
t_obat_masuk.total_harga,
t_obat_masuk.harga_satuan,
t_obat_masuk.discount_harga,
t_obat_masuk.discount,
t_obat_masuk.harga_jual,
date_format(t_obat_masuk.create_at,'%d/%m/%Y %H:%i' ) as create_at,
t_obat_masuk.total_harga_beli,
t_obat_masuk.total_diskon,
t_obat_masuk.total_harga_iclude_ppn,
t_obat_masuk.ppn,
t_obat_masuk.lunas,
t_obat_masuk.tgl_bayar,
t_user.username



 FROM t_obat_masuk 
 INNER JOIN t_obat ON t_obat.id = t_obat_masuk.id_obat
 inner join t_user on t_user.id = t_obat_masuk.id_user

inner join t_supplier on t_supplier.id = t_obat_masuk.id_suppli

/*Menampilkan hnya data obat yg 1 tahun*/
 where
	DATEDIFF(CURRENT_DATE(),t_obat_masuk.tgl_fakur) <360 
	
 ORDER BY t_obat_masuk.tgl_fakur desc ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
