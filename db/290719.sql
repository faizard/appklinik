-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_apotek_praya
DROP DATABASE IF EXISTS `db_apotek_praya`;
CREATE DATABASE IF NOT EXISTS `db_apotek_praya` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_apotek_praya`;

-- Dumping structure for table db_apotek_praya.t_biaya_lain
DROP TABLE IF EXISTS `t_biaya_lain`;
CREATE TABLE IF NOT EXISTS `t_biaya_lain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_biaya` varchar(100) DEFAULT NULL,
  `biaya` mediumint(9) DEFAULT NULL,
  `statusenabled` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table db_apotek_praya.t_dokter
DROP TABLE IF EXISTS `t_dokter`;
CREATE TABLE IF NOT EXISTS `t_dokter` (
  `id` bigint(20) DEFAULT NULL,
  `nm_dokter` varchar(200) DEFAULT NULL,
  `no_hp` char(12) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `jk` enum('L','P') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table db_apotek_praya.t_jenis
DROP TABLE IF EXISTS `t_jenis`;
CREATE TABLE IF NOT EXISTS `t_jenis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table db_apotek_praya.t_obat
DROP TABLE IF EXISTS `t_obat`;
CREATE TABLE IF NOT EXISTS `t_obat` (
  `id` bigint(20) NOT NULL DEFAULT 0,
  `nm_obat` varchar(100) DEFAULT NULL,
  `quantity` mediumint(9) NOT NULL,
  `jlhper_raciik` mediumint(9) NOT NULL,
  `satuan_racik` varchar(50) NOT NULL,
  `satuan` char(15) NOT NULL,
  `satuan_pack` varchar(100) NOT NULL,
  `hrga_jual` mediumint(9) NOT NULL,
  `jenis` enum('Cream','Obat') NOT NULL,
  `jlhper_pack` mediumint(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table db_apotek_praya.t_obat_keluar
DROP TABLE IF EXISTS `t_obat_keluar`;
CREATE TABLE IF NOT EXISTS `t_obat_keluar` (
  `norec` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama_pengunjung` varchar(100) DEFAULT NULL,
  `id_pasien` bigint(20) DEFAULT NULL,
  `tgl_obat_keluar` datetime NOT NULL,
  `id_obat` bigint(20) NOT NULL,
  `jumlah_obat_keluar` smallint(6) NOT NULL,
  `biaya_listrik` mediumint(9) NOT NULL DEFAULT 0,
  `biaya_lain` mediumint(9) NOT NULL DEFAULT 0,
  `nm_dokter` varchar(100) NOT NULL,
  `tot_harga` int(11) NOT NULL,
  `insert_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `racikan` enum('0','1') NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`norec`),
  KEY `FK_t_obat_keluar_t_pasien` (`id_pasien`),
  KEY `FK_t_obat_keluar_t_obat` (`id_obat`),
  KEY `FK_t_obat_keluar_t_user` (`id_user`),
  CONSTRAINT `FK_t_obat_keluar_t_obat` FOREIGN KEY (`id_obat`) REFERENCES `t_obat` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_keluar_t_pasien` FOREIGN KEY (`id_pasien`) REFERENCES `t_pasien` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_keluar_t_user` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1002224 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table db_apotek_praya.t_obat_masuk
DROP TABLE IF EXISTS `t_obat_masuk`;
CREATE TABLE IF NOT EXISTS `t_obat_masuk` (
  `norec` bigint(20) NOT NULL AUTO_INCREMENT,
  `no_faktur` char(20) NOT NULL,
  `id_suppli` bigint(20) NOT NULL,
  `tgl_fakur` datetime NOT NULL,
  `batch` varchar(50) DEFAULT NULL,
  `id_obat` bigint(20) NOT NULL,
  `expire_date` date NOT NULL,
  `jlh_obat_masuk` mediumint(9) NOT NULL,
  `total_harga` bigint(20) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `discount_harga` float(10,2) NOT NULL,
  `discount` float(10,2) NOT NULL,
  `harga_jual` float NOT NULL,
  `total_harga_beli` bigint(20) NOT NULL,
  `ppn` bigint(20) NOT NULL,
  `total_diskon` bigint(20) NOT NULL,
  `total_harga_iclude_ppn` bigint(20) NOT NULL,
  `tgl_jatuh_tempo` date NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT 0,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`norec`),
  KEY `FK_t_obat_masuk_t_suppli` (`id_suppli`),
  KEY `FK_t_obat_masuk_t_obat` (`id_obat`),
  KEY `FK_t_obat_masuk_t_user` (`id_user`),
  CONSTRAINT `FK_t_obat_masuk_t_obat` FOREIGN KEY (`id_obat`) REFERENCES `t_obat` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_masuk_t_suppli` FOREIGN KEY (`id_suppli`) REFERENCES `t_supplier` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_masuk_t_user` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table db_apotek_praya.t_pabrik
DROP TABLE IF EXISTS `t_pabrik`;
CREATE TABLE IF NOT EXISTS `t_pabrik` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nm_pabrik` varchar(200) DEFAULT NULL,
  `no_contact` varchar(50) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table db_apotek_praya.t_pasien
DROP TABLE IF EXISTS `t_pasien`;
CREATE TABLE IF NOT EXISTS `t_pasien` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nam_pasien` varchar(100) NOT NULL,
  `alamat` varchar(300) DEFAULT NULL,
  `no_hp` char(12) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `no_rekam_medis` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `no_rekam_medis` (`no_rekam_medis`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table db_apotek_praya.t_satuan
DROP TABLE IF EXISTS `t_satuan`;
CREATE TABLE IF NOT EXISTS `t_satuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `satuan` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table db_apotek_praya.t_supplier
DROP TABLE IF EXISTS `t_supplier`;
CREATE TABLE IF NOT EXISTS `t_supplier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `namapbf` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `no_kontak` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table db_apotek_praya.t_user
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE IF NOT EXISTS `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `status` enum('A','O') DEFAULT NULL,
  `jk` enum('L','P') DEFAULT NULL,
  `no_hp` varchar(12) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for view db_apotek_praya.v_obat_keluar
DROP VIEW IF EXISTS `v_obat_keluar`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat_keluar` (
	`id` BIGINT(20) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrga_jual` MEDIUMINT(9) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`norec` BIGINT(20) NOT NULL,
	`nama_pengunjung` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`id_pasien` BIGINT(20) NULL,
	`nm_dokter` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`jumlah_obat_keluar` SMALLINT(6) NOT NULL,
	`tot_harga` INT(11) NOT NULL,
	`tgl_obat_keluar` DATETIME NOT NULL,
	`insert_at` TIMESTAMP NOT NULL
) ENGINE=MyISAM;

-- Dumping structure for view db_apotek_praya.v_obat_masuk
DROP VIEW IF EXISTS `v_obat_masuk`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat_masuk` (
	`id` BIGINT(20) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrga_jual` MEDIUMINT(9) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`no_faktur` CHAR(20) NOT NULL COLLATE 'latin1_swedish_ci',
	`tgl_fakur` DATETIME NOT NULL,
	`batch` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`jlh_obat_masuk` MEDIUMINT(9) NOT NULL,
	`total_harga` BIGINT(20) NOT NULL,
	`harga_satuan` INT(11) NOT NULL,
	`discount_harga` FLOAT(10,2) NOT NULL,
	`discount` FLOAT(10,2) NOT NULL,
	`harga_jual` FLOAT NOT NULL,
	`create_at` VARCHAR(21) NULL COLLATE 'utf8mb4_general_ci',
	`total_harga_beli` BIGINT(20) NOT NULL,
	`total_diskon` BIGINT(20) NOT NULL,
	`total_harga_iclude_ppn` BIGINT(20) NOT NULL,
	`ppn` BIGINT(20) NOT NULL
) ENGINE=MyISAM;

-- Dumping structure for trigger db_apotek_praya.trg_barang_keluar
DROP TRIGGER IF EXISTS `trg_barang_keluar`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_barang_keluar` AFTER INSERT ON `t_obat_keluar` FOR EACH ROW BEGIN
		update t_obat set t_obat.quantity = quantity - NEW.jumlah_obat_keluar
	where t_obat.id = new.id_obat;
			
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_apotek_praya.trg_obat_masuk
DROP TRIGGER IF EXISTS `trg_obat_masuk`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_obat_masuk` AFTER INSERT ON `t_obat_masuk` FOR EACH ROW BEGIN

 


	update t_obat set quantity = quantity + NEW.jlh_obat_masuk
	where t_obat.id = new.id_obat;
	
 
	
	
		
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for view db_apotek_praya.v_obat_keluar
DROP VIEW IF EXISTS `v_obat_keluar`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat_keluar`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat_keluar` AS SELECT 

t_obat.*,
ok.norec,
ok.nama_pengunjung,
ok.id_pasien,
ok.nm_dokter,
ok.jumlah_obat_keluar,
ok.tot_harga,
ok.tgl_obat_keluar,
ok.insert_at 


 FROM t_obat_keluar as ok 
 INNER JOIN t_obat
ON t_obat.id = ok.id_obat

/*Menampilkan hnya data obat yg 1 tahun*/
 where
	DATEDIFF(CURRENT_DATE(),ok.tgl_obat_keluar) <360 
	
 ORDER BY ok.insert_at desc ;

-- Dumping structure for view db_apotek_praya.v_obat_masuk
DROP VIEW IF EXISTS `v_obat_masuk`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat_masuk`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat_masuk` AS SELECT 

t_obat.*,

t_obat_masuk.no_faktur,
t_obat_masuk.tgl_fakur,
-- t_obat_masuk.nm_pbf,
t_obat_masuk.batch,
t_obat_masuk.jlh_obat_masuk,
t_obat_masuk.total_harga,
t_obat_masuk.harga_satuan,
t_obat_masuk.discount_harga,
t_obat_masuk.discount,
t_obat_masuk.harga_jual,
date_format(t_obat_masuk.create_at,'%d/%m/%Y %H:%i' ) as create_at,
t_obat_masuk.total_harga_beli,
t_obat_masuk.total_diskon,
t_obat_masuk.total_harga_iclude_ppn,
t_obat_masuk.ppn  


 FROM t_obat_masuk INNER JOIN t_obat
ON t_obat.id = t_obat_masuk.id_obat

/*Menampilkan hnya data obat yg 1 tahun*/
 where
	DATEDIFF(CURRENT_DATE(),t_obat_masuk.tgl_fakur) <360 
	
 ORDER BY t_obat_masuk.create_at desc ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
