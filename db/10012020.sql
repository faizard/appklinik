-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.3.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_apotek_praya
CREATE DATABASE IF NOT EXISTS `db_apotek_praya` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_apotek_praya`;

-- Dumping structure for function db_apotek_praya.getBiayaBersih
DELIMITER //
CREATE DEFINER=`` FUNCTION `getBiayaBersih`(
	`noNota` VARCHAR(50)



) RETURNS bigint(20)
BEGIN
DECLARE a BIGINT;

 	SELECT  sum(ok.hrgajualbersih) AS biaya FROM t_obat_keluar AS ok WHERE ok.no_nota = noNota ORDER BY biaya into a;
 	
 	return round(a,-2);
END//
DELIMITER ;

-- Dumping structure for function db_apotek_praya.getBiayaListrik
DELIMITER //
CREATE DEFINER=`` FUNCTION `getBiayaListrik`(
	`nonota` INT

) RETURNS int(11)
BEGIN
DECLARE a int;

 	SELECT ok.biaya_listrik FROM t_obat_keluar AS ok WHERE ok.no_nota = nonota ORDER BY ok.biaya_listrik DESC LIMIT 1 into a;
 	
 	return a;
END//
DELIMITER ;

-- Dumping structure for function db_apotek_praya.getBiayaServis
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `getBiayaServis`(
	`nota` VARCHAR(50)
,
	`biayaSatuan` INT



) RETURNS int(11)
BEGIN
	DECLARE biaya int;
	
	SELECT  count(ok.no_nota) * biayaSatuan as biaya_servis 
	from t_obat_keluar as ok where ok.no_nota = nota   AND ok.racikan = 1
	into biaya;
	
	if biaya > 5000 then 
		return 5000 ;
		else
			return biaya;
	end if;		
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.getSatuan
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `getSatuan`(
	IN `kd` BIGINT

)
BEGIN
SELECT * from (

	SELECT DISTINCT t_obat.satuan,'1' as urut FROM `t_obat` WHERE id = kd
	UNION
	SELECT DISTINCT t_obat.satuan_pack,'2' FROM `t_obat` WHERE id = kd
	
	) as r order by urut;
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_cetak_nota
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cetak_nota`(
	IN `nomornota` VARCHAR(50)

,
	IN `adaracikan` TINYINT
,
	IN `biayaRacik` INT














)
BEGIN
	if adaracikan = 0 then 
	begin
	SELECT
				ok.no_nota,
				ok.id_obat,
				'0' as racikan,
				ok.nm_obat as nama_obat,
				ok.jumlah_obat_keluar,
				ok.satuan,
				ok.satuan as hargaJual,
				ok.hrga_jual as harga_satuan,
				ok.biaya as biayaPerRacikan,
				SUM( ok.hrga_jual * ok.jumlah_obat_keluar ) AS total_harga,
				ok.uang,
				( ok.uang - ok.tot_harga ) as  kembalian,
				ok.biaya_listrik,
			--	ok.biaya_admin,
				'0' AS biayaadmin, -- biaya listrik
				ok.tot_harga,
				ok.biaya_racikan as  biayaRacik,
				getBiayaServis(ok.no_nota, 1000) as biayaServis,
				ok.hrga_jual
			FROM
				v_obat_keluar AS ok 
			WHERE
				ok.no_nota = nomornota 
				AND ok.racikan = 0 
			GROUP BY
				ok.id_obat;
				
	end;
		else
			begin
			 SELECT
				ok.no_nota,
				'R00000' as id_obat,
				'1' as racikan,
				ok.diskripsi as nama_obat,
				SUM( ok.jumlah_obat_keluar ) AS jumlah_obat_keluar,
				"Item" as satuan,
				0 AS harga_satuan,
				sum(ok.hrga_jual) as hargaJual,
				ok.biaya as biayaPerRacikan,
				SUM( round( ok.hrga_jual / ok.jlhper_raciik ) * ok.jumlah_obat_keluar ) AS total_harga,
				ok.uang,
				( ok.uang - ok.tot_harga ) as  kembalian,
				ok.biaya_listrik,
			--	ok.biaya_admin, 
				getBiayaListrik(ok.no_nota) AS biayaadmin, -- biaya listrik
				ok.tot_harga ,
				ok.biaya_racikan as  biayaRacik,
				getBiayaServis(ok.no_nota, 1000) as biayaServis,
				sum(ok.harga_satuan) as hargaJualSatuan
			FROM
				v_obat_keluar AS ok 
			WHERE
				ok.no_nota = nomornota 
				AND ok.racikan = 1 
			group by ok.diskripsi	
			UNION
			SELECT
				ok.no_nota,
				ok.id_obat,
				ok.racikan,
				ok.nm_obat,
				ok.jumlah_obat_keluar,
				ok.satuan,
				sum(ok.hrga_jual) as hargaJual,
				ok.biaya as biayaPerRacikan,
				ok.hrga_jual,
				SUM( ok.hrga_jual * ok.jumlah_obat_keluar ) AS total_harga,
				ok.uang,
				( ok.uang - ok.tot_harga ) as  kembalian,
				ok.biaya_listrik,
			--	ok.biaya_admin,
				getBiayaListrik(ok.no_nota) AS biayaadmin, -- biaya listrik
				ok.tot_harga ,
				ok.biaya_racikan as  biayaRacik,
				getBiayaServis(ok.no_nota, 1000) as biayaServis,
				ok.hrga_jual
			FROM
				v_obat_keluar AS ok 
			WHERE
				ok.no_nota = nomornota 
				AND ok.racikan = 0 
			GROUP BY
				ok.id_obat;

			end;	
	 end if;
	END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat`()
BEGIN
	SELECT t_obat.*, CONCAT(t_obat.quantity,",",t_obat.quantity_racik) AS jumlah FROM t_obat;
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat_expire
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat_expire`()
BEGIN
		SELECT count(id) as jlh from t_obat 
			where DATEDIFF(t_obat.expire_date, CURRENT_DATE()) <= 180;
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat_jatuhtempo
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat_jatuhtempo`()
BEGIN
		SELECT count(id_obat) as jlh from t_obat_masuk 
		where DATEDIFF(t_obat_masuk.tgl_jatuh_tempo, CURRENT_DATE()) <= 182 and  (lunas ="0" or lunas is null) ;
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat_keluar
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat_keluar`(
	IN `tglAwal` DATE,
	IN `tglAkhir` DATE







)
BEGIN
-- v_obat_keluar


		SELECT 
		
		t_obat.*,
		case when t_obat.jenis='obat' then t_obat.hrga_jual else 0 end as harga_jual_obat,
		case when t_obat.jenis='cream' then t_obat.hrga_jual else 0 end as harga_jual_cream,
		ok.*,
		case when ok.racikan = 1 then "v" else "" end as racikan1,
		jn.id as id_jenis_racikan1,
		(SELECT racikan from t_obat_keluar where no_nota = ok.no_nota order by racikan desc limit 1) as adaRacikan,
		
		 case when ok.racikan = 1 then concat (jn.diskripsi,' / ',ok.jlh_sub_racikan) else 'Non Racikan' end as diskripsi,
		jn.biaya,
		t_user.username,
		
		(SELECT getBiayaServis(no_nota, 1000)) as biayaServis,
		(SELECT `getBiayaBersih`(no_nota)) as totalBersih,
		round((SELECT `getBiayaBersih`(no_nota)* 0.1), - 2 ) as ppn,
		(select ok.jlh_sub_racikan * jn.biaya as bb from t_obat_keluar as okk where okk.no_nota = ok.no_nota order by bb desc limit 1 ) as biaya_racikan,
		 -- sum( case when ok.racikan = 1 then 1000 else 0 end ) as biayaServis,
		(SELECT SUM(biaya_listrik) AS biaya FROM ( SELECT biaya_listrik FROM t_obat_keluar WHERE 
			  tgl_obat_keluar between tglAwal and tglAkhir GROUP by no_nota ORDER BY  biaya_listrik DESC ) as f
		) as tot_biaya_listrik,
		
		case when ok.racikan = 1 then t_obat.satuan_racik else t_obat.satuan end as satuanjual
		
	
		
		 FROM t_obat_keluar as ok 
				JOIN t_obat ON t_obat.id = ok.id_obat
				JOIN t_user on t_user.id = ok.id_user
				join t_jenis_racikan as jn on jn.id = ok.id_jenis_racikan
			 
		
		/*Menampilkan hnya data obat yg 1 tahun*/
		 where
			DATEDIFF(CURRENT_DATE(),ok.tgl_obat_keluar) <360 
			and ok.tgl_obat_keluar between tglAwal and tglAkhir
		--	where ok.tgl
			
			group by norec
			
		 ORDER BY ok.insert_at desc 
		 
; END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat_masuk
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat_masuk`(
	IN `tglAwal` DATE,
	IN `tglAkhir` DATE


)
BEGIN

-- v_obat_masuk
		
		SELECT 
		
		t_obat.*,
		t_supplier.namapbf,
		t_obat_masuk.no_faktur,
		t_obat_masuk.tgl_fakur,
		t_obat_masuk.tgl_jatuh_tempo as tgl_jatuh_tempo_real,
		DATEDIFF(t_obat_masuk.tgl_jatuh_tempo, CURRENT_DATE()) as i_jatuhtempo,
		t_obat_masuk.id_suppli,
		t_obat_masuk.batch,
		t_obat_masuk.jlh_obat_masuk,
		t_obat_masuk.total_harga,
		t_obat_masuk.harga_satuan,
		t_obat_masuk.discount_harga,
		t_obat_masuk.discount,
		t_obat_masuk.harga_jual,
		date_format(t_obat_masuk.create_at,'%d/%m/%Y %H:%i' ) as create_at,
		t_obat_masuk.total_harga_beli,
		(t_obat_masuk.jlh_obat_masuk * t_obat_masuk.harga_satuan ) as total_harga_beli1,
		
		t_obat_masuk.total_diskon,
		t_obat_masuk.total_harga_iclude_ppn,
		t_obat_masuk.ppn,
		(SELECT SUM(ppn) FROM ( SELECT om.ppn  FROM t_obat_masuk AS om where om.tgl_fakur between tglAwal and tglAkhir GROUP BY no_faktur) AS d) as tot_ppn,
		round((t_obat_masuk.jlh_obat_masuk * t_obat_masuk.harga_satuan) * 0.1 ) as ppn1,
		(SELECT SUM(jlh) FROM ( SELECT om.total_harga_iclude_ppn as jlh  FROM t_obat_masuk AS om where om.tgl_fakur between tglAwal and tglAkhir GROUP BY no_faktur) AS d) as tot_harga_icludePPN,
		t_obat_masuk.lunas,
		t_obat_masuk.tgl_bayar,
		t_user.username
		
		
		
		 FROM t_obat_masuk 
		 INNER JOIN t_obat ON t_obat.id = t_obat_masuk.id_obat
		 inner join t_user on t_user.id = t_obat_masuk.id_user
		
		inner join t_supplier on t_supplier.id = t_obat_masuk.id_suppli
		
		/*Menampilkan hnya data obat yg 1 tahun*/
		 where
			DATEDIFF(CURRENT_DATE(),t_obat_masuk.tgl_fakur) <360 
			and t_obat_masuk.tgl_fakur between tglAwal and tglAkhir
			
		 ORDER BY t_obat_masuk.tgl_fakur desc 
 
; END//
DELIMITER ;

-- Dumping structure for table db_apotek_praya.t_biaya_lain
CREATE TABLE IF NOT EXISTS `t_biaya_lain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_biaya` varchar(100) DEFAULT NULL,
  `biaya` mediumint(9) DEFAULT NULL,
  `statusenabled` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_biaya_lain: ~2 rows (approximately)
/*!40000 ALTER TABLE `t_biaya_lain` DISABLE KEYS */;
INSERT IGNORE INTO `t_biaya_lain` (`id`, `nm_biaya`, `biaya`, `statusenabled`) VALUES
	(1, 'Biaya Listrik', 2000, NULL);
/*!40000 ALTER TABLE `t_biaya_lain` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_closing
CREATE TABLE IF NOT EXISTS `t_closing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_obat` bigint(20) NOT NULL DEFAULT 0,
  `quantity` int(11) NOT NULL,
  `quantity_racik` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `jlhper_raciik` int(11) NOT NULL,
  `tahun` year(4) NOT NULL,
  `priode` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_t_closing_t_obat` (`id_obat`),
  KEY `FK_t_closing_t_user` (`id_user`),
  CONSTRAINT `FK_t_closing_t_obat` FOREIGN KEY (`id_obat`) REFERENCES `t_obat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_t_closing_t_user` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_closing: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_closing` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_closing` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_dokter
CREATE TABLE IF NOT EXISTS `t_dokter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nm_dokter` varchar(200) DEFAULT NULL,
  `dranak` enum('1','0') DEFAULT NULL,
  `no_hp` char(12) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `jk` enum('L','P') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_dokter: ~3 rows (approximately)
/*!40000 ALTER TABLE `t_dokter` DISABLE KEYS */;
INSERT IGNORE INTO `t_dokter` (`id`, `nm_dokter`, `dranak`, `no_hp`, `alamat`, `jk`) VALUES
	(24, 'dr. Ella', '0', NULL, NULL, NULL),
	(25, 'dr.Angel', '0', NULL, NULL, NULL),
	(26, 'dr.Andrew', '0', NULL, NULL, NULL),
	(56, 'dr. Eka', '1', NULL, NULL, NULL),
	(57, '-', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `t_dokter` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_jenis
CREATE TABLE IF NOT EXISTS `t_jenis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_jenis: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_jenis` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_jenis` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_jenis_racikan
CREATE TABLE IF NOT EXISTS `t_jenis_racikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diskripsi` varchar(50) DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  `Column a` int(11) DEFAULT NULL,
  `Column b` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_jenis_racikan: ~8 rows (approximately)
/*!40000 ALTER TABLE `t_jenis_racikan` DISABLE KEYS */;
INSERT IGNORE INTO `t_jenis_racikan` (`id`, `diskripsi`, `biaya`, `Column a`, `Column b`) VALUES
	(0, '-', 0, NULL, NULL),
	(1, 'Puyer', 600, NULL, NULL),
	(2, 'Kapsul1', 400, NULL, NULL),
	(3, 'Syrup', 300, NULL, NULL),
	(4, 'Kapsul', 700, NULL, NULL),
	(83, 'Pot 5-15  gram', 5000, NULL, NULL),
	(86, 'Pot > 30 gram', 10000, NULL, NULL);
/*!40000 ALTER TABLE `t_jenis_racikan` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_obat
CREATE TABLE IF NOT EXISTS `t_obat` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enabled` bit(1) NOT NULL,
  `nm_obat` varchar(100) DEFAULT NULL,
  `quantity` float NOT NULL DEFAULT 0,
  `satuan` char(15) NOT NULL,
  `quantity_racik` float NOT NULL DEFAULT 0,
  `jlhper_raciik` mediumint(9) NOT NULL,
  `satuan_racik` varchar(50) NOT NULL,
  `expire_date` date NOT NULL,
  `tgl_jatuh_tempo` date DEFAULT NULL,
  `hrgper_racik` int(11) NOT NULL,
  `hrga_jual` int(11) NOT NULL DEFAULT 0,
  `harga_satuan` int(11) NOT NULL DEFAULT 0,
  `jenis` enum('Cream','Obat') NOT NULL,
  `jlhper_pack` mediumint(9) NOT NULL,
  `satuan_pack` varchar(100) NOT NULL,
  `hrgper_pack` int(11) NOT NULL,
  `ket` text DEFAULT NULL,
  `hrgbeli_cream` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000944 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_obat: ~278 rows (approximately)
/*!40000 ALTER TABLE `t_obat` DISABLE KEYS */;
INSERT IGNORE INTO `t_obat` (`id`, `enabled`, `nm_obat`, `quantity`, `satuan`, `quantity_racik`, `jlhper_raciik`, `satuan_racik`, `expire_date`, `tgl_jatuh_tempo`, `hrgper_racik`, `hrga_jual`, `harga_satuan`, `jenis`, `jlhper_pack`, `satuan_pack`, `hrgper_pack`, `ket`, `hrgbeli_cream`) VALUES
	(1000386, b'1', 'Acyclovir generik', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 1000, 966, 'Obat', 100, 'Box', 0, '0', 0),
	(1000387, b'1', 'Alkohol 70% 100 ml botol', 1, 'botol', 0, 1, 'botol', '2024-07-02', '2020-07-01', 0, 4000, 3049, 'Obat', 1, 'botol', 0, '0', 0),
	(1000388, b'1', 'Alkohol 70% 300 ml botol', 1, 'botol', 0, 1, 'botol', '2024-07-03', '2020-07-01', 0, 0, 12650, 'Obat', 1, 'botol', 0, '0', 0),
	(1000389, b'1', 'Nobor ', 30, 'tablet', 0, 1, 'tablet', '2024-07-04', '2020-07-01', 0, 6000, 4180, 'Obat', 30, 'Box', 0, '0', 0),
	(1000390, b'1', 'Multigyn active gel ', 12, 'botol', 0, 1, 'botol', '2024-07-05', '2021-11-19', 0, 297000, 17500, 'Obat', 1, 'botol', 0, '0', 0),
	(1000391, b'1', 'Epexol tablet', 100, 'tablet', 0, 1, 'tablet', '2024-07-06', '2020-07-01', 0, 1200, 891, 'Obat', 100, 'Box', 0, '0', 0),
	(1000392, b'1', 'Epexol sirup', 1, 'botol', 0, 1, 'botol', '2024-07-07', '2020-07-01', 0, 25000, 18480, 'Obat', 1, 'botol', 0, '0', 0),
	(1000393, b'1', 'Epexol drop ', 4, 'botol', 0, 1, 'botol', '2019-11-09', '2019-11-16', 0, 55000, 40370, 'Obat', 1, 'botol', 0, '0', 0),
	(1000394, b'1', 'Mucera drop', 1, 'botol', 0, 1, 'botol', '2024-07-09', '2020-07-01', 0, 55000, 40700, 'Obat', 1, 'botol', 0, '0', 0),
	(1000395, b'1', 'Mucera sirup', 1, 'botol', 0, 1, 'botol', '2024-07-10', '2020-07-01', 0, 33000, 24200, 'Obat', 1, 'botol', 0, '0', 0),
	(1000396, b'1', 'Mucos drop', 0, 'botol', 0, 1, 'botol', '2024-07-11', '2020-07-01', 0, 52000, 37950, 'Obat', 1, 'botol', 0, '0', 0),
	(1000397, b'1', 'Mucos sirup', 1, 'botol', 0, 1, 'botol', '2024-07-12', '2020-07-01', 0, 25000, 18150, 'Obat', 1, 'botol', 0, '0', 0),
	(1000398, b'1', 'Amlodipine 10 mg ', 97, 'tablet', 0, 1, 'tablet', '2024-07-13', '2023-11-16', 0, 1300, 873, 'Obat', 100, 'Box', 0, '0', 0),
	(1000399, b'1', 'Amlodipine generik', 44, 'tablet', 0, 1, 'tablet', '2024-07-14', '2020-07-01', 0, 1300, 1067, 'Obat', 50, 'Box', 0, '0', 0),
	(1000400, b'1', 'Amoxiciliin  generik', 130, 'tablet', 0, 1, 'tablet', '2024-07-15', '2023-11-16', 0, 600, 399, 'Obat', 100, 'Box', 0, '0', 0),
	(1000401, b'1', 'Amoxan 500 mg', 100, 'kapsul', 0, 1, 'kapsul', '2024-07-16', '2020-07-01', 0, 5000, 3410, 'Obat', 100, 'Box', 0, '0', 0),
	(1000402, b'1', 'Opimox', 100, 'kapsul', 0, 1, 'kapsul', '2024-07-17', '2020-07-01', 0, 5000, 3465, 'Obat', 100, 'Box', 0, '0', 0),
	(1000403, b'1', 'Amoxan drop ', 1, 'botol', 0, 1, 'botol', '2024-07-18', '2020-07-01', 0, 33000, 24035, 'Obat', 1, 'botol', 0, '0', 0),
	(1000404, b'1', 'Amoxan sirup ', 1, 'botol', 0, 1, 'botol', '2024-07-19', '2020-07-01', 0, 45000, 24090, 'Obat', 1, 'botol', 0, '0', 0),
	(1000405, b'1', 'Amoxan Forte sirup', 0, 'botol', 0, 1, 'botol', '2024-07-20', '2020-07-01', 0, 45000, 34210, 'Obat', 1, 'botol', 0, '0', 0),
	(1000406, b'1', 'Antalgin generik', 97, 'tablet', 0, 1, 'tablet', '2024-07-21', '2020-07-01', 0, 0, 319, 'Obat', 100, 'Box', 0, '0', 0),
	(1000407, b'1', 'Antasida DOEN tablet', 100, 'tablet', 0, 1, 'tablet', '2024-07-22', '2020-07-01', 0, 0, 211, 'Obat', 100, 'Box', 0, '0', 0),
	(1000408, b'1', 'Polysilane tablet', 5, 'strip', 0, 1, 'strip ', '2024-07-23', '2020-07-01', 0, 11000, 7700, 'Obat', 5, 'Box', 0, '0', 0),
	(1000409, b'1', 'Promag', 12, 'tablet', 0, 1, 'tablet', '2024-07-24', '2020-07-01', 0, 8000, 6411, 'Obat', 12, 'Box', 0, '0', 0),
	(1000410, b'1', 'Sanmaag sirup', 1, 'botol', 0, 1, 'botol', '2024-07-25', '2020-07-01', 0, 36500, 26180, 'Obat', 1, 'botol', 0, '0', 0),
	(1000411, b'1', 'Mylanta sirup', 1, 'botol', 0, 1, 'botol', '2024-07-26', '2020-07-01', 0, 16000, 12067, 'Obat', 1, 'botol', 0, '0', 0),
	(1000412, b'1', 'Antasida DOEN sirup', 1, 'botol', 0, 1, 'botol', '2024-07-27', '2023-11-16', 0, 15000, 3830, 'Obat', 1, 'botol', 0, '0', 0),
	(1000413, b'1', 'Burraginol N supp', 1, 'tube', 0, 1, 'tube', '2024-07-28', '2020-07-01', 0, 0, 71445, 'Obat', 1, 'tube', 0, '0', 0),
	(1000414, b'1', 'Burraginol S supp', 1, 'supp', 0, 1, 'supp', '2024-07-29', '2020-07-01', 0, 113000, 122500, 'Obat', 1, 'supp', 0, '0', 0),
	(1000415, b'1', 'Thrombopop gel ', 1, 'tube', 0, 20, 'gram', '2024-07-30', '2020-07-01', 0, 113000, 0, 'Obat', 1, 'tube', 0, '0', 0),
	(1000416, b'1', 'Laderma Cream', 1, 'tube', 0, 1, 'tube', '2024-07-31', '2020-07-01', 0, 113000, 79200, 'Obat', 1, 'tube', 0, '0', 0),
	(1000417, b'1', 'Folavit', 100, 'tablet', 0, 1, 'tablet', '2024-08-01', '2020-07-01', 0, 1500, 891, 'Obat', 100, 'Box', 0, '0', 0),
	(1000418, b'1', 'Mefinal', 100, 'tablet', 0, 1, 'tablet', '2024-08-02', '2020-07-01', 0, 2000, 1430, 'Obat', 100, 'Box', 0, '0', 0),
	(1000419, b'1', 'Plasminex', 100, 'tablet', 0, 1, 'tablet', '2024-08-03', '2020-07-01', 0, 4000, 2904, 'Obat', 100, 'Box', 0, '0', 0),
	(1000420, b'1', 'Depakene sirup', 1, 'botol', 0, 1, 'botol', '2024-08-04', '2020-07-01', 0, 233000, 172370, 'Obat', 1, 'botol', 0, '0', 0),
	(1000421, b'1', 'Xidane', 30, 'kapsul', 0, 1, 'kapsul', '2024-08-05', '2020-07-01', 0, 11500, 9075, 'Obat', 30, 'Box', 0, '0', 0),
	(1000422, b'1', 'Asta plus', 30, 'kaplet', 0, 1, 'kaplet', '2024-08-06', '2020-07-01', 0, 13000, 9350, 'Obat', 30, 'Box', 0, '0', 0),
	(1000423, b'1', 'Dermia cream 15 gr', 1, 'tube', 0, 15, 'tube', '2024-08-07', '2020-07-01', 0, 60000, 42900, 'Obat', 1, 'tube', 0, '0', 0),
	(1000424, b'1', 'Xidane gel', 96, 'tube', 22.5, 30, 'Gram', '2024-08-08', '2020-07-01', 0, 105000, 80800, 'Obat', 1, 'tube', 0, '0', 0),
	(1000425, b'1', 'Mezatrin 250 mg ', 30, 'tablet', 0, 1, 'tablet', '2024-08-09', '2020-07-01', 0, 20000, 0, 'Obat', 30, 'Box', 0, '0', 0),
	(1000426, b'1', 'Azitromisin generik', 20, 'tablet', 0, 1, 'tablet', '2024-08-10', '2020-07-01', 0, 0, 3245, 'Obat', 20, 'Box', 0, '0', 0),
	(1000427, b'1', 'Hot in Cream ', 1, 'tube', 0, 1, 'tube', '2024-08-11', '2020-07-01', 0, 0, 18700, 'Obat', 1, 'tube', 0, '0', 0),
	(1000428, b'1', 'Batugin Elixir', 1, 'botol', 0, 1, 'botol', '2024-08-12', '2020-07-01', 0, 56000, 41040, 'Obat', 1, 'botol', 0, '0', 0),
	(1000429, b'1', 'Neozep forte', 100, 'tablet', 0, 1, 'tablet', '2024-08-13', '2020-07-01', 0, 750, 504, 'Obat', 100, 'Box', 0, '0', 0),
	(1000430, b'1', 'Paratusin tablet', 20, 'strip', 0, 1, 'strip', '2024-08-14', '2020-07-01', 0, 20000, 11066, 'Obat', 20, 'Box', 0, '0', 0),
	(1000431, b'1', 'Decolsin tablet', 25, 'strip', 0, 1, 'strip', '2024-08-15', '2020-07-01', 0, 3500, 2332, 'Obat', 25, 'Box', 0, '0', 0),
	(1000432, b'1', 'Flucadex', 100, 'kaplet', 0, 1, 'kaplet', '2024-08-16', '2020-07-01', 0, 0, 529, 'Obat', 100, 'Box', 0, '0', 0),
	(1000433, b'1', 'Dextral tablet ', 150, 'tablet', 0, 1, 'tablet', '2024-08-17', '2020-07-01', 0, 0, 557, 'Obat', 150, 'Box', 0, '0', 0),
	(1000434, b'1', 'Actifed merah Batuk pilek sirup', 0, 'botol', 0, 1, 'botol', '2024-08-18', '2020-07-01', 0, 64000, 46872, 'Obat', 1, 'botol', 0, '0', 0),
	(1000435, b'1', 'Uni baby cough sirup', 1, 'botol', 0, 1, 'botol', '2024-08-19', '2020-07-01', 0, 6000, 4959, 'Obat', 1, 'botol', 0, '0', 0),
	(1000436, b'1', 'OBH combi anak ', 1, 'botol', 0, 1, 'botol', '2024-08-20', '2020-07-01', 0, 10000, 12100, 'Obat', 1, 'botol', 0, '0', 0),
	(1000437, b'1', 'OBH combi dewasa jahe ', 11, 'botol', 0, 1, 'botol', '2024-08-21', '2023-11-16', 0, 15000, 920, 'Obat', 1, 'botol', 0, '0', 0),
	(1000438, b'1', 'OBH combi dewasa mentol', 1, 'botol', 0, 1, 'botol', '2024-08-22', '2020-07-01', 0, 15000, 10120, 'Obat', 1, 'botol', 0, '0', 0),
	(1000439, b'1', 'Skinbright Lightening CC cream Beige', 1, 'tube', 0, 1, 'tube', '2024-08-23', '2020-07-01', 0, 90000, 65450, 'Obat', 1, 'tube', 0, '0', 0),
	(1000440, b'1', 'Skinbright Lightening CC cream Normal', 1, 'tube', 0, 1, 'tube', '2024-08-24', '2020-07-01', 0, 90000, 65450, 'Obat', 1, 'tube', 0, '0', 0),
	(1000441, b'1', 'Betadine gargle', 1, 'botol', 0, 1, 'botol', '2024-08-25', '2020-07-01', 0, 0, 12133, 'Obat', 1, 'botol', 0, '0', 0),
	(1000442, b'1', 'Betadine sol 5 ml', 1, 'botol', 0, 1, 'botol', '2024-08-26', '2020-07-01', 0, 6000, 4235, 'Obat', 1, 'botol', 0, '0', 0),
	(1000443, b'1', 'Betadine 300 ml', 1, 'botol', 0, 1, 'botol', '2024-08-27', '2020-07-01', 0, 14000, 0, 'Obat', 1, 'botol', 0, '0', 0),
	(1000444, b'1', 'Betason krim 5 gr', 1, 'tube', 0, 5, 'gram', '2024-08-28', '2020-07-01', 0, 12000, 8745, 'Obat', 1, 'tube', 0, '0', 0),
	(1000445, b'1', 'Betason N krim 5 gr ', 1, 'tube', 0, 5, 'gram', '2024-08-29', '2020-07-01', 0, 19000, 13475, 'Obat', 1, 'tube', 0, '0', 0),
	(1000446, b'1', 'Mucohexin', 100, 'tablet', 0, 1, 'tablet', '2024-08-30', '2020-07-01', 0, 1000, 539, 'Obat', 100, 'Box', 0, '0', 0),
	(1000447, b'1', 'Mucohexin sirup', 1, 'botol', 0, 1, 'botol', '2024-08-31', '2020-07-01', 0, 24000, 17160, 'Obat', 1, 'botol', 0, '0', 0),
	(1000449, b'1', 'Qcef sirup ', 1, 'botol', 0, 1, 'botol', '2024-09-02', '2020-07-01', 0, 73000, 53900, 'Obat', 1, 'botol', 0, '0', 0),
	(1000450, b'1', 'Cefat sirup', 1, 'botol', 0, 1, 'botol', '2024-09-03', '2020-07-01', 0, 62000, 45980, 'Obat', 1, 'botol', 0, '0', 0),
	(1000451, b'1', 'Cefat forte sirup', 1, 'botol', 0, 1, 'botol', '2024-09-04', '2020-07-01', 0, 109000, 77550, 'Obat', 1, 'botol', 0, '0', 0),
	(1000452, b'1', 'Cefadroxil generik', 50, 'kapsul', 0, 1, 'kapsul', '2024-09-05', '2020-07-01', 0, 1250, 1716, 'Obat', 50, 'Box', 0, '0', 0),
	(1000453, b'1', 'Qcef 500 mg', 30, 'kapsul', 0, 1, 'kapsul', '2024-09-06', '2020-07-01', 0, 15000, 11000, 'Obat', 30, 'Box', 0, '0', 0),
	(1000454, b'1', 'Cefat 500 mg', 100, 'kapsul', 0, 1, 'kapsul', '2024-09-07', '2020-07-01', 0, 14700, 10885, 'Obat', 100, 'Box', 0, '0', 0),
	(1000455, b'1', 'Widoxil', 100, 'kapsul', 0, 1, 'kapsul', '2024-09-08', '2020-07-01', 0, 13000, 9350, 'Obat', 100, 'Box', 0, '0', 0),
	(1000456, b'1', 'Cefixime 100 mg Hexa/KF', 50, 'kapsul', 0, 1, 'kapsul', '2024-09-09', '2020-07-01', 0, 3250, 1437, 'Obat', 50, 'Box', 0, '0', 0),
	(1000457, b'1', 'Cefixime 100 mg Nulab ', 50, 'kapsul', 0, 1, 'kapsul', '2024-09-10', '2020-07-01', 0, 3250, 2395, 'Obat', 50, 'Box', 0, '0', 0),
	(1000458, b'1', 'Sporetik', 30, 'kapsul', 0, 1, 'kapsul', '2024-09-11', '2020-07-01', 0, 29000, 21208, 'Obat', 30, 'Box', 0, '0', 0),
	(1000459, b'1', 'Inbacef 200 mg ', 30, 'kaplet', 0, 1, 'kaplet', '2024-09-12', '2020-07-01', 0, 30000, 22000, 'Obat', 30, 'Box', 0, '0', 0),
	(1000460, b'1', 'Cetirizine Hexa', 50, 'tablet', 0, 1, 'tablet', '2024-09-13', '2020-07-01', 0, 500, 314, 'Obat', 50, 'Box', 0, '0', 0),
	(1000461, b'1', 'Cetirizine Nulab', 100, 'tablet', 0, 1, 'tablet', '2024-09-14', '2020-07-01', 0, 500, 314, 'Obat', 100, 'Box', 0, '0', 0),
	(1000462, b'1', 'Cerini tablet', 20, 'kapsul', 0, 1, 'kapsul', '2024-09-15', '2020-07-01', 0, 5000, 3680, 'Obat', 20, 'Box', 0, '0', 0),
	(1000463, b'1', 'Cerini sirup', 1, 'botol', 0, 1, 'botol', '2024-09-16', '2020-07-01', 0, 58000, 42680, 'Obat', 1, 'botol', 0, '0', 0),
	(1000464, b'1', 'Cerini drop ', 1, 'botol', 0, 1, 'botol', '2024-09-17', '2020-07-01', 0, 91500, 67650, 'Obat', 1, 'botol', 0, '0', 0),
	(1000465, b'1', 'Erlamycetin tetes telinga ', 1, 'botol', 0, 1, 'botol', '2024-09-18', '2020-07-01', 0, 11000, 7810, 'Obat', 1, 'botol', 0, '0', 0),
	(1000466, b'1', 'Alleron ', 200, 'kapsul', 0, 1, 'kapsul', '2024-09-19', '2020-07-01', 0, 200, 94, 'Obat', 200, 'Box', 0, '0', 0),
	(1000467, b'1', 'Ciprofloxacin generik', 100, 'tablet', 0, 1, 'tablet', '2024-09-20', '2020-07-01', 0, 1000, 330, 'Obat', 100, 'Box', 0, '0', 0),
	(1000468, b'1', 'Clindamycin 150 mg ', 50, 'kapsul', 0, 1, 'kapsul', '2024-09-21', '2020-07-01', 0, 1000, 0, 'Obat', 50, 'Box', 0, '0', 0),
	(1000469, b'1', 'Clindamycin 300 mg ', 50, 'kapsul', 0, 1, 'kapsul', '2024-09-22', '2020-07-01', 0, 1900, 0, 'Obat', 50, 'Box', 0, '0', 0),
	(1000470, b'1', 'Clinoma', 100, 'Kapsul', 0, 1, 'Kapsul', '2024-09-23', '2020-07-01', 0, 1700, 1375, 'Obat', 100, 'Box', 0, '0', 0),
	(1000471, b'1', 'Indanox 300 mg', 59, 'kapsul', 0, 1, 'kapsul', '2024-09-24', '2020-07-01', 0, 10000, 7480, 'Obat', 60, 'Box', 0, '0', 0),
	(1000472, b'1', 'Mediklin gel 15 gr ', 1, 'tube', 0, 15, 'Gram', '2024-09-25', '2020-07-01', 0, 31000, 25190, 'Obat', 1, 'tube', 0, '0', 0),
	(1000473, b'1', 'Mediklin TR gel 15 gr', 1, 'tube', 0, 15, 'Gram', '2024-09-26', '2020-07-01', 0, 0, 44330, 'Obat', 1, 'tube', 0, '0', 0),
	(1000474, b'1', 'Provula', 28, 'tablet', 0, 1, 'tablet', '2024-09-27', '2020-07-01', 0, 21500, 15950, 'Obat', 30, 'Box', 0, '0', 0),
	(1000475, b'1', 'Clabat', 20, 'kaplet', 0, 1, 'kaplet', '2024-09-28', '2020-07-01', 0, 23500, 17160, 'Obat', 20, 'Box', 0, '0', 0),
	(1000476, b'1', 'Claneksi sirup', 1, 'botol', 0, 1, 'botol', '2024-09-29', '2020-07-01', 0, 77000, 56980, 'Obat', 1, 'botol', 0, '0', 0),
	(1000477, b'1', 'Biocollagenta', 50, 'kapsul', 0, 1, 'kapsul', '2024-09-30', '2020-07-01', 0, 5000, 3311, 'Obat', 50, 'Box', 0, '0', 0),
	(1000478, b'1', 'Sanprima', 100, 'tablet', 0, 1, 'tablet', '2024-10-01', '2020-07-01', 0, 1500, 957, 'Obat', 100, 'Box', 0, '0', 0),
	(1000479, b'1', 'Sanprima sirup', 1, 'botol', 0, 1, 'botol', '2024-10-02', '2020-07-01', 0, 37000, 26620, 'Obat', 1, 'botol', 0, '0', 0),
	(1000480, b'1', 'Helmig\'s Curcumin', 5, 'sachet', 0, 1, 'sachet', '2024-10-03', '2020-07-01', 0, 0, 0, 'Obat', 5, 'Box', 0, '0', 0),
	(1000481, b'1', 'Heptasan', 100, 'tablet', 0, 1, 'tablet', '2024-10-04', '2020-07-01', 0, 350, 0, 'Obat', 100, 'Box', 0, '0', 0),
	(1000482, b'1', 'Pronicy', 100, 'kaplet', 0, 1, 'kaplet', '2024-10-05', '2020-07-01', 0, 350, 275, 'Obat', 100, 'Box', 0, '0', 0),
	(1000483, b'1', 'Inerson krim 15 gr ', 0, 'tube', 3.5, 15, 'gram', '2024-10-06', '2020-07-01', 0, 102000, 75075, 'Obat', 1, 'tube', 0, '0', 0),
	(1000484, b'1', 'Nupeson krim 10 gr ', 1, 'tube', 0, 10, 'gram', '2024-10-07', '2020-07-01', 0, 30000, 24200, 'Obat', 1, 'tube', 0, '0', 0),
	(1000485, b'1', 'Dextamine', 100, 'kaplet', 0, 1, 'kaplet', '2024-10-08', '2020-07-01', 0, 2500, 1815, 'Obat', 100, 'Box', 0, '0', 0),
	(1000486, b'1', 'Soldextam', 200, 'kaplet', 0, 1, 'kaplet', '2024-10-09', '2020-07-01', 0, 300, 231, 'Obat', 200, 'Box', 0, '0', 0),
	(1000487, b'1', 'BDM sirup', 1, 'botol', 0, 1, 'botol', '2024-10-10', '2020-07-01', 0, 55000, 40700, 'Obat', 1, 'botol', 0, '0', 0),
	(1000488, b'1', 'Cortidex', 92.2, 'tablet', 0, 1, 'tablet', '2024-10-11', '2020-07-01', 0, 400, 308, 'Obat', 100, 'Box', 0, '0', 0),
	(1000489, b'1', 'Dexaharsen', 200, 'kaplet', 0, 1, 'kaplet', '2024-10-12', '2020-07-01', 0, 300, 173, 'Obat', 200, 'Box', 0, '0', 0),
	(1000490, b'1', 'Grathazone', 200, 'tablet', 0, 1, 'tablet', '2024-10-13', '2020-07-01', 0, 300, 114, 'Obat', 200, 'Box', 0, '0', 0),
	(1000491, b'1', 'Faridexon', 100, 'kaplet', 0, 1, 'kaplet', '2024-10-14', '2020-07-01', 0, 300, 91, 'Obat', 100, 'Box', 0, '0', 0),
	(1000492, b'1', 'Carbidu', 200, 'tablet', 0, 1, 'tablet', '2024-10-15', '2020-07-01', 0, 250, 198, 'Obat', 200, 'Box', 0, '0', 0),
	(1000493, b'1', 'Confortin cream 20 gr tube', 1, 'tube', 0, 1, 'tube', '2024-10-16', '2020-07-01', 0, 39000, 26400, 'Obat', 1, 'tube', 0, '0', 0),
	(1000494, b'1', 'Doksisiklin 100 mg', 100, 'kapsul', 0, 1, 'kapsul', '2024-10-17', '2020-07-01', 0, 0, 660, 'Obat', 100, 'Box', 0, '0', 0),
	(1000495, b'1', 'Dohixat', 100, 'kapsul', 0, 1, 'kapsul', '2024-10-18', '2020-07-01', 0, 500, 395, 'Obat', 100, 'Box', 0, '0', 0),
	(1000496, b'1', 'Vosedon sirup', 1, 'botol', 0, 1, 'botol', '2024-10-19', '2020-07-01', 0, 41500, 30470, 'Obat', 1, 'botol', 0, '0', 0),
	(1000497, b'1', 'Vometa drop', 1, 'botol', 0, 1, 'botol', '2024-10-20', '2020-07-01', 0, 59000, 43450, 'Obat', 1, 'botol', 0, '0', 0),
	(1000498, b'1', 'Erysanbe 500 mg', 97, 'kaplet', 0, 1, 'kaplet', '2024-10-21', '2020-07-01', 0, 3450, 2863, 'Obat', 100, 'Box', 0, '0', 0),
	(1000499, b'1', 'Erysanbe sirup', 1, 'botol', 0, 1, 'botol', '2024-10-22', '2020-07-01', 0, 35000, 23760, 'Obat', 1, 'botol', 0, '0', 0),
	(1000500, b'1', 'Erymed krim 20 gr', 0, 'tube', 16.6, 20, 'Gram', '2024-10-23', '2020-07-01', 0, 39000, 31570, 'Obat', 1, 'tube', 0, '0', 0),
	(1000501, b'1', 'Govazol', 3, 'kapsul', 0, 1, 'kapsul', '2024-10-24', '2020-07-01', 0, 0, 82500, 'Obat', 3, 'Box', 0, '0', 0),
	(1000502, b'1', 'Fluconazole 150 mg generik', 10, 'kapsul', 0, 1, 'kapsul', '2024-10-25', '2020-07-01', 0, 27000, 22000, 'Obat', 10, 'Box', 0, '0', 0),
	(1000503, b'1', 'Furosemide generik', 200, 'tablet', 0, 1, 'tablet', '2024-10-26', '2020-07-01', 0, 0, 133, 'Obat', 200, 'Box', 0, '0', 0),
	(1000504, b'1', 'Sagestam 10  gr salep kulit', 1, 'tube', 0, 10, 'Gram', '2024-10-27', '2020-07-01', 0, 18000, 13200, 'Obat', 1, 'tube', 0, '0', 0),
	(1000505, b'1', 'Sagestam salep mata 3', 3, 'tube', 3, 4, 'Gram', '2024-10-28', '2020-07-01', 0, 0, 7832, 'Obat', 1, '1', 0, '0', 0),
	(1000506, b'1', 'gentamisin salep kulit', 1, 'tube', 0, 5, 'Gram', '2024-10-29', '2020-07-01', 0, 5000, 3651, 'Obat', 1, 'tube', 0, '0', 0),
	(1000507, b'1', 'Genalten cream', 1, 'tube', 0, 1, 'tube', '2024-10-30', '2020-07-01', 0, 5000, 3398, 'Obat', 1, 'tube', 0, '0', 0),
	(1000508, b'1', 'Sagestam tetes mata/telinga', 1, 'botol', 0, 1, 'botol', '2024-10-31', '2020-07-01', 0, 42000, 30910, 'Obat', 1, 'botol', 0, '0', 0),
	(1000509, b'1', 'Genoint 15 gr', 0, 'tube', 12, 15, 'Gram', '2024-11-01', '2020-07-01', 0, 8000, 5445, 'Obat', 1, 'tube', 0, '0', 0),
	(1000510, b'1', 'Granopi ', 10, 'tablet', 0, 1, 'tablet', '2024-11-02', '2020-07-01', 0, 30000, 22000, 'Obat', 10, 'Box', 0, '0', 0),
	(1000511, b'1', 'Griseofulvin 500 mg ', 100, 'tablet', 0, 1, 'tablet', '2024-11-03', '2020-07-01', 0, 2500, 1859, 'Obat', 100, 'Box', 0, '0', 0),
	(1000512, b'1', 'Proris sirup 100 mg ', 1, 'botol', 0, 1, 'botol', '2024-11-04', '2020-07-01', 0, 35000, 25300, 'Obat', 1, 'botol', 0, '0', 0),
	(1000513, b'1', 'Bufect sirup 100 mg', 1, 'botol', 0, 1, 'botol', '2024-11-05', '2020-07-01', 0, 22000, 15620, 'Obat', 1, 'botol', 0, '0', 0),
	(1000514, b'1', 'Bufect forte sirup 200 mg', 1, 'botol', 0, 1, 'botol', '2024-11-06', '2020-07-01', 0, 30000, 21450, 'Obat', 1, 'botol', 0, '0', 0),
	(1000515, b'1', 'Termofen 100 mg sirup', 1, 'botol', 0, 1, 'botol', '2024-11-07', '2020-07-01', 0, 37500, 27500, 'Obat', 1, 'botol', 0, '0', 0),
	(1000516, b'1', 'BD Gard', 30, 'kapsul', 0, 1, 'kapsul', '2024-11-08', '2020-07-01', 0, 15000, 11000, 'Obat', 30, 'Box', 0, '0', 0),
	(1000517, b'1', 'Forcanox', 18, 'kapsul', 0, 1, 'kapsul', '2024-11-09', '2020-07-01', 0, 3500, 23100, 'Obat', 18, 'Box', 0, '0', 0),
	(1000518, b'1', 'Kassa steril One med', 1, 'box', 0, 1, 'box', '2024-11-10', '2020-07-01', 0, 16000, 7810, 'Obat', 1, 'box', 0, '0', 0),
	(1000519, b'1', 'Kassa steril Bradja', 1, 'box', 0, 1, 'box', '2024-11-11', '2020-07-01', 0, 16000, 0, 'Obat', 1, 'box', 0, '0', 0),
	(1000520, b'1', 'Ketoconazole tab generik', 50, 'tablet', 0, 1, 'tablet', '2024-11-12', '2020-07-01', 0, 700, 410, 'Obat', 50, 'Box', 0, '0', 0),
	(1000521, b'1', 'Fungasol', 50, 'tablet', 0, 1, 'tablet', '2024-11-13', '2020-07-01', 0, 7500, 5500, 'Obat', 50, 'Box', 0, '0', 0),
	(1000522, b'1', 'Formyco tablet', 50, 'tablet', 0, 1, 'tablet', '2024-11-14', '2020-07-01', 0, 7500, 5467, 'Obat', 50, 'Box', 0, '0', 0),
	(1000523, b'1', 'Mycoral ', 50, 'tablet', 0, 1, 'tablet', '2024-11-15', '2020-07-01', 0, 6000, 4290, 'Obat', 50, 'Box', 0, '0', 0),
	(1000524, b'1', 'Formyco 10 gr', 1, 'tube', 0, 10, 'gram', '2024-11-16', '2020-07-01', 0, 27000, 19800, 'Obat', 1, 'tube', 0, '0', 0),
	(1000525, b'1', 'Ketoconazole krim generik ', 0, 'tube', 0, 10, 'gram', '2024-11-17', '2020-07-01', 0, 8000, 5683, 'Obat', 1, 'tube', 0, '0', 0),
	(1000526, b'1', 'Fungasol krim 10 gr', 1, 'tube', 0, 10, 'gram', '2024-11-18', '2020-07-01', 0, 40000, 29700, 'Obat', 1, 'tube', 0, '0', 0),
	(1000527, b'1', 'Fungasol SS ', 1, 'botol', 0, 1, 'botol', '2024-11-19', '2020-07-01', 0, 114500, 84700, 'Obat', 1, 'botol', 0, '0', 0),
	(1000528, b'1', 'Lamodex 10 gr krim', 1, 'tube', 0, 10, 'gram', '2024-11-20', '2020-07-01', 0, 60000, 44000, 'Obat', 1, 'tube', 0, '0', 0),
	(1000529, b'1', 'Kloderma 10 gr krim', 49, 'tube', 8, 10, 'gram', '2024-11-21', '2020-07-01', 0, 47000, 34788, 'Obat', 1, 'tube', 0, '0', 0),
	(1000530, b'1', 'Kondom sutra', 1, 'box', 0, 1, 'box', '2024-11-22', '2020-07-01', 0, 26000, 0, 'Obat', 1, 'box', 0, '0', 0),
	(1000531, b'1', 'Lacbon', 100, 'tablet', 0, 1, 'tablet', '2024-11-23', '2020-07-01', 0, 2500, 1573, 'Obat', 100, 'Box', 0, '0', 0),
	(1000532, b'1', 'Probiokid', 10, 'sachet', 0, 1, 'sachet', '2024-11-24', '2020-07-01', 0, 18500, 13695, 'Obat', 10, 'Box', 0, '0', 0),
	(1000533, b'1', 'Lacto B ', 40, 'sachet', 0, 1, 'sachet', '2024-11-25', '2020-07-01', 0, 10000, 6490, 'Obat', 40, 'Box', 0, '0', 0),
	(1000534, b'1', 'L-Bio', 30, 'sachet', 0, 1, 'sachet', '2024-11-26', '2020-07-01', 0, 10000, 7700, 'Obat', 30, 'Box', 0, '0', 0),
	(1000535, b'1', 'Dulcolactol sirup', 1, 'botol', 0, 1, 'botol', '2024-11-27', '2020-07-01', 0, 83500, 61820, 'Obat', 1, 'botol', 0, '0', 0),
	(1000536, b'1', 'Laxadine sirup', 1, 'botol', 0, 1, 'botol', '2024-11-28', '2020-07-01', 0, 50000, 41250, 'Obat', 1, 'botol', 0, '0', 0),
	(1000537, b'1', 'Lansoprazole generik', 20, 'tablet', 0, 1, 'tablet', '2024-11-29', '2020-07-01', 0, 1500, 1000, 'Obat', 20, 'Box', 0, '0', 0),
	(1000538, b'1', 'Lodia', 50, 'tablet', 0, 1, 'tablet', '2024-11-30', '2020-07-01', 0, 1500, 1287, 'Obat', 50, 'Box', 0, '0', 0),
	(1000539, b'1', 'Loran', 30, 'tablet', 0, 1, 'tablet', '2024-12-01', '2020-07-01', 0, 7500, 5830, 'Obat', 30, 'Box', 0, '0', 0),
	(1000540, b'1', 'Loratadine generik', 49, 'tablet', -9, 1, 'tablet', '2024-12-02', '2020-07-01', 0, 500, 366, 'Obat', 50, 'Box', 0, '0', 0),
	(1000541, b'1', 'Alloris tablet', 100, 'tablet', 0, 1, 'tablet', '2024-12-03', '2020-07-01', 0, 7000, 4923, 'Obat', 100, 'Box', 0, '0', 0),
	(1000542, b'1', 'Alloris sirup', 1, 'botol', 0, 1, 'botol', '2024-12-04', '2020-07-01', 0, 78500, 57860, 'Obat', 1, 'botol', 0, '0', 0),
	(1000543, b'1', 'Cusson baby moscare', 1, 'botol', 0, 1, 'botol', '2024-12-05', '2020-07-01', 0, 0, 0, 'Obat', 1, 'botol', 0, '0', 0),
	(1000544, b'1', 'Sanoskin Melladerm Plus', 1, 'botol', 0, 1, 'botol', '2024-12-06', '2020-07-01', 0, 0, 198000, 'Obat', 1, 'botol', 0, '0', 0),
	(1000545, b'1', 'Interhistin', 100, 'tablet', 0, 1, 'tablet', '2024-12-07', '2020-07-01', 0, 1200, 869, 'Obat', 100, 'Box', 0, '0', 0),
	(1000546, b'1', 'Spasminal', 100, 'tablet', 0, 1, 'tablet', '2024-12-08', '2020-07-01', 0, 1000, 792, 'Obat', 100, 'Box', 0, '0', 0),
	(1000547, b'1', 'Metformin generik', 100, 'tablet', 0, 1, 'tablet', '2024-12-09', '2020-07-01', 0, 300, 193, 'Obat', 100, 'Box', 0, '0', 0),
	(1000548, b'1', 'Lameson 16 mg', 100, 'tablet', 0, 1, 'tablet', '2024-12-10', '2020-07-01', 0, 11900, 9240, 'Obat', 100, 'Box', 0, '0', 0),
	(1000549, b'1', 'Stenirol 16 ', 48, 'tablet', -18, 1, 'tablet', '2024-12-11', '2020-07-01', 0, 11500, 8250, 'Obat', 50, 'Box', 0, '0', 0),
	(1000550, b'1', 'Carmeson 4 ', 100, 'tablet', 0, 1, 'tablet', '2024-12-12', '2020-07-01', 0, 600, 379, 'Obat', 100, 'Box', 0, '0', 0),
	(1000551, b'1', 'Sanexon 4 mg', 100, 'tablet', 0, 1, 'tablet', '2024-12-13', '2020-07-01', 0, 3000, 2316, 'Obat', 100, 'Box', 0, '0', 0),
	(1000552, b'1', 'Metil Prednisolon 8 mg ', 100, 'tablet', 0, 1, 'tablet', '2024-12-14', '2020-07-01', 0, 800, 585, 'Obat', 100, 'Box', 0, '0', 0),
	(1000553, b'1', 'Damaben tablet', 100, 'tablet', 0, 1, 'tablet', '2024-12-15', '2020-07-01', 0, 0, 462, 'Obat', 100, 'Box', 0, '0', 0),
	(1000554, b'1', 'Damaben drop', 1, 'botol', 0, 1, 'botol', '2024-12-16', '2020-07-01', 0, 20000, 14190, 'Obat', 1, 'botol', 0, '0', 0),
	(1000555, b'1', 'Damaben sirup', 1, 'botol', 0, 1, 'botol', '2024-12-17', '2020-07-01', 0, 15000, 10560, 'Obat', 1, 'botol', 0, '0', 0),
	(1000556, b'1', '"""Rheu-trex 2"', 5, 'tablet', 0, 1, 'tablet', '2024-12-18', '2020-07-01', 0, 0, 60500, 'Obat', 50, 'Box', 0, '0', 0),
	(1000557, b'1', 'Miragyl', 100, 'tablet', 0, 1, 'tablet', '2024-12-19', '2020-07-01', 0, 1000, 0, 'Obat', 100, 'Box', 0, '0', 0),
	(1000558, b'1', 'Miconazole krim generik', 1, 'tube', 0, 10, 'Gram', '2024-12-20', '2020-07-01', 0, 5000, 3585, 'Obat', 1, 'tube', 0, '0', 0),
	(1000559, b'1', 'Microlax gel', 1, 'tube', 0, 1, 'tube', '2024-12-21', '2020-07-01', 0, 0, 20625, 'Obat', 1, 'tube', 0, '0', 0),
	(1000560, b'1', 'Utrogestan 200 mg ', 15, 'kapsul', 0, 1, 'kapsul', '2024-12-22', '2020-07-01', 0, 29000, 21450, 'Obat', 15, 'Box', 0, '0', 0),
	(1000561, b'1', 'Utrogestan 100 mg ', 30, 'kapsul', 0, 1, 'kapsul', '2024-12-23', '2020-07-01', 0, 18000, 13200, 'Obat', 30, 'Box', 0, '0', 0),
	(1000562, b'1', 'Minyak kayu putih caplang 60 ml', 1, 'botol', 0, 1, 'botol', '2024-12-24', '2020-07-01', 0, 27000, 19638, 'Obat', 1, 'botol', 0, '0', 0),
	(1000563, b'1', 'Minyak kayu putih caplang 120 ml', 1, 'botol', 0, 1, 'botol', '2024-12-25', '2020-07-01', 0, 49000, 37860, 'Obat', 1, 'botol', 0, '0', 0),
	(1000564, b'1', 'Telon Lang ', 1, 'botol', 0, 1, 'botol', '2024-12-26', '2020-07-01', 0, 0, 26153, 'Obat', 1, 'botol', 0, '0', 0),
	(1000565, b'1', 'Loksin 10 gr', 1, 'tube', 0, 10, 'gram', '2024-12-27', '2020-07-01', 0, 94000, 69300, 'Obat', 1, 'tube', 0, '0', 0),
	(1000566, b'1', 'Elox krim 5 gr', 0, 'tube', 0, 5, 'gram', '2024-12-28', '2020-07-01', 0, 66000, 50600, 'Obat', 1, 'tube', 0, '0', 0),
	(1000567, b'1', 'Erdomex sirup', 1, 'botol', 0, 1, 'botol', '2024-12-29', '2020-07-01', 0, 66000, 44000, 'Obat', 1, 'botol', 0, '0', 0),
	(1000568, b'1', 'HB Vit', 30, 'tablet', 0, 1, 'tablet', '2024-12-30', '2020-07-01', 0, 5000, 3630, 'Obat', 30, 'Box', 0, '0', 0),
	(1000569, b'1', 'Nulacta', 60, 'kapsul', 0, 1, 'kapsul', '2024-12-31', '2020-07-01', 0, 7000, 4840, 'Obat', 60, 'Box', 0, '0', 0),
	(1000570, b'1', 'Caviplex', 100, 'tablet', 0, 1, 'tablet', '2025-01-01', '2020-07-01', 0, 700, 510, 'Obat', 100, 'Box', 0, '0', 0),
	(1000571, b'1', 'Obdhamin', 50, 'tablet', 0, 1, 'tablet', '2025-01-02', '2020-07-01', 0, 6000, 4400, 'Obat', 50, 'Box', 0, '0', 0),
	(1000572, b'1', 'Oxcal ', 100, 'kapsul', 0, 1, 'kapsul', '2025-01-03', '2020-07-01', 0, 6000, 3850, 'Obat', 100, 'Box', 0, '0', 0),
	(1000573, b'1', 'Folamil Genio', 30, 'kapsul', 0, 1, 'kapsul', '2025-01-04', '2020-07-01', 0, 5666, 4180, 'Obat', 30, 'Box', 0, '0', 0),
	(1000574, b'1', 'Inlacin 100 mg', 30, 'kapsul', 0, 1, 'kapsul', '2025-01-05', '2020-07-01', 0, 7500, 5500, 'Obat', 30, 'Box', 0, '0', 0),
	(1000575, b'1', 'Neurosanbe 5000', 100, 'tablet', 0, 1, 'tablet', '2025-01-06', '2020-07-01', 0, 3150, 2486, 'Obat', 100, 'Box', 0, '0', 0),
	(1000576, b'1', 'Neuralgin Rx', 100, 'tablet', 0, 1, 'tablet', '2025-01-07', '2020-07-01', 0, 1000, 0, 'Obat', 100, 'Box', 0, '0', 0),
	(1000577, b'1', 'Neurosanbe', 100, 'tablet', 0, 1, 'tablet', '2025-01-08', '2020-07-01', 0, 1500, 1161, 'Obat', 100, 'Box', 0, '0', 0),
	(1000578, b'1', 'Sanvita B sirup ', 1, 'botol', 0, 1, 'botol', '2025-01-09', '2020-07-01', 0, 18000, 13200, 'Obat', 1, 'botol', 0, '0', 0),
	(1000579, b'1', 'Muveron drop ', 1, 'botol', 0, 1, 'botol', '2025-01-10', '2020-07-01', 0, 40000, 29700, 'Obat', 1, 'botol', 0, '0', 0),
	(1000580, b'1', 'Apialis sirup', 1, 'botol', 0, 1, 'botol', '2025-01-11', '2020-07-01', 0, 49000, 36300, 'Obat', 1, 'botol', 0, '0', 0),
	(1000581, b'1', 'Muveron sirup', 1, 'botol', 0, 1, 'botol', '2025-01-12', '2020-07-01', 0, 57000, 42350, 'Obat', 1, 'botol', 0, '0', 0),
	(1000582, b'1', 'San B Plex drop', 1, 'botol', 0, 1, 'botol', '2025-01-13', '2020-07-01', 0, 25000, 18590, 'Obat', 1, 'botol', 0, '0', 0),
	(1000583, b'1', 'SIM DHA sirup', 1, 'botol', 0, 1, 'botol', '2025-01-14', '2020-07-01', 0, 66000, 49500, 'Obat', 1, 'botol', 0, '0', 0),
	(1000584, b'1', 'Zamel sirup', 1, 'botol', 0, 1, 'botol', '2025-01-15', '2020-07-01', 0, 56000, 41250, 'Obat', 1, 'botol', 0, '0', 0),
	(1000585, b'1', 'Sangobion 250 ', 250, 'tablet', 0, 1, 'tablet', '2025-01-16', '2020-07-01', 0, 15000, 1144, 'Obat', 250, 'Box', 0, '0', 0),
	(1000586, b'1', 'Elkana sirup', 1, 'botol', 0, 1, 'botol', '2025-01-17', '2020-07-01', 0, 31000, 22440, 'Obat', 1, 'botol', 0, '0', 0),
	(1000587, b'1', 'Neuropyron V', 100, 'tablet', 0, 1, 'tablet', '2025-01-18', '2020-07-01', 0, 1000, 957, 'Obat', 100, 'Box', 0, '0', 0),
	(1000588, b'1', 'Pirotop 5 gr ', 1, 'tube', 0, 5, 'Gram', '2025-01-19', '2020-07-01', 0, 57000, 41580, 'Obat', 1, 'tube', 0, '0', 0),
	(1000589, b'1', 'Pirotop 10 gr', 1, 'tube', 0, 10, 'Gram', '2025-01-20', '2020-07-01', 0, 79000, 65560, 'Obat', 1, 'tube', 0, '0', 0),
	(1000590, b'1', 'Natrium diklofenak 50 mg', 49, 'tablet', 0, 1, 'tablet', '2025-01-21', '2020-07-01', 0, 700, 468, 'Obat', 50, 'Box', 0, '0', 0),
	(1000591, b'1', 'Fenaren', 100, 'tablet', 0, 1, 'tablet', '2025-01-22', '2020-07-01', 0, 1000, 0, 'Obat', 100, 'Box', 0, '0', 0),
	(1000592, b'1', 'Nymiko drop', 1, 'botol', 0, 1, 'botol', '2025-01-23', '2020-07-01', 0, 48000, 35310, 'Obat', 1, 'botol', 0, '0', 0),
	(1000593, b'1', 'Norestil 2', 30, 'tablet', 0, 1, 'tablet', '2025-01-24', '2020-07-01', 0, 6000, 4400, 'Obat', 30, 'Box', 0, '0', 0),
	(1000594, b'1', 'Enterostop', 2, 'strip', 0, 1, 'strip ', '2025-01-25', '2020-07-01', 0, 0, 34668, 'Obat', 2, 'Box', 0, '0', 0),
	(1000595, b'1', 'Omeprazole generik', 30, 'kapsul', 0, 1, 'kapsul', '2025-01-26', '2020-07-01', 0, 10000, 704, 'Obat', 30, 'Box', 0, '0', 0),
	(1000596, b'1', 'Ondansentron Generik', 50, 'tablet', 0, 1, 'tablet', '2025-01-27', '2020-07-01', 0, 2000, 1540, 'Obat', 50, 'Box', 0, '0', 0),
	(1000597, b'1', 'Dehidralit', 1, 'botol', 0, 1, 'botol', '2025-01-28', '2020-07-01', 0, 24000, 16500, 'Obat', 1, 'botol', 0, '0', 0),
	(1000598, b'1', 'Renalite', 1, 'botol', 0, 1, 'botol', '2025-01-29', '2020-07-01', 0, 24000, 17710, 'Obat', 1, 'botol', 0, '0', 0),
	(1000599, b'1', 'Sanmol tablet', 100, 'tablet', 0, 1, 'tablet', '2025-01-30', '2020-07-01', 0, 1000, 308, 'Obat', 100, 'Box', 0, '0', 0),
	(1000600, b'1', 'Paracetamol 500 mg generik', 100, 'tablet', 0, 1, 'tablet', '2025-01-31', '2020-07-01', 0, 400, 287, 'Obat', 100, 'Box', 0, '0', 0),
	(1000601, b'1', 'Grafadon', 100, 'kaplet', 0, 1, 'kaplet', '2025-02-01', '2020-07-01', 0, 400, 257, 'Obat', 100, 'Box', 0, '0', 0),
	(1000602, b'1', 'Paramex sakit kepala tablet', 1, 'strip', 0, 1, 'strip', '2025-02-02', '2020-07-01', 0, 3000, 0, 'Obat', 1, 'strip', 0, '0', 0),
	(1000603, b'1', 'Panadol biru ', 10, 'strip', 0, 1, 'strip', '2025-02-03', '2020-07-01', 0, 10000, 8131, 'Obat', 10, 'Box', 0, '0', 0),
	(1000604, b'1', 'Panadol extra', 10, 'strip', 0, 1, 'strip', '2025-02-04', '2020-07-01', 0, 11000, 8963, 'Obat', 10, 'Box', 0, '0', 0),
	(1000605, b'1', 'Pamol 125 mg supp', 11, 'supp', 0, 1, 'supp', '2025-02-05', '2020-07-01', 0, 0, 12980, 'Obat', 12, 'Box', 0, '0', 0),
	(1000606, b'1', 'Sanmol sirup 120 mg', 1, 'botol', 0, 1, 'botol', '2025-02-06', '2020-07-01', 0, 17000, 12540, 'Obat', 1, 'botol', 0, '0', 0),
	(1000607, b'1', 'Sanmol drop 60 mg', 1, 'botol', 0, 1, 'botol', '2025-02-07', '2020-07-01', 0, 0, 16830, 'Obat', 1, 'botol', 0, '0', 0),
	(1000608, b'1', 'Kamolas Forte 250 mg sirup', 1, 'botol', 0, 1, 'botol', '2025-02-08', '2020-07-01', 0, 36000, 26400, 'Obat', 1, 'botol', 0, '0', 0),
	(1000609, b'1', 'Paracetamol sirup 120 mg', 1, 'botol', 0, 1, 'botol', '2025-02-09', '2020-07-01', 0, 7000, 4605, 'Obat', 1, 'botol', 0, '0', 0),
	(1000610, b'1', 'Kapsida bersih darah kembang bulan', 1, 'botol', 0, 1, 'botol', '2025-02-10', '2020-07-01', 0, 0, 11654, 'Obat', 1, 'botol', 0, '0', 0),
	(1000611, b'1', 'QV cream ', 0, 'tube', 96, 100, 'gram', '2025-02-11', '2020-07-01', 0, 197000, 145475, 'Obat', 1, 'botol', 0, '0', 0),
	(1000612, b'1', 'Medscab', 1, 'tube', 0, 30, 'gram', '2025-02-12', '2020-07-01', 0, 98000, 71995, 'Obat', 1, 'tube', 0, '0', 0),
	(1000613, b'1', 'Scacid cream 10 gr', 0, 'tube', 7.5, 10, 'gram', '2025-02-13', '2020-07-01', 0, 57000, 42350, 'Obat', 1, 'tube', 0, '0', 0),
	(1000614, b'1', 'Combantrin sirup', 1, 'botol', 0, 1, 'botol', '2025-02-14', '2020-07-01', 0, 22000, 15862, 'Obat', 1, 'botol', 0, '0', 0),
	(1000615, b'1', 'Combantrin 125 mg tablet', 25, 'strip', 0, 1, 'strip', '2025-02-15', '2020-07-01', 0, 0, 13068, 'Obat', 25, 'Box', 0, '0', 0),
	(1000616, b'1', 'Combantrin 250 mg tablet', 25, 'strip', 0, 1, 'strip', '2025-02-16', '2020-07-01', 0, 18000, 13068, 'Obat', 25, 'Box', 0, '0', 0),
	(1000617, b'1', 'Piroxicam 10 mg generik', 100, 'tablet', 0, 1, 'tablet', '2025-02-17', '2020-07-01', 0, 200, 0, 'Obat', 100, 'Box', 0, '0', 0),
	(1000618, b'1', 'Miradene 20 mg', 100, 'kapsul', 0, 1, 'kapsul', '2025-02-18', '2020-07-01', 0, 0, 0, 'Obat', 100, 'Box', 0, '0', 0),
	(1000619, b'1', 'Novaxicam', 100, 'kapsul', 0, 1, 'kapsul', '2025-02-19', '2020-07-01', 0, 0, 0, 'Obat', 100, 'Box', 0, '0', 0),
	(1000620, b'1', 'Bioplacenton 15 gr', 1, 'tube', 0, 15, 'Gram', '2025-02-20', '2020-07-01', 0, 23000, 17050, 'Obat', 1, 'tube', 0, '0', 0),
	(1000621, b'1', 'chilli plast plester coklat', 0, 'piece', 0, 1, 'piece', '2025-02-21', '2020-07-01', 0, 0, 0, 'Obat', 0, 'Box', 0, '0', 0),
	(1000622, b'1', 'Hansaplast', 100, 'lembar', 0, 1, 'lembar', '2025-02-22', '2020-07-01', 0, 500, 255, 'Obat', 100, 'Box', 0, '0', 0),
	(1000623, b'1', 'Hansaplast rol kain', 1, 'roll', 0, 1, 'roll', '2025-02-23', '2020-07-01', 0, 4000, 0, 'Obat', 1, 'roll', 0, '0', 0),
	(1000624, b'1', 'Meptin mini', 100, 'tablet', 0, 1, 'tablet', '2025-02-24', '2020-07-01', 0, 5000, 2830, 'Obat', 100, 'Box', 0, '0', 0),
	(1000625, b'1', 'Prothyra 10 mg ', 30, 'tablet', 0, 1, 'tablet', '2025-02-25', '2020-07-01', 0, 0, 5078, 'Obat', 30, 'Box', 0, '0', 0),
	(1000626, b'1', 'Ranitidine Hcl', 100, 'tablet', 0, 1, 'tablet', '2025-02-26', '2020-07-01', 0, 3000, 220, 'Obat', 100, 'Box', 0, '0', 0),
	(1000627, b'1', 'Topicare Cleanser', 1, 'botol', 0, 1, 'botol', '2025-02-27', '2020-07-01', 0, 98000, 66550, 'Obat', 1, 'botol', 0, '0', 0),
	(1000628, b'1', 'Salbutamol 2 mg generik', 100, '0', 0, 1, '0', '2025-02-28', '2020-07-01', 0, 200, 0, 'Obat', 100, '0', 0, '0', 0),
	(1000629, b'1', 'Salbutamol 2 mg generik', 100, 'tablet', 0, 1, 'tablet', '2025-03-01', '2020-07-01', 0, 200, 84, 'Obat', 100, 'tablet', 0, '0', 0),
	(1000630, b'1', 'Salbutamol 4 mg generik', 100, '0', 0, 1, '0', '2025-03-02', '2020-07-01', 0, 200, 0, 'Obat', 100, '0', 0, '0', 0),
	(1000631, b'1', 'Salbutamol 4 mg generik', 100, 'tablet', 0, 1, 'tablet', '2025-03-03', '2020-07-01', 0, 300, 0, 'Obat', 100, 'tablet', 0, '0', 0),
	(1000632, b'1', 'Ventolin nebul', 20, 'unit', 0, 1, 'unit', '2025-03-04', '2020-07-01', 0, 14000, 10381, 'Obat', 20, 'unit', 0, '0', 0),
	(1000633, b'1', 'Velutine inhalation sol', 10, 'vial', 0, 1, 'vial', '2025-03-05', '2020-07-01', 0, 15000, 0, 'Obat', 10, 'vial', 0, '0', 0),
	(1000634, b'1', 'Teosal', 100, 'tablet', 0, 1, 'tablet', '2025-03-06', '2020-07-01', 0, 300, 209, 'Obat', 100, 'tablet', 0, '0', 0),
	(1000635, b'1', 'Lasal sirup', 1, 'botol', 0, 1, 'botol', '2025-03-07', '2020-07-01', 0, 35000, 28600, 'Obat', 1, 'botol', 0, '0', 0),
	(1000636, b'1', 'Salep 2-4', 1, 'botol', 0, 1, 'botol', '2025-03-08', '2020-07-01', 0, 0, 0, 'Obat', 1, 'botol', 0, '0', 0),
	(1000637, b'1', 'Bedak Salisil ', 1, 'pot', 0, 1, 'pot', '2025-03-09', '2020-07-01', 0, 10000, 0, 'Obat', 1, 'pot', 0, '0', 0),
	(1000638, b'1', 'Caladine lotion', 1, 'botol', 0, 1, 'botol', '2025-03-10', '2020-07-01', 0, 0, 13052, 'Obat', 1, 'botol', 0, '0', 0),
	(1000639, b'1', 'Kodomo shampoo', 1, 'botol', 0, 1, 'botol', '2025-03-11', '2020-07-01', 0, 0, 0, 'Obat', 1, 'botol', 0, '0', 0),
	(1000640, b'1', 'Gramax 100 mg', 4, 'kapsul', 0, 1, 'kapsul', '2025-03-12', '2020-07-01', 0, 71000, 52250, 'Obat', 4, 'kapsul', 0, '0', 0),
	(1000641, b'1', 'Bye-bye fever anak ', 5, 'strip', 0, 1, 'strip', '2025-03-13', '2020-07-01', 0, 12500, 9085, 'Obat', 5, 'strip', 0, '0', 0),
	(1000642, b'1', 'Bye-bye fever bayi', 10, 'strip', 0, 1, 'strip', '2025-03-14', '2020-07-01', 0, 10000, 6381, 'Obat', 10, 'strip', 0, '0', 0),
	(1000643, b'1', 'Tes hamil Onemed', 50, 'piece', 0, 1, 'piece', '2025-03-15', '2020-07-01', 0, 3000, 1452, 'Obat', 50, 'piece', 0, '0', 0),
	(1000644, b'1', 'Tes hamil Quick & sure', 25, 'piece', 0, 1, 'piece', '2025-03-16', '2020-07-01', 0, 10000, 0, 'Obat', 25, 'piece', 0, '0', 0),
	(1000645, b'1', 'Tes hamil Steril', 50, 'piece', 0, 1, 'piece', '2025-03-17', '2020-07-01', 0, 7000, 0, 'Obat', 50, 'piece', 0, '0', 0),
	(1000646, b'1', 'Rohto tetes mata', 1, 'botol', 0, 1, 'botol', '2025-03-18', '2020-07-01', 0, 15000, 10989, 'Obat', 1, 'botol', 0, '0', 0),
	(1000647, b'1', 'Insto tetes mata', 1, 'botol', 0, 1, 'botol', '2025-03-19', '2020-07-01', 0, 16000, 12127, 'Obat', 1, 'botol', 0, '0', 0),
	(1000648, b'1', 'Cendoxitrol Tetes Mata', 1, 'botol', 0, 1, 'botol', '2025-03-20', '2020-07-01', 0, 41000, 29975, 'Obat', 1, 'botol', 0, '0', 0),
	(1000649, b'1', 'Thiamycin 1000', 50, 'kaplet', 0, 1, 'kaplet', '2025-03-21', '2020-07-01', 0, 11500, 8250, 'Obat', 50, 'kaplet', 0, '0', 0),
	(1000650, b'1', 'Tolak Angin ', 12, 'sachet', 0, 1, 'sachet', '2025-03-22', '2020-07-01', 0, 4000, 2526, 'Obat', 12, 'sachet', 0, '0', 0),
	(1000651, b'1', 'Antangin JRG', 12, 'sachet', 0, 1, 'sachet', '2025-03-23', '2020-07-01', 0, 3000, 2373, 'Obat', 12, 'sachet', 0, '0', 0),
	(1000652, b'1', 'Rafacort tab', 50, 'tablet', 0, 1, 'tablet', '2025-03-24', '2020-07-01', 0, 5000, 966, 'Obat', 1, 'tablet', 0, '0', 0),
	(1000653, b'1', 'Bufacomb cream in ora base', 1, 'tube', 0, 5, 'Gram', '2025-03-25', '2020-07-01', 0, 23000, 181500, 'Obat', 50, 'tube', 0, '0', 0),
	(1000654, b'1', 'Carmed 10% krim 40 gr', 1, 'tube', 0, 40, 'Gram', '2025-03-26', '2020-07-01', 0, 39000, 16500, 'Obat', 1, 'tube', 0, '0', 0),
	(1000655, b'1', 'Carmed 20% krim 40 gr', 1, 'tube', 0, 40, 'Gram', '2025-03-27', '2020-07-01', 0, 51000, 31900, 'Obat', 1, 'tube', 0, '0', 0),
	(1000656, b'1', 'Vomil B6', 100, 'tablet', 0, 1, 'tablet', '2025-03-28', '2020-07-01', 0, 3600, 410, 'Obat', 1, 'tablet', 0, '0', 0),
	(1000657, b'1', 'Vit B complex IPI', 1, 'botol', 0, 1, 'botol', '2025-03-29', '2020-07-01', 0, 6000, 264000, 'Obat', 100, 'botol', 0, '0', 0),
	(1000658, b'1', 'Vit C IPI ', 1, 'botol', 0, 1, 'botol', '2025-03-30', '2020-07-01', 0, 6000, 4253, 'Obat', 1, 'botol', 0, '0', 0),
	(1000659, b'1', 'Redoxon', 1, 'botol', 0, 1, 'botol', '2025-03-31', '2020-07-01', 0, 40000, 4253, 'Obat', 1, 'botol', 0, '0', 0),
	(1000660, b'1', 'Calcido ', 30, 'kaplet', 0, 1, 'kaplet', '2025-04-01', '2020-07-01', 0, 7000, 1151, 'Obat', 1, 'kaplet', 0, '0', 0),
	(1000661, b'1', 'Eturol', 100, 'kapsul', 0, 1, 'kapsul', '2025-04-02', '2020-07-01', 0, 8000, 1392, 'Obat', 30, 'kapsul', 0, '0', 0),
	(1000662, b'1', 'Maltofer', 1, 'botol', 0, 1, 'botol', '2025-04-03', '2020-07-01', 0, 79000, 605000, 'Obat', 100, 'botol', 0, '0', 0),
	(1000663, b'1', 'Zinc 20 mg Kimia farma', 100, 'tablet', 0, 1, 'tablet', '2025-04-04', '2020-07-01', 0, 900, 0, 'Obat', 1, 'tablet', 0, '0', 0),
	(1000664, b'1', 'L-zinc sirup', 1, 'botol', 0, 1, 'botol', '2025-04-05', '2020-07-01', 0, 0, 55100, 'Obat', 100, 'botol', 0, '0', 0);
/*!40000 ALTER TABLE `t_obat` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_obat_keluar
CREATE TABLE IF NOT EXISTS `t_obat_keluar` (
  `norec` bigint(20) NOT NULL AUTO_INCREMENT,
  `no_nota` varchar(50) DEFAULT '0',
  `nama_pengunjung` varchar(100) DEFAULT NULL,
  `id_pasien` bigint(20) DEFAULT NULL,
  `tgl_obat_keluar` datetime NOT NULL,
  `id_obat` bigint(20) NOT NULL,
  `jumlah_obat_keluar` float NOT NULL DEFAULT 0,
  `jlh_sub_racikan` smallint(6) NOT NULL,
  `biaya_listrik` mediumint(9) NOT NULL DEFAULT 0,
  `biaya_admin` mediumint(9) NOT NULL DEFAULT 0,
  `nm_dokter` varchar(150) NOT NULL,
  `tot_harga` int(11) NOT NULL,
  `uang` int(11) NOT NULL DEFAULT 0,
  `kembalian` int(11) NOT NULL DEFAULT 0,
  `insert_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `racikan` char(1) NOT NULL,
  `biaya_dokter` int(11) NOT NULL,
  `id_jenis_racikan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT 0,
  `hrgajualbersih` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`norec`),
  KEY `FK_t_obat_keluar_t_obat` (`id_obat`),
  KEY `FK_t_obat_keluar_t_user` (`id_user`),
  KEY `FK_t_obat_keluar_t_jenis_racikan` (`id_jenis_racikan`),
  CONSTRAINT `FK_t_obat_keluar_t_jenis_racikan` FOREIGN KEY (`id_jenis_racikan`) REFERENCES `t_jenis_racikan` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_keluar_t_obat` FOREIGN KEY (`id_obat`) REFERENCES `t_obat` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_keluar_t_user` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1002653 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_obat_keluar: ~35 rows (approximately)
/*!40000 ALTER TABLE `t_obat_keluar` DISABLE KEYS */;
INSERT IGNORE INTO `t_obat_keluar` (`norec`, `no_nota`, `nama_pengunjung`, `id_pasien`, `tgl_obat_keluar`, `id_obat`, `jumlah_obat_keluar`, `jlh_sub_racikan`, `biaya_listrik`, `biaya_admin`, `nm_dokter`, `tot_harga`, `uang`, `kembalian`, `insert_at`, `racikan`, `biaya_dokter`, `id_jenis_racikan`, `id_user`, `hrgajualbersih`) VALUES
	(1002612, 'N241119155324', '', NULL, '2019-11-24 00:00:00', 1000483, 2.5, 1, 2000, 0, '-', 25500, 26000, 500, '2019-11-24 19:53:32', '1', 0, 83, 1, 17000),
	(1002613, 'N241119692704', '', NULL, '2019-11-24 00:00:00', 1000483, 2.5, 1, 2000, 0, '-', 25500, 30000, 4500, '2019-11-24 19:54:40', '1', 0, 83, 1, 17000),
	(1002614, 'N241119997545', '', NULL, '2019-11-24 00:00:00', 1000483, 2.5, 1, 2000, 0, '-', 25500, 30000, 4500, '2019-11-24 19:56:26', '1', 0, 83, 1, 17000),
	(1002615, 'N271119643294', '', NULL, '2019-11-27 00:00:00', 1000549, 10, 1, 2000, 0, 'DR.ANDREW', 199000, 200000, 1000, '2019-11-27 21:58:54', '1', 0, 83, 1, 115000),
	(1002616, 'N271119643294', '', NULL, '2019-11-27 00:00:00', 1000540, 10, 1, 2000, 0, 'DR.ANDREW', 199000, 200000, 1000, '2019-11-27 21:58:55', '1', 0, 83, 1, 5000),
	(1002617, 'N271119643294', '', NULL, '2019-11-27 00:00:00', 1000566, 5, 1, 2000, 0, 'DR.ANDREW', 199000, 200000, 1000, '2019-11-27 21:58:55', '1', 0, 83, 1, 66000),
	(1002618, 'N271119643294', '', NULL, '2019-11-27 00:00:00', 1000611, 1, 1, 2000, 0, 'DR.ANDREW', 199000, 200000, 1000, '2019-11-27 21:58:55', '1', 0, 83, 1, 1970),
	(1002619, 'N271119782787', '', NULL, '2019-11-27 00:00:00', 1000549, 10, 0, 2000, 0, 'DR. ELLA', 118500, 120000, 1500, '2019-11-27 22:01:12', '1', 0, 83, 1, 115000),
	(1002620, 'N281119450800', '', NULL, '2019-11-28 00:00:00', 1000505, 1, 12, 2000, 0, 'DR. EKA', 13000, 15000, 2000, '2019-11-28 19:06:26', '1', 0, 1, 1, 0),
	(1002621, 'N281119450800', '', NULL, '2019-11-28 00:00:00', 1000488, 1.5, 12, 2000, 0, 'DR. EKA', 13000, 15000, 2000, '2019-11-28 19:06:26', '1', 0, 1, 1, 600),
	(1002622, 'N281119450800', '', NULL, '2019-11-28 00:00:00', 1000605, 1, 12, 2000, 0, 'DR. EKA', 13000, 15000, 2000, '2019-11-28 19:06:26', '1', 0, 1, 1, 0),
	(1002623, 'N281119127399', '', NULL, '2019-11-28 00:00:00', 1000488, 2, 15, 2000, 0, 'DR. ELLA', 4000, 5000, 1000, '2019-11-28 19:10:56', '1', 0, 0, 1, 800),
	(1002624, 'N281119284594', '', NULL, '2019-11-28 00:00:00', 1000488, 1.5, 12, 2000, 0, 'DR.ANDREW', 11000, 12000, 1000, '2019-11-28 19:22:14', '1', 0, 1, 1, 600),
	(1002625, 'N28111940775', '', NULL, '2019-11-28 00:00:00', 1000488, 2.5, 1, 2000, 0, 'DR.ANDREW', 4500, 5000, 500, '2019-11-28 19:35:07', '1', 0, 2, 1, 1000),
	(1002627, 'N281119443305', '', NULL, '2019-11-28 00:00:00', 1000488, 2.3, 1, 2000, 0, 'DR.ANGEL', 9000, 10000, 1000, '2019-11-28 19:44:33', '1', 0, 83, 1, 920),
	(1002628, 'N281119729043', '', NULL, '2019-11-28 00:00:00', 1000613, 2.5, 1, 2000, 0, 'DR. EKA', 22500, 25000, 2500, '2019-11-28 20:06:22', '1', 0, 83, 1, 14250),
	(1002629, 'N281119315299', '', NULL, '2019-11-28 00:00:00', 1000613, 2.5, 1, 2000, 0, 'DR.ANGEL', 18000, 20000, 2000, '2019-11-28 20:10:17', '1', 0, 4, 1, 14250),
	(1002630, 'N281119563821', '', NULL, '2019-11-28 00:00:00', 1000416, 0, 0, 0, 0, '-', 133500, 150000, 16500, '2019-11-28 20:29:44', '0', 0, 0, 1, 113000),
	(1002631, 'N281119563821', '', NULL, '2019-11-28 00:00:00', 1000471, 0, 0, 0, 0, '-', 133500, 150000, 16500, '2019-11-28 20:29:44', '0', 0, 0, 1, 20000),
	(1002632, 'N281119104558', '', NULL, '2019-11-28 00:00:00', 1000398, 0, 0, 0, 0, 'DR.ANDREW', 4000, 5000, 1000, '2019-11-28 20:34:52', '0', 0, 0, 1, 3900),
	(1002633, 'N171219351605', '', NULL, '2019-12-17 00:00:00', 1000474, 2, 0, 0, 0, 'DR.ANGEL', 122000, 130000, 8000, '2019-12-17 11:59:44', '0', 0, 0, 1, 43000),
	(1002634, 'N171219351605', '', NULL, '2019-12-17 00:00:00', 1000396, 1, 0, 0, 0, 'DR.ANGEL', 122000, 130000, 8000, '2019-12-17 11:59:46', '0', 0, 0, 1, 52000),
	(1002635, 'N171219351605', '', NULL, '2019-12-17 00:00:00', 1000423, 0, 0, 0, 0, 'DR.ANGEL', 122000, 130000, 8000, '2019-12-17 11:59:46', '0', 0, 0, 1, 0),
	(1002636, 'N171219351605', '', NULL, '2019-12-17 00:00:00', 1000420, 0, 0, 0, 0, 'DR.ANGEL', 122000, 130000, 8000, '2019-12-17 11:59:46', '0', 0, 0, 1, 0),
	(1002637, 'N171219351605', '', NULL, '2019-12-17 00:00:00', 1000424, 2.5, 1, 2000, 0, 'DR.ANGEL', 122000, 130000, 8000, '2019-12-17 11:59:46', '1', 0, 4, 1, 10000),
	(1002638, 'N171219351605', '', NULL, '2019-12-17 00:00:00', 1000399, 1, 1, 2000, 0, 'DR.ANGEL', 122000, 130000, 8000, '2019-12-17 11:59:46', '1', 0, 4, 1, 1300),
	(1002639, 'N171219351605', '', NULL, '2019-12-17 00:00:00', 1000471, 1, 1, 2000, 0, 'DR.ANGEL', 122000, 130000, 8000, '2019-12-17 11:59:46', '1', 0, 4, 1, 10000),
	(1002640, 'N191219415276', '', NULL, '2019-12-19 00:00:00', 1000424, 2.5, 1, 2000, 0, 'DR. EKA', 52000, 60000, 8000, '2019-12-19 14:23:19', '1', 0, 83, 1, 8750),
	(1002641, 'N191219415276', '', NULL, '2019-12-19 00:00:00', 1000483, 4, 1, 2000, 0, 'DR. EKA', 52000, 60000, 8000, '2019-12-19 14:23:19', '1', 0, 83, 1, 27200),
	(1002642, 'N191219415276', '', NULL, '2019-12-19 00:00:00', 1000611, 3, 1, 2000, 0, 'DR. EKA', 52000, 60000, 8000, '2019-12-19 14:23:19', '1', 0, 83, 1, 5910),
	(1002643, 'N191219333525', '', NULL, '2019-12-19 00:00:00', 1000424, 2.5, 1, 2000, 0, 'DR. EKA', 39500, 40000, 500, '2019-12-19 14:26:35', '1', 0, 83, 1, 10000),
	(1002644, 'N191219333525', '', NULL, '2019-12-19 00:00:00', 1000529, 2, 1, 2000, 0, 'DR. EKA', 39500, 40000, 500, '2019-12-19 14:26:35', '1', 0, 83, 1, 9400),
	(1002645, 'N191219333525', '', NULL, '2019-12-19 00:00:00', 1000500, 1, 1, 2000, 0, 'DR. EKA', 39500, 40000, 500, '2019-12-19 14:26:36', '1', 0, 83, 1, 1950),
	(1002646, 'N191219333525', '', NULL, '2019-12-19 00:00:00', 1000525, 1, 0, 0, 0, 'DR. EKA', 39500, 40000, 500, '2019-12-19 14:26:36', '0', 0, 0, 1, 8000),
	(1002647, 'N191219333525', '', NULL, '2019-12-19 00:00:00', 1000505, 1, 0, 0, 0, 'DR. EKA', 39500, 40000, 500, '2019-12-19 14:26:36', '0', 0, 0, 1, 0),
	(1002648, 'N201219616580', '', NULL, '2019-12-20 00:00:00', 1000424, 2, 0, 0, 0, 'DR.ANDREW', 292000, 300000, 8000, '2019-12-20 11:00:05', '0', 0, 0, 1, 210000),
	(1002649, 'N201219616580', '', NULL, '2019-12-20 00:00:00', 1000412, 2, 1, 2000, 0, 'DR.ANDREW', 292000, 300000, 8000, '2019-12-20 11:00:05', '1', 0, 4, 1, 30000),
	(1002650, 'N201219616580', '', NULL, '2019-12-20 00:00:00', 1000483, 3, 1, 2000, 0, 'DR.ANDREW', 292000, 300000, 8000, '2019-12-20 11:00:05', '1', 0, 4, 1, 20400),
	(1002651, 'N201219616580', '', NULL, '2019-12-20 00:00:00', 1000500, 2.4, 1, 2000, 0, 'DR.ANDREW', 292000, 300000, 8000, '2019-12-20 11:00:05', '1', 0, 4, 1, 4680),
	(1002652, 'N201219616580', '', NULL, '2019-12-20 00:00:00', 1000509, 3, 1, 2000, 0, 'DR.ANDREW', 292000, 300000, 8000, '2019-12-20 11:00:05', '1', 0, 4, 1, 20000);
/*!40000 ALTER TABLE `t_obat_keluar` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_obat_masuk
CREATE TABLE IF NOT EXISTS `t_obat_masuk` (
  `norec` bigint(20) NOT NULL AUTO_INCREMENT,
  `no_faktur` char(20) NOT NULL,
  `id_suppli` bigint(20) NOT NULL,
  `nama_pabrik` varchar(50) NOT NULL,
  `tgl_fakur` datetime NOT NULL,
  `batch` varchar(50) DEFAULT NULL,
  `id_obat` bigint(20) NOT NULL,
  `expire_date` date NOT NULL,
  `jlh_obat_masuk` mediumint(9) NOT NULL,
  `total_harga` bigint(20) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `discount_harga` float(10,2) NOT NULL,
  `discount` float(10,2) NOT NULL,
  `harga_jual` float NOT NULL,
  `total_harga_beli` bigint(20) NOT NULL,
  `ppn` bigint(20) NOT NULL,
  `total_diskon` bigint(20) NOT NULL,
  `total_harga_iclude_ppn` bigint(20) NOT NULL,
  `tgl_jatuh_tempo` date DEFAULT NULL,
  `lunas` enum('0','1') DEFAULT NULL,
  `tgl_bayar` date DEFAULT NULL,
  `id_user` int(11) NOT NULL DEFAULT 0,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`norec`),
  KEY `FK_t_obat_masuk_t_suppli` (`id_suppli`),
  KEY `FK_t_obat_masuk_t_obat` (`id_obat`),
  KEY `FK_t_obat_masuk_t_user` (`id_user`),
  CONSTRAINT `FK_t_obat_masuk_t_obat` FOREIGN KEY (`id_obat`) REFERENCES `t_obat` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_masuk_t_suppli` FOREIGN KEY (`id_suppli`) REFERENCES `t_supplier` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_masuk_t_user` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_obat_masuk: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_obat_masuk` DISABLE KEYS */;
INSERT IGNORE INTO `t_obat_masuk` (`norec`, `no_faktur`, `id_suppli`, `nama_pabrik`, `tgl_fakur`, `batch`, `id_obat`, `expire_date`, `jlh_obat_masuk`, `total_harga`, `harga_satuan`, `discount_harga`, `discount`, `harga_jual`, `total_harga_beli`, `ppn`, `total_diskon`, `total_harga_iclude_ppn`, `tgl_jatuh_tempo`, `lunas`, `tgl_bayar`, `id_user`, `create_at`) VALUES
	(203, 'SDAFASDFASDF', 10, '', '2019-11-28 00:00:00', '', 1000393, '2019-11-09', 3, 333, 111, 0.00, 0.00, 0, 333, 33, 0, 366, '2019-11-16', '1', '2019-11-28', 1, '2019-11-28 20:53:27');
/*!40000 ALTER TABLE `t_obat_masuk` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_pabrik
CREATE TABLE IF NOT EXISTS `t_pabrik` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nm_pabrik` varchar(200) DEFAULT NULL,
  `no_contact` varchar(50) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_pabrik: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_pabrik` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pabrik` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_pasien
CREATE TABLE IF NOT EXISTS `t_pasien` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nam_pasien` varchar(100) NOT NULL,
  `alamat` varchar(300) DEFAULT NULL,
  `no_hp` char(12) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `no_rekam_medis` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `no_rekam_medis` (`no_rekam_medis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_pasien: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_pasien` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pasien` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_pot
CREATE TABLE IF NOT EXISTS `t_pot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namapot` varchar(50) DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_pot: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_pot` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pot` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_puyer
CREATE TABLE IF NOT EXISTS `t_puyer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namacetakan` varchar(50) DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_puyer: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_puyer` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_puyer` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_retur
CREATE TABLE IF NOT EXISTS `t_retur` (
  `norec` bigint(20) NOT NULL AUTO_INCREMENT,
  `statusenabled` bit(1) NOT NULL,
  `norec_obatkeluar` bigint(20) NOT NULL,
  `id_obat` bigint(20) NOT NULL DEFAULT 0,
  `jlh_retur` mediumint(9) NOT NULL DEFAULT 0,
  `no_nota` bigint(20) NOT NULL DEFAULT 0,
  `tgl_retur` date NOT NULL,
  `alasan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`norec`),
  KEY `FK_t_retur_t_obat_keluar` (`norec_obatkeluar`),
  KEY `FK_t_retur_t_obat` (`id_obat`),
  CONSTRAINT `FK_t_retur_t_obat` FOREIGN KEY (`id_obat`) REFERENCES `t_obat` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_retur_t_obat_keluar` FOREIGN KEY (`norec_obatkeluar`) REFERENCES `t_obat_keluar` (`norec`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_retur: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_retur` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_retur` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_satuan
CREATE TABLE IF NOT EXISTS `t_satuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `satuan` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_satuan: ~16 rows (approximately)
/*!40000 ALTER TABLE `t_satuan` DISABLE KEYS */;
INSERT IGNORE INTO `t_satuan` (`id`, `satuan`) VALUES
	(1, 'Tablet'),
	(2, 'Botol'),
	(3, 'Kapsul'),
	(4, 'Strip '),
	(5, 'Tube'),
	(6, 'Supp'),
	(7, 'Kaplet'),
	(8, 'Strip'),
	(9, 'Sachet'),
	(10, 'Box'),
	(11, 'Piece'),
	(12, 'Lembar'),
	(13, 'Roll'),
	(14, 'Unit'),
	(15, 'Vial'),
	(16, 'Pot');
/*!40000 ALTER TABLE `t_satuan` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_supplier
CREATE TABLE IF NOT EXISTS `t_supplier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `namapbf` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `no_kontak` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_supplier: ~14 rows (approximately)
/*!40000 ALTER TABLE `t_supplier` DISABLE KEYS */;
INSERT IGNORE INTO `t_supplier` (`id`, `namapbf`, `alamat`, `no_kontak`, `email`) VALUES
	(8, 'Kimia Farma', '', '', ''),
	(9, 'MAS', '', '', ''),
	(10, 'Penta Valen', '', '', ''),
	(11, 'SST/United dico citus ', '', '', ''),
	(12, 'BSP', '', '', ''),
	(13, 'Enseval', '', '', ''),
	(14, 'Mandiri', '', '', ''),
	(15, 'SST', '', '', ''),
	(16, 'MAS/mandiri', '', '', ''),
	(17, 'United Dico citus ', '', '', ''),
	(18, 'SST,BSP', '', '', ''),
	(19, 'Enseval,MAS', '', '', ''),
	(20, 'Yarindo', '', '', '');
/*!40000 ALTER TABLE `t_supplier` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_user
CREATE TABLE IF NOT EXISTS `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `status` enum('A','O') DEFAULT NULL,
  `jk` enum('L','P') DEFAULT NULL,
  `no_hp` varchar(12) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_user: ~1 rows (approximately)
/*!40000 ALTER TABLE `t_user` DISABLE KEYS */;
INSERT IGNORE INTO `t_user` (`id`, `username`, `password`, `status`, `jk`, `no_hp`, `alamat`) VALUES
	(1, 'admin', '123', 'A', NULL, NULL, NULL),
	(4, 'operator', '123', 'O', NULL, NULL, NULL);
/*!40000 ALTER TABLE `t_user` ENABLE KEYS */;

-- Dumping structure for view db_apotek_praya.v_obat
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat` (
	`id` BIGINT(20) NOT NULL,
	`enabled` BIT(1) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` FLOAT NOT NULL,
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity_racik` FLOAT NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`expire_date` DATE NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`hrgper_racik` INT(11) NOT NULL,
	`hrga_jual` INT(11) NOT NULL,
	`harga_satuan` INT(11) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrgper_pack` INT(11) NOT NULL,
	`ket` TEXT NULL COLLATE 'latin1_swedish_ci',
	`hrgbeli_cream` INT(11) NULL,
	`jumlah` VARCHAR(25) NOT NULL COLLATE 'utf8mb4_general_ci',
	`i_expire` INT(7) NULL,
	`i_jatuh_tempo` INT(7) NULL
) ENGINE=MyISAM;

-- Dumping structure for view db_apotek_praya.v_obat_jatuh_tempo
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat_jatuh_tempo` (
	`namapbf` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity` FLOAT NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`norec` BIGINT(20) NOT NULL,
	`no_faktur` CHAR(20) NOT NULL COLLATE 'latin1_swedish_ci',
	`id_suppli` BIGINT(20) NOT NULL,
	`tgl_fakur` DATETIME NOT NULL,
	`batch` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`id_obat` BIGINT(20) NOT NULL,
	`expire_date` DATE NOT NULL,
	`jlh_obat_masuk` MEDIUMINT(9) NOT NULL,
	`total_harga` BIGINT(20) NOT NULL,
	`harga_satuan` INT(11) NOT NULL,
	`discount_harga` FLOAT(10,2) NOT NULL,
	`discount` FLOAT(10,2) NOT NULL,
	`harga_jual` FLOAT NOT NULL,
	`total_harga_beli` BIGINT(20) NOT NULL,
	`ppn` BIGINT(20) NOT NULL,
	`total_diskon` BIGINT(20) NOT NULL,
	`total_harga_iclude_ppn` BIGINT(20) NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`lunas` ENUM('0','1') NULL COLLATE 'latin1_swedish_ci',
	`tgl_bayar` DATE NULL,
	`id_user` INT(11) NOT NULL,
	`create_at` TIMESTAMP NOT NULL,
	`username` VARCHAR(200) NULL COLLATE 'latin1_swedish_ci',
	`i_jatuhtempo` INT(7) NULL
) ENGINE=MyISAM;

-- Dumping structure for view db_apotek_praya.v_obat_keluar
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat_keluar` (
	`id` BIGINT(20) NOT NULL,
	`enabled` BIT(1) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` FLOAT NOT NULL,
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity_racik` FLOAT NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`expire_date` DATE NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`hrgper_racik` INT(11) NOT NULL,
	`hrga_jual` INT(11) NOT NULL,
	`harga_satuan` INT(11) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrgper_pack` INT(11) NOT NULL,
	`ket` TEXT NULL COLLATE 'latin1_swedish_ci',
	`hrgbeli_cream` INT(11) NULL,
	`harga_jual_obat` INT(11) NOT NULL,
	`harga_jual_cream` INT(11) NOT NULL,
	`norec` BIGINT(20) NOT NULL,
	`no_nota` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`nama_pengunjung` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`id_pasien` BIGINT(20) NULL,
	`tgl_obat_keluar` DATETIME NOT NULL,
	`id_obat` BIGINT(20) NOT NULL,
	`jumlah_obat_keluar` FLOAT NOT NULL,
	`jlh_sub_racikan` SMALLINT(6) NOT NULL,
	`biaya_listrik` MEDIUMINT(9) NOT NULL,
	`biaya_admin` MEDIUMINT(9) NOT NULL,
	`nm_dokter` VARCHAR(150) NOT NULL COLLATE 'latin1_swedish_ci',
	`tot_harga` INT(11) NOT NULL,
	`uang` INT(11) NOT NULL,
	`kembalian` INT(11) NOT NULL,
	`insert_at` TIMESTAMP NOT NULL,
	`racikan` CHAR(1) NOT NULL COLLATE 'latin1_swedish_ci',
	`biaya_dokter` INT(11) NOT NULL,
	`id_jenis_racikan` INT(11) NOT NULL,
	`id_user` INT(11) NOT NULL,
	`hrgajualbersih` INT(11) NOT NULL,
	`totalAndDokter` BIGINT(12) NOT NULL,
	`racikan1` VARCHAR(1) NOT NULL COLLATE 'utf8mb4_general_ci',
	`id_jenis_racikan1` INT(11) NOT NULL,
	`adaRacikan` VARCHAR(1) NULL COLLATE 'latin1_swedish_ci',
	`diskripsi` VARCHAR(59) NULL COLLATE 'latin1_swedish_ci',
	`biaya` INT(11) NULL,
	`username` VARCHAR(200) NULL COLLATE 'latin1_swedish_ci',
	`biayaServis` INT(11) NULL,
	`totalBersih` BIGINT(20) NULL,
	`ppn` DECIMAL(21,0) NULL,
	`biaya_racikan` BIGINT(16) NULL,
	`satuanjual` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- Dumping structure for view db_apotek_praya.v_obat_masuk
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat_masuk` (
	`id` BIGINT(20) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` FLOAT NOT NULL,
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity_racik` FLOAT NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`expire_date` DATE NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`hrgper_racik` INT(11) NOT NULL,
	`hrga_jual` INT(11) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrgper_pack` INT(11) NOT NULL,
	`ket` TEXT NULL COLLATE 'latin1_swedish_ci',
	`namapbf` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`no_faktur` CHAR(20) NOT NULL COLLATE 'latin1_swedish_ci',
	`tgl_fakur` DATETIME NOT NULL,
	`tgl_jatuh_tempo_real` DATE NULL,
	`i_jatuhtempo` INT(7) NULL,
	`id_suppli` BIGINT(20) NOT NULL,
	`batch` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`jlh_obat_masuk` MEDIUMINT(9) NOT NULL,
	`total_harga` BIGINT(20) NOT NULL,
	`harga_satuan` INT(11) NOT NULL,
	`discount_harga` FLOAT(10,2) NOT NULL,
	`discount` FLOAT(10,2) NOT NULL,
	`harga_jual` FLOAT NOT NULL,
	`create_at` VARCHAR(21) NULL COLLATE 'utf8mb4_general_ci',
	`total_harga_beli` BIGINT(20) NOT NULL,
	`total_harga_beli1` BIGINT(19) NOT NULL,
	`total_diskon` BIGINT(20) NOT NULL,
	`total_harga_iclude_ppn` BIGINT(20) NOT NULL,
	`ppn` BIGINT(20) NOT NULL,
	`ppn1` DECIMAL(20,0) NOT NULL,
	`lunas` ENUM('0','1') NULL COLLATE 'latin1_swedish_ci',
	`tgl_bayar` DATE NULL,
	`username` VARCHAR(200) NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- Dumping structure for trigger db_apotek_praya.trg_barang_keluar
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_barang_keluar` AFTER INSERT ON `t_obat_keluar` FOR EACH ROW BEGIN
 	declare racikan, jlhPerRacik,jlh_stok_racik,sisa_stok_racik float;
	
	SELECT t_obat_keluar.racikan from t_obat_keluar where t_obat_keluar.norec = new.norec into racikan; 
	SELECT t_obat.quantity_racik from t_obat where t_obat.id = new.id_obat into jlh_stok_racik;
	
	SELECT t_obat.jlhper_raciik from t_obat where t_obat.id = new.id_obat into jlhPerRacik;

-- jika bukan racikan

	if racikan ='0' then -- if racikan
	begin
			update t_obat set t_obat.quantity = quantity - NEW.jumlah_obat_keluar
			where t_obat.id = new.id_obat;
	end;	
		--	if racikan ='1' then
		else
			-- jika satuan racikan	= 1
			if jlhPerRacik in (1,0)  then -- if aaa
			begin
				update t_obat set t_obat.quantity = quantity - NEW.jumlah_obat_keluar where t_obat.id = new.id_obat;
			end;
				else
					begin -- 1
						set sisa_stok_racik = jlh_stok_racik - new.jumlah_obat_keluar;
						if sisa_stok_racik <= 0 then
						begin
							update t_obat set t_obat.quantity = t_obat.quantity - 1, 
							t_obat.quantity_racik = (t_obat.jlhper_raciik + sisa_stok_racik) where t_obat.id = new.id_obat;
						end;	
							else
								begin
										update t_obat set t_obat.quantity_racik = quantity_racik- new.jumlah_obat_keluar 
										where t_obat.id = new.id_obat;
								end;	
						end if;
						
					end;	-- 1
			end if; -- if aa		
	end if;	 -- if racikan
			
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_apotek_praya.trg_obat_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_obat_insert` AFTER INSERT ON `t_obat` FOR EACH ROW BEGIN
	--	update t_obat set t_obat.quantity = quantity-1  where t_obat.id = new.id;
		
	 
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_apotek_praya.trg_obat_masuk
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_obat_masuk` AFTER INSERT ON `t_obat_masuk` FOR EACH ROW BEGIN
		UPDATE t_obat 
		SET t_obat.quantity = quantity + NEW.jlh_obat_masuk,
		t_obat.tgl_jatuh_tempo = new.tgl_jatuh_tempo,
		t_obat.expire_date = new.expire_date 
		WHERE
			t_obat.id = new.id_obat;	
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for view db_apotek_praya.v_obat
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat` AS SELECT 
	t_obat.*,
	CONCAT(t_obat.quantity,",",t_obat.quantity_racik) AS jumlah,
	DATEDIFF(t_obat.expire_date, CURRENT_DATE()) AS i_expire,
	DATEDIFF(t_obat.tgl_jatuh_tempo, CURRENT_DATE()) AS i_jatuh_tempo
	from t_obat 
	where enabled = 1 
	and hrga_jual <> 0 ;

-- Dumping structure for view db_apotek_praya.v_obat_jatuh_tempo
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat_jatuh_tempo`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat_jatuh_tempo` AS -- v_obat_jatuh_tempo


SELECT 
 sup.namapbf,
ob.nm_obat,
ob.satuan,
ob.quantity,
ob.jenis,
 om.*,
 us.username,

 DATEDIFF(om.tgl_jatuh_tempo, CURRENT_DATE()) AS i_jatuhtempo 
 
 
 FROM t_obat_masuk AS om

INNER JOIN t_obat AS ob ON ob.id = om.id_obat
inner join t_user as us on us.id = om.id_user
INNER JOIN t_supplier AS sup ON sup.id = om.id_suppli ;

-- Dumping structure for view db_apotek_praya.v_obat_keluar
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat_keluar`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat_keluar` AS -- v_obat_keluar


SELECT 

t_obat.*,
case when t_obat.jenis='obat' then t_obat.hrga_jual else 0 end as harga_jual_obat,
case when t_obat.jenis='cream' then t_obat.hrga_jual else 0 end as harga_jual_cream,
ok.*,

ok.tot_harga + ok.biaya_dokter as totalAndDokter,
case when ok.racikan = 1 then "v" else "" end as racikan1,
jn.id as id_jenis_racikan1,
(SELECT racikan from t_obat_keluar where no_nota = ok.no_nota order by racikan desc limit 1) as adaRacikan,

 case when ok.racikan = 1 then concat (jn.diskripsi,' / ',ok.jlh_sub_racikan) else 'Non Racikan' end as diskripsi,
jn.biaya,
t_user.username,

(SELECT getBiayaServis(no_nota, 1000)) as biayaServis,
(SELECT `getBiayaBersih`(no_nota)) as totalBersih,
round((SELECT `getBiayaBersih`(no_nota)* 0.1), - 2 ) as ppn,
(select ok.jlh_sub_racikan * jn.biaya as bb from t_obat_keluar as okk where okk.no_nota = ok.no_nota order by bb desc limit 1 ) as biaya_racikan,
 -- sum( case when ok.racikan = 1 then 1000 else 0 end ) as biayaServis,

case when ok.racikan = 1 then t_obat.satuan_racik else t_obat.satuan end as satuanjual

 FROM t_obat_keluar as ok 
		JOIN t_obat ON t_obat.id = ok.id_obat
		JOIN t_user on t_user.id = ok.id_user
		join t_jenis_racikan as jn on jn.id = ok.id_jenis_racikan
	 

/*Menampilkan hnya data obat yg 1 tahun*/
 where
	DATEDIFF(CURRENT_DATE(),ok.tgl_obat_keluar) <360 
	
	group by norec
	
 ORDER BY ok.insert_at desc ;

-- Dumping structure for view db_apotek_praya.v_obat_masuk
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat_masuk`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat_masuk` AS -- v_obat_masuk

SELECT 

t_obat.*,
t_supplier.namapbf,
t_obat_masuk.no_faktur,
t_obat_masuk.tgl_fakur,
t_obat_masuk.tgl_jatuh_tempo as tgl_jatuh_tempo_real,
DATEDIFF(t_obat_masuk.tgl_jatuh_tempo, CURRENT_DATE()) as i_jatuhtempo,
t_obat_masuk.id_suppli,
t_obat_masuk.batch,
t_obat_masuk.jlh_obat_masuk,
t_obat_masuk.total_harga,
t_obat_masuk.harga_satuan,
t_obat_masuk.discount_harga,
t_obat_masuk.discount,
t_obat_masuk.harga_jual,
date_format(t_obat_masuk.create_at,'%d/%m/%Y %H:%i' ) as create_at,
t_obat_masuk.total_harga_beli,
(t_obat_masuk.jlh_obat_masuk * t_obat_masuk.harga_satuan ) as total_harga_beli1,

t_obat_masuk.total_diskon,
t_obat_masuk.total_harga_iclude_ppn,
t_obat_masuk.ppn,
round((t_obat_masuk.jlh_obat_masuk * t_obat_masuk.harga_satuan) * 0.1 ) as ppn1,
t_obat_masuk.lunas,
t_obat_masuk.tgl_bayar,
t_user.username



 FROM t_obat_masuk 
 INNER JOIN t_obat ON t_obat.id = t_obat_masuk.id_obat
 inner join t_user on t_user.id = t_obat_masuk.id_user

inner join t_supplier on t_supplier.id = t_obat_masuk.id_suppli

/*Menampilkan hnya data obat yg 1 tahun*/
 where
	DATEDIFF(CURRENT_DATE(),t_obat_masuk.tgl_fakur) <360 
	
 ORDER BY t_obat_masuk.tgl_fakur desc ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
