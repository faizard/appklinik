-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_apotek_praya
CREATE DATABASE IF NOT EXISTS `db_apotek_praya` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_apotek_praya`;

-- Dumping structure for procedure db_apotek_praya.sp_cetak_nota
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cetak_nota`(
	IN `nomornota` VARCHAR(50)
)
BEGIN
SELECT
	ok.no_nota,
	'OBAT RACIKAN' AS nama_obat,
	SUM( ok.jumlah_obat_keluar ) AS jumlah_obat_keluar,
	ok.satuan_racik,
	0 AS harga_satuan,
	SUM( round( ok.hrga_jual / ok.jlhper_raciik ) * ok.jumlah_obat_keluar ) AS total_harga,
	ok.uang,
	ok.kembalian,
	ok.biaya_listrik,
	ok.biaya_admin,
	( ok.biaya_listrik + ok.biaya_admin ) AS biayaadmin,
	ok.tot_harga 
FROM
	v_obat_keluar AS ok 
WHERE
	ok.no_nota = nomornota 
	AND ok.racikan = 1 UNION
SELECT
	ok.no_nota,
	ok.nm_obat,
	ok.jumlah_obat_keluar,
	ok.satuan,
	ok.hrga_jual,
	SUM( ok.hrga_jual * ok.jumlah_obat_keluar ) AS total_harga,
	ok.uang,
	ok.kembalian,
	ok.biaya_listrik,
	ok.biaya_admin,
	( ok.biaya_listrik + ok.biaya_admin ) AS biayaadmin,
	ok.tot_harga 
FROM
	v_obat_keluar AS ok 
WHERE
	ok.no_nota = nomornota 
	AND ok.racikan = 0 
GROUP BY
	ok.id_obat;
	
	
	 
	END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat`()
BEGIN
	SELECT t_obat.*, CONCAT(t_obat.quantity,",",t_obat.quantity_racik) AS jumlah FROM t_obat;
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat_expire
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat_expire`()
BEGIN
		SELECT count(id) as jlh from t_obat 
			where DATEDIFF(t_obat.expire_date, CURRENT_DATE()) <= 180;
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat_jatuhtempo
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat_jatuhtempo`()
BEGIN
		SELECT count(id) as jlh from t_obat 
		where DATEDIFF(t_obat.tgl_jatuh_tempo, CURRENT_DATE()) <= 14;
END//
DELIMITER ;

-- Dumping structure for table db_apotek_praya.t_biaya_lain
CREATE TABLE IF NOT EXISTS `t_biaya_lain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_biaya` varchar(100) DEFAULT NULL,
  `biaya` mediumint(9) DEFAULT NULL,
  `statusenabled` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_biaya_lain: ~2 rows (approximately)
DELETE FROM `t_biaya_lain`;
/*!40000 ALTER TABLE `t_biaya_lain` DISABLE KEYS */;
INSERT INTO `t_biaya_lain` (`id`, `nm_biaya`, `biaya`, `statusenabled`) VALUES
	(1, 'Biaya Listrik', 3000, NULL),
	(2, 'Biaya Admin', 2000, NULL);
/*!40000 ALTER TABLE `t_biaya_lain` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_dokter
CREATE TABLE IF NOT EXISTS `t_dokter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nm_dokter` varchar(200) DEFAULT NULL,
  `no_hp` char(12) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `jk` enum('L','P') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_dokter: ~2 rows (approximately)
DELETE FROM `t_dokter`;
/*!40000 ALTER TABLE `t_dokter` DISABLE KEYS */;
INSERT INTO `t_dokter` (`id`, `nm_dokter`, `no_hp`, `alamat`, `jk`) VALUES
	(2, 'Dokter A', NULL, NULL, NULL),
	(3, 'Dokter B', NULL, NULL, NULL);
/*!40000 ALTER TABLE `t_dokter` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_jenis
CREATE TABLE IF NOT EXISTS `t_jenis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_jenis: ~0 rows (approximately)
DELETE FROM `t_jenis`;
/*!40000 ALTER TABLE `t_jenis` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_jenis` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_obat
CREATE TABLE IF NOT EXISTS `t_obat` (
  `id` bigint(20) NOT NULL DEFAULT 0,
  `nm_obat` varchar(100) DEFAULT NULL,
  `quantity` mediumint(9) NOT NULL,
  `satuan` char(15) NOT NULL,
  `quantity_racik` mediumint(9) NOT NULL,
  `jlhper_raciik` mediumint(9) NOT NULL,
  `satuan_racik` varchar(50) NOT NULL,
  `expire_date` date NOT NULL,
  `tgl_jatuh_tempo` date DEFAULT NULL,
  `hrgper_racik` int(11) NOT NULL,
  `hrga_jual` mediumint(9) NOT NULL,
  `jenis` enum('Cream','Obat') NOT NULL,
  `jlhper_pack` mediumint(9) NOT NULL,
  `satuan_pack` varchar(100) NOT NULL,
  `hrgper_pack` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_obat: ~12 rows (approximately)
DELETE FROM `t_obat`;
/*!40000 ALTER TABLE `t_obat` DISABLE KEYS */;
INSERT INTO `t_obat` (`id`, `nm_obat`, `quantity`, `satuan`, `quantity_racik`, `jlhper_raciik`, `satuan_racik`, `expire_date`, `tgl_jatuh_tempo`, `hrgper_racik`, `hrga_jual`, `jenis`, `jlhper_pack`, `satuan_pack`, `hrgper_pack`) VALUES
	(1000001, 'OPPO', 4, 'BUAH', 0, 10, 'GRAM', '2023-08-19', '2023-08-19', 0, 2000, 'Cream', 0, '', 0),
	(1000002, 'OBAT LAGI', 3, 'BUTIR', 0, 0, '', '2024-08-09', '2024-08-09', 0, 500, 'Obat', 0, '', 0),
	(1000003, 'CREAM BARU', 18, 'TUBE', 1, 10, 'GRAM', '2024-08-09', '2021-08-13', 0, 15000, 'Cream', 0, '', 0),
	(1000011, 'PANADOL', 34, 'SASET', 10, 12, 'BUTIR', '2022-08-19', '2022-08-19', 0, 1000, 'Obat', 0, '', 0),
	(1000013, 'CREAM123', 9, 'TUBE', 0, 10, 'GRAM', '2022-08-13', '2021-08-13', 0, 3000, 'Cream', 0, '', 0),
	(1000018, 'AMOXILIN', 100, 'PCS', 0, 10, 'MG', '2024-08-17', '2024-08-09', 0, 1000, 'Obat', 12, 'BOX', 0),
	(1000020, 'PARAMEX', 0, 'SASET', 0, 0, '', '2023-08-19', '2023-08-19', 0, 4000, 'Obat', 0, '', 0),
	(1000024, 'KOMIX', 9, 'SASET', 7, 10, 'GRAM', '2023-08-19', '2019-08-03', 0, 1000, 'Obat', 0, '', 0),
	(1000032, 'OBAT RACIKAN', 0, 'TUBE', 5, 10, 'GRAM', '2022-08-13', '2022-08-13', 0, 5000, 'Cream', 0, '', 0),
	(1000039, 'PARATUSIN', 0, 'BUTIR', 0, 0, '', '2020-01-13', '2021-08-13', 0, 1000, 'Obat', 0, '', 0),
	(1000046, 'WWWW', 10, 'BUTIR', 0, 3, 'BUTIR', '2019-08-07', NULL, 0, 1000, 'Obat', 0, '', 0),
	(1000053, 'KOMIX ISI 100', 130, 'SASET', 0, 5, 'BUTIR', '2019-08-09', '2019-08-17', 0, 4000, 'Obat', 10, 'IKAT', 0);
/*!40000 ALTER TABLE `t_obat` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_obat_keluar
CREATE TABLE IF NOT EXISTS `t_obat_keluar` (
  `norec` bigint(20) NOT NULL AUTO_INCREMENT,
  `no_nota` varchar(50) NOT NULL DEFAULT '0',
  `nama_pengunjung` varchar(100) DEFAULT NULL,
  `id_pasien` bigint(20) DEFAULT NULL,
  `tgl_obat_keluar` datetime NOT NULL,
  `id_obat` bigint(20) NOT NULL,
  `jumlah_obat_keluar` smallint(6) NOT NULL,
  `biaya_listrik` mediumint(9) NOT NULL DEFAULT 0,
  `biaya_admin` mediumint(9) NOT NULL DEFAULT 0,
  `nm_dokter` varchar(150) NOT NULL,
  `tot_harga` int(11) NOT NULL,
  `uang` int(11) NOT NULL DEFAULT 0,
  `kembalian` int(11) NOT NULL DEFAULT 0,
  `insert_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `racikan` char(1) NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`norec`),
  KEY `FK_t_obat_keluar_t_obat` (`id_obat`),
  KEY `FK_t_obat_keluar_t_user` (`id_user`),
  CONSTRAINT `FK_t_obat_keluar_t_obat` FOREIGN KEY (`id_obat`) REFERENCES `t_obat` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_keluar_t_user` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1002387 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_obat_keluar: ~24 rows (approximately)
DELETE FROM `t_obat_keluar`;
/*!40000 ALTER TABLE `t_obat_keluar` DISABLE KEYS */;
INSERT INTO `t_obat_keluar` (`norec`, `no_nota`, `nama_pengunjung`, `id_pasien`, `tgl_obat_keluar`, `id_obat`, `jumlah_obat_keluar`, `biaya_listrik`, `biaya_admin`, `nm_dokter`, `tot_harga`, `uang`, `kembalian`, `insert_at`, `racikan`, `id_user`) VALUES
	(1002360, 'N03081947610', '', NULL, '2019-08-03 00:00:00', 1000003, 4, 0, 0, '', 17000, 20000, 3000, '2019-08-03 09:08:01', '1', 1),
	(1002361, 'N03081947610', '', NULL, '2019-08-03 00:00:00', 1000002, 2, 0, 0, '', 17000, 20000, 3000, '2019-08-03 09:08:01', '0', 1),
	(1002363, 'N030819550922', '', NULL, '2019-08-03 00:00:00', 1000003, 3, 0, 0, '', 24500, 30000, 5500, '2019-08-03 09:10:13', '1', 1),
	(1002365, 'N030819784813', '', NULL, '2019-08-03 00:00:00', 1000003, 3, 3000, 2000, '', 19500, 20000, 500, '2019-08-03 09:12:36', '1', 1),
	(1002366, 'N030819983683', '', NULL, '1899-12-30 00:00:00', 1000002, 2, 3000, 2000, '', 6000, 10000, 4000, '2019-08-03 09:13:52', '0', 1),
	(1002368, 'N030819560408', '', NULL, '2019-08-03 00:00:00', 1000002, 1, 3000, 2000, '', 5500, 10000, 4500, '2019-08-03 09:15:27', '0', 1),
	(1002369, 'N030819588631', '', NULL, '2019-08-03 00:00:00', 1000002, 3, 3000, 2000, '', 9500, 10000, 500, '2019-08-03 09:18:20', '0', 1),
	(1002370, 'N030819588631', '', NULL, '2019-08-03 00:00:00', 1000003, 2, 3000, 2000, '', 9500, 10000, 500, '2019-08-03 09:18:20', '1', 1),
	(1002371, 'N030819599986', '', NULL, '2019-08-03 00:00:00', 1000011, 2, 3000, 2000, '', 7000, 10000, 3000, '2019-08-03 10:46:55', '0', 1),
	(1002372, 'N030819182306', 'B', NULL, '2019-08-03 00:00:00', 1000011, 5, 3000, 2000, 'A', 10000, 10000, 0, '2019-08-03 12:05:14', '0', 1),
	(1002373, 'N03081929824', 'RRR', NULL, '2019-08-03 00:00:00', 1000013, 5, 3000, 2000, 'RRR', 11500, 15000, 3500, '2019-08-03 12:08:49', '1', 1),
	(1002374, 'N03081929824', 'RRR', NULL, '2019-08-03 00:00:00', 1000011, 5, 3000, 2000, 'RRR', 11500, 15000, 3500, '2019-08-03 12:08:49', '0', 1),
	(1002375, 'N030819602202', '', NULL, '2019-08-14 00:00:00', 1000003, 4, 3000, 2000, '', 14400, 15000, 600, '2019-08-03 12:29:56', '1', 1),
	(1002376, 'N030819602202', '', NULL, '2019-08-14 00:00:00', 1000013, 3, 3000, 2000, '', 14400, 15000, 600, '2019-08-03 12:29:56', '1', 1),
	(1002377, 'N030819602202', '', NULL, '2019-08-14 00:00:00', 1000032, 5, 3000, 2000, '', 14400, 15000, 600, '2019-08-03 12:29:56', '1', 1),
	(1002378, 'N070819192505', 'OOSSSSSSS', NULL, '2019-08-07 00:00:00', 1000024, 10, 3000, 2000, 'Dokter B', 15000, 15000, 0, '2019-08-07 08:42:02', '0', 1),
	(1002379, 'N070819722917', 'KAMANDANU', NULL, '2019-08-07 00:00:00', 1000024, 3, 3000, 2000, 'Dokter A', 8000, 10000, 2000, '2019-08-07 08:46:08', '0', 1),
	(1002380, 'N070819922302', 'PENGUNJUNG', NULL, '2019-08-07 00:00:00', 1000024, 3, 3000, 2000, 'Dokter B', 8300, 10000, 1700, '2019-08-07 09:41:13', '1', 1),
	(1002381, 'N070819922302', 'PENGUNJUNG', NULL, '2019-08-07 00:00:00', 1000011, 3, 3000, 2000, 'Dokter B', 8300, 10000, 1700, '2019-08-07 09:41:13', '1', 1),
	(1002382, 'N070819354432', '', NULL, '1899-12-30 00:00:00', 1000011, 1, 3000, 2000, '', 6000, 10000, 4000, '2019-08-07 09:44:08', '1', 1),
	(1002383, 'N0708195254', '', NULL, '1899-12-30 00:00:00', 1000011, 2, 3000, 2000, '', 7000, 10000, 3000, '2019-08-07 09:44:44', '1', 1),
	(1002384, 'N070819702193', '', NULL, '1899-12-30 00:00:00', 1000011, 2, 3000, 2000, '', 7000, 10000, 3000, '2019-08-07 17:17:04', '1', 1),
	(1002385, 'N080819147504', '', NULL, '2019-08-08 00:00:00', 1000003, 3, 3000, 2000, '', 10100, 12000, 1900, '2019-08-08 08:36:43', '1', 1),
	(1002386, 'N080819147504', '', NULL, '2019-08-08 00:00:00', 1000013, 2, 3000, 2000, '', 10100, 12000, 1900, '2019-08-08 08:36:43', '1', 1);
/*!40000 ALTER TABLE `t_obat_keluar` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_obat_masuk
CREATE TABLE IF NOT EXISTS `t_obat_masuk` (
  `norec` bigint(20) NOT NULL AUTO_INCREMENT,
  `no_faktur` char(20) NOT NULL,
  `id_suppli` bigint(20) NOT NULL,
  `tgl_fakur` datetime NOT NULL,
  `batch` varchar(50) DEFAULT NULL,
  `id_obat` bigint(20) NOT NULL,
  `expire_date` date NOT NULL,
  `jlh_obat_masuk` mediumint(9) NOT NULL,
  `total_harga` bigint(20) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `discount_harga` float(10,2) NOT NULL,
  `discount` float(10,2) NOT NULL,
  `harga_jual` float NOT NULL,
  `total_harga_beli` bigint(20) NOT NULL,
  `ppn` bigint(20) NOT NULL,
  `total_diskon` bigint(20) NOT NULL,
  `total_harga_iclude_ppn` bigint(20) NOT NULL,
  `tgl_jatuh_tempo` date DEFAULT NULL,
  `id_user` int(11) NOT NULL DEFAULT 0,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`norec`),
  KEY `FK_t_obat_masuk_t_suppli` (`id_suppli`),
  KEY `FK_t_obat_masuk_t_obat` (`id_obat`),
  KEY `FK_t_obat_masuk_t_user` (`id_user`),
  CONSTRAINT `FK_t_obat_masuk_t_obat` FOREIGN KEY (`id_obat`) REFERENCES `t_obat` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_masuk_t_suppli` FOREIGN KEY (`id_suppli`) REFERENCES `t_supplier` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_masuk_t_user` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_obat_masuk: ~16 rows (approximately)
DELETE FROM `t_obat_masuk`;
/*!40000 ALTER TABLE `t_obat_masuk` DISABLE KEYS */;
INSERT INTO `t_obat_masuk` (`norec`, `no_faktur`, `id_suppli`, `tgl_fakur`, `batch`, `id_obat`, `expire_date`, `jlh_obat_masuk`, `total_harga`, `harga_satuan`, `discount_harga`, `discount`, `harga_jual`, `total_harga_beli`, `ppn`, `total_diskon`, `total_harga_iclude_ppn`, `tgl_jatuh_tempo`, `id_user`, `create_at`) VALUES
	(122, '101001', 3, '2019-08-03 00:00:00', '1000111', 1000003, '2022-08-06', 10, 130000, 13000, 0.00, 0.00, 0, 160000, 16000, 0, 176000, NULL, 1, '2019-08-03 09:00:44'),
	(123, '101001', 3, '2019-08-03 00:00:00', '1000111', 1000003, '2022-08-06', 10, 130000, 13000, 0.00, 0.00, 0, 160000, 16000, 0, 176000, NULL, 1, '2019-08-03 09:00:44'),
	(125, 'EDSF', 3, '2019-08-03 00:00:00', 'DSFSD', 1000001, '2023-08-19', 30, 30000, 1000, 0.00, 0.00, 0, 33000, 3300, 0, 36300, NULL, 1, '2019-08-03 09:06:29'),
	(126, 'EDSF', 3, '2019-08-03 00:00:00', 'ASSS', 1000002, '2024-08-09', 12, 3000, 250, 0.00, 0.00, 0, 33000, 3300, 0, 36300, NULL, 1, '2019-08-03 09:06:29'),
	(128, '102992023', 3, '2019-08-03 00:00:00', '109191', 1000032, '2022-08-13', 10, 30000, 3000, 0.00, 0.00, 0, 45000, 4500, 0, 49500, NULL, 1, '2019-08-03 10:17:16'),
	(129, '102992023', 3, '2019-08-03 00:00:00', 'DDSDD', 1000024, '2019-08-03', 10, 2000, 200, 0.00, 0.00, 0, 45000, 4500, 0, 49500, NULL, 1, '2019-08-03 10:17:16'),
	(130, '102992023', 3, '2019-08-03 00:00:00', 'DSDDS', 1000020, '2023-08-19', 8, 10000, 1250, 0.00, 0.00, 0, 45000, 4500, 0, 49500, NULL, 1, '2019-08-03 10:17:16'),
	(131, '102992023', 4, '2019-08-03 00:00:00', 'XZXX', 1000011, '2021-11-13', 30, 3000, 100, 0.00, 0.00, 0, 45000, 4500, 0, 49500, NULL, 1, '2019-08-06 20:43:49'),
	(132, '1000002222', 4, '2019-08-03 00:00:00', 'TYYUUUYY', 1000011, '2022-08-12', 10, 9000, 900, 0.00, 0.00, 0, 12000, 1200, 0, 13200, '2019-08-15', 1, '2019-08-06 20:43:52'),
	(133, '1000002222', 3, '2019-08-03 00:00:00', '10020022', 1000039, '2021-08-13', 10, 3000, 300, 0.00, 0.00, 0, 12000, 1200, 0, 13200, '2019-08-15', 1, '2019-08-03 12:03:56'),
	(134, '233444', 3, '2019-08-03 00:00:00', 'GGGTGTG', 1000011, '2022-08-19', 10, 9000, 900, 0.00, 0.00, 0, 9000, 900, 0, 9900, NULL, 1, '2019-08-03 12:04:40'),
	(135, '44555', 3, '2019-08-03 00:00:00', 'TT', 1000013, '2019-08-03', 10, 30000, 3000, 0.00, 0.00, 0, 30000, 3000, 0, 33000, NULL, 1, '2019-08-03 12:07:05'),
	(136, '33242', 3, '2019-08-07 00:00:00', 'DDDDDD', 1000046, '2019-08-07', 10, 3000, 300, 0.00, 0.00, 0, 3000, 300, 0, 3300, NULL, 1, '2019-08-07 09:05:42'),
	(137, 'WERWER', 3, '2019-08-07 00:00:00', '10010022', 1000053, '2026-08-07', 30, 40000, 1333, 0.00, 0.00, 0, 40000, 4000, 0, 44000, '2024-08-09', 1, '2019-08-07 09:52:02'),
	(138, 'EWEW', 4, '2019-08-21 00:00:00', 'ERE', 1000053, '2019-08-09', 100, 44433, 444, 0.00, 0.00, 0, 44433, 4443, 0, 48876, '2019-08-17', 1, '2019-08-07 10:18:02'),
	(139, '020213134444', 4, '2019-08-07 00:00:00', 'SDDSD', 1000018, '2024-08-17', 100, 30000, 300, 0.00, 0.00, 0, 30000, 3000, 0, 33000, '2024-08-09', 1, '2019-08-07 17:12:47');
/*!40000 ALTER TABLE `t_obat_masuk` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_pabrik
CREATE TABLE IF NOT EXISTS `t_pabrik` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nm_pabrik` varchar(200) DEFAULT NULL,
  `no_contact` varchar(50) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_pabrik: ~0 rows (approximately)
DELETE FROM `t_pabrik`;
/*!40000 ALTER TABLE `t_pabrik` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pabrik` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_pasien
CREATE TABLE IF NOT EXISTS `t_pasien` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nam_pasien` varchar(100) NOT NULL,
  `alamat` varchar(300) DEFAULT NULL,
  `no_hp` char(12) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `no_rekam_medis` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `no_rekam_medis` (`no_rekam_medis`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_pasien: ~0 rows (approximately)
DELETE FROM `t_pasien`;
/*!40000 ALTER TABLE `t_pasien` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pasien` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_pot
CREATE TABLE IF NOT EXISTS `t_pot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namapot` varchar(50) DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_pot: ~3 rows (approximately)
DELETE FROM `t_pot`;
/*!40000 ALTER TABLE `t_pot` DISABLE KEYS */;
INSERT INTO `t_pot` (`id`, `namapot`, `biaya`) VALUES
	(1, 'Kecil', 1000),
	(2, 'Sedang', 1500),
	(3, 'Besar', 2000);
/*!40000 ALTER TABLE `t_pot` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_puyer
CREATE TABLE IF NOT EXISTS `t_puyer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namacetakan` varchar(50) DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_puyer: ~2 rows (approximately)
DELETE FROM `t_puyer`;
/*!40000 ALTER TABLE `t_puyer` DISABLE KEYS */;
INSERT INTO `t_puyer` (`id`, `namacetakan`, `biaya`) VALUES
	(1, 'Kertas', 1000),
	(2, 'Kapsul', 2000);
/*!40000 ALTER TABLE `t_puyer` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_satuan
CREATE TABLE IF NOT EXISTS `t_satuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `satuan` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_satuan: ~18 rows (approximately)
DELETE FROM `t_satuan`;
/*!40000 ALTER TABLE `t_satuan` DISABLE KEYS */;
INSERT INTO `t_satuan` (`id`, `satuan`) VALUES
	(10, 'TUBE'),
	(11, 'SASET'),
	(12, 'PCS'),
	(13, 'BOX'),
	(14, 'BOTOL'),
	(15, 'BUAH'),
	(16, 'PC'),
	(17, 'UNIT'),
	(18, 'GRAM'),
	(19, 'ML'),
	(20, 'LEMBAR'),
	(21, 'METER'),
	(22, 'PASANG'),
	(23, 'AMVUL'),
	(24, 'TAB'),
	(25, 'TABLET'),
	(26, 'KAPSUL'),
	(27, 'PAKET');
/*!40000 ALTER TABLE `t_satuan` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_supplier
CREATE TABLE IF NOT EXISTS `t_supplier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `namapbf` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `no_kontak` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_supplier: ~2 rows (approximately)
DELETE FROM `t_supplier`;
/*!40000 ALTER TABLE `t_supplier` DISABLE KEYS */;
INSERT INTO `t_supplier` (`id`, `namapbf`, `alamat`, `no_kontak`, `email`) VALUES
	(3, 'PT. Obat Asli', '', '', ''),
	(4, 'PT. orang Angkasa', '', '', '');
/*!40000 ALTER TABLE `t_supplier` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_user
CREATE TABLE IF NOT EXISTS `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `status` enum('A','O') DEFAULT NULL,
  `jk` enum('L','P') DEFAULT NULL,
  `no_hp` varchar(12) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_user: ~1 rows (approximately)
DELETE FROM `t_user`;
/*!40000 ALTER TABLE `t_user` DISABLE KEYS */;
INSERT INTO `t_user` (`id`, `username`, `password`, `status`, `jk`, `no_hp`, `alamat`) VALUES
	(1, 'admin', '*23AE809DDACAF96AF0FD78ED04B6A265E05AA257', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `t_user` ENABLE KEYS */;

-- Dumping structure for view db_apotek_praya.v_obat
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat` (
	`id` BIGINT(20) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity_racik` MEDIUMINT(9) NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`expire_date` DATE NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`hrgper_racik` INT(11) NOT NULL,
	`hrga_jual` MEDIUMINT(9) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrgper_pack` INT(11) NOT NULL,
	`jumlah` VARCHAR(19) NOT NULL COLLATE 'utf8mb4_general_ci',
	`i_expire` INT(7) NULL,
	`i_jatuh_tempo` INT(7) NULL
) ENGINE=MyISAM;

-- Dumping structure for view db_apotek_praya.v_obat_keluar
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat_keluar` (
	`id` BIGINT(20) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity_racik` MEDIUMINT(9) NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`expire_date` DATE NOT NULL,
	`hrgper_racik` INT(11) NOT NULL,
	`hrga_jual` MEDIUMINT(9) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrgper_pack` INT(11) NOT NULL,
	`norec` BIGINT(20) NOT NULL,
	`no_nota` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`nama_pengunjung` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`id_pasien` BIGINT(20) NULL,
	`tgl_obat_keluar` DATETIME NOT NULL,
	`id_obat` BIGINT(20) NOT NULL,
	`jumlah_obat_keluar` SMALLINT(6) NOT NULL,
	`biaya_listrik` MEDIUMINT(9) NOT NULL,
	`biaya_admin` MEDIUMINT(9) NOT NULL,
	`nm_dokter` VARCHAR(150) NOT NULL COLLATE 'latin1_swedish_ci',
	`tot_harga` INT(11) NOT NULL,
	`uang` INT(11) NOT NULL,
	`kembalian` INT(11) NOT NULL,
	`insert_at` TIMESTAMP NOT NULL,
	`racikan` CHAR(1) NOT NULL COLLATE 'latin1_swedish_ci',
	`id_user` INT(11) NOT NULL,
	`username` VARCHAR(200) NULL COLLATE 'latin1_swedish_ci',
	`satuanjual` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- Dumping structure for view db_apotek_praya.v_obat_masuk
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat_masuk` (
	`id` BIGINT(20) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity_racik` MEDIUMINT(9) NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`expire_date` DATE NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`hrgper_racik` INT(11) NOT NULL,
	`hrga_jual` MEDIUMINT(9) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrgper_pack` INT(11) NOT NULL,
	`namapbf` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`no_faktur` CHAR(20) NOT NULL COLLATE 'latin1_swedish_ci',
	`tgl_fakur` DATETIME NOT NULL,
	`id_suppli` BIGINT(20) NOT NULL,
	`batch` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`jlh_obat_masuk` MEDIUMINT(9) NOT NULL,
	`total_harga` BIGINT(20) NOT NULL,
	`harga_satuan` INT(11) NOT NULL,
	`discount_harga` FLOAT(10,2) NOT NULL,
	`discount` FLOAT(10,2) NOT NULL,
	`harga_jual` FLOAT NOT NULL,
	`create_at` VARCHAR(21) NULL COLLATE 'utf8mb4_general_ci',
	`total_harga_beli` BIGINT(20) NOT NULL,
	`total_diskon` BIGINT(20) NOT NULL,
	`total_harga_iclude_ppn` BIGINT(20) NOT NULL,
	`ppn` BIGINT(20) NOT NULL,
	`username` VARCHAR(200) NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- Dumping structure for trigger db_apotek_praya.trg_barang_keluar
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_barang_keluar` AFTER INSERT ON `t_obat_keluar` FOR EACH ROW BEGIN
	declare racikan,jlh_stok_racik,sisa_stok_racik int;
	
	SELECT t_obat_keluar.racikan from t_obat_keluar where t_obat_keluar.norec = new.norec into racikan; 

-- jika bukan racikan

	if racikan ='0' then
	begin
			update t_obat set t_obat.quantity = quantity - NEW.jumlah_obat_keluar
			where t_obat.id = new.id_obat;
	end;		
		else
		-- jika satuan racikan
			begin
				SELECT t_obat.quantity_racik from t_obat where t_obat.id = new.id_obat into jlh_stok_racik;
				set sisa_stok_racik = jlh_stok_racik - new.jumlah_obat_keluar;
				
				if sisa_stok_racik <0 then
				begin
					update t_obat set t_obat.quantity = t_obat.quantity - 1, 
					t_obat.quantity_racik = (t_obat.jlhper_raciik + sisa_stok_racik) where t_obat.id = new.id_obat;
				end;	
					else
						begin
								update t_obat set t_obat.quantity_racik = quantity_racik- new.jumlah_obat_keluar 
								where t_obat.id = new.id_obat;
						end;	
				end if;
				
				
			end;	
			
		
	end if;	
			
			
			
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_apotek_praya.trg_obat_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_obat_insert` AFTER INSERT ON `t_obat` FOR EACH ROW BEGIN
	--	update t_obat set t_obat.quantity = quantity-1  where t_obat.id = new.id;
		
	 
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_apotek_praya.trg_obat_masuk
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_obat_masuk` AFTER INSERT ON `t_obat_masuk` FOR EACH ROW BEGIN
		UPDATE t_obat 
		SET t_obat.quantity = quantity + NEW.jlh_obat_masuk,
		t_obat.tgl_jatuh_tempo = new.tgl_jatuh_tempo,
		t_obat.expire_date = new.expire_date 
		WHERE
			t_obat.id = new.id_obat;	
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for view db_apotek_praya.v_obat
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat` AS SELECT 
	t_obat.*,
	CONCAT(t_obat.quantity,",",t_obat.quantity_racik) AS jumlah,
	DATEDIFF(t_obat.expire_date, CURRENT_DATE()) AS i_expire,
	DATEDIFF(t_obat.tgl_jatuh_tempo, CURRENT_DATE()) AS i_jatuh_tempo
	from t_obat ;

-- Dumping structure for view db_apotek_praya.v_obat_keluar
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat_keluar`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat_keluar` AS SELECT 

t_obat.*,
ok.*,
t_user.username,

case when ok.racikan = 1 then t_obat.satuan_racik else t_obat.satuan end as satuanjual

 FROM t_obat_keluar as ok 
 INNER JOIN t_obat
ON t_obat.id = ok.id_obat

INNER JOIN t_user on t_user.id = ok.id_user

/*Menampilkan hnya data obat yg 1 tahun*/
 where
	DATEDIFF(CURRENT_DATE(),ok.tgl_obat_keluar) <360 
	
 ORDER BY ok.insert_at desc ;

-- Dumping structure for view db_apotek_praya.v_obat_masuk
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat_masuk`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat_masuk` AS SELECT 

t_obat.*,

t_supplier.namapbf,
t_obat_masuk.no_faktur,
t_obat_masuk.tgl_fakur,
t_obat_masuk.id_suppli,
t_obat_masuk.batch,
t_obat_masuk.jlh_obat_masuk,
t_obat_masuk.total_harga,
t_obat_masuk.harga_satuan,
t_obat_masuk.discount_harga,
t_obat_masuk.discount,
t_obat_masuk.harga_jual,
date_format(t_obat_masuk.create_at,'%d/%m/%Y %H:%i' ) as create_at,
t_obat_masuk.total_harga_beli,
t_obat_masuk.total_diskon,
t_obat_masuk.total_harga_iclude_ppn,
t_obat_masuk.ppn,
t_user.username  


 FROM t_obat_masuk 
 INNER JOIN t_obat ON t_obat.id = t_obat_masuk.id_obat
 inner join t_user on t_user.id = t_obat_masuk.id_user

inner join t_supplier on t_supplier.id = t_obat_masuk.id_suppli

/*Menampilkan hnya data obat yg 1 tahun*/
 where
	DATEDIFF(CURRENT_DATE(),t_obat_masuk.tgl_fakur) <360 
	
 ORDER BY t_obat_masuk.create_at desc ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
