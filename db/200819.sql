-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.1.41 - Source distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_apotek_praya
CREATE DATABASE IF NOT EXISTS `db_apotek_praya` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_apotek_praya`;

-- Dumping structure for procedure db_apotek_praya.sp_cetak_nota
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cetak_nota`(
	IN `nomornota` VARCHAR(50)

)
BEGIN
SELECT
	ok.no_nota,
	'OBAT RACIKAN' AS nama_obat,
	SUM( ok.jumlah_obat_keluar ) AS jumlah_obat_keluar,
	ok.satuan_racik,
	0 AS harga_satuan,
	SUM( round( ok.hrga_jual / ok.jlhper_raciik ) * ok.jumlah_obat_keluar ) AS total_harga,
	ok.uang,
	ok.kembalian,
	ok.biaya_listrik,
	ok.biaya_admin,
	( ok.biaya_listrik + ok.biaya_admin ) AS biayaadmin,
	ok.tot_harga 
FROM
	v_obat_keluar AS ok 
WHERE
	ok.no_nota = nomornota 
	AND ok.racikan = 1 
	
UNION
SELECT
	ok.no_nota,
	ok.nm_obat,
	ok.jumlah_obat_keluar,
	ok.satuan,
	ok.hrga_jual,
	SUM( ok.hrga_jual * ok.jumlah_obat_keluar ) AS total_harga,
	ok.uang,
	ok.kembalian,
	ok.biaya_listrik,
	ok.biaya_admin,
	( ok.biaya_listrik + ok.biaya_admin ) AS biayaadmin,
	ok.tot_harga 
FROM
	v_obat_keluar AS ok 
WHERE
	ok.no_nota = nomornota 
	AND ok.racikan = 0 
GROUP BY
	ok.id_obat;
	
	
	 
	END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat`()
BEGIN
	SELECT t_obat.*, CONCAT(t_obat.quantity,",",t_obat.quantity_racik) AS jumlah FROM t_obat;
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat_expire
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat_expire`()
BEGIN
		SELECT count(id) as jlh from t_obat 
			where DATEDIFF(t_obat.expire_date, CURRENT_DATE()) <= 180;
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat_jatuhtempo
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat_jatuhtempo`()
BEGIN
		SELECT count(id_obat) as jlh from t_obat_masuk 
		where DATEDIFF(t_obat_masuk.tgl_jatuh_tempo, CURRENT_DATE()) <= 14 and lunas ="0" ;
END//
DELIMITER ;

-- Dumping structure for table db_apotek_praya.t_biaya_lain
CREATE TABLE IF NOT EXISTS `t_biaya_lain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_biaya` varchar(100) DEFAULT NULL,
  `biaya` mediumint(9) DEFAULT NULL,
  `statusenabled` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_biaya_lain: ~2 rows (approximately)
DELETE FROM `t_biaya_lain`;
/*!40000 ALTER TABLE `t_biaya_lain` DISABLE KEYS */;
INSERT INTO `t_biaya_lain` (`id`, `nm_biaya`, `biaya`, `statusenabled`) VALUES
	(1, 'Biaya Listrik', 2000, NULL),
	(2, 'Biaya Admin', 3000, NULL);
/*!40000 ALTER TABLE `t_biaya_lain` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_closing
CREATE TABLE IF NOT EXISTS `t_closing` (
  `id` int(11) NOT NULL,
  `id_obat` bigint(20) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL,
  `quantity_racik` int(11) NOT NULL,
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `jlhper_raciik` int(11) NOT NULL,
  `tahun` year(4) NOT NULL,
  `priode` int(11) NOT NULL,
  KEY `FK_t_closing_t_user` (`id_user`),
  CONSTRAINT `FK_t_closing_t_user` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_closing: ~16 rows (approximately)
DELETE FROM `t_closing`;
/*!40000 ALTER TABLE `t_closing` DISABLE KEYS */;
INSERT INTO `t_closing` (`id`, `id_obat`, `quantity`, `quantity_racik`, `id_user`, `jlhper_raciik`, `tahun`, `priode`) VALUES
	(0, 1000001, 3, 0, 1, 10, '2019', 82019),
	(0, 1000002, 3, 0, 1, 0, '2019', 82019),
	(0, 1000003, 18, 0, 1, 10, '2019', 82019),
	(0, 1000008, 60, 0, 1, 0, '2019', 82019),
	(0, 1000011, 34, 3, 1, 12, '2019', 82019),
	(0, 1000013, 8, 6, 1, 10, '2019', 82019),
	(0, 1000018, 98, 9, 1, 10, '2019', 82019),
	(0, 1000020, 7, 0, 1, 0, '2019', 82019),
	(0, 1000024, 5, 2, 1, 10, '2019', 82019),
	(0, 1000032, 50, 5, 1, 10, '2019', 82019),
	(0, 1000039, 100, 0, 1, 0, '2019', 82019),
	(0, 1000046, 10, 0, 1, 3, '2019', 82019),
	(0, 1000053, 127, 0, 1, 5, '2019', 82019),
	(0, 1000060, 300, 0, 1, 10, '2019', 82019),
	(0, 1000068, 10, 0, 1, 0, '2019', 82019),
	(0, 1000083, 14, 0, 1, 0, '2019', 82019);
/*!40000 ALTER TABLE `t_closing` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_dokter
CREATE TABLE IF NOT EXISTS `t_dokter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nm_dokter` varchar(200) DEFAULT NULL,
  `no_hp` char(12) DEFAULT NULL,
  `alamat` text,
  `jk` enum('L','P') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_dokter: ~2 rows (approximately)
DELETE FROM `t_dokter`;
/*!40000 ALTER TABLE `t_dokter` DISABLE KEYS */;
INSERT INTO `t_dokter` (`id`, `nm_dokter`, `no_hp`, `alamat`, `jk`) VALUES
	(2, 'Dokter A', NULL, NULL, NULL),
	(3, 'Dokter B', NULL, NULL, NULL);
/*!40000 ALTER TABLE `t_dokter` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_jenis
CREATE TABLE IF NOT EXISTS `t_jenis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_jenis: ~0 rows (approximately)
DELETE FROM `t_jenis`;
/*!40000 ALTER TABLE `t_jenis` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_jenis` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_jenis_racikan
CREATE TABLE IF NOT EXISTS `t_jenis_racikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diskripsi` varchar(50) DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_jenis_racikan: ~8 rows (approximately)
DELETE FROM `t_jenis_racikan`;
/*!40000 ALTER TABLE `t_jenis_racikan` DISABLE KEYS */;
INSERT INTO `t_jenis_racikan` (`id`, `diskripsi`, `biaya`) VALUES
	(0, '-', 0),
	(11, 'Pot - Kecil', 500),
	(22, 'Pot - Sedang', 1000),
	(33, 'Pot - Besar', 1500),
	(44, 'Puyer - Kertas', 600),
	(55, 'Puyer - Kapsul', 1000),
	(66, 'Kapsul', NULL),
	(77, 'Syrup', NULL);
/*!40000 ALTER TABLE `t_jenis_racikan` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_obat
CREATE TABLE IF NOT EXISTS `t_obat` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `nm_obat` varchar(100) DEFAULT NULL,
  `quantity` mediumint(9) NOT NULL,
  `satuan` char(15) NOT NULL,
  `quantity_racik` mediumint(9) NOT NULL,
  `jlhper_raciik` mediumint(9) NOT NULL,
  `satuan_racik` varchar(50) NOT NULL,
  `expire_date` date NOT NULL,
  `tgl_jatuh_tempo` date DEFAULT NULL,
  `hrgper_racik` int(11) NOT NULL,
  `hrga_jual` mediumint(9) NOT NULL,
  `jenis` enum('Cream','Obat') NOT NULL,
  `jlhper_pack` mediumint(9) NOT NULL,
  `satuan_pack` varchar(100) NOT NULL,
  `hrgper_pack` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_obat: ~16 rows (approximately)
DELETE FROM `t_obat`;
/*!40000 ALTER TABLE `t_obat` DISABLE KEYS */;
INSERT INTO `t_obat` (`id`, `nm_obat`, `quantity`, `satuan`, `quantity_racik`, `jlhper_raciik`, `satuan_racik`, `expire_date`, `tgl_jatuh_tempo`, `hrgper_racik`, `hrga_jual`, `jenis`, `jlhper_pack`, `satuan_pack`, `hrgper_pack`) VALUES
	(1000001, 'OPPO', 3, 'BUAH', 0, 10, 'GRAM', '2023-08-19', '2023-08-19', 0, 2000, 'Cream', 0, '', 0),
	(1000002, 'OBAT LAGI', 3, 'BUTIR', 0, 0, '', '2024-08-09', '2024-08-09', 0, 500, 'Obat', 0, '', 0),
	(1000003, 'CREAM BARU', 18, 'TUBE', 0, 10, 'GRAM', '2024-08-09', '2021-08-13', 0, 15000, 'Cream', 0, '', 0),
	(1000008, 'WWWWW', 60, 'TABLET', 0, 0, '-', '2023-10-20', '2019-09-19', 0, 500, 'Obat', 10, 'UNIT', 0),
	(1000011, 'PANADOL', 34, 'SASET', 3, 12, 'BUTIR', '2022-08-19', '2022-08-19', 0, 1000, 'Obat', 0, '', 0),
	(1000013, 'CREAM123', 8, 'TUBE', 6, 10, 'GRAM', '2022-08-13', '2021-08-13', 0, 3000, 'Cream', 0, '', 0),
	(1000018, 'AMOXILIN', 98, 'PCS', 9, 10, 'MG', '2024-08-17', '2024-08-09', 0, 1000, 'Obat', 12, 'BOX', 0),
	(1000020, 'PARAMEX', 7, 'SASET', 0, 0, '', '2023-08-19', '2023-08-19', 0, 4000, 'Obat', 0, '', 0),
	(1000024, 'KOMIX', 5, 'SASET', 2, 10, 'GRAM', '2023-08-19', '2019-08-03', 0, 1000, 'Obat', 0, '', 0),
	(1000032, 'OBAT RACIKAN', 50, 'TUBE', 5, 10, 'GRAM', '2022-08-13', '2022-08-13', 0, 5000, 'Cream', 0, '', 0),
	(1000039, 'PARATUSIN', 100, 'BUTIR', 0, 0, '', '2020-01-13', '2021-08-13', 0, 1000, 'Obat', 0, '', 0),
	(1000046, 'WWWW', 10, 'BUTIR', 0, 3, 'BUTIR', '2019-08-07', NULL, 0, 1000, 'Obat', 0, '', 0),
	(1000053, 'KOMIX ISI 100', 127, 'SASET', 0, 5, 'BUTIR', '2019-08-09', '2019-08-17', 0, 4000, 'Obat', 10, 'IKAT', 0),
	(1000060, 'KANGKKUNG', 300, 'KAPSUL', 0, 10, 'GRAM', '2021-08-13', '2019-08-30', 0, 1000, 'Obat', 10, 'BOTOL', 0),
	(1000068, 'KELENGKENG', 10, 'KAPSUL', 0, 0, '-', '2022-08-25', '2019-08-22', 0, 300, 'Obat', 5, 'BOTOL', 0),
	(1000083, 'KARUNG', 14, 'KAPSUL', 0, 0, '-', '2023-06-22', NULL, 0, 1000, 'Obat', 0, '', 0);
/*!40000 ALTER TABLE `t_obat` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_obat_keluar
CREATE TABLE IF NOT EXISTS `t_obat_keluar` (
  `norec` bigint(20) NOT NULL AUTO_INCREMENT,
  `no_nota` varchar(50) DEFAULT '0',
  `nama_pengunjung` varchar(100) DEFAULT NULL,
  `id_pasien` bigint(20) DEFAULT NULL,
  `tgl_obat_keluar` datetime NOT NULL,
  `id_obat` bigint(20) NOT NULL,
  `jumlah_obat_keluar` smallint(6) NOT NULL,
  `biaya_listrik` mediumint(9) NOT NULL DEFAULT '0',
  `biaya_admin` mediumint(9) NOT NULL DEFAULT '0',
  `nm_dokter` varchar(150) NOT NULL,
  `tot_harga` int(11) NOT NULL,
  `uang` int(11) NOT NULL DEFAULT '0',
  `kembalian` int(11) NOT NULL DEFAULT '0',
  `insert_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `racikan` char(1) NOT NULL,
  `id_jenis_racikan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`norec`),
  KEY `FK_t_obat_keluar_t_obat` (`id_obat`),
  KEY `FK_t_obat_keluar_t_user` (`id_user`),
  KEY `FK_t_obat_keluar_t_jenis_racikan` (`id_jenis_racikan`),
  CONSTRAINT `FK_t_obat_keluar_t_jenis_racikan` FOREIGN KEY (`id_jenis_racikan`) REFERENCES `t_jenis_racikan` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_keluar_t_obat` FOREIGN KEY (`id_obat`) REFERENCES `t_obat` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_keluar_t_user` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1002412 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_obat_keluar: ~48 rows (approximately)
DELETE FROM `t_obat_keluar`;
/*!40000 ALTER TABLE `t_obat_keluar` DISABLE KEYS */;
INSERT INTO `t_obat_keluar` (`norec`, `no_nota`, `nama_pengunjung`, `id_pasien`, `tgl_obat_keluar`, `id_obat`, `jumlah_obat_keluar`, `biaya_listrik`, `biaya_admin`, `nm_dokter`, `tot_harga`, `uang`, `kembalian`, `insert_at`, `racikan`, `id_jenis_racikan`, `id_user`) VALUES
	(1002360, 'N03081947610', '', NULL, '2019-08-03 00:00:00', 1000003, 4, 0, 0, '', 17000, 20000, 3000, '2019-08-12 22:10:01', '1', 11, 1),
	(1002361, 'N03081947610', '', NULL, '2019-08-03 00:00:00', 1000002, 2, 0, 0, '', 17000, 20000, 3000, '2019-08-12 22:10:01', '0', 11, 1),
	(1002363, 'N030819550922', '', NULL, '2019-08-03 00:00:00', 1000003, 3, 0, 0, '', 24500, 30000, 5500, '2019-08-12 22:10:01', '1', 11, 1),
	(1002365, 'N030819784813', '', NULL, '2019-08-03 00:00:00', 1000003, 3, 3000, 2000, '', 19500, 20000, 500, '2019-08-12 22:10:01', '1', 11, 1),
	(1002366, 'N030819983683', '', NULL, '1899-12-30 00:00:00', 1000002, 2, 3000, 2000, '', 6000, 10000, 4000, '2019-08-12 22:10:01', '0', 11, 1),
	(1002368, 'N030819560408', '', NULL, '2019-08-03 00:00:00', 1000002, 1, 3000, 2000, '', 5500, 10000, 4500, '2019-08-12 22:10:01', '0', 11, 1),
	(1002369, 'N030819588631', '', NULL, '2019-08-03 00:00:00', 1000002, 3, 3000, 2000, '', 9500, 10000, 500, '2019-08-12 22:10:01', '0', 11, 1),
	(1002370, 'N030819588631', '', NULL, '2019-08-03 00:00:00', 1000003, 2, 3000, 2000, '', 9500, 10000, 500, '2019-08-12 22:10:01', '1', 11, 1),
	(1002371, 'N030819599986', '', NULL, '2019-08-03 00:00:00', 1000011, 2, 3000, 2000, '', 7000, 10000, 3000, '2019-08-12 22:10:01', '0', 11, 1),
	(1002372, 'N030819182306', 'B', NULL, '2019-08-03 00:00:00', 1000011, 5, 3000, 2000, 'A', 10000, 10000, 0, '2019-08-12 22:10:01', '0', 11, 1),
	(1002373, 'N03081929824', 'RRR', NULL, '2019-08-03 00:00:00', 1000013, 5, 3000, 2000, 'RRR', 11500, 15000, 3500, '2019-08-12 22:10:01', '1', 11, 1),
	(1002374, 'N03081929824', 'RRR', NULL, '2019-08-03 00:00:00', 1000011, 5, 3000, 2000, 'RRR', 11500, 15000, 3500, '2019-08-12 22:10:01', '0', 11, 1),
	(1002375, 'N030819602202', '', NULL, '2019-08-14 00:00:00', 1000003, 4, 3000, 2000, '', 14400, 15000, 600, '2019-08-12 22:10:01', '1', 11, 1),
	(1002376, 'N030819602202', '', NULL, '2019-08-14 00:00:00', 1000013, 3, 3000, 2000, '', 14400, 15000, 600, '2019-08-12 22:10:01', '1', 11, 1),
	(1002377, 'N030819602202', '', NULL, '2019-08-14 00:00:00', 1000032, 5, 3000, 2000, '', 14400, 15000, 600, '2019-08-12 22:10:01', '1', 11, 1),
	(1002378, 'N070819192505', 'OOSSSSSSS', NULL, '2019-08-07 00:00:00', 1000024, 10, 3000, 2000, 'Dokter B', 15000, 15000, 0, '2019-08-12 22:10:01', '0', 11, 1),
	(1002379, 'N070819722917', 'KAMANDANU', NULL, '2019-08-07 00:00:00', 1000024, 3, 3000, 2000, 'Dokter A', 8000, 10000, 2000, '2019-08-12 22:10:01', '0', 11, 1),
	(1002380, 'N070819922302', 'PENGUNJUNG', NULL, '2019-08-07 00:00:00', 1000024, 3, 3000, 2000, 'Dokter B', 8300, 10000, 1700, '2019-08-12 22:10:01', '1', 11, 1),
	(1002381, 'N070819922302', 'PENGUNJUNG', NULL, '2019-08-07 00:00:00', 1000011, 3, 3000, 2000, 'Dokter B', 8300, 10000, 1700, '2019-08-12 22:10:01', '1', 11, 1),
	(1002382, 'N070819354432', '', NULL, '1899-12-30 00:00:00', 1000011, 1, 3000, 2000, '', 6000, 10000, 4000, '2019-08-12 22:10:01', '1', 11, 1),
	(1002383, 'N0708195254', '', NULL, '1899-12-30 00:00:00', 1000011, 2, 3000, 2000, '', 7000, 10000, 3000, '2019-08-12 22:10:01', '1', 11, 1),
	(1002384, 'N070819702193', '', NULL, '1899-12-30 00:00:00', 1000011, 2, 3000, 2000, '', 7000, 10000, 3000, '2019-08-12 22:10:01', '1', 11, 1),
	(1002385, 'N080819147504', '', NULL, '2019-08-08 00:00:00', 1000003, 3, 3000, 2000, '', 10100, 12000, 1900, '2019-08-12 22:10:01', '1', 11, 1),
	(1002386, 'N080819147504', '', NULL, '2019-08-08 00:00:00', 1000013, 2, 3000, 2000, '', 10100, 12000, 1900, '2019-08-12 22:10:01', '1', 11, 1),
	(1002387, 'N080819686911', 'DDDD', NULL, '1899-12-30 00:00:00', 1000018, 3, 3000, 2000, 'Dokter A', 8300, 10000, 1700, '2019-08-12 22:10:01', '1', 11, 1),
	(1002388, 'N080819686911', 'DDDD', NULL, '1899-12-30 00:00:00', 1000024, 3, 3000, 2000, 'Dokter A', 8300, 10000, 1700, '2019-08-12 22:10:01', '0', 11, 1),
	(1002389, '', '', NULL, '2019-08-08 00:00:00', 1000018, 2, 3000, 2000, '', 7600, 10000, 2400, '2019-08-12 22:10:01', '1', 11, 1),
	(1002390, 'N080819943498', '', NULL, '2019-08-08 00:00:00', 1000053, 3, 3000, 2000, '', 7600, 10000, 2400, '2019-08-12 22:10:01', '1', 11, 1),
	(1002391, '', '', NULL, '2019-08-08 00:00:00', 1000018, 3, 3000, 2000, '', 5300, 10000, 4700, '2019-08-12 22:10:01', '1', 11, 1),
	(1002392, 'N080819388568', '', NULL, '2019-08-08 00:00:00', 1000018, 3, 3000, 2000, '', 7700, 10000, 2300, '2019-08-12 22:10:01', '1', 11, 1),
	(1002393, 'N080819388568', '', NULL, '2019-08-08 00:00:00', 1000053, 3, 3000, 2000, '', 7700, 10000, 2300, '2019-08-12 22:10:01', '1', 11, 1),
	(1002394, 'N120819548231', '', NULL, '2019-08-12 00:00:00', 1000024, 3, 3000, 2000, '', 12732, 15000, 2268, '2019-08-12 22:31:41', '1', 0, 1),
	(1002395, 'N120819548231', '', NULL, '2019-08-12 00:00:00', 1000013, 3, 3000, 2000, '', 12732, 15000, 2268, '2019-08-12 22:31:41', '1', 0, 1),
	(1002396, 'N120819548231', '', NULL, '2019-08-12 00:00:00', 1000011, 4, 3000, 2000, '', 12732, 15000, 2268, '2019-08-12 22:31:41', '1', 0, 1),
	(1002397, 'N130819152855', '', NULL, '2019-08-13 00:00:00', 1000024, 1, 3000, 2000, '', 9583, 10000, 417, '2019-08-13 20:54:47', '1', 0, 1),
	(1002398, 'N130819152855', '', NULL, '2019-08-13 00:00:00', 1000011, 1, 3000, 2000, '', 9583, 10000, 417, '2019-08-13 20:54:47', '1', 0, 1),
	(1002399, 'N130819152855', '', NULL, '2019-08-13 00:00:00', 1000053, 3, 3000, 2000, '', 9583, 10000, 417, '2019-08-13 20:54:47', '1', 0, 1),
	(1002400, 'N130819152855', '', NULL, '2019-08-13 00:00:00', 1000001, 1, 3000, 2000, '', 9583, 10000, 417, '2019-08-13 20:54:47', '0', 0, 1),
	(1002401, 'N140819297357', '', NULL, '2019-08-14 00:00:00', 1000083, 1, 3000, 2000, '', 6000, 10000, 4000, '2019-08-14 17:55:25', '0', 0, 1),
	(1002402, 'N150819641999', 'SSSSASS', NULL, '2019-08-15 00:00:00', 1000024, 1, 3000, 2000, 'Dokter B', 15183, 20000, 4817, '2019-08-15 22:39:24', '0', 0, 1),
	(1002403, 'N150819641999', 'SSSSASS', NULL, '2019-08-15 00:00:00', 1000011, 1, 3000, 2000, 'Dokter B', 15183, 20000, 4817, '2019-08-15 22:39:24', '1', 0, 1),
	(1002404, 'N150819641999', 'SSSSASS', NULL, '2019-08-15 00:00:00', 1000053, 1, 3000, 2000, 'Dokter B', 15183, 20000, 4817, '2019-08-15 22:39:24', '1', 0, 1),
	(1002405, 'N150819641999', 'SSSSASS', NULL, '2019-08-15 00:00:00', 1000020, 1, 3000, 2000, 'Dokter B', 15183, 20000, 4817, '2019-08-15 22:39:24', '0', 0, 1),
	(1002406, 'N150819641999', 'SSSSASS', NULL, '2019-08-15 00:00:00', 1000003, 1, 3000, 2000, 'Dokter B', 15183, 20000, 4817, '2019-08-15 22:39:24', '1', 0, 1),
	(1002407, 'N150819641999', 'SSSSASS', NULL, '2019-08-15 00:00:00', 1000013, 1, 3000, 2000, 'Dokter B', 15183, 20000, 4817, '2019-08-15 22:39:24', '1', 0, 1),
	(1002409, 'N160819168950', '', NULL, '2019-08-16 00:00:00', 1000024, 1, 3000, 2000, '', 10783, 15000, 4217, '2019-08-16 13:59:37', '1', 44, 1),
	(1002410, 'N160819168950', '', NULL, '2019-08-16 00:00:00', 1000053, 1, 3000, 2000, '', 10783, 15000, 4217, '2019-08-16 13:59:37', '0', 44, 1),
	(1002411, 'N160819168950', '', NULL, '2019-08-16 00:00:00', 1000011, 1, 3000, 2000, '', 10783, 15000, 4217, '2019-08-16 13:59:37', '1', 44, 1);
/*!40000 ALTER TABLE `t_obat_keluar` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_obat_masuk
CREATE TABLE IF NOT EXISTS `t_obat_masuk` (
  `norec` bigint(20) NOT NULL AUTO_INCREMENT,
  `no_faktur` char(20) NOT NULL,
  `id_suppli` bigint(20) NOT NULL,
  `tgl_fakur` datetime NOT NULL,
  `batch` varchar(50) DEFAULT NULL,
  `id_obat` bigint(20) NOT NULL,
  `expire_date` date NOT NULL,
  `jlh_obat_masuk` mediumint(9) NOT NULL,
  `total_harga` bigint(20) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `discount_harga` float(10,2) NOT NULL,
  `discount` float(10,2) NOT NULL,
  `harga_jual` float NOT NULL,
  `total_harga_beli` bigint(20) NOT NULL,
  `ppn` bigint(20) NOT NULL,
  `total_diskon` bigint(20) NOT NULL,
  `total_harga_iclude_ppn` bigint(20) NOT NULL,
  `tgl_jatuh_tempo` date DEFAULT NULL,
  `lunas` enum('0','1') DEFAULT NULL,
  `tgl_bayar` date DEFAULT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`norec`),
  KEY `FK_t_obat_masuk_t_suppli` (`id_suppli`),
  KEY `FK_t_obat_masuk_t_obat` (`id_obat`),
  KEY `FK_t_obat_masuk_t_user` (`id_user`),
  CONSTRAINT `FK_t_obat_masuk_t_obat` FOREIGN KEY (`id_obat`) REFERENCES `t_obat` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_masuk_t_suppli` FOREIGN KEY (`id_suppli`) REFERENCES `t_supplier` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_masuk_t_user` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_obat_masuk: ~22 rows (approximately)
DELETE FROM `t_obat_masuk`;
/*!40000 ALTER TABLE `t_obat_masuk` DISABLE KEYS */;
INSERT INTO `t_obat_masuk` (`norec`, `no_faktur`, `id_suppli`, `tgl_fakur`, `batch`, `id_obat`, `expire_date`, `jlh_obat_masuk`, `total_harga`, `harga_satuan`, `discount_harga`, `discount`, `harga_jual`, `total_harga_beli`, `ppn`, `total_diskon`, `total_harga_iclude_ppn`, `tgl_jatuh_tempo`, `lunas`, `tgl_bayar`, `id_user`, `create_at`) VALUES
	(122, '101001', 3, '2019-08-03 00:00:00', '1000111', 1000003, '2022-08-06', 10, 130000, 13000, 0.00, 0.00, 0, 160000, 16000, 0, 176000, NULL, NULL, NULL, 1, '2019-08-03 09:00:44'),
	(123, '101001', 3, '2019-08-03 00:00:00', '1000111', 1000003, '2022-08-06', 10, 130000, 13000, 0.00, 0.00, 0, 160000, 16000, 0, 176000, NULL, NULL, NULL, 1, '2019-08-03 09:00:44'),
	(125, 'EDSF', 3, '2019-08-03 00:00:00', 'DSFSD', 1000001, '2023-08-19', 30, 30000, 1000, 0.00, 0.00, 0, 33000, 3300, 0, 36300, NULL, NULL, NULL, 1, '2019-08-03 09:06:29'),
	(126, 'EDSF', 3, '2019-08-03 00:00:00', 'ASSS', 1000002, '2024-08-09', 12, 3000, 250, 0.00, 0.00, 0, 33000, 3300, 0, 36300, NULL, NULL, NULL, 1, '2019-08-03 09:06:29'),
	(128, '102992023', 3, '2019-08-03 00:00:00', '109191', 1000032, '2022-08-13', 10, 30000, 3000, 0.00, 0.00, 0, 45000, 4500, 0, 49500, NULL, NULL, NULL, 1, '2019-08-03 10:17:16'),
	(129, '102992023', 3, '2019-08-03 00:00:00', 'DDSDD', 1000024, '2019-08-03', 10, 2000, 200, 0.00, 0.00, 0, 45000, 4500, 0, 49500, NULL, NULL, NULL, 1, '2019-08-03 10:17:16'),
	(130, '102992023', 3, '2019-08-03 00:00:00', 'DSDDS', 1000020, '2023-08-19', 8, 10000, 1250, 0.00, 0.00, 0, 45000, 4500, 0, 49500, NULL, NULL, NULL, 1, '2019-08-03 10:17:16'),
	(131, '102992023', 4, '2019-08-03 00:00:00', 'XZXX', 1000011, '2021-11-13', 30, 3000, 100, 0.00, 0.00, 0, 45000, 4500, 0, 49500, NULL, NULL, NULL, 1, '2019-08-06 20:43:49'),
	(132, '1000002222', 4, '2019-08-03 00:00:00', 'TYYUUUYY', 1000011, '2022-08-12', 10, 9000, 900, 0.00, 0.00, 0, 12000, 1200, 0, 13200, '2019-08-15', NULL, NULL, 1, '2019-08-06 20:43:52'),
	(133, '1000002222', 3, '2019-08-03 00:00:00', '10020022', 1000039, '2021-08-13', 10, 3000, 300, 0.00, 0.00, 0, 12000, 1200, 0, 13200, '2019-08-15', NULL, NULL, 1, '2019-08-03 12:03:56'),
	(134, '233444', 3, '2019-08-03 00:00:00', 'GGGTGTG', 1000011, '2022-08-19', 10, 9000, 900, 0.00, 0.00, 0, 9000, 900, 0, 9900, NULL, NULL, NULL, 1, '2019-08-03 12:04:40'),
	(135, '44555', 3, '2019-08-03 00:00:00', 'TT', 1000013, '2019-08-03', 10, 30000, 3000, 0.00, 0.00, 0, 30000, 3000, 0, 33000, NULL, NULL, NULL, 1, '2019-08-03 12:07:05'),
	(136, '33242', 3, '2019-08-07 00:00:00', 'DDDDDD', 1000046, '2019-08-07', 10, 3000, 300, 0.00, 0.00, 0, 3000, 300, 0, 3300, NULL, NULL, NULL, 1, '2019-08-07 09:05:42'),
	(137, 'WERWER', 3, '2019-08-07 00:00:00', '10010022', 1000053, '2026-08-07', 30, 40000, 1333, 0.00, 0.00, 0, 40000, 4000, 0, 44000, '2024-08-09', NULL, NULL, 1, '2019-08-07 09:52:02'),
	(138, 'EWEW', 4, '2019-08-21 00:00:00', 'ERE', 1000053, '2019-08-09', 100, 44433, 444, 0.00, 0.00, 0, 44433, 4443, 0, 48876, '2019-08-17', NULL, NULL, 1, '2019-08-07 10:18:02'),
	(139, '020213134444', 4, '2019-08-07 00:00:00', 'SDDSD', 1000018, '2024-08-17', 100, 30000, 300, 0.00, 0.00, 0, 30000, 3000, 0, 33000, '2024-08-09', NULL, NULL, 1, '2019-08-07 17:12:47'),
	(140, 'AAAAAAA', 3, '2019-08-29 00:00:00', 'DDSDS', 1000008, '2024-11-08', 10, 3000, 300, 0.00, 0.00, 0, 3000, 300, 0, 3300, '2019-09-18', NULL, NULL, 1, '2019-08-14 17:20:29'),
	(141, 'WQWEQW', 3, '2019-08-28 00:00:00', 'EEWER', 1000008, '2023-10-20', 50, 20000, 400, 0.00, 0.00, 0, 20000, 2000, 0, 22000, '2019-09-19', NULL, NULL, 1, '2019-08-14 17:22:44'),
	(142, '66666666', 4, '2019-08-14 00:00:00', 'SDFSDF', 1000060, '2021-08-13', 300, 30000, 100, 0.00, 0.00, 0, 30000, 3000, 0, 33000, '2019-08-30', NULL, NULL, 1, '2019-08-14 17:38:54'),
	(143, 'AQEQWE', 4, '2019-08-14 00:00:00', 'WERWERWER', 1000068, '2022-08-25', 10, 2000, 200, 0.00, 0.00, 0, 2000, 200, 0, 2200, '2019-08-22', NULL, NULL, 1, '2019-08-14 17:40:30'),
	(144, 'SDFSDFSDF', 4, '2019-08-21 00:00:00', 'ERT', 1000083, '2022-08-12', 12, 3344, 279, 0.00, 0.00, 0, 3344, 334, 0, 3678, '2019-08-30', NULL, NULL, 1, '2019-08-14 17:49:58'),
	(145, 'ASAS', 4, '2019-08-15 00:00:00', 'SDFSD', 1000083, '2023-06-22', 3, 1233, 411, 0.00, 0.00, 0, 1233, 123, 0, 1356, NULL, NULL, NULL, 1, '2019-08-14 17:51:31');
/*!40000 ALTER TABLE `t_obat_masuk` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_pabrik
CREATE TABLE IF NOT EXISTS `t_pabrik` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nm_pabrik` varchar(200) DEFAULT NULL,
  `no_contact` varchar(50) DEFAULT NULL,
  `alamat` text,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_pabrik: ~0 rows (approximately)
DELETE FROM `t_pabrik`;
/*!40000 ALTER TABLE `t_pabrik` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pabrik` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_pasien
CREATE TABLE IF NOT EXISTS `t_pasien` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nam_pasien` varchar(100) NOT NULL,
  `alamat` varchar(300) DEFAULT NULL,
  `no_hp` char(12) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `no_rekam_medis` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `no_rekam_medis` (`no_rekam_medis`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_pasien: ~0 rows (approximately)
DELETE FROM `t_pasien`;
/*!40000 ALTER TABLE `t_pasien` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pasien` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_pot
CREATE TABLE IF NOT EXISTS `t_pot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namapot` varchar(50) DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_pot: ~3 rows (approximately)
DELETE FROM `t_pot`;
/*!40000 ALTER TABLE `t_pot` DISABLE KEYS */;
INSERT INTO `t_pot` (`id`, `namapot`, `biaya`) VALUES
	(1, 'Kecil', 1000),
	(2, 'Sedang', 1500),
	(3, 'Besar', 2000);
/*!40000 ALTER TABLE `t_pot` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_puyer
CREATE TABLE IF NOT EXISTS `t_puyer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namacetakan` varchar(50) DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_puyer: ~2 rows (approximately)
DELETE FROM `t_puyer`;
/*!40000 ALTER TABLE `t_puyer` DISABLE KEYS */;
INSERT INTO `t_puyer` (`id`, `namacetakan`, `biaya`) VALUES
	(1, 'Kertas', 1000),
	(2, 'Kapsul', 2000);
/*!40000 ALTER TABLE `t_puyer` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_satuan
CREATE TABLE IF NOT EXISTS `t_satuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `satuan` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_satuan: ~14 rows (approximately)
DELETE FROM `t_satuan`;
/*!40000 ALTER TABLE `t_satuan` DISABLE KEYS */;
INSERT INTO `t_satuan` (`id`, `satuan`) VALUES
	(13, 'BOX'),
	(14, 'BOTOL'),
	(15, 'BUAH'),
	(16, 'PC'),
	(17, 'UNIT'),
	(18, 'GRAM'),
	(19, 'ML'),
	(20, 'LEMBAR'),
	(21, 'METER'),
	(22, 'PASANG'),
	(23, 'AMVUL'),
	(24, 'TAB'),
	(25, 'TABLET'),
	(26, 'KAPSUL');
/*!40000 ALTER TABLE `t_satuan` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_supplier
CREATE TABLE IF NOT EXISTS `t_supplier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `namapbf` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `no_kontak` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_supplier: ~3 rows (approximately)
DELETE FROM `t_supplier`;
/*!40000 ALTER TABLE `t_supplier` DISABLE KEYS */;
INSERT INTO `t_supplier` (`id`, `namapbf`, `alamat`, `no_kontak`, `email`) VALUES
	(3, 'PT. Obat Asli', '', '', ''),
	(4, 'PT. orang Angkasa', '', '', ''),
	(5, '21231', '12313', '1231', '');
/*!40000 ALTER TABLE `t_supplier` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_user
CREATE TABLE IF NOT EXISTS `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `status` enum('A','O') DEFAULT NULL,
  `jk` enum('L','P') DEFAULT NULL,
  `no_hp` varchar(12) DEFAULT NULL,
  `alamat` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_user: ~1 rows (approximately)
DELETE FROM `t_user`;
/*!40000 ALTER TABLE `t_user` DISABLE KEYS */;
INSERT INTO `t_user` (`id`, `username`, `password`, `status`, `jk`, `no_hp`, `alamat`) VALUES
	(1, 'admin', '123', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `t_user` ENABLE KEYS */;

-- Dumping structure for view db_apotek_praya.v_obat
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat` (
	`id` BIGINT(20) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity_racik` MEDIUMINT(9) NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`expire_date` DATE NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`hrgper_racik` INT(11) NOT NULL,
	`hrga_jual` MEDIUMINT(9) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrgper_pack` INT(11) NOT NULL,
	`jumlah` VARBINARY(17) NOT NULL,
	`i_expire` INT(7) NULL,
	`i_jatuh_tempo` INT(7) NULL
) ENGINE=MyISAM;

-- Dumping structure for view db_apotek_praya.v_obat_jatuh_tempo
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat_jatuh_tempo` (
	`namapbf` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`norec` BIGINT(20) NOT NULL,
	`no_faktur` CHAR(20) NOT NULL COLLATE 'latin1_swedish_ci',
	`id_suppli` BIGINT(20) NOT NULL,
	`tgl_fakur` DATETIME NOT NULL,
	`batch` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`id_obat` BIGINT(20) NOT NULL,
	`expire_date` DATE NOT NULL,
	`jlh_obat_masuk` MEDIUMINT(9) NOT NULL,
	`total_harga` BIGINT(20) NOT NULL,
	`harga_satuan` INT(11) NOT NULL,
	`discount_harga` FLOAT(10,2) NOT NULL,
	`discount` FLOAT(10,2) NOT NULL,
	`harga_jual` FLOAT NOT NULL,
	`total_harga_beli` BIGINT(20) NOT NULL,
	`ppn` BIGINT(20) NOT NULL,
	`total_diskon` BIGINT(20) NOT NULL,
	`total_harga_iclude_ppn` BIGINT(20) NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`lunas` ENUM('0','1') NULL COLLATE 'latin1_swedish_ci',
	`tgl_bayar` DATE NULL,
	`id_user` INT(11) NOT NULL,
	`create_at` TIMESTAMP NOT NULL,
	`username` VARCHAR(200) NULL COLLATE 'latin1_swedish_ci',
	`i_jatuhtempo` INT(7) NULL
) ENGINE=MyISAM;

-- Dumping structure for view db_apotek_praya.v_obat_keluar
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat_keluar` (
	`id` BIGINT(20) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity_racik` MEDIUMINT(9) NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`expire_date` DATE NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`hrgper_racik` INT(11) NOT NULL,
	`hrga_jual` MEDIUMINT(9) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrgper_pack` INT(11) NOT NULL,
	`norec` BIGINT(20) NOT NULL,
	`no_nota` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`nama_pengunjung` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`id_pasien` BIGINT(20) NULL,
	`tgl_obat_keluar` DATETIME NOT NULL,
	`id_obat` BIGINT(20) NOT NULL,
	`jumlah_obat_keluar` SMALLINT(6) NOT NULL,
	`biaya_listrik` MEDIUMINT(9) NOT NULL,
	`biaya_admin` MEDIUMINT(9) NOT NULL,
	`nm_dokter` VARCHAR(150) NOT NULL COLLATE 'latin1_swedish_ci',
	`tot_harga` INT(11) NOT NULL,
	`uang` INT(11) NOT NULL,
	`kembalian` INT(11) NOT NULL,
	`insert_at` TIMESTAMP NOT NULL,
	`racikan` CHAR(1) NOT NULL COLLATE 'latin1_swedish_ci',
	`id_jenis_racikan` INT(11) NOT NULL,
	`id_user` INT(11) NOT NULL,
	`username` VARCHAR(200) NULL COLLATE 'latin1_swedish_ci',
	`satuanjual` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- Dumping structure for view db_apotek_praya.v_obat_masuk
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat_masuk` (
	`id` BIGINT(20) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity_racik` MEDIUMINT(9) NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`expire_date` DATE NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`hrgper_racik` INT(11) NOT NULL,
	`hrga_jual` MEDIUMINT(9) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrgper_pack` INT(11) NOT NULL,
	`namapbf` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`no_faktur` CHAR(20) NOT NULL COLLATE 'latin1_swedish_ci',
	`tgl_fakur` DATETIME NOT NULL,
	`tgl_jatuh_tempo_real` DATE NULL,
	`i_jatuhtempo` INT(7) NULL,
	`id_suppli` BIGINT(20) NOT NULL,
	`batch` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`jlh_obat_masuk` MEDIUMINT(9) NOT NULL,
	`total_harga` BIGINT(20) NOT NULL,
	`harga_satuan` INT(11) NOT NULL,
	`discount_harga` FLOAT(10,2) NOT NULL,
	`discount` FLOAT(10,2) NOT NULL,
	`harga_jual` FLOAT NOT NULL,
	`create_at` VARCHAR(21) NULL COLLATE 'utf8_general_ci',
	`total_harga_beli` BIGINT(20) NOT NULL,
	`total_diskon` BIGINT(20) NOT NULL,
	`total_harga_iclude_ppn` BIGINT(20) NOT NULL,
	`ppn` BIGINT(20) NOT NULL,
	`lunas` ENUM('0','1') NULL COLLATE 'latin1_swedish_ci',
	`tgl_bayar` DATE NULL,
	`username` VARCHAR(200) NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- Dumping structure for trigger db_apotek_praya.trg_barang_keluar
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_barang_keluar` AFTER INSERT ON `t_obat_keluar` FOR EACH ROW BEGIN
	declare racikan,jlh_stok_racik,sisa_stok_racik int;
	
	SELECT t_obat_keluar.racikan from t_obat_keluar where t_obat_keluar.norec = new.norec into racikan; 

-- jika bukan racikan

	if racikan ='0' then
	begin
			update t_obat set t_obat.quantity = quantity - NEW.jumlah_obat_keluar
			where t_obat.id = new.id_obat;
	end;		
		else
		-- jika satuan racikan
			begin
				SELECT t_obat.quantity_racik from t_obat where t_obat.id = new.id_obat into jlh_stok_racik;
				set sisa_stok_racik = jlh_stok_racik - new.jumlah_obat_keluar;
				
				if sisa_stok_racik <0 then
				begin
					update t_obat set t_obat.quantity = t_obat.quantity - 1, 
					t_obat.quantity_racik = (t_obat.jlhper_raciik + sisa_stok_racik) where t_obat.id = new.id_obat;
				end;	
					else
						begin
								update t_obat set t_obat.quantity_racik = quantity_racik- new.jumlah_obat_keluar 
								where t_obat.id = new.id_obat;
						end;	
				end if;
				
				
			end;	
			
		
	end if;	
			
			
			
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_apotek_praya.trg_obat_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_obat_insert` AFTER INSERT ON `t_obat` FOR EACH ROW BEGIN
	--	update t_obat set t_obat.quantity = quantity-1  where t_obat.id = new.id;
		
	 
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_apotek_praya.trg_obat_masuk
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_obat_masuk` AFTER INSERT ON `t_obat_masuk` FOR EACH ROW BEGIN
		UPDATE t_obat 
		SET t_obat.quantity = quantity + NEW.jlh_obat_masuk,
		t_obat.tgl_jatuh_tempo = new.tgl_jatuh_tempo,
		t_obat.expire_date = new.expire_date 
		WHERE
			t_obat.id = new.id_obat;	
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for view db_apotek_praya.v_obat
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat` AS SELECT 
	t_obat.*,
	CONCAT(t_obat.quantity,",",t_obat.quantity_racik) AS jumlah,
	DATEDIFF(t_obat.expire_date, CURRENT_DATE()) AS i_expire,
	DATEDIFF(t_obat.tgl_jatuh_tempo, CURRENT_DATE()) AS i_jatuh_tempo
	from t_obat ;

-- Dumping structure for view db_apotek_praya.v_obat_jatuh_tempo
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat_jatuh_tempo`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat_jatuh_tempo` AS -- v_obat_jatuh_tempo


SELECT 
 sup.namapbf,
ob.nm_obat,
ob.satuan,
ob.quantity,
ob.jenis,
 om.*,
 us.username,

 DATEDIFF(om.tgl_jatuh_tempo, CURRENT_DATE()) AS i_jatuhtempo 
 
 
 FROM t_obat_masuk AS om

INNER JOIN t_obat AS ob ON ob.id = om.id_obat
inner join t_user as us on us.id = om.id_user
INNER JOIN t_supplier AS sup ON sup.id = om.id_suppli ;

-- Dumping structure for view db_apotek_praya.v_obat_keluar
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat_keluar`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat_keluar` AS -- v_obat_keluar


SELECT 

t_obat.*,
ok.*,
t_user.username,

case when ok.racikan = 1 then t_obat.satuan_racik else t_obat.satuan end as satuanjual

 FROM t_obat_keluar as ok 
		JOIN t_obat ON t_obat.id = ok.id_obat
		JOIN t_user on t_user.id = ok.id_user
	 

/*Menampilkan hnya data obat yg 1 tahun*/
 where
	DATEDIFF(CURRENT_DATE(),ok.tgl_obat_keluar) <360 
	
 ORDER BY ok.insert_at desc ;

-- Dumping structure for view db_apotek_praya.v_obat_masuk
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat_masuk`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat_masuk` AS -- v_obat_masuk

SELECT 

t_obat.*,
t_supplier.namapbf,
t_obat_masuk.no_faktur,
t_obat_masuk.tgl_fakur,
t_obat_masuk.tgl_jatuh_tempo as tgl_jatuh_tempo_real,
DATEDIFF(t_obat_masuk.tgl_jatuh_tempo, CURRENT_DATE()) as i_jatuhtempo,
t_obat_masuk.id_suppli,
t_obat_masuk.batch,
t_obat_masuk.jlh_obat_masuk,
t_obat_masuk.total_harga,
t_obat_masuk.harga_satuan,
t_obat_masuk.discount_harga,
t_obat_masuk.discount,
t_obat_masuk.harga_jual,
date_format(t_obat_masuk.create_at,'%d/%m/%Y %H:%i' ) as create_at,
t_obat_masuk.total_harga_beli,
t_obat_masuk.total_diskon,
t_obat_masuk.total_harga_iclude_ppn,
t_obat_masuk.ppn,
t_obat_masuk.lunas,
t_obat_masuk.tgl_bayar,
t_user.username



 FROM t_obat_masuk 
 INNER JOIN t_obat ON t_obat.id = t_obat_masuk.id_obat
 inner join t_user on t_user.id = t_obat_masuk.id_user

inner join t_supplier on t_supplier.id = t_obat_masuk.id_suppli

/*Menampilkan hnya data obat yg 1 tahun*/
 where
	DATEDIFF(CURRENT_DATE(),t_obat_masuk.tgl_fakur) <360 
	
 ORDER BY t_obat_masuk.tgl_fakur desc ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
