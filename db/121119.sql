-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_apotek_praya
CREATE DATABASE IF NOT EXISTS `db_apotek_praya` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_apotek_praya`;

-- Dumping structure for procedure db_apotek_praya.getSatuan
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `getSatuan`(
	IN `kd` BIGINT

)
BEGIN
SELECT * from (

	SELECT DISTINCT t_obat.satuan,'1' as urut FROM `t_obat` WHERE id = kd
	UNION
	SELECT DISTINCT t_obat.satuan_pack,'2' FROM `t_obat` WHERE id = kd
	
	) as r order by urut;
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_cetak_nota
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cetak_nota`(
	IN `nomornota` VARCHAR(50)

,
	IN `adaracikan` TINYINT
,
	IN `biayaRacik` INT
)
BEGIN
	if adaracikan = 0 then 
	begin
	SELECT
				ok.no_nota,
				ok.nm_obat as nama_obat,
				ok.jumlah_obat_keluar,
				ok.satuan,
				ok.hrga_jual as harga_satuan,
				SUM( ok.hrga_jual * ok.jumlah_obat_keluar ) AS total_harga,
				ok.uang,
				ok.kembalian,
				ok.biaya_listrik,
				ok.biaya_admin,
				( ok.biaya_listrik + ok.biaya_admin ) AS biayaadmin,
				ok.tot_harga,
				biayaRacik  
			FROM
				v_obat_keluar AS ok 
			WHERE
				ok.no_nota = nomornota 
				AND ok.racikan = 0 
			GROUP BY
				ok.id_obat;
				
	end;
		else
			begin
			 SELECT
				ok.no_nota,
				'OBAT RACIKAN' AS nama_obat,
				SUM( ok.jumlah_obat_keluar ) AS jumlah_obat_keluar,
				"Item" as satuan,
				0 AS harga_satuan,
				SUM( round( ok.hrga_jual / ok.jlhper_raciik ) * ok.jumlah_obat_keluar ) AS total_harga,
				ok.uang,
				ok.kembalian,
				ok.biaya_listrik,
				ok.biaya_admin,
				( ok.biaya_listrik + ok.biaya_admin ) AS biayaadmin,
				ok.tot_harga ,
				biayaRacik 
			FROM
				v_obat_keluar AS ok 
			WHERE
				ok.no_nota = nomornota 
				AND ok.racikan = 1 
			UNION
			SELECT
				ok.no_nota,
				ok.nm_obat,
				ok.jumlah_obat_keluar,
				ok.satuan,
				ok.hrga_jual,
				SUM( ok.hrga_jual * ok.jumlah_obat_keluar ) AS total_harga,
				ok.uang,
				ok.kembalian,
				ok.biaya_listrik,
				ok.biaya_admin,
				( ok.biaya_listrik + ok.biaya_admin ) AS biayaadmin,
				ok.tot_harga ,
				biayaRacik 
			FROM
				v_obat_keluar AS ok 
			WHERE
				ok.no_nota = nomornota 
				AND ok.racikan = 0 
			GROUP BY
				ok.id_obat;

			end;	
	 end if;
	END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat`()
BEGIN
	SELECT t_obat.*, CONCAT(t_obat.quantity,",",t_obat.quantity_racik) AS jumlah FROM t_obat;
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat_expire
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat_expire`()
BEGIN
		SELECT count(id) as jlh from t_obat 
			where DATEDIFF(t_obat.expire_date, CURRENT_DATE()) <= 180;
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat_jatuhtempo
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat_jatuhtempo`()
BEGIN
		SELECT count(id_obat) as jlh from t_obat_masuk 
		where DATEDIFF(t_obat_masuk.tgl_jatuh_tempo, CURRENT_DATE()) <= 182 and  (lunas ="0" or lunas is null) ;
END//
DELIMITER ;

-- Dumping structure for table db_apotek_praya.t_biaya_lain
CREATE TABLE IF NOT EXISTS `t_biaya_lain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_biaya` varchar(100) DEFAULT NULL,
  `biaya` mediumint(9) DEFAULT NULL,
  `statusenabled` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_biaya_lain: ~2 rows (approximately)
/*!40000 ALTER TABLE `t_biaya_lain` DISABLE KEYS */;
INSERT IGNORE INTO `t_biaya_lain` (`id`, `nm_biaya`, `biaya`, `statusenabled`) VALUES
	(1, 'Biaya Listrik', 2000, NULL),
	(2, 'Biaya Servis', 3000, NULL);
/*!40000 ALTER TABLE `t_biaya_lain` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_closing
CREATE TABLE IF NOT EXISTS `t_closing` (
  `id` int(11) NOT NULL,
  `id_obat` bigint(20) NOT NULL DEFAULT 0,
  `quantity` int(11) NOT NULL,
  `quantity_racik` int(11) NOT NULL,
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `jlhper_raciik` int(11) NOT NULL,
  `tahun` year(4) NOT NULL,
  `priode` int(11) NOT NULL,
  KEY `FK_t_closing_t_user` (`id_user`),
  CONSTRAINT `FK_t_closing_t_user` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_closing: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_closing` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_closing` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_dokter
CREATE TABLE IF NOT EXISTS `t_dokter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nm_dokter` varchar(200) DEFAULT NULL,
  `no_hp` char(12) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `jk` enum('L','P') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_dokter: ~8 rows (approximately)
/*!40000 ALTER TABLE `t_dokter` DISABLE KEYS */;
INSERT IGNORE INTO `t_dokter` (`id`, `nm_dokter`, `no_hp`, `alamat`, `jk`) VALUES
	(24, '', NULL, NULL, NULL),
	(25, '', NULL, NULL, NULL),
	(26, '', NULL, NULL, NULL),
	(27, '', NULL, NULL, NULL),
	(28, '', NULL, NULL, NULL),
	(29, '', NULL, NULL, NULL),
	(30, '', NULL, NULL, NULL),
	(31, '', NULL, NULL, NULL),
	(32, '1', NULL, NULL, NULL);
/*!40000 ALTER TABLE `t_dokter` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_jenis
CREATE TABLE IF NOT EXISTS `t_jenis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_jenis: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_jenis` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_jenis` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_jenis_racikan
CREATE TABLE IF NOT EXISTS `t_jenis_racikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diskripsi` varchar(50) DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_jenis_racikan: ~4 rows (approximately)
/*!40000 ALTER TABLE `t_jenis_racikan` DISABLE KEYS */;
INSERT IGNORE INTO `t_jenis_racikan` (`id`, `diskripsi`, `biaya`) VALUES
	(78, 'Pot', 500),
	(79, 'Puyer', 600),
	(80, 'Kapsul', 400),
	(81, 'Syrup', NULL);
/*!40000 ALTER TABLE `t_jenis_racikan` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_obat
CREATE TABLE IF NOT EXISTS `t_obat` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nm_obat` varchar(100) DEFAULT NULL,
  `quantity` mediumint(9) NOT NULL,
  `satuan` char(15) NOT NULL,
  `quantity_racik` mediumint(9) NOT NULL,
  `jlhper_raciik` mediumint(9) NOT NULL,
  `satuan_racik` varchar(50) NOT NULL,
  `expire_date` date NOT NULL,
  `tgl_jatuh_tempo` date DEFAULT NULL,
  `hrgper_racik` int(11) NOT NULL,
  `hrga_jual` mediumint(9) NOT NULL,
  `jenis` enum('Cream','Obat') NOT NULL,
  `jlhper_pack` mediumint(9) NOT NULL,
  `satuan_pack` varchar(100) NOT NULL,
  `hrgper_pack` int(11) NOT NULL,
  `ket` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000056 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_obat: ~7 rows (approximately)
/*!40000 ALTER TABLE `t_obat` DISABLE KEYS */;
INSERT IGNORE INTO `t_obat` (`id`, `nm_obat`, `quantity`, `satuan`, `quantity_racik`, `jlhper_raciik`, `satuan_racik`, `expire_date`, `tgl_jatuh_tempo`, `hrgper_racik`, `hrga_jual`, `jenis`, `jlhper_pack`, `satuan_pack`, `hrgper_pack`, `ket`) VALUES
	(1000006, 'SDFAFE', 20, 'BTL', 0, 0, 'Btl', '2019-11-09', '2019-11-20', 0, 3333, 'Obat', 10, '', 0, NULL),
	(1000013, 'DSFSAF', 50, 'BTL', 0, 0, 'Btl', '2019-11-14', '2019-11-20', 0, 10000, 'Obat', 10, 'Box', 0, NULL),
	(1000020, 'PRAMIDARO', 40, 'SASET', 0, 1, 'Saset', '2019-11-19', '2019-11-19', 0, 12000, 'Obat', 10, 'Box', 0, NULL),
	(1000027, 'KOMIF 1023', 9, 'SASET', 0, 1, 'Saset', '2020-11-25', '2019-11-12', 0, 2500, 'Obat', 1, 'Saset', 0, NULL),
	(1000034, 'AMOXICILIN 500 MG', 50, 'TUBE', 0, 10, 'ML', '1899-12-30', '2021-11-26', 0, 7000, 'Obat', 5, 'Box', 0, NULL),
	(1000043, 'ANTASIDA', 3, 'BTL', 0, 1, 'Btl', '1899-12-30', '2021-11-26', 0, 20000, 'Obat', 1, 'Btl', 0, NULL),
	(1000047, 'AMBROXOL', 30, 'TUBE', 0, 1, 'Tube', '2019-11-21', '2021-11-26', 0, 2500, 'Obat', 10, 'Box', 0, NULL),
	(1000054, 'BETADINE', 8, 'SASET', 9, 10, 'Gram', '2019-11-12', '2020-11-20', 0, 5000, 'Obat', 10, 'Box', 0, NULL);
/*!40000 ALTER TABLE `t_obat` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_obat_keluar
CREATE TABLE IF NOT EXISTS `t_obat_keluar` (
  `norec` bigint(20) NOT NULL AUTO_INCREMENT,
  `no_nota` varchar(50) DEFAULT '0',
  `nama_pengunjung` varchar(100) DEFAULT NULL,
  `id_pasien` bigint(20) DEFAULT NULL,
  `tgl_obat_keluar` datetime NOT NULL,
  `id_obat` bigint(20) NOT NULL,
  `jumlah_obat_keluar` smallint(6) NOT NULL,
  `biaya_listrik` mediumint(9) NOT NULL DEFAULT 0,
  `biaya_admin` mediumint(9) NOT NULL DEFAULT 0,
  `nm_dokter` varchar(150) NOT NULL,
  `tot_harga` int(11) NOT NULL,
  `uang` int(11) NOT NULL DEFAULT 0,
  `kembalian` int(11) NOT NULL DEFAULT 0,
  `insert_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `racikan` char(1) NOT NULL,
  `biaya_dokter` int(11) NOT NULL,
  `id_jenis_racikan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`norec`),
  KEY `FK_t_obat_keluar_t_obat` (`id_obat`),
  KEY `FK_t_obat_keluar_t_user` (`id_user`),
  KEY `FK_t_obat_keluar_t_jenis_racikan` (`id_jenis_racikan`),
  CONSTRAINT `FK_t_obat_keluar_t_jenis_racikan` FOREIGN KEY (`id_jenis_racikan`) REFERENCES `t_jenis_racikan` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_keluar_t_obat` FOREIGN KEY (`id_obat`) REFERENCES `t_obat` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_keluar_t_user` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1002447 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_obat_keluar: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_obat_keluar` DISABLE KEYS */;
INSERT IGNORE INTO `t_obat_keluar` (`norec`, `no_nota`, `nama_pengunjung`, `id_pasien`, `tgl_obat_keluar`, `id_obat`, `jumlah_obat_keluar`, `biaya_listrik`, `biaya_admin`, `nm_dokter`, `tot_harga`, `uang`, `kembalian`, `insert_at`, `racikan`, `biaya_dokter`, `id_jenis_racikan`, `id_user`) VALUES
	(1002444, 'N121119437195', '', NULL, '2019-11-12 00:00:00', 1000054, 10, 2000, 3000, '', 10500, 100000, 4500, '2019-11-12 01:11:51', '1', 15000, 78, 1),
	(1002445, 'N121119285589', '', NULL, '2019-11-12 00:00:00', 1000054, 1, 2000, 3000, '1', 8900, 13000, 1100, '2019-11-12 11:58:45', '1', 10000, 80, 1),
	(1002446, 'N121119285589', '', NULL, '2019-11-12 00:00:00', 1000027, 1, 2000, 3000, '1', 8900, 13000, 1100, '2019-11-12 11:58:47', '1', 10000, 78, 1);
/*!40000 ALTER TABLE `t_obat_keluar` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_obat_masuk
CREATE TABLE IF NOT EXISTS `t_obat_masuk` (
  `norec` bigint(20) NOT NULL AUTO_INCREMENT,
  `no_faktur` char(20) NOT NULL,
  `id_suppli` bigint(20) NOT NULL,
  `tgl_fakur` datetime NOT NULL,
  `batch` varchar(50) DEFAULT NULL,
  `id_obat` bigint(20) NOT NULL,
  `expire_date` date NOT NULL,
  `jlh_obat_masuk` mediumint(9) NOT NULL,
  `total_harga` bigint(20) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `discount_harga` float(10,2) NOT NULL,
  `discount` float(10,2) NOT NULL,
  `harga_jual` float NOT NULL,
  `total_harga_beli` bigint(20) NOT NULL,
  `ppn` bigint(20) NOT NULL,
  `total_diskon` bigint(20) NOT NULL,
  `total_harga_iclude_ppn` bigint(20) NOT NULL,
  `tgl_jatuh_tempo` date DEFAULT NULL,
  `lunas` enum('0','1') DEFAULT NULL,
  `tgl_bayar` date DEFAULT NULL,
  `id_user` int(11) NOT NULL DEFAULT 0,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`norec`),
  KEY `FK_t_obat_masuk_t_suppli` (`id_suppli`),
  KEY `FK_t_obat_masuk_t_obat` (`id_obat`),
  KEY `FK_t_obat_masuk_t_user` (`id_user`),
  CONSTRAINT `FK_t_obat_masuk_t_obat` FOREIGN KEY (`id_obat`) REFERENCES `t_obat` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_masuk_t_suppli` FOREIGN KEY (`id_suppli`) REFERENCES `t_supplier` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_masuk_t_user` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_obat_masuk: ~7 rows (approximately)
/*!40000 ALTER TABLE `t_obat_masuk` DISABLE KEYS */;
INSERT IGNORE INTO `t_obat_masuk` (`norec`, `no_faktur`, `id_suppli`, `tgl_fakur`, `batch`, `id_obat`, `expire_date`, `jlh_obat_masuk`, `total_harga`, `harga_satuan`, `discount_harga`, `discount`, `harga_jual`, `total_harga_beli`, `ppn`, `total_diskon`, `total_harga_iclude_ppn`, `tgl_jatuh_tempo`, `lunas`, `tgl_bayar`, `id_user`, `create_at`) VALUES
	(166, 'ASDFSADF', 10, '2019-11-20 00:00:00', 'DSGSDFS', 1000013, '2019-11-14', 50, 450000, 9000, 0.00, 0.00, 0, 450000, 45000, 0, 495000, '2019-11-20', NULL, NULL, 1, '2019-11-09 19:43:07'),
	(167, 'SDAFASDF', 10, '2019-11-15 00:00:00', '234234', 1000006, '2019-11-09', 20, 45000, 2250, 0.00, 0.00, 0, 45000, 4500, 0, 49500, '2019-11-20', NULL, NULL, 1, '2019-11-09 19:44:45'),
	(168, 'DDDDD', 8, '2019-11-21 00:00:00', 'ASDSAD', 1000020, '2019-11-19', 40, 40000, 1000, 0.00, 0.00, 0, 40000, 4000, 0, 44000, '2019-11-19', NULL, NULL, 1, '2019-11-11 22:15:42'),
	(169, '1028324', 9, '2019-11-12 00:00:00', 'SDFSDFS', 1000027, '2020-11-25', 10, 20000, 2000, 0.00, 0.00, 0, 20000, 2000, 0, 22000, '2019-11-12', NULL, NULL, 1, '2019-11-11 23:48:45'),
	(170, '16304', 13, '2019-11-11 00:00:00', 'FGFHFGH', 1000047, '2019-11-21', 30, 30000, 1000, 0.00, 0.00, 0, 350000, 0, 0, 350000, '2021-11-26', NULL, NULL, 1, '2019-11-12 00:02:21'),
	(171, '16304', 13, '2019-11-11 00:00:00', 'DWRWERW', 1000043, '1899-12-30', 3, 50000, 16667, 0.00, 0.00, 0, 350000, 0, 0, 350000, '2021-11-26', NULL, NULL, 1, '2019-11-12 00:02:21'),
	(172, '16304', 13, '2019-11-11 00:00:00', 'DSFSDF', 1000034, '1899-12-30', 50, 270000, 6000, 30000.00, 10.00, 0, 350000, 0, 0, 350000, '2021-11-26', NULL, NULL, 1, '2019-11-12 00:02:21'),
	(175, '113343534543', 10, '2019-11-22 00:00:00', 'WQWEQWEQWE', 1000054, '2019-11-12', 10, 30000, 3000, 0.00, 0.00, 0, 30000, 3000, 0, 33000, '2020-11-20', NULL, NULL, 1, '2019-11-12 00:56:38');
/*!40000 ALTER TABLE `t_obat_masuk` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_pabrik
CREATE TABLE IF NOT EXISTS `t_pabrik` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nm_pabrik` varchar(200) DEFAULT NULL,
  `no_contact` varchar(50) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_pabrik: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_pabrik` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pabrik` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_pasien
CREATE TABLE IF NOT EXISTS `t_pasien` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nam_pasien` varchar(100) NOT NULL,
  `alamat` varchar(300) DEFAULT NULL,
  `no_hp` char(12) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `no_rekam_medis` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `no_rekam_medis` (`no_rekam_medis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_pasien: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_pasien` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pasien` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_pot
CREATE TABLE IF NOT EXISTS `t_pot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namapot` varchar(50) DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_pot: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_pot` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pot` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_puyer
CREATE TABLE IF NOT EXISTS `t_puyer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namacetakan` varchar(50) DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_puyer: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_puyer` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_puyer` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_satuan
CREATE TABLE IF NOT EXISTS `t_satuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `satuan` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_satuan: ~6 rows (approximately)
/*!40000 ALTER TABLE `t_satuan` DISABLE KEYS */;
INSERT IGNORE INTO `t_satuan` (`id`, `satuan`) VALUES
	(28, 'Gram'),
	(29, 'Btl'),
	(30, 'Box'),
	(31, 'ML'),
	(32, 'Tube'),
	(33, 'Saset');
/*!40000 ALTER TABLE `t_satuan` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_supplier
CREATE TABLE IF NOT EXISTS `t_supplier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `namapbf` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `no_kontak` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_supplier: ~14 rows (approximately)
/*!40000 ALTER TABLE `t_supplier` DISABLE KEYS */;
INSERT IGNORE INTO `t_supplier` (`id`, `namapbf`, `alamat`, `no_kontak`, `email`) VALUES
	(7, 'Kimia Farma\r\nMAS\r\nMAS\r\nPenta Valen\r\nSST/United dico citus \r\nBSP\r\nBSP\r\nBSP\r\nMAS\r\nMAS\r\nMAS\r\nMAS\r\nKimia Farma\r\nKimia Farma\r\nKimia Farma\r\nBSP\r\nMAS\r\nBSP\r\nBSP\r\nBSP\r\nKimia Farma\r\nKimia Farma\r\nMAS\r\nMAS\r\nBSP\r\nMAS\r\nKimia Farma\r\nMAS\r\nMAS\r\nEnseval\r\nBSP\r\nBSP\r\nBSP\r\nMAS', '', '', ''),
	(8, 'Kimia Farma', '', '', ''),
	(9, 'MAS', '', '', ''),
	(10, 'Penta Valen', '', '', ''),
	(11, 'SST/United dico citus ', '', '', ''),
	(12, 'BSP', '', '', ''),
	(13, 'Enseval', '', '', ''),
	(14, 'Mandiri', '', '', ''),
	(15, 'SST', '', '', ''),
	(16, 'MAS/mandiri', '', '', ''),
	(17, 'United Dico citus ', '', '', ''),
	(18, 'SST,BSP', '', '', ''),
	(19, 'Enseval,MAS', '', '', ''),
	(20, 'Yarindo', '', '', '');
/*!40000 ALTER TABLE `t_supplier` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_user
CREATE TABLE IF NOT EXISTS `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `status` enum('A','O') DEFAULT NULL,
  `jk` enum('L','P') DEFAULT NULL,
  `no_hp` varchar(12) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_user: ~1 rows (approximately)
/*!40000 ALTER TABLE `t_user` DISABLE KEYS */;
INSERT IGNORE INTO `t_user` (`id`, `username`, `password`, `status`, `jk`, `no_hp`, `alamat`) VALUES
	(1, 'admin', '123', 'A', NULL, NULL, NULL);
/*!40000 ALTER TABLE `t_user` ENABLE KEYS */;

-- Dumping structure for view db_apotek_praya.v_obat
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat` (
	`id` BIGINT(20) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity_racik` MEDIUMINT(9) NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`expire_date` DATE NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`hrgper_racik` INT(11) NOT NULL,
	`hrga_jual` MEDIUMINT(9) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrgper_pack` INT(11) NOT NULL,
	`ket` TEXT NULL COLLATE 'latin1_swedish_ci',
	`jumlah` VARCHAR(19) NOT NULL COLLATE 'utf8mb4_general_ci',
	`i_expire` INT(7) NULL,
	`i_jatuh_tempo` INT(7) NULL
) ENGINE=MyISAM;

-- Dumping structure for view db_apotek_praya.v_obat_jatuh_tempo
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat_jatuh_tempo` (
	`namapbf` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`norec` BIGINT(20) NOT NULL,
	`no_faktur` CHAR(20) NOT NULL COLLATE 'latin1_swedish_ci',
	`id_suppli` BIGINT(20) NOT NULL,
	`tgl_fakur` DATETIME NOT NULL,
	`batch` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`id_obat` BIGINT(20) NOT NULL,
	`expire_date` DATE NOT NULL,
	`jlh_obat_masuk` MEDIUMINT(9) NOT NULL,
	`total_harga` BIGINT(20) NOT NULL,
	`harga_satuan` INT(11) NOT NULL,
	`discount_harga` FLOAT(10,2) NOT NULL,
	`discount` FLOAT(10,2) NOT NULL,
	`harga_jual` FLOAT NOT NULL,
	`total_harga_beli` BIGINT(20) NOT NULL,
	`ppn` BIGINT(20) NOT NULL,
	`total_diskon` BIGINT(20) NOT NULL,
	`total_harga_iclude_ppn` BIGINT(20) NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`lunas` ENUM('0','1') NULL COLLATE 'latin1_swedish_ci',
	`tgl_bayar` DATE NULL,
	`id_user` INT(11) NOT NULL,
	`create_at` TIMESTAMP NOT NULL,
	`username` VARCHAR(200) NULL COLLATE 'latin1_swedish_ci',
	`i_jatuhtempo` INT(7) NULL
) ENGINE=MyISAM;

-- Dumping structure for view db_apotek_praya.v_obat_keluar
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat_keluar` (
	`id` BIGINT(20) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity_racik` MEDIUMINT(9) NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`expire_date` DATE NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`hrgper_racik` INT(11) NOT NULL,
	`hrga_jual` MEDIUMINT(9) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrgper_pack` INT(11) NOT NULL,
	`ket` TEXT NULL COLLATE 'latin1_swedish_ci',
	`norec` BIGINT(20) NOT NULL,
	`no_nota` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`nama_pengunjung` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`id_pasien` BIGINT(20) NULL,
	`tgl_obat_keluar` DATETIME NOT NULL,
	`id_obat` BIGINT(20) NOT NULL,
	`jumlah_obat_keluar` SMALLINT(6) NOT NULL,
	`biaya_listrik` MEDIUMINT(9) NOT NULL,
	`biaya_admin` MEDIUMINT(9) NOT NULL,
	`nm_dokter` VARCHAR(150) NOT NULL COLLATE 'latin1_swedish_ci',
	`tot_harga` INT(11) NOT NULL,
	`uang` INT(11) NOT NULL,
	`kembalian` INT(11) NOT NULL,
	`insert_at` TIMESTAMP NOT NULL,
	`racikan` CHAR(1) NOT NULL COLLATE 'latin1_swedish_ci',
	`id_jenis_racikan` INT(11) NOT NULL,
	`id_user` INT(11) NOT NULL,
	`id_jenis_racikan1` INT(11) NOT NULL,
	`diskripsi` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`biaya` INT(11) NULL,
	`username` VARCHAR(200) NULL COLLATE 'latin1_swedish_ci',
	`satuanjual` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- Dumping structure for view db_apotek_praya.v_obat_masuk
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat_masuk` (
	`id` BIGINT(20) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity_racik` MEDIUMINT(9) NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`expire_date` DATE NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`hrgper_racik` INT(11) NOT NULL,
	`hrga_jual` MEDIUMINT(9) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrgper_pack` INT(11) NOT NULL,
	`ket` TEXT NULL COLLATE 'latin1_swedish_ci',
	`namapbf` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`no_faktur` CHAR(20) NOT NULL COLLATE 'latin1_swedish_ci',
	`tgl_fakur` DATETIME NOT NULL,
	`tgl_jatuh_tempo_real` DATE NULL,
	`i_jatuhtempo` INT(7) NULL,
	`id_suppli` BIGINT(20) NOT NULL,
	`batch` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`jlh_obat_masuk` MEDIUMINT(9) NOT NULL,
	`total_harga` BIGINT(20) NOT NULL,
	`harga_satuan` INT(11) NOT NULL,
	`discount_harga` FLOAT(10,2) NOT NULL,
	`discount` FLOAT(10,2) NOT NULL,
	`harga_jual` FLOAT NOT NULL,
	`create_at` VARCHAR(21) NULL COLLATE 'utf8mb4_general_ci',
	`total_harga_beli` BIGINT(20) NOT NULL,
	`total_diskon` BIGINT(20) NOT NULL,
	`total_harga_iclude_ppn` BIGINT(20) NOT NULL,
	`ppn` BIGINT(20) NOT NULL,
	`lunas` ENUM('0','1') NULL COLLATE 'latin1_swedish_ci',
	`tgl_bayar` DATE NULL,
	`username` VARCHAR(200) NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- Dumping structure for trigger db_apotek_praya.trg_barang_keluar
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_barang_keluar` AFTER INSERT ON `t_obat_keluar` FOR EACH ROW BEGIN
	declare racikan,jlh_stok_racik,sisa_stok_racik int;
	
	SELECT t_obat_keluar.racikan from t_obat_keluar where t_obat_keluar.norec = new.norec into racikan; 

-- jika bukan racikan

	if racikan ='0' then
	begin
			update t_obat set t_obat.quantity = quantity - NEW.jumlah_obat_keluar
			where t_obat.id = new.id_obat;
	end;		
		else
		-- jika satuan racikan
			begin
				SELECT t_obat.quantity_racik from t_obat where t_obat.id = new.id_obat into jlh_stok_racik;
				set sisa_stok_racik = jlh_stok_racik - new.jumlah_obat_keluar;
				
				if sisa_stok_racik <0 then
				begin
					update t_obat set t_obat.quantity = t_obat.quantity - 1, 
					t_obat.quantity_racik = (t_obat.jlhper_raciik + sisa_stok_racik) where t_obat.id = new.id_obat;
				end;	
					else
						begin
								update t_obat set t_obat.quantity_racik = quantity_racik- new.jumlah_obat_keluar 
								where t_obat.id = new.id_obat;
						end;	
				end if;
				
				
			end;	
			
		
	end if;	
			
			
			
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_apotek_praya.trg_obat_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_obat_insert` AFTER INSERT ON `t_obat` FOR EACH ROW BEGIN
	--	update t_obat set t_obat.quantity = quantity-1  where t_obat.id = new.id;
		
	 
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_apotek_praya.trg_obat_masuk
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_obat_masuk` AFTER INSERT ON `t_obat_masuk` FOR EACH ROW BEGIN
		UPDATE t_obat 
		SET t_obat.quantity = quantity + NEW.jlh_obat_masuk,
		t_obat.tgl_jatuh_tempo = new.tgl_jatuh_tempo,
		t_obat.expire_date = new.expire_date 
		WHERE
			t_obat.id = new.id_obat;	
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for view db_apotek_praya.v_obat
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat` AS SELECT 
	t_obat.*,
	CONCAT(t_obat.quantity,",",t_obat.quantity_racik) AS jumlah,
	DATEDIFF(t_obat.expire_date, CURRENT_DATE()) AS i_expire,
	DATEDIFF(t_obat.tgl_jatuh_tempo, CURRENT_DATE()) AS i_jatuh_tempo
	from t_obat ;

-- Dumping structure for view db_apotek_praya.v_obat_jatuh_tempo
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat_jatuh_tempo`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat_jatuh_tempo` AS -- v_obat_jatuh_tempo


SELECT 
 sup.namapbf,
ob.nm_obat,
ob.satuan,
ob.quantity,
ob.jenis,
 om.*,
 us.username,

 DATEDIFF(om.tgl_jatuh_tempo, CURRENT_DATE()) AS i_jatuhtempo 
 
 
 FROM t_obat_masuk AS om

INNER JOIN t_obat AS ob ON ob.id = om.id_obat
inner join t_user as us on us.id = om.id_user
INNER JOIN t_supplier AS sup ON sup.id = om.id_suppli ;

-- Dumping structure for view db_apotek_praya.v_obat_keluar
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat_keluar`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat_keluar` AS -- v_obat_keluar


SELECT 

t_obat.*,
ok.*,
jn.id as id_jenis_racikan1,
jn.diskripsi,
jn.biaya,
t_user.username,

case when ok.racikan = 1 then t_obat.satuan_racik else t_obat.satuan end as satuanjual

 FROM t_obat_keluar as ok 
		JOIN t_obat ON t_obat.id = ok.id_obat
		JOIN t_user on t_user.id = ok.id_user
		join t_jenis_racikan as jn on jn.id = ok.id_jenis_racikan
	 

/*Menampilkan hnya data obat yg 1 tahun*/
 where
	DATEDIFF(CURRENT_DATE(),ok.tgl_obat_keluar) <360 
	
 ORDER BY ok.insert_at desc ;

-- Dumping structure for view db_apotek_praya.v_obat_masuk
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat_masuk`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat_masuk` AS -- v_obat_masuk

SELECT 

t_obat.*,
t_supplier.namapbf,
t_obat_masuk.no_faktur,
t_obat_masuk.tgl_fakur,
t_obat_masuk.tgl_jatuh_tempo as tgl_jatuh_tempo_real,
DATEDIFF(t_obat_masuk.tgl_jatuh_tempo, CURRENT_DATE()) as i_jatuhtempo,
t_obat_masuk.id_suppli,
t_obat_masuk.batch,
t_obat_masuk.jlh_obat_masuk,
t_obat_masuk.total_harga,
t_obat_masuk.harga_satuan,
t_obat_masuk.discount_harga,
t_obat_masuk.discount,
t_obat_masuk.harga_jual,
date_format(t_obat_masuk.create_at,'%d/%m/%Y %H:%i' ) as create_at,
t_obat_masuk.total_harga_beli,
t_obat_masuk.total_diskon,
t_obat_masuk.total_harga_iclude_ppn,
t_obat_masuk.ppn,
t_obat_masuk.lunas,
t_obat_masuk.tgl_bayar,
t_user.username



 FROM t_obat_masuk 
 INNER JOIN t_obat ON t_obat.id = t_obat_masuk.id_obat
 inner join t_user on t_user.id = t_obat_masuk.id_user

inner join t_supplier on t_supplier.id = t_obat_masuk.id_suppli

/*Menampilkan hnya data obat yg 1 tahun*/
 where
	DATEDIFF(CURRENT_DATE(),t_obat_masuk.tgl_fakur) <360 
	
 ORDER BY t_obat_masuk.tgl_fakur desc ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
