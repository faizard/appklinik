-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_apotek_praya
CREATE DATABASE IF NOT EXISTS `db_apotek_praya` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_apotek_praya`;

-- Dumping structure for function db_apotek_praya.getBiayaBersih
DELIMITER //
CREATE DEFINER=`` FUNCTION `getBiayaBersih`(
	`noNota` VARCHAR(50)



) RETURNS bigint(20)
BEGIN
DECLARE a BIGINT;

 	SELECT  sum(ok.hrgajualbersih) AS biaya FROM t_obat_keluar AS ok WHERE ok.no_nota = noNota ORDER BY biaya into a;
 	
 	return round(a,-2);
END//
DELIMITER ;

-- Dumping structure for function db_apotek_praya.getBiayaListrik
DELIMITER //
CREATE DEFINER=`` FUNCTION `getBiayaListrik`(
	`nonota` INT

) RETURNS int(11)
BEGIN
DECLARE a int;

 	SELECT ok.biaya_listrik FROM t_obat_keluar AS ok WHERE ok.no_nota = nonota ORDER BY ok.biaya_listrik DESC LIMIT 1 into a;
 	
 	return a;
END//
DELIMITER ;

-- Dumping structure for function db_apotek_praya.getBiayaServis
DELIMITER //
CREATE DEFINER=`` FUNCTION `getBiayaServis`(
	`nota` VARCHAR(50)
,
	`biayaSatuan` INT


) RETURNS int(11)
BEGIN
	DECLARE biaya int;
	
	SELECT  count(ok.no_nota) * biayaSatuan as biaya_servis 
	from t_obat_keluar as ok where ok.no_nota = nota   AND ok.racikan = 1
	into biaya;
	
	if biaya > 5000 then 
		return 5000 ;
		else
			return biaya;
	end if;		
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.getSatuan
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `getSatuan`(
	IN `kd` BIGINT

)
BEGIN
SELECT * from (

	SELECT DISTINCT t_obat.satuan,'1' as urut FROM `t_obat` WHERE id = kd
	UNION
	SELECT DISTINCT t_obat.satuan_pack,'2' FROM `t_obat` WHERE id = kd
	
	) as r order by urut;
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_cetak_nota
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cetak_nota`(
	IN `nomornota` VARCHAR(50)

,
	IN `adaracikan` TINYINT
,
	IN `biayaRacik` INT









)
BEGIN
	if adaracikan = 0 then 
	begin
	SELECT
				ok.no_nota,
				ok.nm_obat as nama_obat,
				ok.jumlah_obat_keluar,
				ok.satuan,
				ok.hrga_jual as harga_satuan,
				ok.biaya as biayaPerRacikan,
				SUM( ok.hrga_jual * ok.jumlah_obat_keluar ) AS total_harga,
				ok.uang,
				( ok.uang - ok.tot_harga ) as  kembalian,
				ok.biaya_listrik,
			--	ok.biaya_admin,
				'0' AS biayaadmin, -- biaya listrik
				ok.tot_harga,
				ok.biaya_racikan as  biayaRacik,
				getBiayaServis(ok.no_nota, 1000) as biayaServis
			FROM
				v_obat_keluar AS ok 
			WHERE
				ok.no_nota = nomornota 
				AND ok.racikan = 0 
			GROUP BY
				ok.id_obat;
				
	end;
		else
			begin
			 SELECT
				ok.no_nota,
				ok.diskripsi as nama_obat,
				SUM( ok.jumlah_obat_keluar ) AS jumlah_obat_keluar,
				"Item" as satuan,
				0 AS harga_satuan,
				ok.biaya as biayaPerRacikan,
				SUM( round( ok.hrga_jual / ok.jlhper_raciik ) * ok.jumlah_obat_keluar ) AS total_harga,
				ok.uang,
				( ok.uang - ok.tot_harga ) as  kembalian,
				ok.biaya_listrik,
			--	ok.biaya_admin, 
				getBiayaListrik(ok.no_nota) AS biayaadmin, -- biaya listrik
				ok.tot_harga ,
				ok.biaya_racikan as  biayaRacik,
				getBiayaServis(ok.no_nota, 1000) as biayaServis
			FROM
				v_obat_keluar AS ok 
			WHERE
				ok.no_nota = nomornota 
				AND ok.racikan = 1 
			group by ok.diskripsi	
			UNION
			SELECT
				ok.no_nota,
				ok.nm_obat,
				ok.jumlah_obat_keluar,
				ok.satuan,
				ok.biaya as biayaPerRacikan,
				ok.hrga_jual,
				SUM( ok.hrga_jual * ok.jumlah_obat_keluar ) AS total_harga,
				ok.uang,
				( ok.uang - ok.tot_harga ) as  kembalian,
				ok.biaya_listrik,
			--	ok.biaya_admin,
				getBiayaListrik(ok.no_nota) AS biayaadmin, -- biaya listrik
				ok.tot_harga ,
				ok.biaya_racikan as  biayaRacik,
				getBiayaServis(ok.no_nota, 1000) as biayaServis
			FROM
				v_obat_keluar AS ok 
			WHERE
				ok.no_nota = nomornota 
				AND ok.racikan = 0 
			GROUP BY
				ok.id_obat;

			end;	
	 end if;
	END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat`()
BEGIN
	SELECT t_obat.*, CONCAT(t_obat.quantity,",",t_obat.quantity_racik) AS jumlah FROM t_obat;
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat_expire
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat_expire`()
BEGIN
		SELECT count(id) as jlh from t_obat 
			where DATEDIFF(t_obat.expire_date, CURRENT_DATE()) <= 180;
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat_jatuhtempo
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_obat_jatuhtempo`()
BEGIN
		SELECT count(id_obat) as jlh from t_obat_masuk 
		where DATEDIFF(t_obat_masuk.tgl_jatuh_tempo, CURRENT_DATE()) <= 182 and  (lunas ="0" or lunas is null) ;
END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat_keluar
DELIMITER //
CREATE DEFINER=`` PROCEDURE `sp_obat_keluar`(
	IN `tglAwal` DATE,
	IN `tglAkhir` DATE






)
BEGIN
-- v_obat_keluar


		SELECT 
		
		t_obat.*,
		case when t_obat.jenis='obat' then t_obat.hrga_jual else 0 end as harga_jual_obat,
		case when t_obat.jenis='cream' then t_obat.hrga_jual else 0 end as harga_jual_cream,
		ok.*,
		case when ok.racikan = 1 then "v" else "" end as racikan1,
		jn.id as id_jenis_racikan1,
		(SELECT racikan from t_obat_keluar where no_nota = ok.no_nota order by racikan desc limit 1) as adaRacikan,
		
		 case when ok.racikan = 1 then concat (jn.diskripsi,' / ',ok.jlh_sub_racikan) else 'Non Racikan' end as diskripsi,
		jn.biaya,
		t_user.username,
		
		(SELECT getBiayaServis(no_nota, 1000)) as biayaServis,
		(SELECT `getBiayaBersih`(no_nota)) as totalBersih,
		round((SELECT `getBiayaBersih`(no_nota)* 0.1), - 2 ) as ppn,
		(select ok.jlh_sub_racikan * jn.biaya as bb from t_obat_keluar as okk where okk.no_nota = ok.no_nota order by bb desc limit 1 ) as biaya_racikan,
		 -- sum( case when ok.racikan = 1 then 1000 else 0 end ) as biayaServis,
		(SELECT SUM(biaya_listrik) AS biaya FROM ( SELECT biaya_listrik FROM t_obat_keluar WHERE 
			  tgl_obat_keluar between tglAwal and tglAkhir GROUP by no_nota ORDER BY  biaya_listrik DESC ) as f
		) as tot_biaya_listrik,
		
		case when ok.racikan = 1 then t_obat.satuan_racik else t_obat.satuan end as satuanjual
		
	
		
		 FROM t_obat_keluar as ok 
				JOIN t_obat ON t_obat.id = ok.id_obat
				JOIN t_user on t_user.id = ok.id_user
				join t_jenis_racikan as jn on jn.id = ok.id_jenis_racikan
			 
		
		/*Menampilkan hnya data obat yg 1 tahun*/
		 where
			DATEDIFF(CURRENT_DATE(),ok.tgl_obat_keluar) <360 
			and ok.tgl_obat_keluar between tglAwal and tglAkhir
		--	where ok.tgl
			
			group by norec
			
		 ORDER BY ok.insert_at desc 
		 
; END//
DELIMITER ;

-- Dumping structure for procedure db_apotek_praya.sp_obat_masuk
DELIMITER //
CREATE DEFINER=`` PROCEDURE `sp_obat_masuk`(
	IN `tglAwal` DATE,
	IN `tglAkhir` DATE

)
BEGIN

-- v_obat_masuk
		
		SELECT 
		
		t_obat.*,
		t_supplier.namapbf,
		t_obat_masuk.no_faktur,
		t_obat_masuk.tgl_fakur,
		t_obat_masuk.tgl_jatuh_tempo as tgl_jatuh_tempo_real,
		DATEDIFF(t_obat_masuk.tgl_jatuh_tempo, CURRENT_DATE()) as i_jatuhtempo,
		t_obat_masuk.id_suppli,
		t_obat_masuk.batch,
		t_obat_masuk.jlh_obat_masuk,
		t_obat_masuk.total_harga,
		t_obat_masuk.harga_satuan,
		t_obat_masuk.discount_harga,
		t_obat_masuk.discount,
		t_obat_masuk.harga_jual,
		date_format(t_obat_masuk.create_at,'%d/%m/%Y %H:%i' ) as create_at,
		t_obat_masuk.total_harga_beli,
		(t_obat_masuk.jlh_obat_masuk * t_obat_masuk.harga_satuan ) as total_harga_beli1,
		
		t_obat_masuk.total_diskon,
		t_obat_masuk.total_harga_iclude_ppn,
		t_obat_masuk.ppn,
		(SELECT SUM(ppn) FROM ( SELECT om.ppn  FROM t_obat_masuk AS om where om.tgl_fakur between tglAwal and tglAkhir GROUP BY no_faktur) AS d) as tot_ppn,
		round((t_obat_masuk.jlh_obat_masuk * t_obat_masuk.harga_satuan) * 0.1 ) as ppn1,
		(SELECT SUM(jlh) FROM ( SELECT om.total_harga_iclude_ppn as jlh  FROM t_obat_masuk AS om where om.tgl_fakur between tglAwal and tglAkhir GROUP BY no_faktur) AS d) as tot_harga_icludePPN,
		t_obat_masuk.lunas,
		t_obat_masuk.tgl_bayar,
		t_user.username
		
		
		
		 FROM t_obat_masuk 
		 INNER JOIN t_obat ON t_obat.id = t_obat_masuk.id_obat
		 inner join t_user on t_user.id = t_obat_masuk.id_user
		
		inner join t_supplier on t_supplier.id = t_obat_masuk.id_suppli
		
		/*Menampilkan hnya data obat yg 1 tahun*/
		 where
			DATEDIFF(CURRENT_DATE(),t_obat_masuk.tgl_fakur) <360 
			and t_obat_masuk.tgl_fakur between tglAwal and tglAkhir
			
		 ORDER BY t_obat_masuk.tgl_fakur desc 
 
; END//
DELIMITER ;

-- Dumping structure for table db_apotek_praya.t_biaya_lain
CREATE TABLE IF NOT EXISTS `t_biaya_lain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_biaya` varchar(100) DEFAULT NULL,
  `biaya` mediumint(9) DEFAULT NULL,
  `statusenabled` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_biaya_lain: ~2 rows (approximately)
/*!40000 ALTER TABLE `t_biaya_lain` DISABLE KEYS */;
INSERT IGNORE INTO `t_biaya_lain` (`id`, `nm_biaya`, `biaya`, `statusenabled`) VALUES
	(1, 'Biaya Listrik', 2000, NULL);
/*!40000 ALTER TABLE `t_biaya_lain` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_closing
CREATE TABLE IF NOT EXISTS `t_closing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_obat` bigint(20) NOT NULL DEFAULT 0,
  `quantity` int(11) NOT NULL,
  `quantity_racik` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `jlhper_raciik` int(11) NOT NULL,
  `tahun` year(4) NOT NULL,
  `priode` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_t_closing_t_obat` (`id_obat`),
  KEY `FK_t_closing_t_user` (`id_user`),
  CONSTRAINT `FK_t_closing_t_obat` FOREIGN KEY (`id_obat`) REFERENCES `t_obat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_t_closing_t_user` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_closing: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_closing` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_closing` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_dokter
CREATE TABLE IF NOT EXISTS `t_dokter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nm_dokter` varchar(200) DEFAULT NULL,
  `dranak` enum('1','0') DEFAULT NULL,
  `no_hp` char(12) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `jk` enum('L','P') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_dokter: ~4 rows (approximately)
/*!40000 ALTER TABLE `t_dokter` DISABLE KEYS */;
INSERT IGNORE INTO `t_dokter` (`id`, `nm_dokter`, `dranak`, `no_hp`, `alamat`, `jk`) VALUES
	(24, 'dr. Ella', '0', NULL, NULL, NULL),
	(25, 'dr.Angel', '0', NULL, NULL, NULL),
	(26, 'dr.Andrew', '0', NULL, NULL, NULL),
	(56, 'dr. Eka', '1', NULL, NULL, NULL),
	(57, '-', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `t_dokter` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_jenis
CREATE TABLE IF NOT EXISTS `t_jenis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_jenis: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_jenis` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_jenis` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_jenis_racikan
CREATE TABLE IF NOT EXISTS `t_jenis_racikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diskripsi` varchar(50) DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  `Column a` int(11) DEFAULT NULL,
  `Column b` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_jenis_racikan: ~7 rows (approximately)
/*!40000 ALTER TABLE `t_jenis_racikan` DISABLE KEYS */;
INSERT IGNORE INTO `t_jenis_racikan` (`id`, `diskripsi`, `biaya`, `Column a`, `Column b`) VALUES
	(0, '-', 0, NULL, NULL),
	(1, 'Puyer', 600, NULL, NULL),
	(2, 'Kapsul1', 400, NULL, NULL),
	(3, 'Syrup', 300, NULL, NULL),
	(4, 'Kapsul', 700, NULL, NULL),
	(83, 'Pot 5-15  gram', 5000, NULL, NULL),
	(86, 'Pot > 30 gram', 10000, NULL, NULL);
/*!40000 ALTER TABLE `t_jenis_racikan` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_obat
CREATE TABLE IF NOT EXISTS `t_obat` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enabled` bit(1) NOT NULL,
  `nm_obat` varchar(100) DEFAULT NULL,
  `quantity` mediumint(9) NOT NULL,
  `satuan` char(15) NOT NULL,
  `quantity_racik` float NOT NULL DEFAULT 0,
  `jlhper_raciik` mediumint(9) NOT NULL,
  `satuan_racik` varchar(50) NOT NULL,
  `expire_date` date NOT NULL,
  `tgl_jatuh_tempo` date DEFAULT NULL,
  `hrgper_racik` int(11) NOT NULL,
  `hrga_jual` int(11) NOT NULL DEFAULT 0,
  `harga_satuan` int(11) NOT NULL DEFAULT 0,
  `jenis` enum('Cream','Obat') NOT NULL,
  `jlhper_pack` mediumint(9) NOT NULL,
  `satuan_pack` varchar(100) NOT NULL,
  `hrgper_pack` int(11) NOT NULL,
  `ket` text DEFAULT NULL,
  `hrgbeli_cream` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000620 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_obat: ~280 rows (approximately)
/*!40000 ALTER TABLE `t_obat` DISABLE KEYS */;
INSERT IGNORE INTO `t_obat` (`id`, `enabled`, `nm_obat`, `quantity`, `satuan`, `quantity_racik`, `jlhper_raciik`, `satuan_racik`, `expire_date`, `tgl_jatuh_tempo`, `hrgper_racik`, `hrga_jual`, `harga_satuan`, `jenis`, `jlhper_pack`, `satuan_pack`, `hrgper_pack`, `ket`, `hrgbeli_cream`) VALUES
	(1000092, b'1', 'Acyclovir generik', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 1000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000093, b'1', 'Alkohol 70% 100 ml botol', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 4000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000094, b'1', 'Alkohol 70% 300 ml botol', 7, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000095, b'1', 'Nobor ', 30, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 6000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000096, b'1', 'Multigyn active gel ', 12, 'botol', 0, 1, 'botol', '0000-00-00', '2021-11-19', 0, 297000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000097, b'1', 'Epexol tablet', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 1200, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000098, b'1', 'Epexol sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 25000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000099, b'1', 'Epexol drop ', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 55000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000100, b'1', 'Mucera drop', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 55000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000101, b'1', 'Mucera sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 33000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000102, b'1', 'Mucos drop', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 52000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000103, b'1', 'Mucos sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 25000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000104, b'1', 'Amlodipine 10 mg ', 96, 'tablet', 0, 1, 'tablet', '0000-00-00', '2023-11-16', 0, 1300, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000105, b'1', 'Amlodipine generik', 45, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 1300, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000106, b'1', 'Amoxiciliin  generik', 130, 'tablet', 0, 1, 'tablet', '0000-00-00', '2023-11-16', 0, 600, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000107, b'1', 'Amoxan 500 mg', 100, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 5000, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000108, b'1', 'Opimox', 100, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 5000, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000109, b'1', 'Amoxan drop ', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 33000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000110, b'1', 'Amoxan sirup ', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 45000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000111, b'1', 'Amoxan Forte sirup', 0, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 45000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000112, b'1', 'Antalgin generik', 97, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000113, b'1', 'Antasida DOEN tablet', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000114, b'1', 'Polysilane tablet', 5, 'strip', 0, 1, 'strip ', '2024-07-01', '2020-07-01', 0, 11000, 0, 'Obat', 1, 'strip', 0, '', 0),
	(1000115, b'1', 'Promag', 12, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 8000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000116, b'1', 'Sanmaag sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 36500, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000117, b'1', 'Mylanta sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 16000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000118, b'1', 'Antasida DOEN sirup', 3, 'botol', 0, 1, 'botol', '0000-00-00', '2023-11-16', 0, 15000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000119, b'1', 'Burraginol N supp', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000120, b'1', 'Burraginol S supp', 1, 'supp', 0, 1, 'supp', '2024-07-01', '2020-07-01', 0, 113000, 0, 'Obat', 1, 'supp', 0, '', 0),
	(1000121, b'1', 'Thrombopop gel ', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000122, b'1', 'Laderma Cream', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 113000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000123, b'1', 'Folavit', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 1500, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000124, b'1', 'Mefinal', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 2000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000125, b'1', 'Plasminex', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 4000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000126, b'1', 'Depakene sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 233000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000127, b'1', 'Xidane', 30, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 11500, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000128, b'1', 'Asta plus', 30, 'kaplet', 0, 1, 'kaplet', '2024-07-01', '2020-07-01', 0, 13000, 0, 'Obat', 1, 'kaplet', 0, '', 0),
	(1000129, b'1', 'Dermia cream 15 gr', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 60000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000130, b'1', 'Xidane gel', 99, 'tube', 13, 30, 'Gram', '2024-07-01', '2020-07-01', 0, 105000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000131, b'1', 'Mezatrin 250 mg ', 30, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 20000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000132, b'1', 'Azitromisin generik', 20, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000133, b'1', 'Hot in Cream ', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000134, b'1', 'Batugin Elixir', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 56000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000135, b'1', 'Neozep forte', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 750, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000136, b'1', 'Paratusin tablet', 20, 'strip', 0, 1, 'strip', '2024-07-01', '2020-07-01', 0, 20000, 0, 'Obat', 1, 'strip', 0, '', 0),
	(1000137, b'1', 'Decolsin tablet', 25, 'strip', 0, 1, 'strip', '2024-07-01', '2020-07-01', 0, 3500, 0, 'Obat', 1, 'strip', 0, '', 0),
	(1000138, b'1', 'Flucadex', 100, 'kaplet', 0, 1, 'kaplet', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'kaplet', 0, '', 0),
	(1000139, b'1', 'Dextral tablet ', 150, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000140, b'1', 'Actifed merah Batuk pilek sirup', 0, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 64000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000141, b'1', 'Uni baby cough sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 6000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000142, b'1', 'OBH combi anak ', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 10000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000143, b'1', 'OBH combi dewasa jahe ', 11, 'botol', 0, 1, 'botol', '0000-00-00', '2023-11-16', 0, 15000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000144, b'1', 'OBH combi dewasa mentol', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 15000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000145, b'1', 'Skinbright Lightening CC cream Beige', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 90000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000146, b'1', 'Skinbright Lightening CC cream Normal', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 90000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000147, b'1', 'Betadine gargle', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000148, b'1', 'Betadine sol 5 ml', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 6000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000149, b'1', 'Betadine 300 ml', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 14000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000150, b'1', 'Betason krim 5 gr', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 12000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000151, b'1', 'Betason N krim 5 gr ', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 19000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000152, b'1', 'Mucohexin', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 1000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000153, b'1', 'Mucohexin sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 24000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000154, b'1', '"Na Cl 0', 9, '1', 0, 0, '1', '2024-07-01', '2020-07-01', 2020, 0, 0, '', 0, '1', 0, '0', 0),
	(1000155, b'1', 'Qcef sirup ', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 73000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000156, b'1', 'Cefat sirup', 0, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 62000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000157, b'1', 'Cefat forte sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 109000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000158, b'1', 'Cefadroxil generik', 50, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 1250, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000159, b'1', 'Qcef 500 mg', 30, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 15000, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000160, b'1', 'Cefat 500 mg', 100, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 14700, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000161, b'1', 'Widoxil', 100, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 13000, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000162, b'1', 'Cefixime 100 mg Hexa/KF', 50, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 3250, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000163, b'1', 'Cefixime 100 mg Nulab ', 50, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 3250, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000164, b'1', 'Sporetik', 30, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 29000, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000165, b'1', 'Inbacef 200 mg ', 30, 'kaplet', 0, 1, 'kaplet', '2024-07-01', '2020-07-01', 0, 30000, 0, 'Obat', 1, 'kaplet', 0, '', 0),
	(1000166, b'1', 'Cetirizine Hexa', 50, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 500, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000167, b'1', 'Cetirizine Nulab', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 500, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000168, b'1', 'Cerini tablet', 20, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 5000, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000169, b'1', 'Cerini sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 58000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000170, b'1', 'Cerini drop ', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 91500, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000171, b'1', 'Erlamycetin tetes telinga ', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 11000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000172, b'1', 'Alleron ', 200, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 200, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000173, b'1', 'Ciprofloxacin generik', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 1000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000174, b'1', 'Clindamycin 150 mg ', 50, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 1000, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000175, b'1', 'Clindamycin 300 mg ', 49, 'kapsul', -2, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 1900, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000176, b'1', 'Clinoma', 100, 'Kapsul', 0, 1, 'Kapsul', '2024-07-01', '2020-07-01', 0, 1700, 0, 'Obat', 1, 'Kapsul', 0, '', 0),
	(1000177, b'1', 'Indanox 300 mg', 60, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 10000, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000178, b'1', 'Mediklin gel 15 gr ', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 31000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000179, b'1', 'Mediklin TR gel 15 gr', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000180, b'1', 'Provula', 30, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 21500, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000181, b'1', 'Clabat', 20, 'kaplet', 0, 1, 'kaplet', '2024-07-01', '2020-07-01', 0, 23500, 0, 'Obat', 1, 'kaplet', 0, '', 0),
	(1000182, b'1', 'Claneksi sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 77000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000183, b'1', 'Biocollagenta', 50, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 5000, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000184, b'1', 'Sanprima', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 1500, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000185, b'1', 'Sanprima sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 37000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000186, b'1', 'Helmig\'s Curcumin', 5, 'sachet', 0, 1, 'sachet', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'sachet', 0, '', 0),
	(1000187, b'1', 'Heptasan', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 350, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000188, b'1', 'Pronicy', 100, 'kaplet', 0, 1, 'kaplet', '2024-07-01', '2020-07-01', 0, 350, 0, 'Obat', 1, 'kaplet', 0, '', 0),
	(1000189, b'1', 'Inerson krim 15 gr ', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 102000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000190, b'1', 'Nupeson krim 10 gr ', 0, 'tube', 7, 10, 'Gram', '2024-07-01', '2020-07-01', 0, 30000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000191, b'1', 'Dextamine', 100, 'kaplet', 0, 1, 'kaplet', '2024-07-01', '2020-07-01', 0, 2500, 0, 'Obat', 1, 'kaplet', 0, '', 0),
	(1000192, b'1', 'Soldextam', 200, 'kaplet', 0, 1, 'kaplet', '2024-07-01', '2020-07-01', 0, 300, 0, 'Obat', 1, 'kaplet', 0, '', 0),
	(1000193, b'1', 'BDM sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 55000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000194, b'1', 'Cortidex', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 400, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000195, b'1', 'Dexaharsen', 200, 'kaplet', 0, 1, 'kaplet', '2024-07-01', '2020-07-01', 0, 300, 0, 'Obat', 1, 'kaplet', 0, '', 0),
	(1000196, b'1', 'Grathazone', 200, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 300, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000197, b'1', 'Faridexon', 100, 'kaplet', 0, 1, 'kaplet', '2024-07-01', '2020-07-01', 0, 300, 0, 'Obat', 1, 'kaplet', 0, '', 0),
	(1000198, b'1', 'Carbidu', 200, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 250, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000199, b'1', 'Confortin cream 20 gr tube', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 39000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000200, b'1', 'Doksisiklin 100 mg', 100, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000201, b'1', 'Dohixat', 100, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 500, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000202, b'1', 'Vosedon sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 41500, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000203, b'1', 'Vometa drop', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 59000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000204, b'1', 'Erysanbe 500 mg', 97, 'kaplet', 0, 1, 'kaplet', '2024-07-01', '2020-07-01', 0, 3450, 0, 'Obat', 1, 'kaplet', 0, '', 0),
	(1000205, b'1', 'Erysanbe sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 35000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000206, b'1', 'Erymed krim 20 gr', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 39000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000207, b'1', 'Govazol', 3, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000208, b'1', 'Fluconazole 150 mg generik', 10, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 27000, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000209, b'1', 'Furosemide generik', 200, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000210, b'1', 'Sagestam 10  gr salep kulit', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 18000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000211, b'1', '"Sagestam salep mata 3', 5, '1', 0, 0, '1', '2024-07-01', '2020-07-01', 2020, 0, 0, '', 0, '1', 0, '0', 0),
	(1000212, b'1', 'gentamisin salep kulit', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 5000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000213, b'1', 'Genalten cream', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 5000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000214, b'1', 'Sagestam tetes mata/telinga', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 42000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000215, b'1', 'Genoint 15 gr', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 8000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000216, b'1', 'Granopi ', 10, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 30000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000217, b'1', 'Griseofulvin 500 mg ', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 2500, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000218, b'1', 'Proris sirup 100 mg ', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 35000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000219, b'1', 'Bufect sirup 100 mg', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 22000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000220, b'1', 'Bufect forte sirup 200 mg', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 30000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000221, b'1', 'Termofen 100 mg sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 37500, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000222, b'1', 'BD Gard', 30, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 15000, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000223, b'1', 'Forcanox', 18, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 3500, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000224, b'1', 'Kassa steril One med', 1, 'box', 0, 1, 'box', '2024-07-01', '2020-07-01', 0, 16000, 0, 'Obat', 1, 'box', 0, '', 0),
	(1000225, b'1', 'Kassa steril Bradja', 1, 'box', 0, 1, 'box', '2024-07-01', '2020-07-01', 0, 16000, 0, 'Obat', 1, 'box', 0, '', 0),
	(1000226, b'1', 'Ketoconazole tab generik', 50, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 700, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000227, b'1', 'Fungasol', 50, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 7500, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000228, b'1', 'Formyco tablet', 50, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 7500, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000229, b'1', 'Mycoral ', 50, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 6000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000230, b'1', 'Formyco 10 gr', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 27000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000231, b'1', 'Ketoconazole krim generik 5 gr', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 8000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000232, b'1', 'Fungasol krim 10 gr', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 40000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000233, b'1', 'Fungasol SS ', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 114500, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000234, b'1', 'Lamodex 10 gr krim', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 60000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000235, b'1', 'Kloderma 10 gr krim', 50, 'tube', 1, 10, 'gram', '2024-07-01', '2020-07-01', 0, 47000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000236, b'1', 'Kondom sutra', 0, 'box', 0, 1, 'box', '2024-07-01', '2020-07-01', 0, 26000, 0, 'Obat', 1, 'box', 0, '', 0),
	(1000237, b'1', 'Lacbon', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 2500, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000238, b'1', 'Probiokid', 10, 'sachet', 0, 1, 'sachet', '2024-07-01', '2020-07-01', 0, 18500, 0, 'Obat', 1, 'sachet', 0, '', 0),
	(1000239, b'1', 'Lacto B ', 40, 'sachet', 0, 1, 'sachet', '2024-07-01', '2020-07-01', 0, 10000, 0, 'Obat', 1, 'sachet', 0, '', 0),
	(1000240, b'1', 'L-Bio', 30, 'sachet', 0, 1, 'sachet', '2024-07-01', '2020-07-01', 0, 10000, 0, 'Obat', 1, 'sachet', 0, '', 0),
	(1000241, b'1', 'Dulcolactol sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 83500, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000242, b'1', 'Laxadine sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 50000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000243, b'1', 'Lansoprazole generik', 20, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 1500, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000244, b'1', 'Lodia', 50, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 1500, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000245, b'1', 'Loran', 30, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 7500, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000246, b'1', 'Loratadine generik', 50, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 500, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000247, b'1', 'Alloris tablet', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 7000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000248, b'1', 'Alloris sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 78500, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000249, b'1', 'Cusson baby moscare', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000250, b'1', 'Sanoskin Melladerm Plus', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000251, b'1', 'Interhistin', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 1200, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000252, b'1', 'Spasminal', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 1000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000253, b'1', 'Metformin generik', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 300, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000254, b'1', 'Lameson 16 mg', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 11900, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000255, b'1', 'Stenirol 16 ', 50, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 11500, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000256, b'1', 'Carmeson 4 ', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 600, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000257, b'1', 'Sanexon 4 mg', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 3000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000258, b'1', 'Metil Prednisolon 8 mg ', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 800, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000259, b'1', 'Damaben tablet', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000260, b'1', 'Damaben drop', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 20000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000261, b'1', 'Damaben sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 15000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000262, b'1', '"Rheu-trex 2', 5, '50', 0, 0, '1', '2024-07-01', '2020-07-01', 2020, 0, 0, '', 0, '50', 0, '0', 0),
	(1000263, b'1', 'Miragyl', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 1000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000264, b'1', 'Miconazole krim generik 5 gr', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 5000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000265, b'1', 'Microlax gel', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000266, b'1', 'Utrogestan 200 mg ', 15, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 29000, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000267, b'1', 'Utrogestan 100 mg ', 30, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 18000, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000268, b'1', 'Minyak kayu putih caplang 60 ml', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 27000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000269, b'1', 'Minyak kayu putih caplang 120 ml', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 49000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000270, b'1', 'Telon Lang ', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000271, b'1', 'Loksin 10 gr', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 94000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000272, b'1', 'Elox krim 5 gr', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 66000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000273, b'1', 'Erdomex sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 66000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000274, b'1', 'HB Vit', 30, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 5000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000275, b'1', 'Nulacta', 60, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 7000, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000276, b'1', 'Caviplex', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 700, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000277, b'1', 'Obdhamin', 50, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 6000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000278, b'1', 'Oxcal ', 100, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 6000, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000279, b'1', 'Folamil Genio', 30, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 5666, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000280, b'1', 'Inlacin 100 mg', 30, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 7500, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000281, b'1', 'Neurosanbe 5000', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 3150, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000282, b'1', 'Neuralgin Rx', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 1000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000283, b'1', 'Neurosanbe', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 1500, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000284, b'1', 'Sanvita B sirup ', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 18000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000285, b'1', 'Muveron drop ', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 40000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000286, b'1', 'Apialis sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 49000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000287, b'1', 'Muveron sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 57000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000288, b'1', 'San B Plex drop', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 25000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000289, b'1', 'SIM DHA sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 66000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000290, b'1', 'Zamel sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 56000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000291, b'1', 'Sangobion 250 ', 250, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 15000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000292, b'1', 'Elkana sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 31000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000293, b'1', 'Neuropyron V', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 1000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000294, b'1', 'Pirotop 5 gr ', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 57000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000295, b'1', 'Pirotop 10 gr', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 79000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000296, b'1', 'Natrium diklofenak 50 mg', 48, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 700, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000297, b'1', 'Fenaren', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 1000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000298, b'1', 'Nymiko drop', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 48000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000299, b'1', 'Norestil 2', 30, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 6000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000300, b'1', 'Enterostop', 2, 'strip', 0, 1, 'strip ', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'strip', 0, '', 0),
	(1000301, b'1', 'Omeprazole generik', 30, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 10000, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000302, b'1', 'Ondansentron Generik', 50, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 2000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000303, b'1', 'Dehidralit', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 24000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000304, b'1', 'Renalite', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 24000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000305, b'1', 'Sanmol tablet', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 1000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000306, b'1', 'Paracetamol 500 mg generik', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 400, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000307, b'1', 'Grafadon', 100, 'kaplet', 0, 1, 'kaplet', '2024-07-01', '2020-07-01', 0, 400, 0, 'Obat', 1, 'kaplet', 0, '', 0),
	(1000308, b'1', 'Paramex sakit kepala tablet', 1, 'strip', 0, 1, 'strip', '2024-07-01', '2020-07-01', 0, 3000, 0, 'Obat', 1, 'strip', 0, '', 0),
	(1000309, b'1', 'Panadol biru ', 10, 'strip', 0, 1, 'strip', '2024-07-01', '2020-07-01', 0, 10000, 0, 'Obat', 1, 'strip', 0, '', 0),
	(1000310, b'1', 'Panadol extra', 10, 'strip', 0, 1, 'strip', '2024-07-01', '2020-07-01', 0, 11000, 0, 'Obat', 1, 'strip', 0, '', 0),
	(1000311, b'1', 'Pamol 125 mg supp', 12, 'supp', 0, 1, 'supp', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'supp', 0, '', 0),
	(1000312, b'1', 'Sanmol sirup 120 mg', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 17000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000313, b'1', 'Sanmol drop 60 mg', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000314, b'1', 'Kamolas Forte 250 mg sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 36000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000315, b'1', 'Paracetamol sirup 120 mg', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 7000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000316, b'1', 'Kapsida bersih darah kembang bulan', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000317, b'1', 'QV cream ', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 197000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000318, b'1', 'Medscab', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 98000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000319, b'1', 'Scacid cream 10 gr', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 57000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000320, b'1', 'Combantrin sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 22000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000321, b'1', 'Combantrin 125 mg tablet', 25, 'strip', 0, 1, 'strip', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'strip', 0, '', 0),
	(1000322, b'1', 'Combantrin 250 mg tablet', 25, 'strip', 0, 1, 'strip', '2024-07-01', '2020-07-01', 0, 18000, 0, 'Obat', 1, 'strip', 0, '', 0),
	(1000323, b'1', 'Piroxicam 10 mg generik', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 200, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000324, b'1', 'Miradene 20 mg', 100, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000325, b'1', 'Novaxicam', 100, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000326, b'1', 'Bioplacenton 15 gr', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 23000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000327, b'1', 'chilli plast plester coklat', 0, 'piece', 0, 1, 'piece', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'piece', 0, '', 0),
	(1000328, b'1', 'Hansaplast', 100, 'lembar', 0, 1, 'lembar', '2024-07-01', '2020-07-01', 0, 500, 0, 'Obat', 1, 'lembar', 0, '', 0),
	(1000329, b'1', 'Hansaplast rol kain', 1, 'roll', 0, 1, 'roll', '2024-07-01', '2020-07-01', 0, 4000, 0, 'Obat', 1, 'roll', 0, '', 0),
	(1000330, b'1', 'Meptin mini', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 5000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000331, b'1', 'Prothyra 10 mg ', 30, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000332, b'1', 'Ranitidine Hcl', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 3000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000333, b'1', 'Topicare Cleanser', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 98000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000334, b'1', 'Salbutamol 2 mg generik', 100, '', 0, 1, '', '2024-07-01', '2020-07-01', 0, 200, 0, 'Obat', 1, '', 0, '', 0),
	(1000335, b'1', 'Salbutamol 2 mg generik', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 200, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000336, b'1', 'Salbutamol 4 mg generik', 100, '', 0, 1, '', '2024-07-01', '2020-07-01', 0, 200, 0, 'Obat', 1, '', 0, '', 0),
	(1000337, b'1', 'Salbutamol 4 mg generik', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 300, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000338, b'1', 'Ventolin nebul', 20, 'unit', 0, 1, 'unit', '2024-07-01', '2020-07-01', 0, 14000, 0, 'Obat', 1, 'unit', 0, '', 0),
	(1000339, b'1', 'Velutine inhalation sol', 10, 'vial', 0, 1, 'vial', '2024-07-01', '2020-07-01', 0, 15000, 0, 'Obat', 1, 'vial', 0, '', 0),
	(1000340, b'1', 'Teosal', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 300, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000341, b'1', 'Lasal sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 35000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000342, b'1', 'Salep 2-4', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000343, b'1', 'Bedak Salisil ', 1, 'pot', 0, 1, 'pot', '2024-07-01', '2020-07-01', 0, 10000, 0, 'Obat', 1, 'pot', 0, '', 0),
	(1000344, b'1', 'Caladine lotion', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000345, b'1', 'Kodomo shampoo', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000346, b'1', 'Gramax 100 mg', 4, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 71000, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000347, b'1', 'Bye-bye fever anak ', 5, 'strip', 0, 1, 'strip', '2024-07-01', '2020-07-01', 0, 12500, 0, 'Obat', 1, 'strip', 0, '', 0),
	(1000348, b'1', 'Bye-bye fever bayi', 10, 'strip', 0, 1, 'strip', '2024-07-01', '2020-07-01', 0, 10000, 0, 'Obat', 1, 'strip', 0, '', 0),
	(1000349, b'1', 'Tes hamil Onemed', 50, 'piece', 0, 1, 'piece', '2024-07-01', '2020-07-01', 0, 3000, 0, 'Obat', 1, 'piece', 0, '', 0),
	(1000350, b'1', 'Tes hamil Quick & sure', 25, 'piece', 0, 1, 'piece', '2024-07-01', '2020-07-01', 0, 10000, 0, 'Obat', 1, 'piece', 0, '', 0),
	(1000351, b'1', 'Tes hamil Steril', 50, 'piece', 0, 1, 'piece', '2024-07-01', '2020-07-01', 0, 7000, 0, 'Obat', 1, 'piece', 0, '', 0),
	(1000352, b'1', 'Rohto tetes mata', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 15000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000353, b'1', 'Insto tetes mata', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 16000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000354, b'1', 'Cendoxitrol Tetes Mata', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 41000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000355, b'1', 'Thiamycin 1000', 50, 'kaplet', 0, 1, 'kaplet', '2024-07-01', '2020-07-01', 0, 11500, 0, 'Obat', 1, 'kaplet', 0, '', 0),
	(1000356, b'1', 'Tolak Angin ', 12, 'sachet', 0, 1, 'sachet', '2024-07-01', '2020-07-01', 0, 4000, 0, 'Obat', 1, 'sachet', 0, '', 0),
	(1000357, b'1', 'Antangin JRG', 12, 'sachet', 0, 1, 'sachet', '2024-07-01', '2020-07-01', 0, 3000, 0, 'Obat', 1, 'sachet', 0, '', 0),
	(1000359, b'1', 'Rafacort tab', 50, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 5000, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000360, b'1', 'Bufacomb cream in ora base', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 23000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000361, b'1', 'Carmed 10% krim 40 gr', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 39000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000362, b'1', 'Carmed 20% krim 40 gr', 1, 'tube', 0, 1, 'tube', '2024-07-01', '2020-07-01', 0, 51000, 0, 'Obat', 1, 'tube', 0, '', 0),
	(1000363, b'1', 'Vomil B6', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 3600, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000364, b'1', 'Vit B complex IPI', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 6000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000365, b'1', 'Vit C IPI ', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 6000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000366, b'1', 'Redoxon', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 40000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000367, b'1', 'Calcido ', 30, 'kaplet', 0, 1, 'kaplet', '2024-07-01', '2020-07-01', 0, 7000, 0, 'Obat', 1, 'kaplet', 0, '', 0),
	(1000368, b'1', 'Eturol', 100, 'kapsul', 0, 1, 'kapsul', '2024-07-01', '2020-07-01', 0, 8000, 0, 'Obat', 1, 'kapsul', 0, '', 0),
	(1000369, b'1', 'Maltofer', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 79000, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000370, b'1', 'Zinc 20 mg Kimia farma', 100, 'tablet', 0, 1, 'tablet', '2024-07-01', '2020-07-01', 0, 900, 0, 'Obat', 1, 'tablet', 0, '', 0),
	(1000371, b'1', 'L-zinc sirup', 1, 'botol', 0, 1, 'botol', '2024-07-01', '2020-07-01', 0, 0, 0, 'Obat', 1, 'botol', 0, '', 0),
	(1000378, b'1', 'OBAT 12345', 98, 'Strip', 2, 10, 'Gram', '2021-11-13', '2019-11-16', 0, 20000, 18500, 'Obat', 10, 'Box', 0, NULL, NULL);
/*!40000 ALTER TABLE `t_obat` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_obat_keluar
CREATE TABLE IF NOT EXISTS `t_obat_keluar` (
  `norec` bigint(20) NOT NULL AUTO_INCREMENT,
  `no_nota` varchar(50) DEFAULT '0',
  `nama_pengunjung` varchar(100) DEFAULT NULL,
  `id_pasien` bigint(20) DEFAULT NULL,
  `tgl_obat_keluar` datetime NOT NULL,
  `id_obat` bigint(20) NOT NULL,
  `jumlah_obat_keluar` float NOT NULL DEFAULT 0,
  `jlh_sub_racikan` smallint(6) NOT NULL,
  `biaya_listrik` mediumint(9) NOT NULL DEFAULT 0,
  `biaya_admin` mediumint(9) NOT NULL DEFAULT 0,
  `nm_dokter` varchar(150) NOT NULL,
  `tot_harga` int(11) NOT NULL,
  `uang` int(11) NOT NULL DEFAULT 0,
  `kembalian` int(11) NOT NULL DEFAULT 0,
  `insert_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `racikan` char(1) NOT NULL,
  `biaya_dokter` int(11) NOT NULL,
  `id_jenis_racikan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT 0,
  `hrgajualbersih` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`norec`),
  KEY `FK_t_obat_keluar_t_obat` (`id_obat`),
  KEY `FK_t_obat_keluar_t_user` (`id_user`),
  KEY `FK_t_obat_keluar_t_jenis_racikan` (`id_jenis_racikan`),
  CONSTRAINT `FK_t_obat_keluar_t_jenis_racikan` FOREIGN KEY (`id_jenis_racikan`) REFERENCES `t_jenis_racikan` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_keluar_t_obat` FOREIGN KEY (`id_obat`) REFERENCES `t_obat` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_keluar_t_user` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1002609 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_obat_keluar: ~24 rows (approximately)
/*!40000 ALTER TABLE `t_obat_keluar` DISABLE KEYS */;
INSERT IGNORE INTO `t_obat_keluar` (`norec`, `no_nota`, `nama_pengunjung`, `id_pasien`, `tgl_obat_keluar`, `id_obat`, `jumlah_obat_keluar`, `jlh_sub_racikan`, `biaya_listrik`, `biaya_admin`, `nm_dokter`, `tot_harga`, `uang`, `kembalian`, `insert_at`, `racikan`, `biaya_dokter`, `id_jenis_racikan`, `id_user`, `hrgajualbersih`) VALUES
	(1002574, 'N201119807982', 'SIAPA AJA', NULL, '2019-11-20 00:00:00', 1000112, 3, 3, 2000, 0, 'DR. EKA', 15500, 16000, 500, '2019-11-20 16:13:38', '1', 105000, 2, 1, 0),
	(1002575, 'N201119807982', 'SIAPA AJA', NULL, '2019-11-20 00:00:00', 1000105, 4, 3, 2000, 0, 'DR. EKA', 15500, 16000, 500, '2019-11-20 16:13:38', '1', 105000, 2, 1, 5200),
	(1002576, 'N201119807982', 'SIAPA AJA', NULL, '2019-11-20 00:00:00', 1000104, 3, 3, 2000, 0, 'DR. EKA', 15500, 16000, 500, '2019-11-20 16:13:38', '1', 105000, 2, 1, 3900),
	(1002588, 'N201119676988', '', NULL, '2019-11-20 00:00:00', 1000130, 2, 1, 2000, 0, 'DR.ANGEL', 21000, 25000, 4000, '2019-11-20 17:44:31', '1', 0, 0, 1, 7000),
	(1002589, 'N201119676988', '', NULL, '2019-11-20 00:00:00', 1000235, 2, 1, 2000, 0, 'DR.ANGEL', 21000, 25000, 4000, '2019-11-20 17:44:31', '1', 0, 83, 1, 9400),
	(1002590, 'N201119318539', '', NULL, '2019-11-20 00:00:00', 1000235, 2, 1, 2000, 0, 'DR.ANGEL', 88500, 100000, 11500, '2019-11-20 17:45:39', '1', 0, 83, 1, 9400),
	(1002591, 'N201119318539', '', NULL, '2019-11-20 00:00:00', 1000130, 2, 1, 2000, 0, 'DR.ANGEL', 88500, 100000, 11500, '2019-11-20 17:45:39', '1', 0, 83, 1, 7000),
	(1002592, 'N201119318539', '', NULL, '2019-11-20 00:00:00', 1000105, 1, 1, 2000, 0, 'DR.ANGEL', 88500, 100000, 11500, '2019-11-20 17:45:39', '1', 0, 83, 1, 1300),
	(1002593, 'N201119318539', '', NULL, '2019-11-20 00:00:00', 1000140, 1, 1, 2000, 0, 'DR.ANGEL', 88500, 100000, 11500, '2019-11-20 17:45:39', '1', 0, 83, 1, 64000),
	(1002594, 'N201119889404', '', NULL, '2019-11-20 00:00:00', 1000130, 2, 1, 2000, 0, 'DR. ELLA', 20000, 20000, 0, '2019-11-20 19:03:58', '1', 0, 4, 1, 7000),
	(1002595, 'N201119889404', '', NULL, '2019-11-20 00:00:00', 1000104, 1, 1, 2000, 0, 'DR. ELLA', 20000, 20000, 0, '2019-11-20 19:03:58', '1', 0, 4, 1, 1300),
	(1002596, 'N201119889404', '', NULL, '2019-11-20 00:00:00', 1000175, 3, 1, 2000, 0, 'DR. ELLA', 20000, 20000, 0, '2019-11-20 19:03:58', '1', 0, 4, 1, 5700),
	(1002597, 'N20111920828', '', NULL, '2019-11-20 00:00:00', 1000130, 2, 1, 2000, 0, 'DR.ANGEL', 42500, 230000, 187500, '2019-11-20 19:06:34', '1', 0, 83, 1, 7000),
	(1002598, 'N20111920828', '', NULL, '2019-11-20 00:00:00', 1000236, 1, 1, 2000, 0, 'DR.ANGEL', 42500, 230000, 187500, '2019-11-20 19:06:34', '1', 0, 83, 1, 26000),
	(1002599, 'N201119396503', '', NULL, '2019-11-20 00:00:00', 1000130, 2, 1, 2000, 0, 'DR. ELLA', 79000, 80000, 1000, '2019-11-20 19:55:13', '1', 0, 83, 1, 7000),
	(1002600, 'N201119396503', '', NULL, '2019-11-20 00:00:00', 1000296, 1, 1, 2000, 0, 'DR. ELLA', 79000, 80000, 1000, '2019-11-20 19:55:13', '1', 0, 83, 1, 700),
	(1002601, 'N201119396503', '', NULL, '2019-11-20 00:00:00', 1000156, 1, 0, 0, 0, 'DR. ELLA', 79000, 80000, 1000, '2019-11-20 19:55:13', '0', 0, 0, 1, 62000),
	(1002602, 'N201119405092', '', NULL, '2019-11-20 00:00:00', 1000378, 3, 1, 2000, 0, '-', 14500, 15000, 500, '2019-11-20 20:18:19', '1', 0, 83, 1, 6000),
	(1002603, 'N20111988901', '', NULL, '2019-11-20 00:00:00', 1000378, 8, 1, 2000, 0, '-', 29500, 30000, 500, '2019-11-20 20:19:18', '1', 0, 86, 1, 16000),
	(1002604, 'N201119622247', '', NULL, '2019-11-20 00:00:00', 1000190, 3, 1, 2000, 0, '-', 17500, 18000, 500, '2019-11-20 20:23:19', '1', 0, 83, 1, 9000),
	(1002605, 'N201119250230', '', NULL, '2019-11-20 00:00:00', 1000378, 2.5, 1, 2000, 0, '-', 13500, 15000, 1500, '2019-11-20 21:08:33', '1', 0, 83, 1, 5000),
	(1002606, 'N201119857610', '', NULL, '2019-11-20 00:00:00', 1000378, 2.5, 1, 2000, 0, 'DR.ANDREW', 13500, 15000, 1500, '2019-11-20 21:17:55', '1', 0, 83, 1, 5000),
	(1002607, 'N201119514030', '', NULL, '2019-11-20 00:00:00', 1000378, 1.5, 1, 2000, 0, 'DR.ANGEL', 16500, 20000, 3500, '2019-11-20 21:20:54', '1', 0, 86, 1, 3000),
	(1002608, 'N201119676412', '', NULL, '2019-11-20 00:00:00', 1000378, 1.5, 1, 2000, 0, 'DR. EKA', 121500, 20000, 3500, '2019-11-20 21:37:21', '1', 105000, 86, 1, 3000);
/*!40000 ALTER TABLE `t_obat_keluar` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_obat_masuk
CREATE TABLE IF NOT EXISTS `t_obat_masuk` (
  `norec` bigint(20) NOT NULL AUTO_INCREMENT,
  `no_faktur` char(20) NOT NULL,
  `id_suppli` bigint(20) NOT NULL,
  `nama_pabrik` varchar(50) NOT NULL,
  `tgl_fakur` datetime NOT NULL,
  `batch` varchar(50) DEFAULT NULL,
  `id_obat` bigint(20) NOT NULL,
  `expire_date` date NOT NULL,
  `jlh_obat_masuk` mediumint(9) NOT NULL,
  `total_harga` bigint(20) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `discount_harga` float(10,2) NOT NULL,
  `discount` float(10,2) NOT NULL,
  `harga_jual` float NOT NULL,
  `total_harga_beli` bigint(20) NOT NULL,
  `ppn` bigint(20) NOT NULL,
  `total_diskon` bigint(20) NOT NULL,
  `total_harga_iclude_ppn` bigint(20) NOT NULL,
  `tgl_jatuh_tempo` date DEFAULT NULL,
  `lunas` enum('0','1') DEFAULT NULL,
  `tgl_bayar` date DEFAULT NULL,
  `id_user` int(11) NOT NULL DEFAULT 0,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`norec`),
  KEY `FK_t_obat_masuk_t_suppli` (`id_suppli`),
  KEY `FK_t_obat_masuk_t_obat` (`id_obat`),
  KEY `FK_t_obat_masuk_t_user` (`id_user`),
  CONSTRAINT `FK_t_obat_masuk_t_obat` FOREIGN KEY (`id_obat`) REFERENCES `t_obat` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_masuk_t_suppli` FOREIGN KEY (`id_suppli`) REFERENCES `t_supplier` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_obat_masuk_t_user` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_obat_masuk: ~9 rows (approximately)
/*!40000 ALTER TABLE `t_obat_masuk` DISABLE KEYS */;
INSERT IGNORE INTO `t_obat_masuk` (`norec`, `no_faktur`, `id_suppli`, `nama_pabrik`, `tgl_fakur`, `batch`, `id_obat`, `expire_date`, `jlh_obat_masuk`, `total_harga`, `harga_satuan`, `discount_harga`, `discount`, `harga_jual`, `total_harga_beli`, `ppn`, `total_diskon`, `total_harga_iclude_ppn`, `tgl_jatuh_tempo`, `lunas`, `tgl_bayar`, `id_user`, `create_at`) VALUES
	(192, '14155548778', 13, '', '2019-11-19 00:00:00', 'PODNSDF', 1000096, '1899-12-30', 3, 4000, 1333, 0.00, 0.00, 0, 4000, 400, 0, 4400, '2019-11-14', '1', '2019-11-20', 1, '2019-11-20 16:31:23'),
	(193, '55454545555', 10, '44444', '2019-11-22 00:00:00', '444444', 1000096, '1899-12-30', 3, 10000, 3333, 0.00, 0.00, 0, 10000, 1000, 0, 11000, '2019-11-21', NULL, NULL, 1, '2019-11-20 16:31:36'),
	(194, '44333444443', 10, 'RRR', '2019-11-16 00:00:00', '', 1000096, '1899-12-30', 2, 444, 222, 0.00, 0.00, 0, 444, 44, 0, 488, '2020-11-20', NULL, NULL, 1, '2019-11-20 16:31:33'),
	(195, '32342333333', 10, 'ASASD', '2019-11-09 00:00:00', 'EWEWE', 1000096, '0000-00-00', 3, 333, 111, 0.00, 0.00, 0, 333, 33, 0, 366, '2021-11-19', NULL, NULL, 1, '2019-11-20 16:31:30'),
	(196, '324423423423423', 10, 'PABRIK A', '2019-11-20 00:00:00', 'WW222', 1000118, '0000-00-00', 2, 1000, 500, 0.00, 0.00, 0, 17180, 1718, 0, 18898, '2023-11-16', NULL, NULL, 1, '2019-11-20 16:34:35'),
	(197, '324423423423423', 10, 'PABRIK A', '2019-11-20 00:00:00', 'WWEEEEE', 1000143, '0000-00-00', 10, 3000, 300, 0.00, 0.00, 0, 17180, 1718, 0, 18898, '2023-11-16', NULL, NULL, 1, '2019-11-20 16:34:37'),
	(198, '324423423423423', 10, 'PABRIK A', '2019-11-20 00:00:00', '22222122', 1000106, '0000-00-00', 30, 180, 7, 20.00, 10.00, 0, 17180, 1718, 0, 18898, '2023-11-16', NULL, NULL, 1, '2019-11-20 16:34:37'),
	(199, '324423423423423', 10, 'PABRIK A', '2019-11-20 00:00:00', 'WWWWWWW', 1000104, '0000-00-00', 3, 13000, 4333, 0.00, 0.00, 0, 17180, 1718, 0, 18898, '2023-11-16', NULL, NULL, 1, '2019-11-20 16:34:37'),
	(201, '101001010122', 12, 'PABRIK C', '2021-11-19 00:00:00', 'SSSSSSSSS', 1000378, '2021-11-13', 100, 1850000, 18500, 0.00, 0.00, 0, 1850000, 185000, 0, 2035000, '2019-11-16', NULL, NULL, 1, '2019-11-20 20:16:34');
/*!40000 ALTER TABLE `t_obat_masuk` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_pabrik
CREATE TABLE IF NOT EXISTS `t_pabrik` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nm_pabrik` varchar(200) DEFAULT NULL,
  `no_contact` varchar(50) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_pabrik: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_pabrik` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pabrik` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_pasien
CREATE TABLE IF NOT EXISTS `t_pasien` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nam_pasien` varchar(100) NOT NULL,
  `alamat` varchar(300) DEFAULT NULL,
  `no_hp` char(12) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `no_rekam_medis` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `no_rekam_medis` (`no_rekam_medis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_pasien: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_pasien` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pasien` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_pot
CREATE TABLE IF NOT EXISTS `t_pot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namapot` varchar(50) DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_pot: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_pot` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pot` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_puyer
CREATE TABLE IF NOT EXISTS `t_puyer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namacetakan` varchar(50) DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_puyer: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_puyer` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_puyer` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_satuan
CREATE TABLE IF NOT EXISTS `t_satuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `satuan` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_satuan: ~16 rows (approximately)
/*!40000 ALTER TABLE `t_satuan` DISABLE KEYS */;
INSERT IGNORE INTO `t_satuan` (`id`, `satuan`) VALUES
	(1, 'Tablet'),
	(2, 'Botol'),
	(3, 'Kapsul'),
	(4, 'Strip '),
	(5, 'Tube'),
	(6, 'Supp'),
	(7, 'Kaplet'),
	(8, 'Strip'),
	(9, 'Sachet'),
	(10, 'Box'),
	(11, 'Piece'),
	(12, 'Lembar'),
	(13, 'Roll'),
	(14, 'Unit'),
	(15, 'Vial'),
	(16, 'Pot');
/*!40000 ALTER TABLE `t_satuan` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_supplier
CREATE TABLE IF NOT EXISTS `t_supplier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `namapbf` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `no_kontak` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_supplier: ~14 rows (approximately)
/*!40000 ALTER TABLE `t_supplier` DISABLE KEYS */;
INSERT IGNORE INTO `t_supplier` (`id`, `namapbf`, `alamat`, `no_kontak`, `email`) VALUES
	(8, 'Kimia Farma', '', '', ''),
	(9, 'MAS', '', '', ''),
	(10, 'Penta Valen', '', '', ''),
	(11, 'SST/United dico citus ', '', '', ''),
	(12, 'BSP', '', '', ''),
	(13, 'Enseval', '', '', ''),
	(14, 'Mandiri', '', '', ''),
	(15, 'SST', '', '', ''),
	(16, 'MAS/mandiri', '', '', ''),
	(17, 'United Dico citus ', '', '', ''),
	(18, 'SST,BSP', '', '', ''),
	(19, 'Enseval,MAS', '', '', ''),
	(20, 'Yarindo', '', '', '');
/*!40000 ALTER TABLE `t_supplier` ENABLE KEYS */;

-- Dumping structure for table db_apotek_praya.t_user
CREATE TABLE IF NOT EXISTS `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `status` enum('A','O') DEFAULT NULL,
  `jk` enum('L','P') DEFAULT NULL,
  `no_hp` varchar(12) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table db_apotek_praya.t_user: ~1 rows (approximately)
/*!40000 ALTER TABLE `t_user` DISABLE KEYS */;
INSERT IGNORE INTO `t_user` (`id`, `username`, `password`, `status`, `jk`, `no_hp`, `alamat`) VALUES
	(1, 'admin', '123', 'A', NULL, NULL, NULL),
	(4, 'operator', '123', 'O', NULL, NULL, NULL);
/*!40000 ALTER TABLE `t_user` ENABLE KEYS */;

-- Dumping structure for view db_apotek_praya.v_obat
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat` (
	`id` BIGINT(20) NOT NULL,
	`enabled` BIT(1) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity_racik` FLOAT NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`expire_date` DATE NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`hrgper_racik` INT(11) NOT NULL,
	`hrga_jual` INT(11) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrgper_pack` INT(11) NOT NULL,
	`ket` TEXT NULL COLLATE 'latin1_swedish_ci',
	`hrgbeli_cream` INT(11) NULL,
	`jumlah` VARCHAR(22) NOT NULL COLLATE 'utf8mb4_general_ci',
	`i_expire` INT(7) NULL,
	`i_jatuh_tempo` INT(7) NULL
) ENGINE=MyISAM;

-- Dumping structure for view db_apotek_praya.v_obat_jatuh_tempo
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat_jatuh_tempo` (
	`namapbf` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`norec` BIGINT(20) NOT NULL,
	`no_faktur` CHAR(20) NOT NULL COLLATE 'latin1_swedish_ci',
	`id_suppli` BIGINT(20) NOT NULL,
	`tgl_fakur` DATETIME NOT NULL,
	`batch` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`id_obat` BIGINT(20) NOT NULL,
	`expire_date` DATE NOT NULL,
	`jlh_obat_masuk` MEDIUMINT(9) NOT NULL,
	`total_harga` BIGINT(20) NOT NULL,
	`harga_satuan` INT(11) NOT NULL,
	`discount_harga` FLOAT(10,2) NOT NULL,
	`discount` FLOAT(10,2) NOT NULL,
	`harga_jual` FLOAT NOT NULL,
	`total_harga_beli` BIGINT(20) NOT NULL,
	`ppn` BIGINT(20) NOT NULL,
	`total_diskon` BIGINT(20) NOT NULL,
	`total_harga_iclude_ppn` BIGINT(20) NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`lunas` ENUM('0','1') NULL COLLATE 'latin1_swedish_ci',
	`tgl_bayar` DATE NULL,
	`id_user` INT(11) NOT NULL,
	`create_at` TIMESTAMP NOT NULL,
	`username` VARCHAR(200) NULL COLLATE 'latin1_swedish_ci',
	`i_jatuhtempo` INT(7) NULL
) ENGINE=MyISAM;

-- Dumping structure for view db_apotek_praya.v_obat_keluar
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat_keluar` (
	`id` BIGINT(20) NOT NULL,
	`enabled` BIT(1) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity_racik` FLOAT NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`expire_date` DATE NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`hrgper_racik` INT(11) NOT NULL,
	`hrga_jual` INT(11) NOT NULL,
	`harga_satuan` INT(11) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrgper_pack` INT(11) NOT NULL,
	`ket` TEXT NULL COLLATE 'latin1_swedish_ci',
	`hrgbeli_cream` INT(11) NULL,
	`harga_jual_obat` INT(11) NOT NULL,
	`harga_jual_cream` INT(11) NOT NULL,
	`norec` BIGINT(20) NOT NULL,
	`no_nota` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`nama_pengunjung` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`id_pasien` BIGINT(20) NULL,
	`tgl_obat_keluar` DATETIME NOT NULL,
	`id_obat` BIGINT(20) NOT NULL,
	`jumlah_obat_keluar` FLOAT NOT NULL,
	`jlh_sub_racikan` SMALLINT(6) NOT NULL,
	`biaya_listrik` MEDIUMINT(9) NOT NULL,
	`biaya_admin` MEDIUMINT(9) NOT NULL,
	`nm_dokter` VARCHAR(150) NOT NULL COLLATE 'latin1_swedish_ci',
	`tot_harga` INT(11) NOT NULL,
	`uang` INT(11) NOT NULL,
	`kembalian` INT(11) NOT NULL,
	`insert_at` TIMESTAMP NOT NULL,
	`racikan` CHAR(1) NOT NULL COLLATE 'latin1_swedish_ci',
	`biaya_dokter` INT(11) NOT NULL,
	`id_jenis_racikan` INT(11) NOT NULL,
	`id_user` INT(11) NOT NULL,
	`hrgajualbersih` INT(11) NOT NULL,
	`totalAndDokter` BIGINT(12) NOT NULL,
	`racikan1` VARCHAR(1) NOT NULL COLLATE 'utf8mb4_general_ci',
	`id_jenis_racikan1` INT(11) NOT NULL,
	`adaRacikan` VARCHAR(1) NULL COLLATE 'latin1_swedish_ci',
	`diskripsi` VARCHAR(59) NULL COLLATE 'latin1_swedish_ci',
	`biaya` INT(11) NULL,
	`username` VARCHAR(200) NULL COLLATE 'latin1_swedish_ci',
	`biayaServis` INT(11) NULL,
	`totalBersih` BIGINT(20) NULL,
	`ppn` DECIMAL(21,0) NULL,
	`biaya_racikan` BIGINT(16) NULL,
	`satuanjual` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- Dumping structure for view db_apotek_praya.v_obat_masuk
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_obat_masuk` (
	`id` BIGINT(20) NOT NULL,
	`nm_obat` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`quantity` MEDIUMINT(9) NOT NULL,
	`satuan` CHAR(15) NOT NULL COLLATE 'latin1_swedish_ci',
	`quantity_racik` FLOAT NOT NULL,
	`jlhper_raciik` MEDIUMINT(9) NOT NULL,
	`satuan_racik` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`expire_date` DATE NOT NULL,
	`tgl_jatuh_tempo` DATE NULL,
	`hrgper_racik` INT(11) NOT NULL,
	`hrga_jual` INT(11) NOT NULL,
	`jenis` ENUM('Cream','Obat') NOT NULL COLLATE 'latin1_swedish_ci',
	`jlhper_pack` MEDIUMINT(9) NOT NULL,
	`satuan_pack` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`hrgper_pack` INT(11) NOT NULL,
	`ket` TEXT NULL COLLATE 'latin1_swedish_ci',
	`namapbf` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`no_faktur` CHAR(20) NOT NULL COLLATE 'latin1_swedish_ci',
	`tgl_fakur` DATETIME NOT NULL,
	`tgl_jatuh_tempo_real` DATE NULL,
	`i_jatuhtempo` INT(7) NULL,
	`id_suppli` BIGINT(20) NOT NULL,
	`batch` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`jlh_obat_masuk` MEDIUMINT(9) NOT NULL,
	`total_harga` BIGINT(20) NOT NULL,
	`harga_satuan` INT(11) NOT NULL,
	`discount_harga` FLOAT(10,2) NOT NULL,
	`discount` FLOAT(10,2) NOT NULL,
	`harga_jual` FLOAT NOT NULL,
	`create_at` VARCHAR(21) NULL COLLATE 'utf8mb4_general_ci',
	`total_harga_beli` BIGINT(20) NOT NULL,
	`total_harga_beli1` BIGINT(19) NOT NULL,
	`total_diskon` BIGINT(20) NOT NULL,
	`total_harga_iclude_ppn` BIGINT(20) NOT NULL,
	`ppn` BIGINT(20) NOT NULL,
	`ppn1` DECIMAL(20,0) NOT NULL,
	`lunas` ENUM('0','1') NULL COLLATE 'latin1_swedish_ci',
	`tgl_bayar` DATE NULL,
	`username` VARCHAR(200) NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- Dumping structure for trigger db_apotek_praya.trg_barang_keluar
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_barang_keluar` AFTER INSERT ON `t_obat_keluar` FOR EACH ROW BEGIN
 	declare racikan,jlh_stok_racik,sisa_stok_racik int;
	
	SELECT t_obat_keluar.racikan from t_obat_keluar where t_obat_keluar.norec = new.norec into racikan; 

-- jika bukan racikan

	if racikan ='0' then
	begin
			update t_obat set t_obat.quantity = quantity - NEW.jumlah_obat_keluar
			where t_obat.id = new.id_obat;
	end;		
		else
		-- jika satuan racikan
			begin
				SELECT t_obat.quantity_racik from t_obat where t_obat.id = new.id_obat into jlh_stok_racik;
				set sisa_stok_racik = jlh_stok_racik - new.jumlah_obat_keluar;
				
				if sisa_stok_racik <0 then
				begin
					update t_obat set t_obat.quantity = t_obat.quantity - 1, 
					t_obat.quantity_racik = (t_obat.jlhper_raciik + sisa_stok_racik) where t_obat.id = new.id_obat;
				end;	
					else
						begin
								update t_obat set t_obat.quantity_racik = quantity_racik- new.jumlah_obat_keluar 
								where t_obat.id = new.id_obat;
						end;	
				end if;
				
				
			end;	
			
		
	end if;	
			
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_apotek_praya.trg_obat_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_obat_insert` AFTER INSERT ON `t_obat` FOR EACH ROW BEGIN
	--	update t_obat set t_obat.quantity = quantity-1  where t_obat.id = new.id;
		
	 
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_apotek_praya.trg_obat_masuk
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `trg_obat_masuk` AFTER INSERT ON `t_obat_masuk` FOR EACH ROW BEGIN
		UPDATE t_obat 
		SET t_obat.quantity = quantity + NEW.jlh_obat_masuk,
		t_obat.tgl_jatuh_tempo = new.tgl_jatuh_tempo,
		t_obat.expire_date = new.expire_date 
		WHERE
			t_obat.id = new.id_obat;	
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for view db_apotek_praya.v_obat
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat` AS SELECT 
	t_obat.*,
	CONCAT(t_obat.quantity,",",t_obat.quantity_racik) AS jumlah,
	DATEDIFF(t_obat.expire_date, CURRENT_DATE()) AS i_expire,
	DATEDIFF(t_obat.tgl_jatuh_tempo, CURRENT_DATE()) AS i_jatuh_tempo
	from t_obat 
	where enabled = 1 ;

-- Dumping structure for view db_apotek_praya.v_obat_jatuh_tempo
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat_jatuh_tempo`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat_jatuh_tempo` AS -- v_obat_jatuh_tempo


SELECT 
 sup.namapbf,
ob.nm_obat,
ob.satuan,
ob.quantity,
ob.jenis,
 om.*,
 us.username,

 DATEDIFF(om.tgl_jatuh_tempo, CURRENT_DATE()) AS i_jatuhtempo 
 
 
 FROM t_obat_masuk AS om

INNER JOIN t_obat AS ob ON ob.id = om.id_obat
inner join t_user as us on us.id = om.id_user
INNER JOIN t_supplier AS sup ON sup.id = om.id_suppli ;

-- Dumping structure for view db_apotek_praya.v_obat_keluar
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat_keluar`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat_keluar` AS -- v_obat_keluar


SELECT 

t_obat.*,
case when t_obat.jenis='obat' then t_obat.hrga_jual else 0 end as harga_jual_obat,
case when t_obat.jenis='cream' then t_obat.hrga_jual else 0 end as harga_jual_cream,
ok.*,

ok.tot_harga + ok.biaya_dokter as totalAndDokter,
case when ok.racikan = 1 then "v" else "" end as racikan1,
jn.id as id_jenis_racikan1,
(SELECT racikan from t_obat_keluar where no_nota = ok.no_nota order by racikan desc limit 1) as adaRacikan,

 case when ok.racikan = 1 then concat (jn.diskripsi,' / ',ok.jlh_sub_racikan) else 'Non Racikan' end as diskripsi,
jn.biaya,
t_user.username,

(SELECT getBiayaServis(no_nota, 1000)) as biayaServis,
(SELECT `getBiayaBersih`(no_nota)) as totalBersih,
round((SELECT `getBiayaBersih`(no_nota)* 0.1), - 2 ) as ppn,
(select ok.jlh_sub_racikan * jn.biaya as bb from t_obat_keluar as okk where okk.no_nota = ok.no_nota order by bb desc limit 1 ) as biaya_racikan,
 -- sum( case when ok.racikan = 1 then 1000 else 0 end ) as biayaServis,

case when ok.racikan = 1 then t_obat.satuan_racik else t_obat.satuan end as satuanjual

 FROM t_obat_keluar as ok 
		JOIN t_obat ON t_obat.id = ok.id_obat
		JOIN t_user on t_user.id = ok.id_user
		join t_jenis_racikan as jn on jn.id = ok.id_jenis_racikan
	 

/*Menampilkan hnya data obat yg 1 tahun*/
 where
	DATEDIFF(CURRENT_DATE(),ok.tgl_obat_keluar) <360 
	
	group by norec
	
 ORDER BY ok.insert_at desc ;

-- Dumping structure for view db_apotek_praya.v_obat_masuk
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_obat_masuk`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_obat_masuk` AS -- v_obat_masuk

SELECT 

t_obat.*,
t_supplier.namapbf,
t_obat_masuk.no_faktur,
t_obat_masuk.tgl_fakur,
t_obat_masuk.tgl_jatuh_tempo as tgl_jatuh_tempo_real,
DATEDIFF(t_obat_masuk.tgl_jatuh_tempo, CURRENT_DATE()) as i_jatuhtempo,
t_obat_masuk.id_suppli,
t_obat_masuk.batch,
t_obat_masuk.jlh_obat_masuk,
t_obat_masuk.total_harga,
t_obat_masuk.harga_satuan,
t_obat_masuk.discount_harga,
t_obat_masuk.discount,
t_obat_masuk.harga_jual,
date_format(t_obat_masuk.create_at,'%d/%m/%Y %H:%i' ) as create_at,
t_obat_masuk.total_harga_beli,
(t_obat_masuk.jlh_obat_masuk * t_obat_masuk.harga_satuan ) as total_harga_beli1,

t_obat_masuk.total_diskon,
t_obat_masuk.total_harga_iclude_ppn,
t_obat_masuk.ppn,
round((t_obat_masuk.jlh_obat_masuk * t_obat_masuk.harga_satuan) * 0.1 ) as ppn1,
t_obat_masuk.lunas,
t_obat_masuk.tgl_bayar,
t_user.username



 FROM t_obat_masuk 
 INNER JOIN t_obat ON t_obat.id = t_obat_masuk.id_obat
 inner join t_user on t_user.id = t_obat_masuk.id_user

inner join t_supplier on t_supplier.id = t_obat_masuk.id_suppli

/*Menampilkan hnya data obat yg 1 tahun*/
 where
	DATEDIFF(CURRENT_DATE(),t_obat_masuk.tgl_fakur) <360 
	
 ORDER BY t_obat_masuk.tgl_fakur desc ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
