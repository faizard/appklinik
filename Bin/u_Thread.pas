unit u_Thread;

interface

uses
   Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Series, TeEngine, ExtCtrls, TeeProcs, Chart, DbChart, DB,
  DBAccess, MyAccess, MemDS, XPMan, sSkinManager, jpeg, sPanel, StdCtrls,
  sCheckBox, sButton, sComboBox, sLabel, sGroupBox, sEdit, Grids,
   Buttons, sSpeedButton,comobj,DBGridEh,sGauge,acProgressBar;

type
  ThreadSql = class(TThread)
  private
    ado,ado1,ado2,ado3 : TMyQuery;
    myQ       : TMyQuery;
    ThreadSql      : string;
    procedure getSql(q: TMyQuery; s:string);

    public
       property ThSQL:String read ThreadSql write ThreadSql;
       property ThMyQuery:TMyQuery read myQ write myQ;

    { Private declarations }
  protected
    procedure Execute; override;

  end;

implementation

uses
  U_DM, u_bantu;

procedure ThreadSql.getSql(q: TMyQuery; s:string);
  var
  sqli: String[6];
begin
  try
    sqli := s;

    with myQ do
    begin
        Connection:=dm.my_connection;
        Close;
        SQL.Text:= s;
//        ShowMessage(s);
//         if UpperCase(sqli)='select' then
            Open;
    end;

  except  on e:Exception do
     MessageError('syntax Cek : '+e.Message);

  end;


end;

procedure ThreadSql.Execute;
begin
        getSql(ThMyQuery, ThSQL);
end;

end.
