unit u_bantu;

 
interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Series, TeEngine, ExtCtrls, TeeProcs, Chart, DbChart, DB,
  DBAccess, MyAccess, MemDS, XPMan, sSkinManager, jpeg, sPanel, StdCtrls,
  sCheckBox, sButton, sComboBox, sLabel, sGroupBox, sEdit, Grids,
   Buttons, sSpeedButton,comobj,DBGridEh,sGauge,DBClient,StrUtils;


 
//  function GetSQL(isi: String): String; overload;
  function GetSQLThrad(isi: String; q: TMyQuery): String;
  function GetSQL(isi: String; q: TMyQuery): String;
  function saveSql(isi: String): String;

  procedure Get_delta_komitment(delta : Real; pnl: TPanel);
  function Get_OS(): Real;
  function Get_tgl_konsul(): String;
  procedure Get_NetLending();
  function Get_netlending_Cabang(): Double;
 
 
  procedure MessageInfo(msg:string);
  procedure MessageError(msg:string);
  procedure MessageWarning(msg:string);
  function DeleteConfirm(ado : TMyQuery):Boolean;
  Function Ribuan(Edit : TEdit):String;
  Function HapusFormat(Nilai:TEdit):Integer;
  procedure CleanClientDs(ds: TClientDataSet);
  procedure ClearAllEditFields;
  procedure getCombo(cbb: TsComboBox; field,tm: String);
  function getIdobat(): Longint;
  function getBiayaListrik : Integer;
  function getBiayaLainnya1 : Integer;
  function CekStokObat(val: Double; id_obat,racik : ShortString): Boolean;
//  function CekStokObat(val: Word; id_obat,racik : ShortString): Boolean;
  function IsFormOpen(const FormName : string): Boolean;

  function executeSQL(isi: String): String;

  function getRandom(): string;
  function getBiayaAdmin: Integer;
  function getBiayaPuyer(b:Byte): Integer;
  function getBiayaPot(b:Byte): Integer;

  procedure startTransaksi() ;
  procedure RollbackTransaksi()   ;
  procedure CommitTransaksi()   ;
  procedure getComboQuery(cbb: TsComboBox; field,query: String);
  function bulatkanKe500(uang: Currency): Currency ;
  function showForm(frm : TForm; tfrm: TFormClass ; fname: ShortString): Boolean;

procedure SplitText(const Delimiter: Char; // delimiter charachter
 Input: string;         // input string
 const Strings: TStrings) ; // list of string result


  var
      userlogin : String[100];
      kd_user : String[10];
      ado,ado1,ado2,ado3 : TMyQuery;
      outlet : String[2];
      kol : String[6];
      cabang : Boolean;
      plafond: Longint;
      id_nasabah,id_aom : ShortString;
      XlApp, XlBook, XlSheet: Variant;
      opendgl : TOpenDialog;
      savedlg: TSaveDialog;

      temp_jlh : Double;
      save_temp,status_barang                   : Boolean;
      temp_harga_tot,temp_harga_beli   :  Integer;
      temp_total_harga_faktur,temp_tot_hargaJual: Currency;
      temp_total_ppn,temp_total_plus_ppn        : Currency;
      temp_harga_jual,temp_idObat               : Integer;
      temp_jlh_per_pack,temp_jlh_per_racik,temp_jlhracik      : Double;
      temp_satuan_pack,temp_satuan_racik        : ShortString;
      temp_tglexpire,temp_tgl_jatuh_tempo       : TDate;
      temp_jenis, temp_bath                     : String[60];
      temp_satuanPack, temp_satuanRacik         : ShortString;
      temp_satuan,temp_namabarang,kd_obat       : ShortString;
      temp_jlhPack,jlh_obatExpire : Word;

      jlh_obatHabis, obatjatuhTempo, temp_stok  : Word;
      temp_tot_bayar, temp_tothargaBeli,temp_hargaBersih , tempTotalDiskon        : Currency;
      biayaRacik, temp_harga_beli_satuan, temp_harga_beli_pack : Integer;
      temp_diskon, temp_diskonHarga : Integer;

      cekobatExpire, cekObatHabis: Boolean;
      string1,string2,string3,string4,no_nota   : String;
      Byte1,byte2,byt23,byte4                   : Byte;
      integer1,integer2,integer3,int4,biayaAdmin: Integer;
      temp_biayaPuyer, temp_biayaPot, temp_biayaDokter  : Integer;
      pykec,pysed,pybes, ptkec,ptsed,ptbes      : String[10];
      jenisPuyer, jenisPot, id_jenis_racikkkk, id_obat : String[20];
      jenisRacikan, oldIdJenis, no_faktur, jenisLogin : String[50];
      temp_biayaServis, temp_biayaListrik,adaListrik : Integer; 


      baru  : Boolean = True;
      lama  : Boolean = False;
      errorMess  : String;


      berhasil_save,type_boolean,temp_racikanBaru  : Boolean;
      temp_racikan, adaRacikan  : Byte;
      jlhPerRacik,racikanKe     : Integer;
      temp_jlh_sub_racik        : Word;
     


      const
        cream ='CREAM';
        nonCream ='OBAT';
        enter ='#13#10';
        operator = 'O';
        admin = 'A';




  procedure CreateADO();



implementation

uses
  U_dm, ComCtrls,u_Thread;


{ Tsql }





function IsFormOpen(const FormName : string): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Screen.FormCount - 1 DownTo 0 do
    if (Screen.Forms[i].Name = FormName) then
    begin
//      Screen.Forms[i].Free;
      Result := True;
      Break;
    end;
end;

procedure ClearAllEditFields;
var
  i: Integer;
begin
//  for i := 0 to ControlCount -1 do
//  begin
//    if Controls[ i ] is TEdit then
//    begin
//      (Controls[ i ] as TEdit).Clear;
//    end;
//  end;
end;


procedure CleanClientDs(ds: TClientDataSet);
begin
   with ds do
   begin
      Open;
      First;
      while not Eof do Delete;
    end;
end;

procedure MessageInfo(msg:string);
begin
        Application.MessageBox(PChar(msg),'Info',MB_ICONINFORMATION);
end;

procedure MessageError(msg:string);
begin
        Application.MessageBox(PChar(msg),'Error',MB_ICONERROR);
end;
procedure MessageWarning(msg:string);
begin
        Application.MessageBox(PChar(msg),'Warning',MB_ICONWARNING);
end;



function DeleteConfirm(ado : TMyQuery):Boolean;
begin
try
      if  Application.MessageBox('Hapus data ?','Warning',MB_ICONWARNING + MB_YESNO) = mrYes then
       begin
            ado.Delete;
            Result := True;
       end
       else
       begin
                ado.Cancel;
                Result := False;
       end;
except
     MessageError('Gagal hapus data ...');
end;




end;





// end class ===========================================================================================================





procedure Get_delta_komitment(delta : Real; pnl: TPanel);
begin
  if (delta >=0) then
    pnl.Color:= clGreen
      else
        pnl.Color:= clRed;
end;



function GetSQLThrad(isi: String; q: TMyQuery): String;
 var
   thred: ThreadSql;
begin
    thred := ThreadSql.Create(True);
    thred.FreeOnTerminate := True;
    thred.ThSQL := isi;
    thred.ThMyQuery:= q;
    thred.Resume;
end;

procedure CreateADO();
begin
      ado := TMyQuery.Create(nil);

      ado.Connection:= dm.my_connection;

      ado1 := TMyQuery.Create(nil);
      ado1.Connection:= dm.my_connection;

      ado2 := TMyQuery.Create(nil);
      ado2.Connection:= dm.my_connection;

      ado3 := TMyQuery.Create(nil);
      ado3.Connection:= dm.my_connection;


end;

function GetSQL(isi: String; q: TMyQuery): String;
var
   sqli : String[6];
begin
//  CreateADO;
  try

    sqli := isi;
    with q do
    begin
        Connection:=dm.my_connection;
        Close;
        SQL.Text:= isi;
//        ShowMessage(sqli);
        
            Open;
    end;

  except  on e:Exception do
     MessageError('syntax Cek : '+e.Message);

  end;

end;

function saveSql(isi: String): String;
var
   sqli : String[6];
begin
  try
    sqli := isi;
    CreateADO;
    with ado do
    begin
        Connection:=dm.my_connection;
        Close;
        SQL.Text:= isi;
        Execute;
    end;

  except  on e:Exception do
     MessageError('syntax Cek : '+e.Message);

  end;

end;


function executeSQL(isi: String): String;
var
   sqli : String[6];
begin
  try
    sqli := isi;
    CreateADO;
    with ado do
    begin
        Connection:=dm.my_connection;
        Close;
        SQL.Text:= isi;
        Execute;
    end;

  except  on e:Exception do
     MessageError('syntax Cek : '+e.Message);

  end;

end;





function Get_OS(): Real;
begin

end;

function Get_tgl_konsul(): String;
var
  tgl : TDateTime;

begin
   CreateADO();
   tgl:= Now();
//   Result := FormatDateTime('dd/mm/yyyy HH:MM',tgl);
    GetSQL('SELECT * from t_time_stamp',ado);
    Result:= ado.FieldByName('waktu_konsul').AsString;


end;

function Get_netlending_Cabang(): Double;
begin
  CreateADO;
  GetSQL('SELECT `get_netlendingCab`() as net',ado);
  Result:= ado.FieldByName('net').AsFloat;

end;

procedure Get_NetLending();
var
  norek : ShortString;
  trans_pokok , net_lending: Integer;
begin
  CreateADO;
  ado.SQL.Text:= 'call sp_get_net_lending()';
  ado.Open;

  ado1.SQL.Text:='update t_pembiayaan set t_pembiayaan.net_lending = t_pembiayaan.Plafond where t_pembiayaan.tipe_kredit ="baru" and plafond<>0';
  ado1.ExecSQL;

   ado.First;
   while not ado.Eof do
   begin
     plafond := ado.FIeldByName('Plafond').AsInteger;
     norek := ado.FIeldByName('no_rekening').AsString;
     trans_pokok:= ado.FIeldByName('Pokok_trans').AsInteger;
     net_lending := plafond- trans_pokok;

      ado1.SQL.Text:='update t_pembiayaan set t_pembiayaan.tipe_kredit="TOPUP", Plafond='+IntToStr(plafond)+',  net_lending='+IntToStr(net_lending)
      +' where t_pembiayaan.no_rekening="'+norek+'"';
      
      ado1.ExecSQL;
      ado.Next;
   end;

end;

Function Ribuan(Edit : TEdit):String;
var
 NilaiRupiah: string;
 AngkaRupiah: Currency;
begin
    if Edit.Text='' then Exit;
     NilaiRupiah := Edit.text;
     NilaiRupiah := StringReplace(NilaiRupiah,',','',[rfReplaceAll,rfIgnoreCase]);
     NilaiRupiah := StringReplace(NilaiRupiah,'.','',[rfReplaceAll,rfIgnoreCase]);
     AngkaRupiah := StrToCurrDef(NilaiRupiah,0);
     Edit.Text := FormatCurr('#,###',AngkaRupiah);
     Edit.SelStart := length(Edit.text);
end;


Function HapusFormat(Nilai:TEdit):Integer;
var
  Hasil:String;
begin
  Hasil:=Nilai.Text;
  Hasil:=StringReplace(Hasil,',','',[rfReplaceAll,rfIgnoreCase]);
  Hasil:=StringReplace(Hasil,'.','',[rfReplaceAll,rfIgnoreCase]);
  Result:=StrToInt(Hasil);
end;

procedure getCombo(cbb: TsComboBox; field,tm: String);
begin
  CreateADO;
  GetSQL('select * from '+tm ,ado);
  cbb.Clear;
  with ado do
  begin
    Open;
    First;
    while not Eof do
    begin
      cbb.Items.Add(fieldbyname(field).AsString);
      Next;
    end;
  end;
end;

procedure getComboQuery(cbb: TsComboBox; field,query: String);
begin
  CreateADO;
  GetSQL(query,ado);
  cbb.Clear;
  with ado do
  begin
    Open;
    First;
    while not Eof do
    begin
      cbb.Items.Add(fieldbyname(field).AsString);
      Next;
    end;
  end;
end;






function getIdobat(): Longint;
begin
    CreateADO;
    ado.SQL.Text:='SELECT id FROM t_obat ORDER BY id DESC LIMIT 1 ';
    ado.Open;
    Result:= ado.fieldByname('id').AsInteger;
end;


function getBiayaListrik : Integer;
begin
  CreateADO;
     ado1.SQL.Text:= 'select biaya from t_biaya_lain where id=1 limit 1';
//  Result:=3000;
  ado1.Open;
   Result:= ado1.fieldbyname('biaya').AsInteger;
end;

 
function getBiayaLainnya1 : Integer;
begin
  CreateADO;
  ado1.SQL.Text:= 'select biaya from t_biaya_lain where id=2 limit 1';
     ado1.Open;
//     Result:=2000;
     Result:= ado1.fieldbyname('biaya').AsInteger;
end;

function CekStokObat(val: Double; id_obat,racik : ShortString): Boolean;
var
  stok,a : Double;
begin
  stok :=0;
  Result:= True;
  ado.SQL.Text:= 'SELECT quantity,quantity_racik,jlhper_raciik FROM t_obat WHERE id ='+id_obat;
  ado.Open;

  a:=  ado.fieldByName('quantity').AsFloat;
//  ShowMessage(racik);

  if racik = '0' then
  stok:= a
    else
    begin
      if  a > 0 then
        stok:= ado.fieldByName('jlhper_raciik').AsFloat *  a
          else
            stok:= ado.fieldByName('quantity_racik').AsFloat
    end;

//   ShowMessage(FloatToStr(stok));
//   ShowMessage(FloatToStr(val));

  if stok < val then
  begin
    MessageError('Stok obat tidak cukup ...');
    Result:= False;
    Exit;
  end;

end;

function getRandom(): string;
var
  tgl,bln,thun : string[40];
begin
    CreateADO;
    GetSQL('select ROUND(1000000*RAND()) AS rand',ado);
    tgl := FormatDateTime('dd',Now);
    bln := FormatDateTime('mm',Now);
    thun := FormatDateTime('yy',Now);

    Result := 'N'+tgl+bln+thun+ ado.fieldByName('rand').Text ;
end;

function getBiayaAdmin: Integer;
begin
     CreateADO;
     GetSQL('SELECT SUM(t_biaya_lain.biaya) AS biaya FROM t_biaya_lain',ado);
     Result:= ado.fieldbyname('biaya').AsInteger;
end;

function getBiayaPuyer(b:Byte): Integer;
begin
     CreateADO;
     GetSQL('SELECT biaya, namacetakan FROM t_puyer WHERE id ='+IntToStr(b),ado);
     jenisPuyer :=  ado.fieldbyname('namacetakan').AsString;
     Result:= ado.fieldbyname('biaya').AsInteger;
end;

function getBiayaPot(b:Byte): Integer;
begin
     CreateADO;
     GetSQL('SELECT biaya,namapot FROM t_pot WHERE id ='+IntToStr(b),ado);
     jenisPot :=  ado.fieldbyname('namapot').AsString;
     Result:= ado.fieldbyname('biaya').AsInteger;
end;

procedure startTransaksi() ;
begin
  dm.my_connection.StartTransaction;
end;

procedure CommitTransaksi() ;
begin
  dm.my_connection.Commit;
end;

procedure RollbackTransaksi()  ;
begin
  dm.my_connection.Rollback;
end;

function bulatkanKe500(uang: Currency): Currency ;
var
    sisa,uang500  : Integer;
    str           : String;
begin
    str := CurrToStr(uang);
    str := RightStr(str,3);
    uang500 := StrToInt(str);
   sisa := uang500 - 500;
   if sisa = -500 then
   begin
      Result := uang;
      Exit;
   end;

   if ( sisa = 0) then Exit;
   if sisa < 0 then
    Result := uang + Abs(sisa)
      else
        begin
            sisa := sisa - 500;
            Result := uang + Abs(sisa);
        end;



end;

function showForm(frm : TForm; tfrm: TFormClass;fname: ShortString): Boolean;
begin
      if not IsFormOpen('fname') then
      frm := tfrm.Create(frm) ;
      frm.Show;
end;


procedure SplitText(const Delimiter: Char; // delimiter charachter
 Input: string;         // input string
 const Strings: TStrings) ; // list of string result
begin
    Assert(Assigned(Strings)) ;
    Strings.Clear;
    Strings.Delimiter := Delimiter;
    Strings.DelimitedText := Input;
end;








end.
