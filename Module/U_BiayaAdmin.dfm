object frm_biaya: Tfrm_biaya
  Left = 687
  Top = 340
  BorderStyle = bsToolWindow
  Caption = 'Biaya Admin'
  ClientHeight = 199
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Trebuchet MS'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object dbg_obat: TDBGridEh
    Left = 0
    Top = 33
    Width = 635
    Height = 129
    Align = alTop
    DataSource = dm.dsbiaya
    DynProps = <>
    FixedColor = clWhite
    Flat = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Trebuchet MS'
    Font.Style = []
    HorzScrollBar.Visible = False
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
    ParentFont = False
    RowDetailPanel.Color = clBtnFace
    TabOrder = 0
    TitleParams.MultiTitle = True
    VertScrollBar.VisibleMode = sbNeverShowEh
    Columns = <
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'id'
        Footers = <>
        Title.Caption = 'Kode'
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'nm_biaya'
        Footers = <>
        Title.Caption = 'Diskripsi Biaya'
        Width = 275
      end
      item
        DisplayFormat = ',0'
        DynProps = <>
        EditButtons = <>
        FieldName = 'biaya'
        Footers = <>
        Title.Caption = 'Biaya'
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'statusenabled'
        Footers = <>
        Title.Caption = 'Status'
        Width = 105
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object sBitBtn1: TsBitBtn
    Left = 528
    Top = 168
    Width = 75
    Height = 25
    Caption = 'Tutup'
    TabOrder = 1
    OnClick = sBitBtn1Click
    SkinData.SkinSection = 'BUTTON'
  end
  object sBitBtn2: TsBitBtn
    Left = 408
    Top = 168
    Width = 107
    Height = 25
    Caption = 'Simpan'
    TabOrder = 2
    OnClick = sBitBtn2Click
    SkinData.SkinSection = 'BUTTON'
  end
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 635
    Height = 33
    Align = alTop
    Caption = 'BIAYA ADMIN'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Trebuchet MS'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    SkinData.SkinSection = 'PANEL'
  end
end
