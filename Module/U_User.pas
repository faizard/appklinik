unit U_User;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, sLabel, ExtCtrls, sPanel, StdCtrls, DBCtrls, Mask,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, Buttons,
  sBitBtn, GridsEh, DBAxisGridsEh, DBGridEh, sButton, XPMan;

type
  Tfrm_user = class(TForm)
    sPanel1: TsPanel;
    sPanel2: TsPanel;
    Label1: TLabel;
    dbedtusername: TDBEdit;
    Label2: TLabel;
    dbedtpassword: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    dbedtno_hp: TDBEdit;
    Label5: TLabel;
    dbmmoalamat: TDBMemo;
    sButton1: TsButton;
    sButton2: TsButton;
    sPanel3: TsPanel;
    dbcbbstatus: TDBComboBox;
    Label6: TLabel;
    dbg_entry_obat: TDBGridEh;
    btn_3: TsBitBtn;
    btn_refresh: TsBitBtn;
    btn_4: TsBitBtn;
    sBitBtn4: TsBitBtn;
    Label7: TLabel;
    edt1: TEdit;
    dbcbbstatus1: TDBComboBox;
    XPManifest1: TXPManifest;
    procedure sButton1Click(Sender: TObject);
    procedure btn_3Click(Sender: TObject);
    procedure btn_4Click(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure sBitBtn4Click(Sender: TObject);
    procedure dbg_entry_obatTitleClick(Column: TColumnEh);
    procedure btn_refreshClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_user: Tfrm_user;

implementation

uses U_DM, u_bantu;

{$R *.dfm}

procedure Tfrm_user.sButton1Click(Sender: TObject);
begin
  try
      if (dbedtusername.Text ='') or (dbedtpassword.Text ='') or ( dbedtpassword.Text <> edt1.Text) then
      begin
          MessageError('Password tidak cocok ..');
          edt1.SetFocus;
          Exit;
      end;

    dm.q_user.Post;
    MessageInfo('Sukses');
    dbg_entry_obat.Enabled:= True;
    sPanel3.Show;
    sPanel2.Hide;
  except
    dm.q_user.Cancel;
    MessageError('Gagal simpan data ...!');

  end;



end;

procedure Tfrm_user.btn_3Click(Sender: TObject);
begin
 DeleteConfirm(dm.q_user);
end;

procedure Tfrm_user.btn_4Click(Sender: TObject);
begin
        sPanel2.Show;
        dbg_entry_obat.Enabled:= False;
        sPanel3.Hide;
        dm.q_user.Insert;
end;

procedure Tfrm_user.sButton2Click(Sender: TObject);
begin
      dbg_entry_obat.Enabled:= true;
      sPanel3.Show;
      sPanel2.Hide;
      dm.q_user.Cancel;
end;

procedure Tfrm_user.sBitBtn4Click(Sender: TObject);
begin
    sPanel2.Show;
    dbg_entry_obat.Enabled:= False;
    sPanel3.Hide;
    dm.q_user.Edit;
end;

procedure Tfrm_user.dbg_entry_obatTitleClick(Column: TColumnEh);
var
   i: Integer;
begin
   // apply grid formatting changes here e.g. title styling
   with dbg_entry_obat do
      for i := 0 to Columns.Count - 1 do
         Columns[i].Title.Font.Style := Columns[i].Title.Font.Style - [fsBold];

 dm.q_user.SQL.Text := 'select * from t_user order by '+dbg_entry_obat.SelectedField.FieldName +' asc' ;
 dm.q_user.Open ;

   Column.Title.Font.Style := Column.Title.Font.Style + [fsBold];




end;

procedure Tfrm_user.btn_refreshClick(Sender: TObject);
begin
    GetSQL('select * from t_user',dm.q_user);
end;

end.
