unit U_ObatMasuk;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh,
  sLabel, acPNG, ExtCtrls, acImage, ImgList, acAlphaImageList, Menus,
  ActnList, XPStyleActnCtrls, ActnMan, DB, DBAccess, MyAccess, MemDS,
  Provider, DBClient, StdCtrls, sCheckBox, Buttons, sBitBtn, sComboBox,
  sGroupBox, sPanel, GridsEh, DBAxisGridsEh, DBGridEh, Mask, sMaskEdit,
  sCustomComboEdit, sToolEdit, sEdit, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinsDefaultPainters, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, sButton, sSpeedButton, ppDB, ppDBPipe,
  ppParameter, ppDesignLayer, ppBands, ppVar, ppCtrls, ppPrnabl, ppClass,
  ppCache, ppComm, ppRelatv, ppProd, ppReport, DBCtrls;

type
  Tfrm_obat_masuk = class(TForm)
    sPanel1: TsPanel;
    sPanel2: TsPanel;
    lbl_1: TLabel;
    lbl_7: TLabel;
    lbl_2: TLabel;
    lbl_18: TLabel;
    edt_kd_faktur: TsEdit;
    date_faktur: TsDateEdit;
    dbg_entry_obat: TDBGridEh;
    dbg_obat: TDBGridEh;
    edt_cari_obat: TsEdit;
    pnl_2: TsPanel;
    pnl1: TPanel;
    pnl2: TPanel;
    lbl_3: TLabel;
    lbl_5: TLabel;
    lbl_6: TLabel;
    edt_total_harga: TsEdit;
    edt_ppn: TsEdit;
    edt_tot_harga_ppn: TsEdit;
    sGroupBox2: TsGroupBox;
    chk_ppn_temp: TsCheckBox;
    cbb_1: TsComboBox;
    pnl_1: TsPanel;
    sPanel4: TsPanel;
    img3: TImage;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    edt_2: TsEdit;
    sDateEdit1: TsDateEdit;
    edt_cariobat: TsEdit;
    cbb1: TcxExtLookupComboBox;
    ds_obat_masuk1_: TClientDataSet;
    ds_obat_masuk1_norec: TLargeintField;
    ds_obat_masuk1_no_faktur: TStringField;
    ds_obat_masuk1_id_suppli: TLargeintField;
    ds_obat_masuk1_tgl_fakur: TDateTimeField;
    ds_obat_masuk1_batch: TStringField;
    ds_obat_masuk1_id_obat: TLargeintField;
    ds_obat_masuk1_expire_date: TDateField;
    ds_obat_masuk1_jlh_obat_masuk: TIntegerField;
    ds_obat_masuk1_total_harga: TLargeintField;
    ds_obat_masuk1_harga_satuan: TIntegerField;
    ds_obat_masuk1_discount_harga: TFloatField;
    ds_obat_masuk1_discount: TFloatField;
    ds_obat_masuk1_harga_jual: TFloatField;
    ds_obat_masuk1_total_harga_beli: TLargeintField;
    ds_obat_masuk1_ppn: TLargeintField;
    ds_obat_masuk1_total_diskon: TLargeintField;
    ds_obat_masuk1_total_harga_iclude_ppn: TLargeintField;
    ds_obat_masuk1_id_user: TIntegerField;
    ds_obat_masuk1_create_at: TDateTimeField;
    ds_obat_masuk1_id: TLargeintField;
    ds_obat_masuk1_nm_obat: TStringField;
    ds_obat_masuk1_satuan_racik: TStringField;
    ds_obat_masuk1_satuan: TStringField;
    ds_obat_masuk1_satuan_pack: TStringField;
    ds_obat_masuk1_hrga_jual: TIntegerField;
    ds_obat_masuk1_jenis: TStringField;
    ds_obat_masuk1_jlhper_pack: TIntegerField;
    prov_obat_masuk: TDataSetProvider;
    q_obat_masuk_dummy: TMyQuery;
    ds_obat_masuk: TMyDataSource;
    actmgr1: TActionManager;
    act_get_obat_client: TAction;
    act_obat_client: TAction;
    act_batal: TAction;
    act_hapus_entry_data: TAction;
    q_obat_masuk: TMyQuery;
    sAlphaImageList1: TsAlphaImageList;
    il1: TImageList;
    view_supplier: TcxGridViewRepository;
    view_data_supplier: TcxGridDBTableView;
    view_data_supplierid: TcxGridDBColumn;
    view_data_suppliernamapbf: TcxGridDBColumn;
    view_data_supplieralamat: TcxGridDBColumn;
    view_data_supplierno_kontak: TcxGridDBColumn;
    view_data_supplieremail: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    ds_obat: TMyDataSource;
    q_obat1: TMyQuery;
    q_obat1id: TLargeintField;
    q_obat1nm_obat: TStringField;
    q_obat1satuan_racik: TStringField;
    q_obat1satuan: TStringField;
    q_obat1satuan_pack: TStringField;
    q_obat1hrga_jual: TIntegerField;
    q_obat1jenis: TStringField;
    q_obat1jlhper_pack: TIntegerField;
    actmgr2: TActionManager;
    actentry: TAction;
    q_obat_masuknorec: TLargeintField;
    q_obat_masukno_faktur: TStringField;
    q_obat_masukid_suppli: TLargeintField;
    q_obat_masuktgl_fakur: TDateTimeField;
    q_obat_masukbatch: TStringField;
    q_obat_masukid_obat: TLargeintField;
    q_obat_masukexpire_date: TDateField;
    q_obat_masukjlh_obat_masuk: TIntegerField;
    q_obat_masuktotal_harga: TLargeintField;
    q_obat_masukharga_satuan: TIntegerField;
    q_obat_masukdiscount_harga: TFloatField;
    q_obat_masukdiscount: TFloatField;
    q_obat_masukharga_jual: TFloatField;
    q_obat_masuktotal_harga_beli: TLargeintField;
    q_obat_masukppn: TLargeintField;
    q_obat_masuktotal_diskon: TLargeintField;
    q_obat_masuktotal_harga_iclude_ppn: TLargeintField;
    q_obat_masukid_user: TIntegerField;
    q_obat_masukcreate_at: TDateTimeField;
    act_delbarang: TAction;
    sPanel5: TsPanel;
    btn_batal: TsSpeedButton;
    actobatbaru: TAction;
    ds_obat_masuk1_jlhper_raciik: TIntegerField;
    ds_obat_masuk1_tgl_jatuh_tempo: TDateField;
    q_obat_masuktgl_jatuh_tempo: TDateField;
    img4: TImage;
    btn_simpan: TsSpeedButton;
    img5: TImage;
    btn_entry: TsSpeedButton;
    sButton1: TsButton;
    pp_Print_obat_masuk: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppShape2: TppShape;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLine10: TppLine;
    ppLine11: TppLine;
    ppLine12: TppLine;
    ppLine13: TppLine;
    ppLine14: TppLine;
    ppLine16: TppLine;
    ppLabel12: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppLine18: TppLine;
    ppLabel15: TppLabel;
    ppLabel_priode: TppLabel;
    ppLabel20: TppLabel;
    ppLine20: TppLine;
    ppLabel21: TppLabel;
    ppLine21: TppLine;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppLine2: TppLine;
    ppLine3: TppLine;
    ppLine4: TppLine;
    ppLine1: TppLine;
    ppLine5: TppLine;
    ppLine6: TppLine;
    ppLine7: TppLine;
    ppLine8: TppLine;
    ppLine15: TppLine;
    ppDBCalc3: TppDBCalc;
    ppDBText8: TppDBText;
    ppLine19: TppLine;
    ppLine22: TppLine;
    ppDBText9: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppLabel9: TppLabel;
    ppSystemVariable1: TppSystemVariable;
    ppLabel10: TppLabel;
    ppSystemVariable2: TppSystemVariable;
    ppSummaryBand1: TppSummaryBand;
    ppShape1: TppShape;
    ppDBCalc1: TppDBCalc;
    ppDBCalc2: TppDBCalc;
    ppLabel8: TppLabel;
    ppLine9: TppLine;
    ppLabel11: TppLabel;
    ppLine17: TppLine;
    ppShape3: TppShape;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppShape4: TppShape;
    ppLabel19: TppLabel;
    ppLabel18: TppLabel;
    ppSystemVariable3: TppSystemVariable;
    ppLabel22: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    ppParameterList1: TppParameterList;
    print_obat: TppDBPipeline;
    q_obat_masuk_print: TMyQuery;
    pm1: TPopupMenu;
    I1: TMenuItem;
    grp_pbfbaru: TsGroupBox;
    lbl2: TLabel;
    dbedtnamapbf1: TDBEdit;
    lbl7: TLabel;
    dbmmoalamat: TDBMemo;
    lbl1: TLabel;
    dbedtnamapbf: TDBEdit;
    btn_1: TsBitBtn;
    btn_2: TsBitBtn;
    lbl8: TLabel;
    date_jatuhtempo: TsDateEdit;
    lbl9: TLabel;
    sEdit1: TsEdit;
    lbl10: TLabel;
    edt_namapabrik: TsEdit;
    q_obat_masuknama_pabrik: TStringField;
    q_obat_masuklunas: TStringField;
    q_obat_masuktgl_bayar: TDateField;
    q_obat1quantity: TFloatField;
    ds_obat_masuk1_quantity: TFloatField;
    dbg_1: TDBGridEh;
    procedure edt_cariobatChange(Sender: TObject);
    procedure ds_obat_masuk1_AfterPost(DataSet: TDataSet);
    procedure edt_total_hargaChange(Sender: TObject);
    procedure E1Click(Sender: TObject);
    procedure resetClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure saveClick(Sender: TObject);
    procedure act_delbarangExecute(Sender: TObject);
    procedure ds_obat_masuk1_BeforeDelete(DataSet: TDataSet);
    procedure actobatbaruExecute(Sender: TObject);
    procedure ds_obat_masuk1_AfterDelete(DataSet: TDataSet);
    procedure chk_ppn_tempClick(Sender: TObject);
    procedure btn_batalClick(Sender: TObject);
    procedure dbg_1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure btn_1Click(Sender: TObject);
    procedure I1Click(Sender: TObject);
    procedure btn_2Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure edt_cariobatKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbg_1CellClick(Column: TColumnEh);
  private

    function Validasi:Boolean;
    { Private declarations }
  public
    procedure ResetInputan;
    { Public declarations }
  end;

var
  frm_obat_masuk: Tfrm_obat_masuk;

implementation

uses
  U_DM, u_bantu, U_inputJumlahObat, Math;

{$R *.dfm}

procedure Tfrm_obat_masuk.ResetInputan;
begin

    CleanClientDs(ds_obat_masuk1_);
    edt_2.Clear;
    cbb_1.Text:='';
    sDateEdit1.Text:='  /  /    ';
    edt_cari_obat.Clear;
    edt_total_harga.Text:='';
    edt_ppn.Text:='';
    edt_tot_harga_ppn.Text:='';
    //      edt_2.SetFocus;

    temp_jlh:=0;
    temp_harga_tot:=0;
    temp_harga_beli:=0;
    temp_total_harga_faktur:=0;
    temp_total_ppn:=0;
    temp_total_plus_ppn:=0;
    temp_harga_jual:=0;
    temp_jlh_per_pack:=0;
    tempTotalDiskon :=0;
//    temp_satuan_pack:=0;

end;


function Tfrm_obat_masuk.Validasi:Boolean;
begin
  Result:= True;
    if (ds_obat_masuk1_.IsEmpty)
      or (edt_2.Text='')
      
      or(sDateEdit1.Text='  /  /    ')
     then
     Result:= False;

end;


procedure Tfrm_obat_masuk.edt_cariobatChange(Sender: TObject);
begin
    GetSQL('select * from v_obat where nm_obat like "%'+edt_cariobat.Text+'%" limit 20',dm.q_obat);
//ShowMessage(dm.q_obatnm_obat.Text);
end;

procedure Tfrm_obat_masuk.ds_obat_masuk1_AfterPost(DataSet: TDataSet);
begin
  try

      edt_cariobat.Text:='';
      ActiveControl:= edt_cariobat;

      
      temp_total_harga_faktur := ( temp_total_harga_faktur+temp_harga_tot)  ;
      tempTotalDiskon := tempTotalDiskon +  temp_diskonHarga;


      edt_total_harga.Text:= CurrToStr(temp_total_harga_faktur);

      if chk_ppn_temp.Checked then
      begin
        temp_total_ppn := Round(temp_total_harga_faktur /10) ;
        Ribuan(edt_ppn);
       end         
        else
          temp_total_ppn:=0;

      edt_ppn.Text:= CurrToStr(temp_total_ppn);


      
      temp_total_plus_ppn := (temp_total_harga_faktur + temp_total_ppn) - tempTotalDiskon;
      edt_tot_harga_ppn.Text:= CurrToStr(temp_total_plus_ppn);
      sEdit1.Text:= CurrToStr(tempTotalDiskon);
      Ribuan(sEdit1);
      Ribuan(edt_tot_harga_ppn);

      btn_simpan.Enabled:= True;
       

  except

  end;




end;

procedure Tfrm_obat_masuk.edt_total_hargaChange(Sender: TObject);
begin
  if  TsEdit(Sender).Text ='' then Exit;
    Ribuan(TsEdit(Sender));

    Ribuan(edt_ppn);



    
end;

procedure Tfrm_obat_masuk.E1Click(Sender: TObject);
var
  status : Byte;
begin

    if (dm.q_obat.RecordCount =0 ) then
    begin
         status_barang:= true;
         temp_idObat:= temp_idObat+ Random(10) + ds_obat_masuk1_.RecordCount;
         status := 0;
    end
      else
      begin
            if ds_obat_masuk1_.Locate('norec',dm.q_obatid.Text,[loCaseInsensitive,loPartialKey]) then
            begin
                  MessageError( dm.q_obatnm_obat.Text+ ' sudah ada di list ...');
                  Exit;
            end;
            status_barang := false;
            status:=1;
            temp_idObat:= dm.q_obatid.AsInteger;
      end;


      if not IsFormOpen('frm_input_jlhObat') then
      frm_input_jlhObat := Tfrm_input_jlhObat.Create(Self) ;

      frm_input_jlhObat.ShowModal;


      if save_temp then
      begin
            ds_obat_masuk1_.Insert;
            ds_obat_masuk1_norec.AsInteger := temp_idObat;
            ds_obat_masuk1_nm_obat.AsVariant := temp_namabarang;
            ds_obat_masuk1_jlh_obat_masuk.AsFloat := temp_jlh;
            ds_obat_masuk1_harga_satuan.AsInteger := temp_harga_beli;
            ds_obat_masuk1_hrga_jual.AsInteger := temp_harga_jual;
            ds_obat_masuk1_jenis.AsString := temp_jenis;
            ds_obat_masuk1_total_harga.AsInteger := temp_harga_tot;

//            ShowMessage(FormatDateTime('yyyy',temp_tglexpire));
             if  StrToInt(FormatDateTime('yyyy',temp_tglexpire)) > 2010 then  ds_obat_masuk1_expire_date.AsDateTime:= temp_tglexpire;
            ds_obat_masuk1_batch.AsString:= temp_bath;

            ds_obat_masuk1_jlhper_pack.AsVariant:=  temp_jlh_per_pack;
            ds_obat_masuk1_jlhper_raciik.AsVariant:=  temp_jlh_per_racik;
            ds_obat_masuk1_satuan_pack.AsVariant:=  temp_satuan_pack;
            ds_obat_masuk1_satuan.AsVariant:=  temp_satuan;
            ds_obat_masuk1_satuan_racik.AsVariant:=  temp_satuan_racik;
            ds_obat_masuk1_tgl_jatuh_tempo.AsDateTime:=  temp_tgl_jatuh_tempo;

            ds_obat_masuk1_discount_harga.AsInteger:= temp_diskonHarga;
             ds_obat_masuk1_discount.AsInteger:= temp_diskon;


            ds_obat_masuk1_total_diskon.AsInteger:=  status;
            ds_obat_masuk1_.Post;

      end;
end;



procedure Tfrm_obat_masuk.resetClick(Sender: TObject);
begin
  if Application.MessageBox('Reset inputan ?','Reset',MB_ICONWARNING + MB_YESNO) = mrYes then
    begin
      ResetInputan();
      edt_2.SetFocus;
      cbb_1.Text:='';
      cbb_1.ItemIndex :=-1;
    end;


end;

procedure Tfrm_obat_masuk.FormShow(Sender: TObject);
begin
//    ResetInputan();
      temp_idObat:= getIdobat;
      ResetInputan();
//      img1.Width:= round(Screen.Width /9);
//      img2.Width:= round(Screen.Width /9);
      sDateEdit1.Date:= Now();
      chk_ppn_temp.Checked:= True;
      GetSQL('SELECT * FROM v_obat limit 20',dm.q_obat);

      id_obat:='';
      dm.q_supplier.Open;
      

      WindowState:= wsMaximized;
end;

procedure Tfrm_obat_masuk.saveClick(Sender: TObject);
begin
  berhasil_save:= False;
  ds_obat_masuk1_.Open;
  dm.q_obat_save.Open;
  ds_obat_masuk1_.First;
  if not Validasi then
  begin
    MessageError('Silahkan lengkapi data ...');
    Exit;
  end;
  if Application.MessageBox('Pastikan inputan sudah di cek kembali ...','Save',MB_ICONQUESTION + MB_YESNO) = mrYes then
    begin
      try
        q_obat_masuk.Open;
        dm.my_connection.StartTransaction;
          while not ds_obat_masuk1_.Eof do
          begin

              if ds_obat_masuk1_total_diskon.AsInteger = 0 then       // total_disko grid untuk membedakan obat baru atau lama
              begin
                
//                dm.q_obat.Insert;
//                dm.q_obatid.AsInteger := ds_obat_masuk1_norec.AsInteger;
//                dm.q_obatnm_obat.AsString := ds_obat_masuk1_nm_obat.AsString;
//                dm.q_obatsatuan_racik.AsString := ds_obat_masuk1_satuan_racik.AsString;
//                dm.q_obatsatuan.AsString := ds_obat_masuk1_satuan.AsString;
//                dm.q_obatsatuan_pack.AsString := ds_obat_masuk1_satuan_pack.AsString;
//                dm.q_obathrga_jual.AsString := ds_obat_masuk1_hrga_jual.AsString;
//                dm.q_obatjenis.AsString := ds_obat_masuk1_jenis.AsString;
//                dm.q_obatjlhper_pack.AsString := ds_obat_masuk1_jlhper_pack.AsString;
//                dm.q_obatjlhper_raciik.AsString := ds_obat_masuk1_jlhper_pack.AsString;
//                dm.q_obat.Post;

                saveSql('INSERT INTO t_obat SET enabled=1, id = "'+ds_obat_masuk1_norec.AsString+'", nm_obat="'+ds_obat_masuk1_nm_obat.AsString+'", satuan_racik ="'
                +ds_obat_masuk1_satuan_racik.AsString+'",satuan="'+ds_obat_masuk1_satuan.AsString+'",satuan_pack="'+ds_obat_masuk1_satuan_pack.AsString+'",hrga_jual="'+ds_obat_masuk1_hrga_jual.AsString+'",jenis="'
                +ds_obat_masuk1_jenis.AsString+'",harga_satuan ='+ds_obat_masuk1_harga_satuan.Text+',jlhper_pack="'+ds_obat_masuk1_jlhper_pack.AsString+'", jlhper_raciik="'+ds_obat_masuk1_jlhper_raciik.AsString+'"')

              end;

              q_obat_masuk.Append;
//              ShowMessage('insrt obat masuk obat');
              q_obat_masukno_faktur.AsString:= edt_2.Text;
              q_obat_masukid_suppli.AsString:= dm.q_supplierid.Text;
              q_obat_masuktgl_fakur.AsDateTime:= sDateEdit1.Date;
              q_obat_masukbatch.Text:= ds_obat_masuk1_batch.Text;
               q_obat_masuknama_pabrik.Text:= edt_namapabrik.Text;
//               q_obat_masukharga_satuan.Text:= ds_obat_masuk1_batch.Text;

              q_obat_masukid_obat.Text:= ds_obat_masuk1_norec.Text;
              q_obat_masukjlh_obat_masuk.Text:= ds_obat_masuk1_jlh_obat_masuk.Text;
              q_obat_masukharga_satuan.AsInteger:= ds_obat_masuk1_harga_satuan.AsInteger;
              if  StrToInt(FormatDateTime('yyyy',ds_obat_masuk1_expire_date.AsDateTime)) > 2010 then  q_obat_masukexpire_date.AsDateTime:= ds_obat_masuk1_expire_date.AsDateTime;
              q_obat_masuktotal_harga.AsInteger:=   ds_obat_masuk1_total_harga.AsInteger;
              q_obat_masuktotal_harga_beli.AsInteger:= HapusFormat(edt_total_harga);

              q_obat_masukppn.AsInteger:=  HapusFormat(edt_ppn);
              q_obat_masuktotal_harga_iclude_ppn.AsInteger:= HapusFormat(edt_tot_harga_ppn) ;

              q_obat_masukdiscount.Text:= ds_obat_masuk1_discount.Text;
               q_obat_masukdiscount_harga.Text:= ds_obat_masuk1_discount_harga.Text;

//              if  temp_tgl_jatuh_tempo >= Now() then
//                q_obat_masuktgl_jatuh_tempo.AsDateTime:= temp_tgl_jatuh_tempo;

              if  date_jatuhtempo.Text <> '  /  /    ' then
                q_obat_masuktgl_jatuh_tempo.AsDateTime:= date_jatuhtempo.Date;

              q_obat_masukid_user.Text:= kd_user;

//              ShowMessage(kd_user);

              q_obat_masuk.Post;
              ds_obat_masuk1_.Next;
              berhasil_save:= True;
          end;
      except
           berhasil_save:= False;
      end;

      if berhasil_save then
        begin
          MessageInfo('Sukses simpan data ke database ...');
          dm.my_connection.Commit;
          ResetInputan;
          dm.q_obat.Close;
          dm.q_obat.Open;;
          edt_2.SetFocus;
        end
          else
          begin
               MessageError('Gagal simpan data ke database ...');
               dm.my_connection.Rollback;
          end;


    end;
end;

procedure Tfrm_obat_masuk.act_delbarangExecute(Sender: TObject);
begin
      
      dbg_entry_obat.SelectedRows.Delete;
end;

procedure Tfrm_obat_masuk.ds_obat_masuk1_BeforeDelete(DataSet: TDataSet);
begin
  temp_total_harga_faktur := temp_total_harga_faktur - ds_obat_masuk1_total_harga.AsInteger;
  tempTotalDiskon :=   tempTotalDiskon -  ds_obat_masuk1_discount_harga.AsInteger  ;

//  ShowMessage(IntToStr(ds_obat_masuk1_total_harga.AsInteger));

  edt_total_harga.Text:= CurrToStr(temp_total_harga_faktur);

  if chk_ppn_temp.Checked then
  begin
      temp_total_ppn := Round(temp_total_harga_faktur /10);
      Ribuan(edt_ppn);
  end
    else
      begin
         temp_total_ppn:=0;
      end;
      

  edt_ppn.Text:= CurrToStr(temp_total_ppn);
  Ribuan(edt_ppn);

  temp_total_plus_ppn := (temp_total_harga_faktur + temp_total_ppn) -tempTotalDiskon ;
  edt_tot_harga_ppn.Text:= CurrToStr(temp_total_plus_ppn);
  sEdit1.Text:= CurrToStr(tempTotalDiskon);
  Ribuan(edt_tot_harga_ppn);
  Ribuan(sEdit1);

end;

procedure Tfrm_obat_masuk.actobatbaruExecute(Sender: TObject);
begin
    status_barang:= baru;
    frm_input_jlhObat.ShowModal;
end;

procedure Tfrm_obat_masuk.ds_obat_masuk1_AfterDelete(DataSet: TDataSet);
begin
  if ds_obat_masuk1_.IsEmpty then 
  btn_simpan.Enabled:= False;
end;

procedure Tfrm_obat_masuk.chk_ppn_tempClick(Sender: TObject);
begin

  if chk_ppn_temp.Checked then
  begin
      temp_total_ppn := Round(temp_total_harga_faktur /10);
      edt_ppn.Text:= CurrToStr(temp_total_ppn);

  end
    else
      begin
         temp_total_ppn:=0;
      end;
      Ribuan(edt_ppn);
      edt_ppn.Text:= CurrToStr(temp_total_ppn);
      temp_total_plus_ppn := (temp_total_harga_faktur + temp_total_ppn) - tempTotalDiskon;
      edt_tot_harga_ppn.Text:= CurrToStr(temp_total_plus_ppn);
      Ribuan(edt_tot_harga_ppn);
end;

procedure Tfrm_obat_masuk.btn_batalClick(Sender: TObject);
begin
  if Application.MessageBox('Reset inputan ...','Reset',MB_ICONWARNING + MB_YESNO) = mrYes then
    ResetInputan();
end;



procedure Tfrm_obat_masuk.dbg_1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
 if dm.q_obat.FieldByName('quantity').AsInteger <= 0
 then
        TDBGridEh(Sender).Canvas.Brush.Color:=$00B7B7FF;
        TDBGridEh(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure Tfrm_obat_masuk.btn_1Click(Sender: TObject);
begin

    dm.q_supplier.Post;
    grp_pbfbaru.Hide;
end;

procedure Tfrm_obat_masuk.I1Click(Sender: TObject);
begin
    grp_pbfbaru.Show;
    dm.q_supplier.Open;

    dbedtnamapbf1.SetFocus;
    dm.q_supplier.Insert;
   
end;

procedure Tfrm_obat_masuk.btn_2Click(Sender: TObject);
begin
    dm.q_supplier.Cancel;
    grp_pbfbaru.Hide;
end;

procedure Tfrm_obat_masuk.FormResize(Sender: TObject);
begin
//    Application.Run;
end;

procedure Tfrm_obat_masuk.edt_cariobatKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if Key = vk_escape then TsEdit(Sender).Clear;
end;

procedure Tfrm_obat_masuk.dbg_1CellClick(Column: TColumnEh);
begin
   id_obat:= dm.q_obatid.AsString;
end;

end.
