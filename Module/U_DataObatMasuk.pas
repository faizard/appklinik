unit U_DataObatMasuk;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinsDefaultPainters, dxSkinsdxStatusBarPainter,
  dxStatusBar, PrnDbgeh, ppDB, ppDBPipe, ppParameter, ppDesignLayer,
  ppBands, ppVar, ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv,
  ppProd, ppReport, StdCtrls, sGroupBox, sComboBox, GridsEh, DBAxisGridsEh,
  DBGridEh, Mask, sMaskEdit, sCustomComboEdit, sToolEdit, Buttons, sBitBtn,
  ComCtrls, acProgressBar, ToolWin, sToolBar, sLabel, acPNG, ExtCtrls,
  acImage, sPanel, DB, DBAccess, MyAccess, MemDS, cxContainer, cxEdit,
  cxTextEdit, cxDBEdit,ShellAPI,ComObj;

type
  Tfrm_data_obat_masuk = class(TForm)
    pnl_1: TsPanel;
    sImage1: TsImage;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sPanel3: TsPanel;
    sToolBar1: TsToolBar;
    btn1: TToolButton;
    img2: TImage;
    sprog_export_obatmasuk: TsProgressBar;
    pnl_2: TsPanel;
    btn_1: TsBitBtn;
    btn_refresh: TsBitBtn;
    btn_5: TsBitBtn;
    pnl_priode: TsPanel;
    lbl_1: TsLabel;
    lbl_2: TsLabel;
    sDateEdit1: TsDateEdit;
    sDateEdit2: TsDateEdit;
    btn_2: TsBitBtn;
    btn_4: TsBitBtn;
    sPanel1: TsPanel;
    lbl_3: TLabel;
    sLabel3: TsLabel;
    dbg_entry_obat: TDBGridEh;
    DBGridEh1: TDBGridEh;
    sPanel4: TsPanel;
    cbb_jenis: TsComboBox;
    PrintDBGridEh1: TPrintDBGridEh;
    dxStatusBar1: TdxStatusBar;
    q_data_obat_masuk: TMyQuery;
    ds_obat_masuk: TMyDataSource;
    sPanel2: TsPanel;
    sLabel4: TsLabel;
    sLabel5: TsLabel;
    date_Awal: TsDateEdit;
    date_Akhir: TsDateEdit;
    DBGridEh2: TDBGridEh;
    grid_obat: TDBGridEh;
    dsdata_obatDetail: TMyDataSource;
    sPanel5: TsPanel;
    cxDBTextEdit1: TcxDBTextEdit;
    sLabel6: TsLabel;
    btn_6: TsBitBtn;
    dsCetak: TMyDataSource;
    ppDBPipeline1: TppDBPipeline;
    report_1: TppReport;
    ppParameterList2: TppParameterList;
    dlgSavedoalog: TSaveDialog;
    DBGridEh3: TDBGridEh;
    ppHeaderBand1: TppHeaderBand;
    ppShape1: TppShape;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppLabel3: TppLabel;
    ppLine23: TppLine;
    ppLine25: TppLine;
    ppLine26: TppLine;
    ppLine27: TppLine;
    ppLine28: TppLine;
    ppLine29: TppLine;
    ppLine30: TppLine;
    ppLine31: TppLine;
    ppLine32: TppLine;
    ppLine33: TppLine;
    ppLabel12: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppLine34: TppLine;
    ppLabel15: TppLabel;
    ppLabel_priode_obat_masuk: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText10: TppDBText;
    ppDBCalc2: TppDBCalc;
    ppLine1: TppLine;
    ppLine4: TppLine;
    ppLine6: TppLine;
    ppLine7: TppLine;
    ppLine9: TppLine;
    ppLine11: TppLine;
    ppLine12: TppLine;
    ppLine13: TppLine;
    ppLine14: TppLine;
    ppLine17: TppLine;
    ppLine18: TppLine;
    ppLine19: TppLine;
    ppLine15: TppLine;
    ppFooterBand1: TppFooterBand;
    ppSummaryBand1: TppSummaryBand;
    ppDBCalc1: TppDBCalc;
    ppDBCalc3: TppDBCalc;
    ppLine35: TppLine;
    ppLine36: TppLine;
    ppLine37: TppLine;
    ppLine38: TppLine;
    ppLine39: TppLine;
    ppLine41: TppLine;
    ppLine42: TppLine;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppLine3: TppLine;
    ppLine5: TppLine;
    ppLine8: TppLine;
    ppLine10: TppLine;
    ppLine20: TppLine;
    ppLine21: TppLine;
    ppLine22: TppLine;
    ppLine16: TppLine;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    q_datacetak: TMyQuery;
    ppLine2: TppLine;
    ppLine24: TppLine;
    ppLabel_grandTotal: TppLabel;
    ppLabel_ppn: TppLabel;
    q_data_obatDetail: TMyQuery;
    procedure FormShow(Sender: TObject);
    procedure date_AwalChange(Sender: TObject);
    procedure date_AkhirChange(Sender: TObject);
    procedure btn_refreshClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGridEh2CellClick(Column: TColumnEh);
    procedure btn_6Click(Sender: TObject);
    procedure btn_1Click(Sender: TObject);
    procedure btn_5Click(Sender: TObject);
    procedure btn_4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_data_obat_masuk: Tfrm_data_obat_masuk;

implementation

uses
  U_DM, u_bantu, DateUtils, U_pembayaran_tagihan;

{$R *.dfm}

procedure Tfrm_data_obat_masuk.FormShow(Sender: TObject);
begin

      GetSQL('SELECT * FROM v_obat_masuk WHERE v_obat_masuk.tgl_fakur BETWEEN "'
        +FormatDateTime('yyyy-mm-dd',date_Awal.Date)+'" AND "'
        +FormatDateTime('yyyy-mm-dd',date_Akhir.Date)+'" group by no_faktur',
        q_data_obat_masuk);

      GetSQL('CALL `sp_obat_masuk`("'+FormatDateTime('yyyy-mm-dd',date_Awal.Date)+'", "'+FormatDateTime('yyyy-mm-dd',date_Akhir.Date)+'")',q_datacetak);






        if jenisLogin = operator then
        begin
            grid_obat.Columns.Items[6].Visible:= False;
            grid_obat.Columns.Items[7].Visible:= False;
        end
          else
            begin
                grid_obat.Columns.Items[6].Visible:= True;
                grid_obat.Columns.Items[7].Visible:= True;
            end;

//        and jenis ="'+cbb_jenis.Text+'" 
end;

procedure Tfrm_data_obat_masuk.date_AwalChange(Sender: TObject);
begin
     FormShow(Sender);
end;

procedure Tfrm_data_obat_masuk.date_AkhirChange(Sender: TObject);
begin
    FormShow(Sender);
end;

procedure Tfrm_data_obat_masuk.btn_refreshClick(Sender: TObject);
begin
    FormShow(Sender);
end;

procedure Tfrm_data_obat_masuk.FormCreate(Sender: TObject);
begin
  date_Awal.Date:= StartOfTheMonth(Now);
  date_Akhir.Date:= Now;
end;

procedure Tfrm_data_obat_masuk.DBGridEh2CellClick(Column: TColumnEh);
begin
  string1:='SELECT * FROM v_obat_masuk WHERE v_obat_masuk.no_faktur ="'+q_data_obat_masuk.fieldByname('no_faktur').Text+'"';
    GetSQL(string1,q_data_obatDetail);
//    ShowMessage(string1);
    end;

procedure Tfrm_data_obat_masuk.btn_6Click(Sender: TObject);
begin

    if not IsFormOpen('frm_pembayaran') then
    frm_pembayaran := Tfrm_pembayaran.Create(Self) ;

      no_faktur := q_data_obat_masuk.fieldbyname('no_faktur').Text;
      frm_pembayaran.ShowModal;
end;

procedure Tfrm_data_obat_masuk.btn_1Click(Sender: TObject);
begin

    ado.SQL.Text :='SELECT SUM(total_harga_iclude_ppn) AS total_harga_iclude_ppn FROM ( SELECT total_harga_iclude_ppn FROM v_obat_masuk WHERE tgl_fakur BETWEEN "'
    +FormatDateTime('yyyy-mm-dd',date_Awal.Date)+'" AND "'+FormatDateTime('yyyy-mm-dd',date_Akhir.Date)+'" GROUP by no_faktur  ) as f';
    ado.Open;
    ppLabel_grandTotal.Caption:= FormatFloat('#,0',ado.fieldByName('total_harga_iclude_ppn').AsInteger);


    ado.SQL.Text :='SELECT SUM(PPN) AS PPN FROM ( SELECT PPN FROM v_obat_masuk WHERE tgl_fakur BETWEEN "'
    +FormatDateTime('yyyy-mm-dd',date_Awal.Date)+'" AND "'+FormatDateTime('yyyy-mm-dd',date_Akhir.Date)+'" GROUP by no_faktur  ) as f';
    ado.Open;
    ppLabel_ppn.Caption:= FormatFloat('#,0',ado.fieldByName('PPN').AsInteger);

    ppLabel_priode_obat_masuk.Caption := FormatDateTime('dd/mm/yyyy', date_Awal.Date) +' s/d '+ FormatDateTime('dd/mm/yyyy', date_Akhir.Date);


    report_1.PrintReport;
end;

procedure Tfrm_data_obat_masuk.btn_5Click(Sender: TObject);
Var i,x:integer;
sfile:string;
begin
  try
sprog_export_obatmasuk.Show;

if dlgSavedoalog.Execute then
begin

    sprog_export_obatmasuk.Position:=0;
    XlApp  := CreateOleObject('Excel.Application');
    XlBook := XlApp.WorkBooks.Add;
    XlSheet  := XlBook.worksheets.add;
    DBGridEh3.Hide;
    XlSheet.cells[1,1].value := 'DATA OBAT MASUK';
   for i:=0 to DBGridEh3.FieldCount-1 do
   begin
      XlSheet.cells[2,i+1].value:=DBGridEh3.columns[i].Title.Caption;
   end;
    q_datacetak.First;
  x:=1;
//      sprog_export_obatmasuk.Show;
      sprog_export_obatmasuk.Max:= q_datacetak.RecordCount;

  while not q_datacetak.Eof do
  begin
   sprog_export_obatmasuk.Position:= q_datacetak.RecNo;
   for i:=0 to DBGridEh3.FieldCount-1 do
    begin
    XlSheet.cells[2+x,i+1].value:=DBGridEh3.Fields[i].Text;
    end;
    q_datacetak.Next;
   inc(x);

  end;

end;
  sprog_export_obatmasuk.Hide;
    XlApp.ActiveWorkbook.SaveAs(dlgSavedoalog.FileName);
    XlApp.visible:=true;

   except Exit;
   end;
    

end;

procedure Tfrm_data_obat_masuk.btn_4Click(Sender: TObject);
begin
    PrintDBGridEh1.Preview;
end;

end.
