unit U_login;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, acPNG, ExtCtrls, StdCtrls, Buttons, sBitBtn, sEdit, sLabel, DB,
  MemDS, DBAccess, MyAccess, ActnList, XPStyleActnCtrls, ActnMan, sPanel;

type
  Tfrm_login = class(TForm)
    actmgr1: TActionManager;
    actlogin: TAction;
    act1: TAction;
    sPanel1: TsPanel;
    img1: TImage;
    lbl_1: TsLabel;
    lbl_2: TsLabel;
    edt_1: TsEdit;
    btn_login: TsBitBtn;
    edt_2: TsEdit;
    btn_2: TsBitBtn;
    procedure btn_2Click(Sender: TObject);
    procedure actloginExecute(Sender: TObject);
    procedure act1Execute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_login: Tfrm_login;

implementation

uses
  U_dm, u_bantu, MenuUtama;

{$R *.dfm}

procedure Tfrm_login.btn_2Click(Sender: TObject);
begin
      Close;
end;

procedure Tfrm_login.actloginExecute(Sender: TObject);
begin
  if Visible = False then Exit;
    dm.q_user.SQL.Text:= 'SELECT * FROM t_user WHERE t_user.username = "'+edt_1.Text+'" AND PASSWORD = PASSWORD("'+edt_2.Text+'")';
    dm.q_user.Open;

    if not dm.q_user.IsEmpty then
    begin
      Free;
      userlogin:= dm.q_userid.Text;;
//      Application.CreateForm(Tfrm_obat_masuk, frm_obat_masuk);
//      Application.CreateForm(Tfrm_obat_keluar, frm_obat_keluar);
      frm_menu_utama.Show;
    end else
    begin
      edt_2.Clear;
      edt_1.SetFocus ;

    end;

end;

procedure Tfrm_login.act1Execute(Sender: TObject);
begin
      Close;
end;

procedure Tfrm_login.FormCreate(Sender: TObject);
begin
    sPanel1.Left := Round(Screen.Width/2) - round(sPanel1.Width /2);
    sPanel1.Top := Round(Screen.Height/2)- round(sPanel1.Height /2);
end;

end.
