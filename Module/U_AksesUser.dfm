object frm_akses_user: Tfrm_akses_user
  Left = 382
  Top = 195
  Width = 1101
  Height = 606
  Caption = '\'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Trebuchet MS'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 16
  object dbg_obat: TDBGridEh
    Left = 0
    Top = 33
    Width = 1085
    Height = 129
    Align = alTop
    DataSource = dm.ds_user
    DynProps = <>
    FixedColor = clWhite
    Flat = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Trebuchet MS'
    Font.Style = []
    HorzScrollBar.Visible = False
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
    ParentFont = False
    RowDetailPanel.Color = clBtnFace
    TabOrder = 0
    TitleParams.MultiTitle = True
    VertScrollBar.VisibleMode = sbNeverShowEh
    Columns = <
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'id'
        Footers = <>
        Width = 91
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'username'
        Footers = <>
        Width = 220
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'password'
        Footers = <>
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'jk'
        Footers = <>
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'no_hp'
        Footers = <>
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'status'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 1085
    Height = 33
    Align = alTop
    Caption = 'AKSES USER'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Trebuchet MS'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    SkinData.SkinSection = 'PANEL'
  end
  object sPanel2: TsPanel
    Left = 0
    Top = 280
    Width = 1085
    Height = 287
    Align = alBottom
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
  end
end
