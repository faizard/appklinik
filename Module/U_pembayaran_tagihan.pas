unit U_pembayaran_tagihan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, Mask, sMaskEdit, sCustomComboEdit, sToolEdit,
  sLabel, acPNG, ExtCtrls, acImage, sPanel, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, DBCtrls, dbcgrids, DB, DBAccess, MyAccess,
  MemDS, GridsEh, DBAxisGridsEh, DBGridEh, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinsDefaultPainters, cxCheckBox, cxDBEdit, cxCurrencyEdit, cxTextEdit,
  cxMaskEdit, DBClient, Provider, cxDropDownEdit, cxCalendar, sCheckBox,
  ComCtrls, acProgressBar, Buttons, sBitBtn, Grids, DBGrids;

type
  Tfrm_pembayaran = class(TForm)
    pnl_1: TsPanel;
    sImage1: TsImage;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sPanel3: TsPanel;
    sPanel1: TsPanel;
    lbl_7: TLabel;
    lbl4: TLabel;
    date_tanggal: TsDateEdit;
    edt_nmpbf: TsEdit;
    lbl1: TLabel;
    lbl2: TLabel;
    q_data_obat_masuk: TMyQuery;
    ds_obat_masuk: TMyDataSource;
    DBCtrlGrid1: TDBCtrlGrid;
    prov_bayar_obat: TDataSetProvider;
    ds_bayar_obat: TClientDataSet;
    cx_nama: TcxDBMaskEdit;
    lbl3: TLabel;
    cx_1: TcxDBMaskEdit;
    dbchklunas: TDBCheckBox;
    lbl5: TLabel;
    cx_2: TcxDBMaskEdit;
    lbl6: TLabel;
    cx_3: TcxDBMaskEdit;
    lbl7: TLabel;
    cx_4: TcxDBMaskEdit;
    lbl8: TLabel;
    lbl9: TLabel;
    cxDBCurrencyEdit1: TcxDBCurrencyEdit;
    cxDBCurrencyEdit2: TcxDBCurrencyEdit;
    lbl10: TLabel;
    cxDBDateEdit1: TcxDBDateEdit;
    sPanel2: TsPanel;
    sProgressBar1: TsProgressBar;
    lbl11: TLabel;
    edt_3: TsEdit;
    sPanel4: TsPanel;
    btn_1: TsBitBtn;
    btn_2: TsBitBtn;
    q_data_obat_masukid: TLargeintField;
    q_data_obat_masuknm_obat: TStringField;
    q_data_obat_masuksatuan: TStringField;
    q_data_obat_masukjlhper_raciik: TIntegerField;
    q_data_obat_masuksatuan_racik: TStringField;
    q_data_obat_masukexpire_date: TDateField;
    q_data_obat_masuktgl_jatuh_tempo: TDateField;
    q_data_obat_masukhrgper_racik: TIntegerField;
    q_data_obat_masukhrga_jual: TIntegerField;
    q_data_obat_masukjenis: TStringField;
    q_data_obat_masukjlhper_pack: TIntegerField;
    q_data_obat_masuksatuan_pack: TStringField;
    q_data_obat_masukhrgper_pack: TIntegerField;
    q_data_obat_masuknamapbf: TStringField;
    q_data_obat_masukno_faktur: TStringField;
    q_data_obat_masuktgl_fakur: TDateTimeField;
    q_data_obat_masukid_suppli: TLargeintField;
    q_data_obat_masukbatch: TStringField;
    q_data_obat_masukjlh_obat_masuk: TIntegerField;
    q_data_obat_masuktotal_harga: TLargeintField;
    q_data_obat_masukharga_satuan: TIntegerField;
    q_data_obat_masukdiscount_harga: TFloatField;
    q_data_obat_masukdiscount: TFloatField;
    q_data_obat_masukharga_jual: TFloatField;
    q_data_obat_masukcreate_at: TStringField;
    q_data_obat_masuktotal_harga_beli: TLargeintField;
    q_data_obat_masuktotal_diskon: TLargeintField;
    q_data_obat_masuktotal_harga_iclude_ppn: TLargeintField;
    q_data_obat_masukppn: TLargeintField;
    q_data_obat_masuklunas: TStringField;
    q_data_obat_masuktgl_bayar: TDateField;
    q_data_obat_masukusername: TStringField;
    lbl12: TLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxCurrencyEdit2: TcxCurrencyEdit;
    cxCurrencyEdit3: TcxCurrencyEdit;
    lbl13: TLabel;
    cxCurrencyEdit4: TcxCurrencyEdit;
    chk_1: TsCheckBox;
    q_data_obat_masukquantity: TFloatField;
    q_data_obat_masukquantity_racik: TFloatField;
    q_data_obat_masukket: TMemoField;
    q_data_obat_masuktgl_jatuh_tempo_real: TDateField;
    q_data_obat_masuktotal_harga_beli1: TLargeintField;
    q_data_obat_masukppn1: TFloatField;
    ds_bayar_obatid: TLargeintField;
    ds_bayar_obatnm_obat: TStringField;
    ds_bayar_obatsatuan: TStringField;
    ds_bayar_obatjlhper_raciik: TIntegerField;
    ds_bayar_obatsatuan_racik: TStringField;
    ds_bayar_obatexpire_date: TDateField;
    ds_bayar_obattgl_jatuh_tempo: TDateField;
    ds_bayar_obathrgper_racik: TIntegerField;
    ds_bayar_obathrga_jual: TIntegerField;
    ds_bayar_obatjenis: TStringField;
    ds_bayar_obatjlhper_pack: TIntegerField;
    ds_bayar_obatsatuan_pack: TStringField;
    ds_bayar_obathrgper_pack: TIntegerField;
    ds_bayar_obatnamapbf: TStringField;
    ds_bayar_obatno_faktur: TStringField;
    ds_bayar_obattgl_fakur: TDateTimeField;
    ds_bayar_obatid_suppli: TLargeintField;
    ds_bayar_obatbatch: TStringField;
    ds_bayar_obatjlh_obat_masuk: TIntegerField;
    ds_bayar_obattotal_harga: TLargeintField;
    ds_bayar_obatharga_satuan: TIntegerField;
    ds_bayar_obatdiscount_harga: TFloatField;
    ds_bayar_obatdiscount: TFloatField;
    ds_bayar_obatharga_jual: TFloatField;
    ds_bayar_obatcreate_at: TStringField;
    ds_bayar_obattotal_harga_beli: TLargeintField;
    ds_bayar_obattotal_diskon: TLargeintField;
    ds_bayar_obattotal_harga_iclude_ppn: TLargeintField;
    ds_bayar_obatppn: TLargeintField;
    ds_bayar_obatlunas: TStringField;
    ds_bayar_obattgl_bayar: TDateField;
    ds_bayar_obatusername: TStringField;
    ds_bayar_obatquantity: TFloatField;
    ds_bayar_obatquantity_racik: TFloatField;
    ds_bayar_obatket: TMemoField;
    ds_bayar_obattgl_jatuh_tempo_real: TDateField;
    ds_bayar_obattotal_harga_beli1: TLargeintField;
    ds_bayar_obatppn1: TFloatField;
    procedure chk_1Click(Sender: TObject);
    procedure dbchklunasClick(Sender: TObject);
    procedure btn_1Click(Sender: TObject);
    procedure btn_2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_pembayaran: Tfrm_pembayaran;
  total_pembayaran : Currency;

implementation

uses
  u_bantu, U_DM, U_Obat;

{$R *.dfm}


{ Tfrm_pembayaran }

procedure Tfrm_pembayaran.chk_1Click(Sender: TObject);
var b : Byte;
begin

  total_pembayaran :=0;
  sProgressBar1.Position :=0;
  sProgressBar1.Show;
  sProgressBar1.Max := ds_bayar_obat.RecordCount;

 with ds_bayar_obat do
        begin
          Open;
          First;

          if  chk_1.Checked then
          begin

                  while not Eof do
                  begin
                      total_pembayaran := total_pembayaran + ds_bayar_obattotal_harga.AsInteger;
                      sProgressBar1.Position:= ds_bayar_obat.RecNo;
                      Edit;
                      FieldByName('lunas').AsInteger := 1;
                      FieldByName('tgl_bayar').AsDateTime := now;
                      Post;

                      Next;
                  end;

          end
            else
              begin
                  while not Eof do
                  begin
                      total_pembayaran := 0;
                      sProgressBar1.Position:= ds_bayar_obat.RecNo;
                      Edit;
                      FieldByName('lunas').AsInteger := 0;
                      FieldByName('tgl_bayar').AsString :='';
                      Post;

                      Next;
                  end;

              end;
        end;


    sProgressBar1.Hide;
    cxCurrencyEdit4.Value := total_pembayaran;
end;

procedure Tfrm_pembayaran.dbchklunasClick(Sender: TObject);
begin
    if   dbchklunas.Checked      then
    begin
        cxDBDateEdit1.Date:= Now;
        total_pembayaran := total_pembayaran + ds_bayar_obattotal_harga.AsInteger;
    end

      else
      begin
           cxDBDateEdit1.Text:='';
           total_pembayaran := total_pembayaran - ds_bayar_obattotal_harga.AsInteger;
      end;

      cxCurrencyEdit4.Value := total_pembayaran;


end;

procedure Tfrm_pembayaran.btn_1Click(Sender: TObject);
begin
  try
      dm.my_connection.StartTransaction;
      sProgressBar1.Position :=0;
      sProgressBar1.Show;
      sProgressBar1.Max := ds_bayar_obat.RecordCount;
      CreateADO;
      with ds_bayar_obat do
      begin
        Open;
        First;
        while not Eof do
        begin
          if dbchklunas.Checked then
          begin
            sProgressBar1.Position:= ds_bayar_obat.RecNo;
            ado.SQL.Text:= 'UPDATE t_obat_masuk AS ob SET ob.lunas ="'+fieldByName('lunas').AsString+'", ob.tgl_bayar ="'
                  + FormatDateTime('yyyy-mm-dd',fieldByName('tgl_bayar').AsDateTime )+'" where no_faktur ="'+edt_3.Text+'" and id_obat ="'+fieldbyName('id').AsString+'"';
            ado.ExecSQL;
//                ShowMessage(fieldByName('lunas').AsString);
          end;
          Next;
        end;
      end;
      sProgressBar1.Hide;
      dm.my_connection.Commit;
      MessageInfo('Sukses ...');
      frm_obat.cbb_obatjatuhtempo.ItemIndex := 1;
      frm_obat.cbb_obatjatuhtempoChange(Sender);
      Close;
  except
      dm.my_connection.Rollback;
       MessageError('Gagal simpan data ...');
  end;

end;

procedure Tfrm_pembayaran.btn_2Click(Sender: TObject);
begin
      q_data_obat_masuk.Cancel;
      ds_bayar_obat.Cancel;
       Close;
end;

procedure Tfrm_pembayaran.FormShow(Sender: TObject);
begin
      total_pembayaran :=0;
      CleanClientDs(ds_bayar_obat);
      GetSQL('SELECT * FROM v_obat_masuk where no_faktur ="'+no_faktur+'" and ( lunas ="0" or lunas is null or lunas ="" )',q_data_obat_masuk);

      q_data_obat_masuk.Open;
      DBCtrlGrid1.RowCount := q_data_obat_masuk.RecordCount;

      ds_bayar_obat.Close;
      ds_bayar_obat.Open;
      edt_3.Text:= q_data_obat_masukno_faktur.Text;
      edt_nmpbf.Text:= q_data_obat_masuknamapbf.Text;
      date_tanggal.Date :=q_data_obat_masuktgl_fakur.AsDateTime;
      cxCurrencyEdit1.Value:= q_data_obat_masukppn.AsCurrency;
      cxCurrencyEdit2.Value:= q_data_obat_masuktotal_harga_beli.AsCurrency;
      cxCurrencyEdit3.Value:= q_data_obat_masuktotal_harga_iclude_ppn.AsCurrency;


end;

end.
