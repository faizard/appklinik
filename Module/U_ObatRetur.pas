unit U_ObatRetur;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sLabel, acPNG, ExtCtrls, acImage, sPanel, sEdit,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, sButton,
  GridsEh, DBAxisGridsEh, DBGridEh, DB, DBAccess, MyAccess, MemDS, Mask,
  DBCtrls, Provider, DBClient;

type
  Tfrm_obat_retur = class(TForm)
    sPanel1: TsPanel;
    sImage1: TsImage;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sPanel2: TsPanel;
    q_retur: TMyQuery;
    ds_retur: TMyDataSource;
    dset_retur: TClientDataSet;
    dtstprvdrretur: TDataSetProvider;
    dset_1: TClientDataSet;
    ds_1: TMyDataSource;
    dset_1no_nota: TStringField;
    dset_1id_obat: TStringField;
    dset_1racikan: TStringField;
    dset_1nama_obat: TStringField;
    dset_1jumlah_obat_keluar: TFloatField;
    dset_1satuan: TStringField;
    dset_1harga_satuan: TFloatField;
    dset_1hargaJual: TFloatField;
    dset_1biayaPerRacikan: TIntegerField;
    dset_1total_harga: TFloatField;
    dset_1uang: TIntegerField;
    dset_1kembalian: TLargeintField;
    dset_1biaya_listrik: TIntegerField;
    dset_1biayaadmin: TIntegerField;
    dset_1tot_harga: TIntegerField;
    dset_1biayaRacik: TLargeintField;
    dset_1biayaServis: TIntegerField;
    dset_returno_nota: TStringField;
    dset_returid_obat: TStringField;
    dset_returracikan: TStringField;
    dset_returnama_obat: TStringField;
    dset_returjumlah_obat_keluar: TFloatField;
    dset_retursatuan: TStringField;
    dset_returharga_satuan: TFloatField;
    dset_returhargaJual: TFloatField;
    dset_returbiayaPerRacikan: TIntegerField;
    dset_returtotal_harga: TFloatField;
    dset_returuang: TIntegerField;
    dset_returkembalian: TLargeintField;
    dset_returbiaya_listrik: TIntegerField;
    dset_returbiayaadmin: TIntegerField;
    dset_returtot_harga: TIntegerField;
    dset_returbiayaRacik: TLargeintField;
    dset_returbiayaServis: TIntegerField;
    ScrollBox1: TScrollBox;
    pnl1: TPanel;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    sLabel5: TsLabel;
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    edt_nonota: TsEdit;
    sButton2: TsButton;
    dbedttot_harga: TDBEdit;
    dbedtbiaya_listrik: TDBEdit;
    dbedtbiayaadmin: TDBEdit;
    dbedtbiayaRacik: TDBEdit;
    DBGridEh1: TDBGridEh;
    pnl3: TPanel;
    sButton1: TsButton;
    DBGridEh2: TDBGridEh;
    dset_returhargaJualSatuan: TFloatField;
    dset_1hargaJualSatuan: TFloatField;
    sPanel3: TsPanel;
    sButton3: TsButton;
    sButton4: TsButton;
    procedure sButton2Click(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sButton4Click(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_obat_retur: Tfrm_obat_retur;

implementation

uses
  u_bantu;

{$R *.dfm}

procedure Tfrm_obat_retur.sButton2Click(Sender: TObject);
begin
      q_retur.SQL.Text:= 'CALL `sp_cetak_nota`("'+edt_nonota.Text+'",1,1000)';
      q_retur.Open;

      CreateADO;
      ado.SQL.Text:= 'SELECT `getBiayaServis`("'+edt_nonota.Text+'","1") AS x';
      ado.Open;

//      Caption:= ado.fieldByname('x').AsString;


      dset_retur.close;
      dset_retur.Open;

end;

procedure Tfrm_obat_retur.sButton1Click(Sender: TObject);
var
    jumlah  : string;
begin
  if dset_retur.RecordCount = 0 then Exit;

  if dset_returracikan.AsInteger = 0 then
    jumlah := InputBox('Jumlah','Jumlah Obat : ', dset_returjumlah_obat_keluar.AsString)
      else
        jumlah := dset_returjumlah_obat_keluar.AsString ;



//     ShowMessage(InputBox('Jumlah','Jumlah Obat : ', dset_returjumlah_obat_keluar.AsString) );

    if (jumlah = '') or (jumlah = '0') then exit;

    if Round(StrToFloat(jumlah)) > dset_returjumlah_obat_keluar.AsInteger then
    begin
      MessageError('Jumlah obat kebanyakan ...');
      Exit;
    end
      else
        begin
          if  StrToFloat(jumlah) < dset_returjumlah_obat_keluar.AsInteger then
          begin
            dset_retur.Edit;
            dset_returjumlah_obat_keluar.AsFloat:= dset_returjumlah_obat_keluar.AsFloat - StrToFloat(jumlah)  ;
            dset_retur.Post;
          end
            else
                dset_retur.Delete;

        end;

    dset_1.Insert;
    dset_1nama_obat.AsString:= dset_returnama_obat.AsString;
    dset_1no_nota.AsString:= dset_returno_nota.AsString;
    dset_1id_obat.AsString:= dset_returid_obat.AsString;
    dset_1jumlah_obat_keluar.AsString:= jumlah ;
    dset_1satuan.AsString:= dset_retursatuan.AsString;
    dset_1hargaJualSatuan.AsString:= dset_returhargaJualSatuan.AsString;
    dset_1total_harga.AsString:= dset_returtotal_harga.AsString;
    dset_1racikan.AsString:= dset_returracikan.AsString;
    dset_1.Post;





end;

procedure Tfrm_obat_retur.FormShow(Sender: TObject);
begin
      CleanClientDs(dset_1);
      CleanClientDs(dset_retur);
      WindowState := wsMaximized;
end;

procedure Tfrm_obat_retur.sButton4Click(Sender: TObject);
begin
    Close;
end;

procedure Tfrm_obat_retur.sButton3Click(Sender: TObject);
begin
  startTransaksi();

  executeSQL('delete from t_obat_keluar where no_nota ="'+edt_nonota.Text+'"');

  if not dset_retur.IsEmpty then
  begin
     dset_retur.First;
  end;



end;

end.
