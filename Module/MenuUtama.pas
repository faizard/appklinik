unit MenuUtama;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sButton, sLabel, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, ActnList, XPStyleActnCtrls, ActnMan,
  ImgList, acAlphaImageList, Menus, GridsEh, DBAxisGridsEh, DBGridEh,
  sEdit, jpeg, ExtCtrls, Buttons, sBitBtn, sPanel, sSpeedButton,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinsDefaultPainters, cxClasses, dxRibbon, acPNG, ToolWin,
  ActnCtrls, ActnMenus, AdvGlowButton, AdvToolBar, AdvShapeButton;

type
  Tfrm_menu_utama = class(TForm)
    sAlphaImageList1: TsAlphaImageList;
    actmgr1: TActionManager;
    act1: TAction;
    actmgr2: TActionManager;
    act2: TAction;
    pnl_212: TsPanel;
    pnl_3: TsPanel;
    edt_1: TsEdit;
    btn_6: TsBitBtn;
    dbg_obat: TDBGridEh;
    spanel_aktif: TsPanel;
    lbl_2: TLabel;
    sButton1: TsButton;
    sLabelFX1: TsLabelFX;
    img2: TImage;
    pm1: TPopupMenu;
    G1: TMenuItem;
    N1: TMenuItem;
    L1: TMenuItem;
    tmrNotif: TTimer;
    sLabelFX3: TsLabelFX;
    sLabelFX4: TsLabelFX;
    pm_masterData: TPopupMenu;
    M1: TMenuItem;
    A1: TMenuItem;
    N2: TMenuItem;
    sPanel2: TsPanel;
    lbl_1: TsLabel;
    sLabel1: TsLabel;
    img3: TImage;
    edt_username: TsEdit;
    btn_login: TsBitBtn;
    edt_password: TsEdit;
    sBitBtn1: TsBitBtn;
    sPanel3: TsPanel;
    N3: TMenuItem;
    S1: TMenuItem;
    tmrcek: TTimer;
    pmWarning: TPopupMenu;
    s_expired: TMenuItem;
    N4: TMenuItem;
    actLogin: TAction;
    S_Stok: TMenuItem;
    N5: TMenuItem;
    s_jatuhtempo: TMenuItem;
    N6: TMenuItem;
    j1: TMenuItem;
    sPanel4: TsPanel;
    N7: TMenuItem;
    C1: TMenuItem;
    pmObaMasuk: TPopupMenu;
    O1: TMenuItem;
    N8: TMenuItem;
    S2: TMenuItem;
    ribonmenu_utama: TAdvToolBarPager;
    AdvToolBarPager11: TAdvPage;
    AdvToolBarPager13: TAdvPage;
    AdvShapeButton1: TAdvShapeButton;
    AdvQuickAccessToolBar1: TAdvQuickAccessToolBar;
    AdvToolBarSeparator1: TAdvToolBarSeparator;
    AdvGlowMenuButton1: TAdvGlowMenuButton;
    btn_obat: TsBitBtn;
    btn_4: TsBitBtn;
    btn_5: TsBitBtn;
    btn_2: TsSpeedButton;
    btn_3: TsBitBtn;
    btn_9: TsSpeedButton;
    btn_1: TsBitBtn;
    btn_10: TsBitBtn;
    btn_11: TsBitBtn;
    btn_8: TsSpeedButton;
    btn_warning_expire: TsSpeedButton;
    sLabelFX2: TsLabelFX;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sButton1Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edt_1Exit(Sender: TObject);
    procedure act2Execute(Sender: TObject);
    procedure btn_obatClick(Sender: TObject);
    procedure btn_4Click(Sender: TObject);
    procedure btn_5Click(Sender: TObject);
    procedure btn_3Click(Sender: TObject);
    procedure btn_2Click(Sender: TObject);
    procedure tmrNotifTimer(Sender: TObject);
    procedure dbg_obatDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure edt_1Change(Sender: TObject);
    procedure M1Click(Sender: TObject);
    procedure A1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btn_loginClick(Sender: TObject);
    procedure btn_7Click(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure L1Click(Sender: TObject);
    procedure K1Click(Sender: TObject);
    procedure S1Click(Sender: TObject);
    procedure tmrcekTimer(Sender: TObject);
    procedure s_expiredClick(Sender: TObject);
    procedure S_StokClick(Sender: TObject);
    procedure j1Click(Sender: TObject);
    procedure s_jatuhtempoClick(Sender: TObject);
    procedure btn_1Click(Sender: TObject);
    procedure btn_10Click(Sender: TObject);
    procedure btn_11Click(Sender: TObject);
    procedure C1Click(Sender: TObject);
    procedure S2Click(Sender: TObject);
  private

  procedure Disconnect;
  procedure Connect;

    function CekStatus: Boolean;

    procedure cekJumlahObatExpire;
    procedure cekJlhStokObat  ;
    procedure cekObatJatuhTempo  ;
    function getJumlahNotif(a:Word):Variant;

    procedure ConnectOperator();
//    function openForm():BOOL;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_menu_utama: Tfrm_menu_utama;
  tKonek: Boolean;
    jlh_notif : Word;

implementation

uses
  U_User, u_bantu, U_DM, U_ObatMasuk, U_Obat, U_ObatKeluar,
  U_JlhObatTerjual, U_DataObatMasuk, U_BiayaAdmin, U_Satuan, DB, 
  U_JenisRacikan, U_Closing, U_graph, creamInput, U_ObatRetur, U_data_obat_keluar;

{$R *.dfm}

procedure Tfrm_menu_utama.Disconnect;
begin
//    pnl_2.Show ;
    edt_username.Clear;
    edt_password.Clear;
    btn_obat.Enabled:= False;
    btn_2.Enabled:= False;
    btn_3.Enabled:= False;
    btn_4.Enabled:= False;
    btn_5.Enabled:= False;
    btn_11.Enabled:= False;
    btn_1.Enabled:= False;
//    btn_7.Enabled:= False;
    btn_8.Enabled:= False;
    btn_9.Enabled:= False;
    btn_10.Enabled:= False;
    sPanel2.Show;
    sLabelFX1.Hide;
    sLabelFX3.Hide;
    sLabelFX4.Hide;
    tmrNotif.Enabled:= False;
//    pnl_2.Hide;
    tKonek:= False;
    img2.Hide;
    edt_username.SetFocus;

end;

function Tfrm_menu_utama.getJumlahNotif(a: Word): Variant;
begin
  jlh_notif:= jlh_notif + a;
  btn_warning_expire.Caption:= IntToStr(jlh_notif);
end;

procedure Tfrm_menu_utama.Connect;
begin
    btn_obat.Enabled:= True;
    btn_2.Enabled:= True;
    btn_3.Enabled:= True;
    btn_4.Enabled:= True;
    btn_5.Enabled:= True;
//    btn_7.Enabled:= True;
    btn_8.Enabled:= True;
    btn_9.Enabled:= True;
    btn_11.Enabled:= True;
    btn_10.Enabled:= true;
    btn_1.Enabled:= true;
    tKonek:= True;

    btn_8.Caption:= UpperCase(edt_username.Text);

        
//    pnl_2.Show;
    img2.Show;
    //        tmrNotif.Enabled:= True;
    sLabelFX1.Show;
    sLabelFX3.Show;
    sLabelFX4.Show;
    cekObatHabis:= False;
    cekobatExpire:= False;

//    frm_data_obat_masuk.grid_obat.Columns.Items[6].Visible:= true;
//    frm_data_obat_masuk.grid_obat.Columns.Items[7].Visible:= true;


    sPanel2.Hide;
    jlh_notif := 0;

end;


procedure Tfrm_menu_utama.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
                Application.Terminate;
end;

procedure Tfrm_menu_utama.sButton1Click(Sender: TObject);
begin
                frm_user.ShowModal;
end;

procedure Tfrm_menu_utama.btn1Click(Sender: TObject);
begin
//                frm_obat_keluar.Show;
end;

procedure Tfrm_menu_utama.FormShow(Sender: TObject);
begin
//        sLabelFX1.Caption:= userlogin;
      Disconnect;
//      Connect();
end;

procedure Tfrm_menu_utama.edt_1Exit(Sender: TObject);
begin
        dbg_obat.Hide;
        edt_1.Clear;
end;

procedure Tfrm_menu_utama.act2Execute(Sender: TObject);
begin
    edt_1Exit(Sender);
end;

procedure Tfrm_menu_utama.btn_obatClick(Sender: TObject);
begin
    if not IsFormOpen('frm_obat') then
    frm_obat := Tfrm_obat.Create(Self) ;

//    pnl_2.Hide;
    frm_obat.Show;

 
end;

 


procedure Tfrm_menu_utama.btn_4Click(Sender: TObject);
begin
    if not IsFormOpen('frm_data_obat_masuk') then
    frm_data_obat_masuk := Tfrm_data_obat_masuk.Create(Self) ;

//    pnl_2.Hide;
    frm_data_obat_masuk.Show;
end;

procedure Tfrm_menu_utama.btn_5Click(Sender: TObject);
begin
    if not IsFormOpen('frm_obat_keluar') then
    frm_obat_keluar := Tfrm_obat_keluar.Create(Self) ;

//    pnl_2.Hide;
    frm_obat_keluar.Show;
end;

procedure Tfrm_menu_utama.btn_3Click(Sender: TObject);
begin
    if not IsFormOpen('frm_barang_terjual') then
        frm_barang_terjual := Tfrm_barang_terjual.Create(Self) ;

//        pnl_2.Hide;
        frm_barang_terjual.Show;

end;

procedure Tfrm_menu_utama.btn_2Click(Sender: TObject);
begin

//  showForm(frm_obat_masuk,'frm_obat_masuk');
 if not IsFormOpen('frm_obat_masuk') then
    frm_obat_masuk := Tfrm_obat_masuk.Create(Self) ;

//    pnl_2.Hide;
    frm_obat_masuk.ResetInputan;
    frm_obat_masuk.Show;
end;

procedure Tfrm_menu_utama.tmrNotifTimer(Sender: TObject);
begin

    if btn_warning_expire.Visible= True then
        btn_warning_expire.Visible := false
          else
            btn_warning_expire.Visible := True ;



end;

procedure Tfrm_menu_utama.dbg_obatDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
 if dm.q_obat.FieldByName('quantity').AsInteger <= 0
 then
        TDBGridEh(Sender).Canvas.Brush.Color:=$00B7B7FF;
        TDBGridEh(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure Tfrm_menu_utama.edt_1Change(Sender: TObject);
begin

  if  edt_1.Text = '' then
  begin
    dbg_obat.Hide;
    Exit;
  end;
   GetSQL('SELECT ob.*, CONCAT(ob.quantity,",",ob.quantity_racik) AS jumlah FROM v_obat as ob where nm_obat like "%'+edt_1.Text+'%" limit 10',dm.q_obat);

    dbg_obat.Show;
end;

procedure Tfrm_menu_utama.M1Click(Sender: TObject);
begin

      if not IsFormOpen('frm_biaya') then
      frm_biaya := tfrm_biaya.Create(Self) ;

      frm_biaya.ShowModal;
end;

procedure Tfrm_menu_utama.A1Click(Sender: TObject);
begin

      if not IsFormOpen('frm_user') then
      frm_user := tfrm_user.Create(Self) ;

    frm_user.ShowModal;
end;

procedure Tfrm_menu_utama.FormCreate(Sender: TObject);
begin
    dbg_obat.Top:=52;
    dbg_obat.Left:=14;
    dbg_obat.Height:=365;
    dbg_obat.Width:=731;

    sPanel2.Left := Round(Screen.Width/2) - round(sPanel2.Width /2);
    sPanel2.Top := (Round(Screen.Height/2)- round(sPanel2.Height /2));

end;

procedure Tfrm_menu_utama.btn_loginClick(Sender: TObject);
begin


  if (Visible = False) or (sPanel2.Visible = False) then Exit;
    dm.q_user.SQL.Text:= 'SELECT * FROM t_user WHERE t_user.username = "'+edt_username.Text+'" AND PASSWORD = "'+edt_password.Text+'"';
    dm.q_user.Open;

    if not dm.q_user.IsEmpty then
    begin
      
      userlogin:= dm.q_userid.Text;
      kd_user := dm.q_userid.Text;
 
       Connect();
//      Application.CreateForm(Tfrm_obat_masuk, frm_obat_masuk);
//      Application.CreateForm(Tfrm_obat_keluar, frm_obat_keluar);
//      frm_menu_utama.Show;
      if dm.q_userstatus.AsString = operator then
      begin
        jenisLogin := operator;
        ConnectOperator();
      end
          else if dm.q_userstatus.AsString = admin then
          begin
            jenisLogin:= admin;
          end;



    end else
    begin
      edt_password.Clear;
      edt_password.SetFocus ;



    end;

end;

procedure CloseAllChildForms;
var
  I     : Integer;
begin
  for I := 0 to Application.MainForm.MDIChildCount - 1 do
    Application.MainForm.MDIChildren[I].Hide;
end;



procedure Tfrm_menu_utama.btn_7Click(Sender: TObject);
begin



//  pnl_2.Show;

 
    end;


procedure Tfrm_menu_utama.sBitBtn1Click(Sender: TObject);
begin
    Application.Terminate;
end;

procedure Tfrm_menu_utama.L1Click(Sender: TObject);
begin
//  frm_barang_terjual.Free;
    Disconnect;
end;

procedure Tfrm_menu_utama.K1Click(Sender: TObject);
begin
    if CallTerminateProcs then PostQuitMessage(0);
end;

procedure Tfrm_menu_utama.S1Click(Sender: TObject);
begin
      if not IsFormOpen('frm_satuan') then
    frm_satuan := Tfrm_satuan.Create(Self) ;

      frm_satuan.ShowModal;
end;



procedure Tfrm_menu_utama.cekJlhStokObat;
begin


end;



procedure Tfrm_menu_utama.s_expiredClick(Sender: TObject);
begin
 if not IsFormOpen('frm_obat') then
    frm_obat := Tfrm_obat.Create(Self) ;

      frm_obat.radioGroup_expire.ItemIndex := 1;
      frm_obat.radioGroup_quantity.ItemIndex := 3;
      frm_obat.radioGroup_expireClick(Sender);
     

      btn_obatClick(Sender);


    
          
end;

procedure Tfrm_menu_utama.cekObatJatuhTempo;
begin


end;


procedure Tfrm_menu_utama.cekJumlahObatExpire;
begin


end;



procedure Tfrm_menu_utama.tmrcekTimer(Sender: TObject);
begin

      CekStatus;
      sPanel3.Caption:= FormatDateTime('dddd, dd-mm-yyyy HH:mm:ss',Now);

end;



procedure Tfrm_menu_utama.S_StokClick(Sender: TObject);
begin
 if not IsFormOpen('frm_obat') then
    frm_obat := Tfrm_obat.Create(Self) ;

      frm_obat.radioGroup_quantity.ItemIndex := 1;
      frm_obat.radioGroup_expire.ItemIndex := 2;
      frm_obat.radioGroup_quantityClick(Sender);
     

      btn_obatClick(Sender);
end;



procedure Tfrm_menu_utama.j1Click(Sender: TObject);
begin
         if not IsFormOpen('frm_jenis_racikan') then
      frm_jenis_racikan := Tfrm_jenis_racikan.Create(Self) ;

    frm_jenis_racikan.ShowModal;
end;

procedure Tfrm_menu_utama.s_jatuhtempoClick(Sender: TObject);
begin

      if not IsFormOpen('frm_obat') then
      frm_obat := Tfrm_obat.Create(Self) ;

      frm_obat.cbb_obatjatuhtempo.ItemIndex := 1;
      frm_obat.cbb_obatjatuhtempoChange(Sender);

      btn_obatClick(Sender);

end;

procedure Tfrm_menu_utama.btn_1Click(Sender: TObject);
begin
      s_jatuhtempoClick(Sender); 

end;

procedure Tfrm_menu_utama.btn_10Click(Sender: TObject);
begin

    if not IsFormOpen('frm_closing') then
    frm_closing := tfrm_closing.Create(Self) ;

    frm_closing.ShowModal;
end;

procedure Tfrm_menu_utama.btn_11Click(Sender: TObject);
begin
    if not IsFormOpen('frm_graph') then
    frm_graph := Tfrm_graph.Create(Self) ;

//    pnl_2.Hide;
    frm_graph.Show;
end;

function Tfrm_menu_utama.CekStatus: Boolean;
var
  rs : Boolean;
begin
    if not tKonek then Exit;
  Result := False;
  rs:= False;



      CreateADO;


     GetSQL('CALL `sp_obat_expire`()',ado);
      ado.Open;
      jlh_obatExpire:= ado.fieldByName('jlh').AsInteger;

      if jlh_obatExpire = 0 then
      begin
        Result :=False;
//        tmrNotif.Enabled:= false;
        s_expired.Visible:= False;

      end
        else
          begin
            rs:= True;
//            jlh_notif := jlh_notif +   jlh_obatExpire;
            s_expired.Visible:= true;
            tmrNotif.Enabled:= true;
//            btn_warning_expire.Caption:= IntToStr(jlh_notif);
            s_expired.Caption:='Obat Expire ['+IntToStr(jlh_obatExpire)+']';
          end;

          
      GetSQL('SELECT count(id) as obathabis from t_obat where quantity <=0',ado);
      ado.Open;
      jlh_obatHabis:= ado.fieldByName('obathabis').AsInteger;

      if jlh_obatHabis =0 then
      begin
        Result := False;
//        tmrNotif.Enabled:= false;
        S_Stok.Visible:= False;

      end
        else
          begin
//             jlh_notif := jlh_notif +   jlh_obatHabis;
            S_Stok.Visible:= true;
            rs := True;
//            tmrNotif.Enabled:= true;
//            btn_warning_expire.Caption:= IntToStr(jlh_notif);
            S_Stok.Caption:='Stok Habis ['+IntToStr(jlh_obatHabis)+']';
          end;


      GetSQL('CALL `sp_obat_jatuhtempo`()',ado);
      ado.Open;
      obatjatuhTempo:= ado.fieldByName('jlh').AsInteger;

      if obatjatuhTempo =0 then
      begin
//        tmrNotif.Enabled:= false;
        s_jatuhtempo.Visible:= False;
        Result := False;
      end
        else
          begin
//             jlh_notif := jlh_notif +   obatjatuhTempo;
            s_jatuhtempo.Visible:= true;
            rs := True;
//            tmrNotif.Enabled:= true;
//            btn_warning_expire.Caption:= IntToStr(jlh_notif);
            s_jatuhtempo.Caption:='Obat Jatuh Tempo ['+IntToStr(obatjatuhTempo)+']';
          end;





       if rs then tmrNotif.Enabled:= True;  

end;

procedure Tfrm_menu_utama.ConnectOperator;
begin
    btn_9.Enabled:=False;
    btn_10.Enabled:=False;
    btn_2.Enabled:=False;

    btn_1.Enabled:= false;
    btn_11.Enabled:= false;


    
end;

procedure Tfrm_menu_utama.C1Click(Sender: TObject);
begin
 if not IsFormOpen('frm_inputCream') then
    frm_inputCream := Tfrm_inputCream.Create(Self) ;

    frm_inputCream.Show;
end;

procedure Tfrm_menu_utama.S2Click(Sender: TObject);
begin
    if not IsFormOpen('frm_obat_retur') then
    frm_obat_retur := Tfrm_obat_retur.Create(Self) ;
    frm_obat_retur.ShowModal;
end;

end.
procedure Tfrm_menu_utama.N7Click(Sender: TObject);
begin

end;


