object frm_koneksi: Tfrm_koneksi
  Left = 367
  Top = 156
  BorderStyle = bsToolWindow
  Caption = 'Form Koneksi'
  ClientHeight = 277
  ClientWidth = 454
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Trebuchet MS'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object lbl_1: TsLabel
    Left = 32
    Top = 56
    Width = 33
    Height = 16
    Caption = 'Server'
  end
  object lbl_2: TsLabel
    Left = 32
    Top = 88
    Width = 23
    Height = 16
    Caption = 'User'
  end
  object lbl_3: TsLabel
    Left = 32
    Top = 120
    Width = 46
    Height = 16
    Caption = 'Password'
  end
  object lbl_4: TsLabel
    Left = 32
    Top = 152
    Width = 21
    Height = 16
    Caption = 'Port'
  end
  object lbl_5: TsLabel
    Left = 32
    Top = 184
    Width = 47
    Height = 16
    Caption = 'Database'
  end
  object pnl_1: TsPanel
    Left = 0
    Top = 0
    Width = 454
    Height = 41
    Align = alTop
    Alignment = taLeftJustify
    Caption = '    SETTING KONEKSI'
    TabOrder = 0
    SkinData.SkinSection = 'TB_MENUBTN'
  end
  object edt_server: TsEdit
    Left = 96
    Top = 56
    Width = 321
    Height = 24
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Trebuchet MS'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    SkinData.SkinSection = 'FORM'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'Tahoma'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
  end
  object edt_user: TsEdit
    Left = 96
    Top = 88
    Width = 321
    Height = 24
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Trebuchet MS'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    SkinData.SkinSection = 'FORM'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'Tahoma'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
  end
  object edt_pass: TsEdit
    Left = 96
    Top = 120
    Width = 321
    Height = 24
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Trebuchet MS'
    Font.Style = []
    ParentFont = False
    PasswordChar = '0'
    TabOrder = 3
    SkinData.SkinSection = 'FORM'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'Tahoma'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
  end
  object edt_port: TsEdit
    Left = 96
    Top = 152
    Width = 321
    Height = 24
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Trebuchet MS'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    Text = '3306'
    SkinData.SkinSection = 'FORM'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'Tahoma'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
  end
  object edt_database: TsEdit
    Left = 96
    Top = 184
    Width = 321
    Height = 24
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Trebuchet MS'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    Text = 'db_klinik'
    SkinData.SkinSection = 'FORM'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'Tahoma'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
  end
  object pnl_2: TsPanel
    Left = 0
    Top = 224
    Width = 454
    Height = 53
    Align = alBottom
    TabOrder = 6
    SkinData.SkinSection = 'PANEL'
    object sButton1: TsButton
      Left = 32
      Top = 16
      Width = 121
      Height = 25
      Caption = 'Test Koneksi'
      TabOrder = 0
      OnClick = sButton1Click
      SkinData.SkinSection = 'BUTTON'
      Images = dm.ilimages
      ImageIndex = 44
    end
    object sButton2: TsButton
      Left = 160
      Top = 16
      Width = 81
      Height = 25
      Caption = 'OK'
      TabOrder = 1
      OnClick = sButton2Click
      SkinData.SkinSection = 'BUTTON'
      Images = dm.ilimages
      ImageIndex = 84
    end
    object sButton3: TsButton
      Left = 256
      Top = 16
      Width = 81
      Height = 25
      Caption = 'Batal'
      TabOrder = 2
      OnClick = sButton3Click
      SkinData.SkinSection = 'BUTTON'
      Images = dm.ilimages
      ImageIndex = 33
    end
  end
end
