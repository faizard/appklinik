unit creamInput;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sLabel, acPNG, ExtCtrls, acImage, sPanel, sEdit,
  Buttons, sBitBtn, sComboBox;

type
  Tfrm_inputCream = class(TForm)
    pnl_1: TsPanel;
    sImage1: TsImage;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sPanel1: TsPanel;
    edt_1: TsEdit;
    sLabel3: TsLabel;
    edt_2: TsEdit;
    sLabel4: TsLabel;
    edt_3: TsEdit;
    sLabel5: TsLabel;
    sBitBtn1: TsBitBtn;
    sBitBtn2: TsBitBtn;
    edt_4: TsEdit;
    sLabel6: TsLabel;
    cbb_satuanracik: TsComboBox;
    sLabel7: TsLabel;
    sLabel8: TsLabel;
    sLabel9: TsLabel;
    sLabel10: TsLabel;
    procedure edt_2Change(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_inputCream: Tfrm_inputCream;

implementation

uses
  u_bantu, U_DM;

{$R *.dfm}

procedure Tfrm_inputCream.edt_2Change(Sender: TObject);
begin
    Ribuan(TsEdit(Sender));
end;

procedure Tfrm_inputCream.sBitBtn1Click(Sender: TObject);
begin
  try
    if (cbb_satuanracik.Text = '') or
    (edt_1.Text = '') or
    (edt_2.Text = '') or
    (edt_3.Text = '') or
    (edt_4.Text = '') 
     then
    begin
      MessageError('Lengkapi data .!');
      Exit;
    end;
    with dm do
    begin
      saveSql('insert into t_obat set nm_obat = "'+edt_1.Text+'", jenis ="cream", satuan_racik ="'+cbb_satuanracik.Text+'", quantity=8388607, hrga_jual ='+IntToStr(HapusFormat(edt_3))+' , jlhper_raciik ='+edt_4.Text
      +', hrgbeli_cream='+IntToStr(HapusFormat(edt_2)));

      MessageInfo('Suksees ...');
    end;

  except on e: Exception do begin
      MessageError('Gagal simpan cream baru ...'+ enter + e.Message);
  end;

  end;
end;

procedure Tfrm_inputCream.FormShow(Sender: TObject);
begin
     getCombo(cbb_satuanracik,'satuan','t_satuan');
end;

procedure Tfrm_inputCream.sBitBtn2Click(Sender: TObject);
begin
    close;
end;

end.
