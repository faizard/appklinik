object frm_satuan: Tfrm_satuan
  Left = 922
  Top = 240
  BorderStyle = bsToolWindow
  Caption = 'Satuan'
  ClientHeight = 478
  ClientWidth = 331
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object dbg_satuan: TDBGridEh
    Left = 0
    Top = 33
    Width = 331
    Height = 380
    Align = alClient
    DataSource = dm.ds_satuan
    DynProps = <>
    FixedColor = clWhite
    Flat = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Trebuchet MS'
    Font.Style = []
    HorzScrollBar.Visible = False
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghMultiSortMarking, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
    ParentFont = False
    RowDetailPanel.Color = clBtnFace
    SearchPanel.Enabled = True
    TabOrder = 0
    TitleParams.MultiTitle = True
    VertScrollBar.VisibleMode = sbNeverShowEh
    Columns = <
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'id'
        Footers = <>
        Title.Caption = 'ID'
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'satuan'
        Footers = <>
        Title.Caption = 'Satuan'
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 331
    Height = 33
    Align = alTop
    Caption = 'MASTER SATUAN'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Trebuchet MS'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    SkinData.SkinSection = 'PANEL'
  end
  object sPanel2: TsPanel
    Left = 0
    Top = 413
    Width = 331
    Height = 65
    Align = alBottom
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object sBitBtn1: TsBitBtn
      Left = 144
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Hapus'
      TabOrder = 0
      OnClick = sBitBtn1Click
      SkinData.SkinSection = 'BUTTON'
    end
    object sBitBtn2: TsBitBtn
      Left = 24
      Top = 16
      Width = 107
      Height = 25
      Caption = 'Simpan'
      TabOrder = 1
      OnClick = sBitBtn2Click
      SkinData.SkinSection = 'BUTTON'
    end
    object sBitBtn3: TsBitBtn
      Left = 232
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Tutup'
      TabOrder = 2
      OnClick = sBitBtn3Click
      SkinData.SkinSection = 'BUTTON'
    end
  end
end
