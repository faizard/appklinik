unit U_Closing;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sLabel, acPNG, ExtCtrls, acImage, sPanel,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, Buttons,
  sBitBtn, GridsEh, DBAxisGridsEh, DBGridEh, ComCtrls, acProgressBar
  , Mask, sMaskEdit, sCustomComboEdit, sToolEdit;

type
  Tfrm_closing = class(TForm)
    pnl_1: TsPanel;
    sImage1: TsImage;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sPanel3: TsPanel;
    dbg__obat: TDBGridEh;
    sPanel1: TsPanel;
    btn_1: TsBitBtn;
    sProgressBar1: TsProgressBar;
    dtp1: TDateTimePicker;
    sLabel3: TsLabel;
    procedure btn_1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_closing: Tfrm_closing;

implementation

uses
  u_bantu, U_DM, DB;

{$R *.dfm}

procedure Tfrm_closing.btn_1Click(Sender: TObject);
var
    priode,bln,thn  : String[10];
begin
   try
        startTransaksi();
        CreateADO;
        priode := FormatDateTime('MM', dtp1.Date )+FormatDateTime('YYYY', Now());

        GetSQL('select * from t_closing where priode ="'+priode+'" limit 1',ado);
        if ado.RecordCount > 0 then
        begin
          MessageError('Closing bulan "'+FormatDateTime('MMMM', dtp1.Date)+' '+FormatDateTime('YYYY', dtp1.Date)+'" ini sudah di lakukan ... ');
          RollbackTransaksi();
          Exit;
        end;
  


        with dm.q_obat do
        begin
          sProgressBar1.Show;
          dbg__obat.Hide;
            sProgressBar1.Max:= RecordCount;
            sProgressBar1.Position:=0;
           Open;
           First;
           while not Eof do begin
              sProgressBar1.Position:=RecNo;
              ado.SQL.Text := 'INSERT INTO t_closing SET priode ="'+priode+'" ,tahun ="'+FormatDateTime('YYYY', dtp1.Date)+'", id_obat="'+
              fieldbyname('id').Text+'" , quantity = "'+
              fieldbyname('quantity').Text+'", quantity_racik = "'+
              fieldbyname('quantity_racik').Text+'", jlhper_raciik = "'+
              fieldbyname('jlhper_raciik').Text+'", id_user = "'+kd_user+'"';
             ado.ExecSQL;
        
            Next;
           end;
        end;
        CommitTransaksi();
   except  on e: Exception do begin
         MessageError('Gagal reposting data ...' +enter+ e.Message);
         RollbackTransaksi();
   end;

  end;
    sProgressBar1.Hide;
    dbg__obat.Show;

end;

procedure Tfrm_closing.FormCreate(Sender: TObject);
begin
//        sPanel2.Caption := 'Priode : '+ FormatDateTime('MMMM YYYY',Now);
end;


procedure Tfrm_closing.FormShow(Sender: TObject);
begin
     GetSQL('select * from v_obat',dm.q_obat);
//      dm.q_obat.Open;                        
end;

end.
