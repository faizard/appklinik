object frm_input_jlhObat: Tfrm_input_jlhObat
  Left = 440
  Top = 44
  BorderStyle = bsToolWindow
  Caption = 'Input Jumlah'
  ClientHeight = 585
  ClientWidth = 916
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Trebuchet MS'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 916
    Height = 201
    Align = alTop
    Color = 14408667
    TabOrder = 0
    SkinData.CustomColor = True
    SkinData.SkinSection = 'PANEL'
    object lbl_11: TsLabel
      Left = 33
      Top = 48
      Width = 92
      Height = 20
      Caption = 'Nama Barang'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
    end
    object sLabel1: TsLabel
      Left = 33
      Top = 83
      Width = 61
      Height = 20
      Caption = 'Sisa Stok'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
    end
    object sLabel8: TsLabel
      Left = 33
      Top = 153
      Width = 36
      Height = 20
      Caption = 'Jenis'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
    end
    object sLabel9: TsLabel
      Left = 33
      Top = 117
      Width = 48
      Height = 20
      Caption = 'Satuan'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
    end
    object sLabel16: TsLabel
      Left = 21
      Top = 50
      Width = 8
      Height = 13
      Caption = '*'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      UseSkinColor = False
    end
    object sLabel17: TsLabel
      Left = 21
      Top = 84
      Width = 8
      Height = 13
      Caption = '*'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      UseSkinColor = False
    end
    object sLabel18: TsLabel
      Left = 21
      Top = 120
      Width = 8
      Height = 13
      Caption = '*'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      UseSkinColor = False
    end
    object sLabel19: TsLabel
      Left = 21
      Top = 155
      Width = 8
      Height = 13
      Caption = '*'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      UseSkinColor = False
    end
    object edt_nm_obat: TsEdit
      Left = 151
      Top = 48
      Width = 359
      Height = 32
      CharCase = ecUpperCase
      Color = 15066597
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object edt_stok: TsEdit
      Left = 151
      Top = 83
      Width = 359
      Height = 32
      CharCase = ecUpperCase
      Color = 15066597
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sPanel2: TsPanel
      Left = 1
      Top = 1
      Width = 914
      Height = 31
      Align = alTop
      Caption = 'DATA OBAT'
      Color = clActiveCaption
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      SkinData.CustomColor = True
      SkinData.OuterEffects.Visibility = ovAlways
      SkinData.SkinSection = 'PANEL'
    end
    object cbb_jenis: TsComboBox
      Left = 152
      Top = 154
      Width = 359
      Height = 32
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.CustomColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      ReadOnly = True
      CharCase = ecUpperCase
      Color = 15066597
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = []
      ItemHeight = 26
      ItemIndex = -1
      ParentFont = False
      TabOrder = 3
      OnChange = cbb_jenisChange
    end
    object cbb_satuan: TsComboBox
      Left = 152
      Top = 118
      Width = 359
      Height = 32
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.CustomColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      ReadOnly = True
      Color = 15066597
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = []
      ItemHeight = 26
      ItemIndex = -1
      ParentFont = False
      TabOrder = 2
      OnChange = cbb_satuanChange
      Items.Strings = (
        'CREAM'
        'OBAT')
    end
    object sGroupBox1: TsGroupBox
      Left = 560
      Top = 32
      Width = 355
      Height = 168
      Align = alRight
      Caption = 'Satuan Pack'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      SkinData.SkinSection = 'GROUPBOX'
      object sLabel4: TsLabel
        Left = 21
        Top = 26
        Width = 51
        Height = 20
        Caption = 'Jumlah'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
      end
      object sLabel5: TsLabel
        Left = 21
        Top = 80
        Width = 23
        Height = 20
        Caption = 'Per'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
      end
      object sLabel6: TsLabel
        Left = 21
        Top = 136
        Width = 88
        Height = 20
        Caption = 'Jumlah Pack'
        ParentFont = False
        Visible = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
      end
      object sLabel20: TsLabel
        Left = 9
        Top = 26
        Width = 8
        Height = 13
        Caption = '*'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        UseSkinColor = False
      end
      object sLabel21: TsLabel
        Left = 9
        Top = 82
        Width = 8
        Height = 13
        Caption = '*'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        UseSkinColor = False
      end
      object edt_1: TsEdit
        Left = 18
        Top = 48
        Width = 143
        Height = 32
        Color = 15066597
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Text = '0'
        OnKeyPress = edt_jlh_obatKeyPress
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object cbb_satuanPack: TsComboBox
        Left = 18
        Top = 102
        Width = 253
        Height = 32
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.CustomColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        ReadOnly = True
        Color = 15066597
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ItemHeight = 26
        ItemIndex = -1
        ParentFont = False
        TabOrder = 2
      end
      object edt_jlh_pack: TsEdit
        Left = 18
        Top = 160
        Width = 251
        Height = 32
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        Text = '0'
        Visible = False
        OnChange = edt_jlh_packChange
        OnKeyPress = edt_jlh_obatKeyPress
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object edt_2: TsEdit
        Left = 168
        Top = 48
        Width = 169
        Height = 32
        CharCase = ecLowerCase
        Color = 15066597
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        OnKeyPress = edt_jlh_obatKeyPress
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
    end
  end
  object sPanel3: TsPanel
    Left = 0
    Top = 533
    Width = 916
    Height = 52
    Align = alBottom
    TabOrder = 1
    SkinData.SkinSection = 'PANEL'
    object btn_obat_client: TsBitBtn
      Left = 610
      Top = 14
      Width = 161
      Height = 25
      Caption = 'Next (Ctrl + Enter)'
      TabOrder = 0
      OnClick = btn_obat_clientClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFAFAFAEAEAEAEBEBEBFCFCFC0A8129037A1DDEEE
        E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F3F3C5C5C5A4A4A4A0A0A0AA
        AAAAA6A6A694949407822E42A05E1A8735D8EADCFFFFFFFFFFFFFFFFFFFFFFFF
        E5E5E5A8A8A8CECECEEDEDED2196511B9049158E43108A3B399E5D7FC09545A2
        61168631D5E9D9FFFFFFFFFFFFFFFFFFAFAFAFDEDEDEF3F3F3DBDBDB289A5A8F
        CAA88CC8A489C5A087C49D69B58481C19647A465158732CFE6D4FFFFFFFFFFFF
        B2B2B2F0F0F0DEDEDED4D4D4309E6293CDAC6EB98D6AB78865B58460B27F66B4
        8182C1973B9F5B038027FFFFFFFFFFFFB4B4B4F2F2F2E2E2E2D8D8D836A26A95
        CEAF93CDAC90CBA98FCBA773BB8F89C7A045A467098736F0F8F3FFFFFFFFFFFF
        B6B6B6F3F3F3E7E7E7DDDDDD3CA46E37A26D33A0672F9C6154AE7B90CBA94EAA
        73178E45F4F9F6FFFFFFFFFFFFFFFFFFB7B7B7F4F4F4EAEAEAE1E1E1DDDDDDE3
        E3E3DEDEDEC9C9C9359F6659B280279756A0A5A2FFFFFFFFFFFFFFFFFFFFFFFF
        B9B9B9F5F5F5EEEEEEE6E6E6E2E2E2E6E6E6E1E1E1CDCDCD3BA36D309E64DDE1
        DEA7A7A7FFFFFFFFFFFFFFFFFFFFFFFFBABABAF6F6F6EBEBEBDEDEDED6D6D6D5
        D5D5D1D1D1C2C2C2BBBBBBBFBFBFE5E5E5AAAAAAFFFFFFFFFFFFFFFFFFFFFFFF
        BCBCBCF7F7F7E7E7E7EFEFEFF6F6F6FBFBFBFAFAFAF0F0F0DEDEDEC2C2C2E6E6
        E6ABABABFFFFFFFFFFFFFFFFFFFFFFFFBEBEBEF8F8F8FEFEFEFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFBFBFBEAEAEAADADADFFFFFFFFFFFFFFFFFFFFFFFF
        CBCBCBE1E1E1FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBCFCF
        CFC8C8C8FFFFFFFFFFFFFFFFFFFFFFFFF4F4F4C6C6C6D0D0D0E8E8E8F3F3F3FD
        FDFDFCFCFCEDEDEDE0E0E0C1C1C1C0C0C0F6F6F6FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFDFDFDE8E8E8CFCFCFC3C3C3B7B7B7B7B7B7C2C2C2CCCCCCE9E9E9FEFE
        FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      SkinData.SkinSection = 'BUTTON'
    end
    object btn_obat_client1: TsBitBtn
      Left = 778
      Top = 14
      Width = 105
      Height = 25
      Caption = 'Batal'
      TabOrder = 1
      OnClick = btn_obat_client1Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E7F79EA9E1536AC63E
        58BF3953BE4B66C197A7DBE1E6F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFC2C8ED5465CB3B51CC7479E88E91EE8E91EE7077E4324CC03F5BBDB9C4
        E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5C9EF505FCD5B64E0A0A5F57D85EF5A
        62E9585CE77C83EE9D9FF4505CD73351B9B9C4E7FFFFFFFFFFFFFFFFFFE8EAF9
        6470D4606AE3A0ABF5535EEC4F5BEA4C58E94D58E64B55E64F55E69DA1F4535F
        D63F5CBEE2E7F5FFFFFFFFFFFFACB0EA4A55DBA1AAF65563F05165EE4C58E94C
        58E94C58E94C58E94B57E65159E69EA2F5334FC395A5DBFFFFFFFFFFFF7277DD
        808BEE7D90F75C72F34C58E94C58E94C58E94C58E94C58E94C58E94E5AE97A82
        F0747AE24B64C3FFFFFFFFFFFF6468DBA0AAF76F85F86781F6FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF4C58E95B65EA959BF13855BDFFFFFFFFFFFF696DDC
        AEB8F97E92FA6F84F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4C58E95D69
        EE959CF13C54BFFFFFFFFFFFFF7C7EE3A4AEF59CAAFA768BF0535EEC535EEC53
        5EEC535EEC535EEC535EEC6276F2808DF4777EE9556AC8FFFFFFFFFFFFB5B4F0
        7C82EACDD4FC8A9CFA7D92F77489EE6B83F66B83F66B83F66B83F66278F3A3AE
        F83D4ED09FAAE0FFFFFFFFFFFFEBEBFB7877E3A2A6F3D4DBFD8699FA7E90F079
        8DF17E93F87D91F9758BF8A7B5F8626DE35767CDE6E8F7FFFFFFFFFFFFFFFFFF
        CFCFF66F6FE1A9ACF2D8DCFDADB9FA90A2FA8A9CFA9BA8FBB9C7FC6F7AE95361
        CEC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFF67878E28D92EDBDC2F8CC
        D3F9C3CBF9A9B3F4656FE2636DD6C6CAEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFEBEBFBB6B5F07C7EE2696ADE676ADC7378DEAEB2EBE8E9F9FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      SkinData.SkinSection = 'DRAGBAR'
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 489
    Width = 916
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object lbl1: TLabel
      Left = 42
      Top = 10
      Width = 124
      Height = 24
      Caption = 'Tanggal Expire'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object sDateEdit1: TsDateEdit
      Left = 254
      Top = 10
      Width = 331
      Height = 32
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Trebuchet MS'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 0
      Text = '  /  /    '
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
    end
  end
  object sPanel4: TsPanel
    Left = 0
    Top = 201
    Width = 916
    Height = 288
    Align = alTop
    BorderWidth = 11
    TabOrder = 3
    SkinData.SkinSection = 'PANEL'
    object lbl_16: TsLabel
      Left = 22
      Top = 18
      Width = 101
      Height = 20
      Caption = 'JUMLAH OBAT'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
    end
    object lbl_8: TsLabel
      Left = 22
      Top = 55
      Width = 132
      Height = 20
      Caption = 'TOTAL HARGA BELI'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -15
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
    end
    object sLabel3: TsLabel
      Left = 22
      Top = 90
      Width = 144
      Height = 20
      Caption = 'HARGA BELI SATUAN'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -15
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
    end
    object sLabel7: TsLabel
      Left = 22
      Top = 241
      Width = 46
      Height = 20
      Caption = 'BATCH'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -15
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
    end
    object sLabel2: TsLabel
      Left = 22
      Top = 166
      Width = 89
      Height = 20
      Caption = 'HARGA JUAL'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
    end
    object sLabel12: TsLabel
      Left = 22
      Top = 204
      Width = 52
      Height = 20
      Caption = 'DISKON'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -15
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
    end
    object sLabel13: TsLabel
      Left = 294
      Top = 207
      Width = 81
      Height = 20
      Caption = '(%) PERSEN'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -15
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
    end
    object sLabel22: TsLabel
      Left = 22
      Top = 128
      Width = 100
      Height = 20
      Caption = 'HARGA / PACK'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
    end
    object edt_jlh_obat: TsEdit
      Left = 189
      Top = 18
      Width = 124
      Height = 32
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnKeyPress = edt_jlh_obatKeyPress
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object edt_tot_harga: TsEdit
      Left = 189
      Top = 54
      Width = 388
      Height = 32
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnChange = edt_tot_hargaChange
      OnExit = edt_tot_hargaExit
      OnKeyPress = edt_jlh_obatKeyPress
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object edt_hrgBeli: TsEdit
      Left = 189
      Top = 90
      Width = 388
      Height = 32
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnChange = edt_hrgBeliChange
      OnKeyPress = edt_jlh_obatKeyPress
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object edt_bath: TsEdit
      Left = 189
      Top = 240
      Width = 388
      Height = 32
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sGroupBox2: TsGroupBox
      Left = 605
      Top = 12
      Width = 299
      Height = 264
      Align = alRight
      Caption = 'Satuan Racikan'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      SkinData.SkinSection = 'GROUPBOX'
      object sLabel10: TsLabel
        Left = 19
        Top = 98
        Width = 107
        Height = 20
        Caption = 'Satuan Racikan'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
      end
      object sLabel11: TsLabel
        Left = 19
        Top = 33
        Width = 152
        Height = 20
        Caption = 'Jumlah Dalam Satuan'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
      end
      object lbl8: TLabel
        Left = 166
        Top = 80
        Width = 6
        Height = 18
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object sLabel14: TsLabel
        Left = 9
        Top = 98
        Width = 20
        Height = 13
        Caption = '*   '
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        UseSkinColor = False
      end
      object sLabel15: TsLabel
        Left = 9
        Top = 34
        Width = 20
        Height = 13
        Caption = '*   '
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        UseSkinColor = False
      end
      object cbb_satuanracik: TsComboBox
        Left = 19
        Top = 119
        Width = 253
        Height = 32
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.CustomColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ItemHeight = 26
        ItemIndex = -1
        ParentFont = False
        TabOrder = 1
      end
      object edt_jlhperRacik: TsEdit
        Left = 19
        Top = 57
        Width = 251
        Height = 32
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        OnKeyPress = edt_jlh_obatKeyPress
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
    end
    object edt_hrgajual: TsEdit
      Left = 189
      Top = 166
      Width = 388
      Height = 32
      CharCase = ecUpperCase
      Color = 15066597
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 5
      OnChange = edt_hrgajualChange
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object cbb_satuanMasuk: TsComboBox
      Left = 316
      Top = 18
      Width = 261
      Height = 32
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.CustomColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ItemHeight = 26
      ItemIndex = -1
      ParentFont = False
      Sorted = True
      TabOrder = 1
      OnChange = cbb_satuanMasukChange
      OnEnter = cbb_satuanMasukEnter
    end
    object sEdit1: TsEdit
      Left = 189
      Top = 203
      Width = 92
      Height = 32
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sEdit2: TsEdit
      Left = 189
      Top = 128
      Width = 388
      Height = 32
      CharCase = ecUpperCase
      Color = 15066597
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      OnChange = edt_hrgajualChange
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
  end
end
