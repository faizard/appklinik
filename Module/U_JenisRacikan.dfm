object frm_jenis_racikan: Tfrm_jenis_racikan
  Left = 523
  Top = 134
  BorderStyle = bsToolWindow
  Caption = 'Jenis Racikan'
  ClientHeight = 636
  ClientWidth = 672
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Trebuchet MS'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 16
  object dbg_obat: TDBGridEh
    Left = 0
    Top = 33
    Width = 489
    Height = 603
    Align = alLeft
    DataSource = dm.ds_jenisracikan
    DynProps = <>
    FixedColor = clWhite
    Flat = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Trebuchet MS'
    Font.Style = []
    HorzScrollBar.Visible = False
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
    ParentFont = False
    RowDetailPanel.Color = clBtnFace
    TabOrder = 0
    TitleParams.MultiTitle = True
    VertScrollBar.VisibleMode = sbNeverShowEh
    Columns = <
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'id'
        Footers = <>
        Title.Caption = 'Kode'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -16
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 105
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'diskripsi'
        Footers = <>
        Title.Caption = 'Diskripsi'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -16
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 206
      end
      item
        DisplayFormat = ',0'
        DynProps = <>
        EditButtons = <>
        FieldName = 'biaya'
        Footers = <>
        Title.Caption = 'Biaya'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -16
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 109
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object btn_1: TsBitBtn
    Left = 520
    Top = 112
    Width = 113
    Height = 25
    Caption = 'Tutup'
    TabOrder = 1
    OnClick = btn_1Click
    SkinData.SkinSection = 'BUTTON'
  end
  object btn_2: TsBitBtn
    Left = 520
    Top = 48
    Width = 113
    Height = 25
    Caption = 'Simpan'
    TabOrder = 2
    OnClick = btn_2Click
    SkinData.SkinSection = 'BUTTON'
  end
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 672
    Height = 33
    Align = alTop
    Caption = 'JENIS RACIKAN'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Trebuchet MS'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    SkinData.SkinSection = 'PANEL'
  end
  object btn_3: TsBitBtn
    Left = 520
    Top = 80
    Width = 113
    Height = 25
    Caption = 'Hapus'
    TabOrder = 4
    OnClick = btn_3Click
    SkinData.SkinSection = 'BUTTON'
  end
end
