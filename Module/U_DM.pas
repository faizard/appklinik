
unit U_DM;

interface

uses
  SysUtils, Classes, DB, DBAccess, MyAccess, MemDS, sSkinManager, ImgList,
  Controls, DBClient, Provider;

type
  Tdm = class(TDataModule)
    my_connection: TMyConnection;
    q_obat: TMyQuery;
    ds_obat: TMyDataSource;
    q_user: TMyQuery;
    ds_user: TMyDataSource;
    skin: TsSkinManager;
    q_userid: TIntegerField;
    q_userusername: TStringField;
    q_userpassword: TStringField;
    q_userjk: TStringField;
    q_userno_hp: TStringField;
    q_useralamat: TMemoField;
    q_userstatus: TStringField;
    ilimages: TImageList;
    q_obatid: TLargeintField;
    q_obatnm_obat: TStringField;
    q_obatsatuan_racik: TStringField;
    q_obatsatuan: TStringField;
    q_obatsatuan_pack: TStringField;
    q_obathrga_jual: TIntegerField;
    q_obatjenis: TStringField;
    q_obatjlhper_pack: TIntegerField;
    q_supplier: TMyQuery;
    ds_supplier: TMyDataSource;
    q_supplierid: TLargeintField;
    q_suppliernamapbf: TStringField;
    q_supplieralamat: TMemoField;
    q_supplierno_kontak: TStringField;
    q_supplieremail: TStringField;
    q_satuan: TMyQuery;
    ds_satuan: TMyDataSource;
    q_satuanid: TIntegerField;
    q_satuansatuan: TStringField;
    q_obatjlhper_raciik: TIntegerField;
    q_obat_save: TMyQuery;
    q_obat_saveid: TLargeintField;
    q_obat_savenm_obat: TStringField;
    q_obat_savejlhper_raciik: TIntegerField;
    q_obat_savesatuan_racik: TStringField;
    q_obat_savesatuan: TStringField;
    q_obat_savesatuan_pack: TStringField;
    q_obat_savehrga_jual: TIntegerField;
    q_obat_savejenis: TStringField;
    q_obat_savejlhper_pack: TIntegerField;
    q_biaya: TMyQuery;
    dsbiaya: TMyDataSource;
    q_obatexpire_date: TDateField;
    q_dokter: TMyQuery;
    ds_dokter: TMyDataSource;
    q_dokterid: TLargeintField;
    q_dokternm_dokter: TStringField;
    q_dokterno_hp: TStringField;
    q_dokteralamat: TMemoField;
    q_dokterjk: TStringField;
    q_obattgl_jatuh_tempo: TDateField;
    q_obathrgper_racik: TIntegerField;
    q_obathrgper_pack: TIntegerField;
    q_obatjumlah: TStringField;
    q_jenisRacikan: TMyQuery;
    ds_jenisracikan: TMyDataSource;
    q_jenisRacikanid: TIntegerField;
    q_jenisRacikandiskripsi: TStringField;
    q_jenisRacikanbiaya: TIntegerField;
    q_jenisRacikanColumna: TIntegerField;
    q_jenisRacikanColumnb: TIntegerField;
    q_dokterdranak: TStringField;
    stokObat: TDataSetProvider;
    ds_stok_obat: TClientDataSet;
    q_obatenabled: TLargeintField;
    q_obatquantity_racik: TFloatField;
    q_obatket: TMemoField;
    q_obathrgbeli_cream: TIntegerField;
    q_obatquantity: TFloatField;
    q_obat_savequantity: TFloatField;
    q_obati_expire: TLargeintField;
    q_obati_jatuh_tempo: TLargeintField;
    procedure q_useralamatGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dm: Tdm;

implementation

{$R *.dfm}

procedure Tdm.q_useralamatGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
        Text := Copy(q_useralamat.AsString, 1, 200);
end;

procedure Tdm.DataModuleCreate(Sender: TObject);
begin
    q_satuan.Open;
     q_jenisRacikan.Open;
end;

end.
