object frm_obat_keluar: Tfrm_obat_keluar
  Left = -8
  Top = -8
  Width = 1936
  Height = 1066
  Align = alClient
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Trebuchet MS'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object sToolBar1: TsToolBar
    Left = 0
    Top = 84
    Width = 1920
    Height = 11
    AutoSize = True
    ButtonHeight = 7
    ButtonWidth = 8
    Caption = 'sToolBar1'
    Customizable = True
    ShowCaptions = True
    TabOrder = 0
    SkinData.SkinSection = 'FORMTITLE'
    object btn1: TToolButton
      Left = 0
      Top = 2
      Width = 8
      Caption = 'btn1'
      Style = tbsSeparator
    end
    object img2: TImage
      Left = 8
      Top = 2
      Width = 8
      Height = 7
    end
  end
  object sprog_export_obatmasuk: TsProgressBar
    Left = 0
    Top = 95
    Width = 1920
    Height = 39
    Align = alTop
    Smooth = True
    TabOrder = 1
    Visible = False
    ProgressSkin = 'DIALOG'
    SkinData.SkinSection = 'GAUGE'
  end
  object pnl_2: TsPanel
    Left = 0
    Top = 134
    Width = 1920
    Height = 41
    Align = alTop
    TabOrder = 2
    SkinData.SkinSection = 'SCROLLBTNTOP'
    object btn_1: TsBitBtn
      Left = 16
      Top = 2
      Width = 116
      Height = 32
      Caption = 'Cetak Laporan'
      TabOrder = 0
      OnClick = btn_1Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFC89561CA9764CA9664CA9664CA9664CA9663C99663C99663CA9764C894
        61FFFFFFFFFFFFFFFFFFA1A1A1797979575757C79460F9F7F6F9F1ECF9F1EBF8
        F0E9F7EDE6F4EAE1F2E8DEFAF8F6C793602323234A4A4A9595956A6A6AA6A6A6
        B4B4B4808080AEABA9C4BFBCC4BFBCC4BFBCC4BFBCC4BFBCC4BFBCACA9A72B2B
        2BB4B4B49A9A9A2222226F6F6FB4B4B4B4B4B49494948080808080807878786D
        6D6D6060605151514242424141416D6D6DB4B4B4B4B4B4242424747474BABABA
        BABABA8C8C8CD4D4D4B8B8B8B8B8B8B8B8B8B8B8B8B8B8B8B8B8B8D3D3D38282
        82BABABABABABA292929797979D7D7D7D7D7D7969696D8D8D8BEBEBEBEBEBEBE
        BEBEBEBEBEBEBEBEBEBEBED7D7D78D8D8DD7D7D7D7D7D73E3E3E7D7D7DF9F9F9
        F9F9F9AAAAAADFDFDFCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBDFDFDFA2A2
        A2F9F9F9F9F9F9606060838383FCFCFCFCFCFCCBCBCBF2F2F2F2F2F2F2F2F2F2
        F2F2F2F2F2F2F2F2F2F2F2F2F2F2C5C5C5FCFCFCFCFCFC707070969696D2D2D2
        E8E8E87C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C
        7CE8E8E8C3C3C36C6C6CDDDDDD999999CCCCCCC78A4DF9F4EDFEE8D8FEE8D7FD
        E5D3FCE4D1FAE0C7F9DDC2FAF4EDC78449C2C2C2737373CDCDCDFFFFFFCECECE
        868686C4884BF9F4EFFEE7D7FDE7D5FCE6D2FBE1CCF8DCC1F6DABCFAF4EFC382
        47606060BCBCBCFFFFFFFFFFFFFFFFFFFBFBFBC58B4EF9F4F0FCE6D3FDE7D3FB
        E3CDFAE0C8F5D6BAF3D4B4F8F4F0C38449F9F9F9FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFC78C50F9F5F1FCE3CFFCE4CFFAE1CAF9DDC3F4E9DFF7F2ECF5EFE9C27F
        47FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC78C51F9F5F1FCE3CDFBE3CDF9
        E0C8F8DCC1FDFBF8FCE6CDE2B583D5A783FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFC4874CF7F2ECF8F4EEF8F3EDF8F3EDF8F2ECF2E6D7E2B17CDB9468FDFB
        FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7CEB8D7A97CC88B4FC88B4ECA
        9054CB8F54C4884CDCAF8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
    end
    object btn_refresh: TsBitBtn
      Left = 258
      Top = 2
      Width = 83
      Height = 32
      Caption = 'Refresh'
      TabOrder = 1
      OnClick = btn_refreshClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFDFEFD9FC2A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8ABB8F5D9C63FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF9ACAA065AF6D60A9673C8A4336823D317A363C7F426396679EBB
        A0E5EDE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAAD8AF72BC7B95D19E93CF9B8E
        CD9589CA9084C78A79BD8064AC6B4A915067966BCDDCCEFFFFFFFFFFFFFFFFFF
        FFFFFFA8DAAF78C3829DD7A69AD4A396D29E91CF998CCC9487CA8F79C1817DC3
        845CA36269986BE6EDE6FFFFFFFFFFFFFFFFFFFFFFFFA4D9AA7AC78476C18053
        AA5D4DA256489A5062AB6A82C28A86C98E81C5884F9655A0BEA2FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF9DD7A47AC683FFFFFFFFFFFFFFFFFF9BC99F5AA16185C5
        8D87C98E6EB275689C6CB8D7BBB6D4B8B3D1B6B1CEB4AFCBB1FDFEFDB4E1BAFF
        FFFFFFFFFFFFFFFFFFFFFFA3CEA7539F5A47944E3F8A46468B4D5CA9634B9B53
        47944E488F4E97BD9AFFFFFFFFFFFFFFFFFFFFFFFF92B193FCFDFCBEDFC1BBDC
        BFB9D9BCB7D6BAB5D2B784C28B7FC2888CCC9482C38953985990BA94FFFFFFFF
        FFFFFFFFFF49804C739C75FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7DEBB74BE7D
        97D2A093CF9B85C78C5DA66438853F337D392D7532488F4E448A497EA480FFFF
        FFFFFFFFFFFFFFFFFFFFEDF7EE8ECC9584C98D9AD4A38ECE9791CF998CCC9487
        CA8F82C58A7DC38478C07E468C4B87AB88FFFFFFFFFFFFFFFFFFFFFFFFDCF0DE
        8FCE9678C28288CA9193D09B94D19D8FCF988BCB9386C98E7FC3864D94538FB3
        91FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8EEB8E1BD89C89063B36B4FA5594A
        9D5244954C5FA7675AA1618CB68FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF67AA6D8ABB8FFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFABD3AFFDFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
    end
    object btn_5: TsBitBtn
      Left = 346
      Top = 2
      Width = 103
      Height = 32
      Caption = 'Export Excel'
      TabOrder = 2
      OnClick = btn_5Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFE2C0A9
        CB8D66BF703FBB6A36BB6A36BB6A36BB6936BB6935BA6934BA6934BA6834BC6D
        3AC98A62E3C2ADFFFFFFFFFFFFC47C4DF8F2EBF7ECDFF6EBDEF6EADEF6EADCF6
        EADCFAF3EBFAF3EBFAF2EAFCF7F3FCF8F4FEFEFDC2794CFFFFFFFFFFFFC1763F
        F5EBDFFCE4D1FCE4D1FCE4D1FCE4D1FCE4D1FCE4D1FCE4D1FCE4D1FCE4D1FCE4
        D1FDFBF8BB6A36FFFFFFFFFFFFC27B41F7EDE3FCE4D1FCE4D1FCE4D1FCE4D1FC
        E4D1FCE4D1FCE4D1FCE4D1FCE4D1FCE4D1FBF7F4BC6B36FFFFFFFFFFFFC57F45
        F7F0E6FCE4D1FCE4D1E5D9C1679D6F5695645896656B9F72FCE4D1FCE4D1FCE4
        D1FCF9F5C0733BFFFFFF318348197432187432187332438951609A6ABAD6C277
        BA8360AA69569563FCE2CCFBE0C9FBE1C8FDFAF7C27940FFFFFFA3C7AD1A7432
        5A9F6D48955B468F5AC7DDCD5CB57066AD74438C571A7432FCE2CDFBE1CBFBE1
        C9FBF7F2C67F44FFFFFFFFFFFF8180421E773647905CC7DDCD69BF8370B58143
        8D58B0C0A0FBE4D0FBE3CCFADFC7FADFC5FAF2EAC78347FFFFFFFFFFFFC38B4E
        609D70C4DCCC75C99672BB86428C5754925FF5E0CCFBE1CCFAE0C7F9DDC2F8DC
        C1FAF4EDC7854AFFFFFFFAFCFA708B54BFD9C881D3A26CC0895394624A955F50
        9663669967F4DCC2F8DCC1F6DABCF6D8BAFAF4EFC7864BFFFFFF80B18EB4D3BD
        9BDAB473C89453946249925E5CA37358A06D4F9663619661E9D1B3F3D4B4F1D2
        B2F8F4F0C5854BFFFFFF5A9B6D558B56529565539462A0B8948CAD822D7E412D
        7E403983473581448FB38FF7F2ECFBF7F3F5EFE9C27F47FFFFFFFFFFFFC78C51
        F9F5F1FCE3CDFBE3CEFBE3CDFBE2CBF9E0C8F8DCC1F5D6B9FDFBF8FCE6CDFAE5
        C9E2B583D5A783FFFFFFFFFFFFC99159FAF6F2FAE0C7FBE1C9FBE2C9FBE0C8F9
        DFC4F8DBC0F4D6B7FFFBF8F6D8B3E1AF7CDC9569FDFBFAFFFFFFFFFFFFD1A173
        F8F3EDF8F4EEF8F4EDF8F3EDF8F3EDF8F3EDF8F2ECF7F2ECF2E6D7E2B17CDC97
        6AFDFBFAFFFFFFFFFFFFFFFFFFE7CEB8D7A97CCC935ACA8F54CA8F54CA8F54CA
        9054CB8F54C98E54CE9C69DDB18FFDFBFAFFFFFFFFFFFFFFFFFF}
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
    end
    object pnl_priode: TsPanel
      Left = 572
      Top = 3
      Width = 445
      Height = 32
      TabOrder = 3
      Visible = False
      SkinData.SkinSection = 'MENUITEM'
      object lbl_1: TsLabel
        Left = 1
        Top = 1
        Width = 36
        Height = 30
        Align = alLeft
        Alignment = taRightJustify
        Caption = '   Dari  '
      end
      object lbl_2: TsLabel
        Left = 141
        Top = 1
        Width = 36
        Height = 30
        Align = alLeft
        Alignment = taRightJustify
        AutoSize = False
        Caption = '   s/d   '
      end
      object sDateEdit1: TsDateEdit
        Left = 37
        Top = 1
        Width = 104
        Height = 30
        AutoSize = False
        Color = clWhite
        EditMask = '!99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Trebuchet MS'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 0
        Text = '  /  /    '
        Align = alLeft
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
      end
      object sDateEdit2: TsDateEdit
        Left = 177
        Top = 1
        Width = 104
        Height = 30
        AutoSize = False
        Color = clWhite
        EditMask = '!99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Trebuchet MS'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        Text = '  /  /    '
        Align = alLeft
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
      end
      object btn_2: TsBitBtn
        Left = 296
        Top = 0
        Width = 113
        Height = 30
        Caption = 'Cetak'
        TabOrder = 2
        OnClick = btn_2Click
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
          FFFFFFC89561CA9764CA9664CA9664CA9664CA9663C99663C99663CA9764C894
          61FFFFFFFFFFFFFFFFFFA1A1A1797979575757C79460F9F7F6F9F1ECF9F1EBF8
          F0E9F7EDE6F4EAE1F2E8DEFAF8F6C793602323234A4A4A9595956A6A6AA6A6A6
          B4B4B4808080AEABA9C4BFBCC4BFBCC4BFBCC4BFBCC4BFBCC4BFBCACA9A72B2B
          2BB4B4B49A9A9A2222226F6F6FB4B4B4B4B4B49494948080808080807878786D
          6D6D6060605151514242424141416D6D6DB4B4B4B4B4B4242424747474BABABA
          BABABA8C8C8CD4D4D4B8B8B8B8B8B8B8B8B8B8B8B8B8B8B8B8B8B8D3D3D38282
          82BABABABABABA292929797979D7D7D7D7D7D7969696D8D8D8BEBEBEBEBEBEBE
          BEBEBEBEBEBEBEBEBEBEBED7D7D78D8D8DD7D7D7D7D7D73E3E3E7D7D7DF9F9F9
          F9F9F9AAAAAADFDFDFCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBDFDFDFA2A2
          A2F9F9F9F9F9F9606060838383FCFCFCFCFCFCCBCBCBF2F2F2F2F2F2F2F2F2F2
          F2F2F2F2F2F2F2F2F2F2F2F2F2F2C5C5C5FCFCFCFCFCFC707070969696D2D2D2
          E8E8E87C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C
          7CE8E8E8C3C3C36C6C6CDDDDDD999999CCCCCCC78A4DF9F4EDFEE8D8FEE8D7FD
          E5D3FCE4D1FAE0C7F9DDC2FAF4EDC78449C2C2C2737373CDCDCDFFFFFFCECECE
          868686C4884BF9F4EFFEE7D7FDE7D5FCE6D2FBE1CCF8DCC1F6DABCFAF4EFC382
          47606060BCBCBCFFFFFFFFFFFFFFFFFFFBFBFBC58B4EF9F4F0FCE6D3FDE7D3FB
          E3CDFAE0C8F5D6BAF3D4B4F8F4F0C38449F9F9F9FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFC78C50F9F5F1FCE3CFFCE4CFFAE1CAF9DDC3F4E9DFF7F2ECF5EFE9C27F
          47FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC78C51F9F5F1FCE3CDFBE3CDF9
          E0C8F8DCC1FDFBF8FCE6CDE2B583D5A783FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFC4874CF7F2ECF8F4EEF8F3EDF8F3EDF8F2ECF2E6D7E2B17CDB9468FDFB
          FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7CEB8D7A97CC88B4FC88B4ECA
          9054CB8F54C4884CDCAF8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        SkinData.SkinSection = 'SCROLLSLIDERV'
      end
    end
    object btn_4: TsBitBtn
      Left = 136
      Top = 2
      Width = 116
      Height = 32
      Caption = 'Print Table'
      TabOrder = 4
      OnClick = btn_4Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FEFEFE9B9B9B7D7D7D7878787373736F6F6F898989FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F6919191D5D3D3E2E0DFDFDCDBE1
        DFDF757575FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFCECECEA8A8A8C3C1C0D4CFCE7F7F7FACACAC9090907373736F6F6F6A6A
        6A666666626262787878CECECEB4B4B4AFAFAFABABAB9E9E9E9D9D9D99999984
        8484838383868685868584A19F9ED3CECDD3CECDE8E5E5636363ADADADE4E2E2
        D7D5D5D5D3D2D1CECDCAC3C2C8C2C1CDC9C8CCCACACCCAC9D8D6D6737272B8B0
        ADB6AEADD3CECD676767B3B3B3E0DDDDA6714CA6714CA6714CA6714CA6714CA6
        714CA6714CA6714CDAD5D4777777B9B1B0B8B0AED4CFCE6C6C6CB9B9B9DEDBDB
        B47F59CE976FD8AD90D9AE90D9AE90DAAE90D69F76A6714CD7D3D17D7D7DBAB3
        B2BAB2B0D4D0CF727272BFBFBFDFDCDCB37E58CB946DCD966ECF9870D19A71D2
        9B73D49D74A6714CD7D4D38383833AA041369336D6D1D0777777C4C4C4E1DEDC
        B27C57C7906AC9926CCB946DCD966FCF9870D19A72A6714CD9D5D48A8A8A9FC8
        A34FA855D7D3D17D7D7DCACACAE1DFDEB07B56C38D67C58F69C8916ACA936CCC
        956ECE976FA6714CDBD6D6909090C0BAB8BFB8B7D7D3D3838383CECECEE2DFDF
        AF7A55B07A55B07B56B17C57B27D57B37E58B47F59A6714CDCD8D79797978C8C
        8C898989D9D5D48A8A8AD3D3D3F1EFEFE2DFDFE2DFDFE1DFDEE1DEDDE0DDDCDF
        DCDBDEDBDBDEDBD9EDECEB9D9D9DC4BEBDC2BCBADAD6D5909090D7D7D7D3D3D3
        D0D0D0CCCCCCC8C8C8C2C2C2BEBEBEB9B9B9B4B4B4AFAFAFA9A9A9A4A4A49392
        92919090DBD7D6979797FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1F1F1D1D1D1DF
        DFDFEAEAEACFCAC9CBC5C4CAC3C2C8C2C0C7C0C0DCD9D89D9D9DFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFCFCFCDDDDDDDCDCDCE6E3E3E1DEDCDFDCDCDFDC
        DBDEDBDBEEECECA4A4A4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFE0E0E0CACACAC3C3C3BFBFBFBABABAB5B5B5AFAFAFC7C7C7}
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
    end
    object sBitBtn1: TsBitBtn
      Left = 458
      Top = 3
      Width = 103
      Height = 32
      Caption = 'Cetak Nota'
      TabOrder = 5
      OnClick = sBitBtn1Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFE2E2E2
        CBCBCBC9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9
        C9CCCCCCE2E2E2FFFFFFFFFFFFCBCBCBF9F9F9FCFCFCFCFCFCFCFCFCFCFCFCFC
        FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF9F9F9CCCCCCFFFFFFFEFEFEC9C9C9
        FCFCFCFCFCFCFCFCFCFCFCFC36007E36007EFCFCFCFCFCFC36007EFCFCFC3600
        7EFCFCFCC9C9C9FFFFFFFEFEFEC9C9C9FCFCFCFCFCFCDDDDDD36007ED9D9D9D7
        D7D736007ED4D4D436007ED4D4D436007EFCFCFCC9C9C9FEFEFEFEFEFEC9C9C9
        FCFCFCFCFCFCFCFCFC36007EFBFBFBFBFBFB36007EFAFAFAFAFAFA36007EFAFA
        FAFCFCFCC9C9C9FEFEFEFEFEFEC9C9C9FCFCFCFCFCFCD6D6D636007ECFCFCFCD
        CDCD36007EC8C8C836007EC5C5C536007EFCFCFCC9C9C9FEFEFEFEFEFEC9C9C9
        FCFCFCFCFCFCFCFCFC36007EFCFCFCFCFCFC36007EFBFBFB36007EF9F9F93600
        7EFCFCFCC9C9C9FEFEFEFEFEFEC9C9C9FCFCFCFCFCFCCECECE36007EC5C5C5C2
        C2C236007EBCBCBCBBBBBBB9B9B9F6F6F6FCFCFCC9C9C9FEFEFEFEFEFEC9C9C9
        FCFCFCFCFCFCFCFCFCFCFCFC36007E36007EFBFBFBF8F8F8F6F6F6F3F3F3F2F2
        F2FCFCFCC9C9C9FEFEFEFEFEFEC9C9C9FCFCFCFCFCFCC7C7C7C1C1C1BDBDBDB7
        B7B7B3B3B3B0B0B0ADADADABABABEDEDEDFCFCFCC9C9C9FEFEFEFEFEFEC9C9C9
        FCFCFCFBFBFBFCFCFCFCFCFCFBFBFBF8F8F8F5F5F5F1F1F1ECECECEAEAEAE6E6
        E6FCFCFCC9C9C9FEFEFEFEFEFEC9C9C9FCFCFCF9F9F9BFBFBFB9B9B9B3B3B3AE
        AEAEA9A9A9A4A4A4FCFCFCFCFCFCFCFCFCFCFCFCC9C9C9FEFEFEFEFEFEC9C9C9
        FCFCFCF7F7F7F9F9F9F7F7F7F7F7F7F3F3F3F0F0F0EAEAEAFCFCFCF6F6F6F4F4
        F4C4C4C4DFDFDFFFFFFFFFFFFFC9C9C9FBFBFBF4F4F4F5F5F5F5F5F5F5F5F5F1
        F1F1EFEFEFE9E9E9FCFCFCE7E7E7C2C2C2DFDFDFFDFDFDFFFFFFFFFFFFCCCCCC
        F8F8F8FBFBFBFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF8F8F8C2C2C2DFDF
        DFFDFDFDFFFFFFFFFFFFFFFFFFE3E3E3CCCCCCC9C9C9C9C9C9C9C9C9C9C9C9C9
        C9C9C9C9C9C9C9C9C9C9C9DFDFDFFDFDFDFFFFFFFFFFFFFFFFFF}
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
    end
  end
  object sPanel1: TsPanel
    Left = 0
    Top = 175
    Width = 1920
    Height = 40
    Align = alTop
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 3
    SkinData.SkinSection = 'TRANSPARENT'
    object lbl_3: TLabel
      Left = 498
      Top = 5
      Width = 85
      Height = 30
      Align = alLeft
      Caption = '      Cari Data :    '
    end
    object sLabel3: TsLabel
      Left = 307
      Top = 5
      Width = 46
      Height = 30
      Align = alLeft
      Caption = 'Jenis    '
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Trebuchet MS'
      Font.Style = []
    end
    object sPanel2: TsPanel
      Left = 5
      Top = 5
      Width = 302
      Height = 30
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      SkinData.SkinSection = 'TRANSPARENT'
      object sLabel1: TsLabel
        Left = 0
        Top = 0
        Width = 46
        Height = 30
        Align = alLeft
        Caption = 'Tanggal   '
      end
      object sLabel2: TsLabel
        Left = 150
        Top = 0
        Width = 36
        Height = 30
        Align = alLeft
        Alignment = taRightJustify
        AutoSize = False
        Caption = '   s/d   '
      end
      object stglAwal: TsDateEdit
        Left = 46
        Top = 0
        Width = 104
        Height = 30
        AutoSize = False
        Color = clWhite
        EditMask = '!99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Trebuchet MS'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 0
        Text = '  /  /    '
        OnChange = stglAwalChange
        Align = alLeft
        BoundLabel.Caption = 'stglAwal'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
      end
      object stglAkhir: TsDateEdit
        Left = 186
        Top = 0
        Width = 104
        Height = 30
        AutoSize = False
        Color = clWhite
        EditMask = '!99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Trebuchet MS'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        Text = '  /  /    '
        OnChange = stglAwalChange
        Align = alLeft
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
      end
    end
    object cbb_jenis: TsComboBox
      Left = 353
      Top = 5
      Width = 145
      Height = 26
      Align = alLeft
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Trebuchet MS'
      Font.Style = []
      ItemHeight = 20
      ItemIndex = -1
      ParentFont = False
      TabOrder = 1
      Text = 'Semua'
      OnChange = cbb_jenisChange
      Items.Strings = (
        'Semua'
        'Cream'
        'Obat')
    end
    object DBGridEh3: TDBGridEh
      Left = 583
      Top = 5
      Width = 617
      Height = 30
      Align = alLeft
      DataSource = ds_obat_keluar
      DrawMemoText = True
      DynProps = <>
      Flat = True
      HorzScrollBar.Visible = False
      IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
      SearchPanel.Enabled = True
      SearchPanel.FilterOnTyping = True
      TabOrder = 2
      TitleParams.MultiTitle = True
      VertScrollBar.VisibleMode = sbNeverShowEh
      Columns = <
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'no_nota'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'No. Nota'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 153
        end
        item
          Alignment = taCenter
          DynProps = <>
          EditButtons = <>
          FieldName = 'tgl_obat_keluar'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Tanggal'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 107
        end
        item
          Alignment = taCenter
          DynProps = <>
          EditButtons = <>
          FieldName = 'id_obat'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Kode Obat'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 88
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'nm_obat'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Nama Obat'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 284
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'jumlah_obat_keluar'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Jumlah'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 65
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'satuanjual'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Satuan'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 97
        end
        item
          DisplayFormat = ',#'
          DynProps = <>
          EditButtons = <>
          FieldName = 'hrga_jual'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Harga Jual'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 86
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'jenis'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Jenis'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 76
        end
        item
          DisplayFormat = ',#'
          DynProps = <>
          EditButtons = <>
          FieldName = 'biaya_listrik'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Biaya|Listrik'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 70
        end
        item
          DisplayFormat = ',#'
          DynProps = <>
          EditButtons = <>
          FieldName = 'biaya_admin'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Biaya|Admin'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 78
        end
        item
          DisplayFormat = ',#'
          DynProps = <>
          EditButtons = <>
          FieldName = 'tot_harga'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Total'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 105
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'nama_pengunjung'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Nama Pengunjung'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 281
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'nm_dokter'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Nama Dokter'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 238
        end
        item
          Alignment = taCenter
          DynProps = <>
          EditButtons = <>
          FieldName = 'racikan'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Racikan'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 76
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  object DBGridEh2: TDBGridEh
    Left = 0
    Top = 215
    Width = 1920
    Height = 812
    Align = alClient
    DataSource = ds_obat_keluar
    DrawMemoText = True
    DynProps = <>
    Flat = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Trebuchet MS'
    Font.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
    ParentFont = False
    SearchPanel.FilterOnTyping = True
    TabOrder = 4
    TitleParams.MultiTitle = True
    OnDrawColumnCell = DBGridEh2DrawColumnCell
    Columns = <
      item
        Alignment = taCenter
        DynProps = <>
        EditButtons = <>
        FieldName = 'no_nota'
        Footers = <>
        HideDuplicates = True
        Title.Caption = 'No. Nota'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 153
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'diskripsi'
        Footers = <>
        HideDuplicates = True
        Title.Caption = 'Jenis Racikan'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 175
      end
      item
        Alignment = taCenter
        DynProps = <>
        EditButtons = <>
        FieldName = 'tgl_obat_keluar'
        Footers = <>
        Title.Caption = 'Tanggal'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 107
      end
      item
        Alignment = taCenter
        DynProps = <>
        EditButtons = <>
        FieldName = 'id_obat'
        Footers = <>
        Title.Caption = 'Kode Obat'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 88
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'nm_obat'
        Footers = <>
        Title.Caption = 'Nama Obat'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 306
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'jumlah_obat_keluar'
        Footers = <>
        Title.Caption = 'Jumlah'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 55
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'satuanjual'
        Footers = <>
        Title.Caption = 'Satuan'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 97
      end
      item
        DisplayFormat = ',#'
        DynProps = <>
        EditButtons = <>
        FieldName = 'hrga_jual'
        Footers = <>
        Title.Caption = 'Harga Jual'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 86
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'jenis'
        Footers = <>
        Title.Caption = 'Jenis'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 76
      end
      item
        DisplayFormat = ',#'
        DynProps = <>
        EditButtons = <>
        FieldName = 'biaya_listrik'
        Footers = <>
        Title.Caption = 'Biaya|Listrik'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Visible = False
        Width = 70
      end
      item
        DisplayFormat = ',#'
        DynProps = <>
        EditButtons = <>
        FieldName = 'biaya_admin'
        Footers = <>
        Title.Caption = 'Biaya|Admin'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Visible = False
        Width = 78
      end
      item
        DisplayFormat = ',#'
        DynProps = <>
        EditButtons = <>
        FieldName = 'biaya'
        Footers = <>
        Title.Caption = 'Biaya|Racikan'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Visible = False
        Width = 85
      end
      item
        DisplayFormat = ',#'
        DynProps = <>
        EditButtons = <>
        FieldName = 'biaya_dokter'
        Footers = <>
        Title.Caption = 'Biaya Dokter'
      end
      item
        DisplayFormat = ',#'
        DynProps = <>
        EditButtons = <>
        FieldName = 'biayaServis'
        Footers = <>
        Title.Caption = 'Biaya|Servis'
        Visible = False
      end
      item
        DisplayFormat = ',#'
        DynProps = <>
        EditButtons = <>
        FieldName = 'tot_harga'
        Footers = <>
        HideDuplicates = True
        Title.Caption = 'Total'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 105
      end
      item
        Alignment = taCenter
        DynProps = <>
        EditButtons = <>
        FieldName = 'racikan1'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        Footers = <>
        Title.Caption = 'Racikan'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 78
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'nama_pengunjung'
        Footers = <>
        HideDuplicates = True
        Title.Caption = 'Nama Pengunjung'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 343
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'nm_dokter'
        Footers = <>
        HideDuplicates = True
        Title.Caption = 'Nama Dokter'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 257
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object sPanel3: TsPanel
    Left = 0
    Top = 0
    Width = 1920
    Height = 84
    Align = alTop
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Trebuchet MS'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    SkinData.SkinSection = 'HINT'
    object sImage1: TsImage
      Left = 1
      Top = 1
      Width = 120
      Height = 82
      Align = alLeft
      Center = True
      Picture.Data = {
        0B54504E474772617068696336400000424D3640000000000000360000002800
        0000400000004000000001002000000000000040000000000000000000000000
        0000000000004DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD
        60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD
        60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD
        60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD
        60FF4DDD60FF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA
        1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA
        1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA
        1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA
        1EFF4ABA1EFF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD
        60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD
        60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD
        60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD
        60FF4DDD60FF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA
        1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA
        1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA
        1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA
        1EFF4ABA1EFF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD
        60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD
        60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD
        60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD60FF4DDD
        60FF4DDD60FF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA
        1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA
        1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA
        1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA1EFF4ABA
        1EFF4ABA1EFF4DDC60C04DDC60C072D963F776D864FF76D864FF76D864FF76D8
        64FF76D864FF76D864FF76D864FF76D864FF76D864FF76D864FF76D864FF76D8
        64FF77DC6DFF77DF75FF77DF75FF77DF75FF77DF75FF77DF75FF77DF75FF77DF
        75FF77DF75FF77DD72FF76D864FF76D864FF76D864FF67CB5BFF57BD52FF57BD
        52FF57BD52FF4AB82AFF4AB82AFF4AB82AFF51BE2BFF53BE2AFF53BE2AFF53BE
        2AFF54C12EFF55C12FFF55C12FFF55C12FFF55C12FFF55C12FFF55C12FFF55C1
        2FFF55C12FFF54C12FFF53BE2AFF53BE2AFF53BE2AFF53BE2AFF53BE2AFF53BE
        2AFF53BE2AFF53BE2AFF53BE2AFF53BE2AFF53BE2AFF53BE2AFF53BE2AFF4BC1
        2BEF4BC12BEFFFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF6DEA7FFF1C86FFFF1C86FFFF1C86FFFB8964DFF775D26FF775D
        26FF775D26FF3B2F13FF3B2F13FF3B2F13FFB78B2BFFCF9415FFCF9415FFCF94
        15FFEABD5BFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF94
        15FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF6DEA7FFF1C86FFFF1C86FFFF1C86FFFB8964DFF775D26FF775D
        26FF775D26FF3B2F13FF3B2F13FF3B2F13FFB78B2BFFCF9415FFCF9415FFCF94
        15FFEABD5BFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF94
        15FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF6DEA7FFF1C86FFFF1C86FFFF1C86FFFB8964DFF775D26FF775D
        26FF775D26FF3B2F13FF3B2F13FF3B2F13FFB78B2BFFCF9415FFCF9415FFCF94
        15FFEABD5BFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF94
        15FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF6DEA7FFF1C86FFFF1C86FFFF1C86FFFB8964DFF775D26FF775D
        26FF775D26FF3B2F13FF3B2F13FF3B2F13FFB78B2BFFCF9415FFCF9415FFCF94
        15FFEABD5BFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF94
        15FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF6DEA7FFF1C86FFFF1C86FFFF1C86FFFB8964DFF775D26FF775D
        26FF775D26FF3B2F13FF3B2F13FF3B2F13FFB78B2BFFCF9415FFCF9415FFCF94
        15FFEABD5BFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF94
        15FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF1CA
        73FFF6DA9EFFF6DA9EFFF6DA9EFFF4D48EFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF6DEA7FFF1C86FFFF1C86FFFF1C86FFFB8964DFF775D26FF775D
        26FF775D26FF3B2F13FF3B2F13FF3B2F13FFB78B2BFFCF9415FFCF9415FFCF94
        15FFEABD5BFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFDEAA3CFFE5B6
        50FFE5B650FFE5B650FFD1971AFFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF2CB
        76FFF8E4B7FFF8E4B7FFF8E4B7FFF6DA9EFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF6DEA7FFF1C86FFFF1C86FFFF1C86FFFB8964DFF775D26FF775D
        26FF775D26FF3B2F13FF3B2F13FF3B2F13FFB78B2BFFCF9415FFCF9415FFCF94
        15FFEABD5BFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFE5B650FFF1C8
        6FFFF1C86FFFF1C86FFFD2991DFFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF2CB
        76FFF8E4B7FFF8E4B7FFF8E4B7FFF6DA9EFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF6DEA7FFF1C86FFFF1C86FFFF1C86FFFB8964DFF775D26FF775D
        26FF775D26FF3B2F13FF3B2F13FF3B2F13FFB78B2BFFCF9415FFCF9415FFCF94
        15FFEABD5BFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFE5B650FFF1C8
        6FFFF1C86FFFF1C86FFFD2991DFFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF2CB
        76FFF8E4B7FFF8E4B7FFF8E4B7FFF6DA9EFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF6DEA7FFF1C86FFFF1C86FFFF1C86FFFB8964DFF775D26FF775D
        26FF775D26FF3B2F13FF3B2F13FF3B2F13FFB78B2BFFCF9415FFCF9415FFCF94
        15FFEABD5BFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFE5B650FFF1C8
        6FFFF1C86FFFF1C86FFFD2991DFFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF2CB
        76FFF8E4B7FFF8E4B7FFF8E4B7FFF6DA9EFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF6DEA7FFF1C86FFFF1C86FFFF1C86FFFB8964DFF775D26FF775D
        26FF775D26FF3B2F13FF3B2F13FF3B2F13FFB78B2BFFCF9415FFCF9415FFCF94
        15FFEABD5BFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFE5B650FFF1C8
        6FFFF1C86FFFF1C86FFFD2991DFFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF1CA
        73FFF6DA9EFFF6DA9EFFF6DA9EFFF4D48EFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF6DEA7FFF1C86FFFF1C86FFFF1C86FFFEAC26BFFE2BB66FFE2BB
        66FFE2BB66FFD0981FFFD0981FFFD0981FFFD0961AFFCF9415FFCF9415FFCF94
        15FFEABD5BFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFDEAA3CFFE5B6
        50FFE5B650FFE5B650FFD1971AFFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF6DEA7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF94
        15FFEABD5BFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF94
        15FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF6DEA7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF94
        15FFEABD5BFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF94
        15FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF7DFA9FFF2CB78FFF2CB78FFF2CB78FFF2CB78FFF2CB78FFF2CB
        78FFF2CB78FFD39A20FFD39A20FFD39A20FFD39A20FFD39A20FFD39A20FFD39A
        20FFEABE5EFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF94
        15FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF94
        15FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF94
        15FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF2CB
        76FFF8E4B7FFF8E4B7FFF8E4B7FFF6DA9EFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFE5B650FFF1C8
        6FFFF1C86FFFF1C86FFFD2991DFFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF2CB
        76FFF8E4B7FFF8E4B7FFF8E4B7FFF6DA9EFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFE5B650FFF1C8
        6FFFF1C86FFFF1C86FFFD2991DFFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF2CB
        76FFF8E4B7FFF8E4B7FFF8E4B7FFF6DA9EFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E5BAFFF9E8C3FFF9E8C3FFF9E8C3FFF9E6BDFFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF2CD7CFFF3D288FFF3D288FFF3D2
        88FFF2CA74FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFE5B650FFF1C8
        6FFFF1C86FFFF1C86FFFD2991DFFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF2CB
        76FFF8E4B7FFF8E4B7FFF8E4B7FFF6DA9EFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF9E7BFFFFBF1DBFFFBF1DBFFFBF1DBFFFAEBCAFFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF5D795FFF8E4B7FFF8E4B7FFF8E4
        B7FFF3CE7FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFE5B650FFF1C8
        6FFFF1C86FFFF1C86FFFD2991DFFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF2CB
        76FFF8E4B7FFF8E4B7FFF8E4B7FFF6DA9EFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF9E7BFFFFBF1DBFFFBF1DBFFFBF1DBFFFAEBCAFFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF5D795FFF8E4B7FFF8E4B7FFF8E4
        B7FFF3CE7FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFE5B650FFF1C8
        6FFFF1C86FFFF1C86FFFD2991DFFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF1C9
        71FFF3D288FFF3D288FFF3D288FFF3CE7FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF9E7BFFFFBF1DBFFFBF1DBFFFBF1DBFFFAEBCAFFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF5D795FFF8E4B7FFF8E4B7FFF8E4
        B7FFF3CE7FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFD7A029FFDBA6
        34FFDBA634FFDBA634FFD09618FFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF9E7BFFFFBF1DBFFFBF1DBFFFBF1DBFFFAEBCAFFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF5D795FFF8E4B7FFF8E4B7FFF8E4
        B7FFF3CE7FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF94
        15FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF9E7BFFFFBF1DBFFFBF1DBFFFBF1DBFFFAEBCAFFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF5D795FFF8E4B7FFF8E4B7FFF8E4
        B7FFF3CE7FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF94
        15FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF94
        15FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF94
        15FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF94
        15FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F1C870E0F1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF5D795FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEBC46DFFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF94
        15FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFCF9415FFD3991EFBFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00F0C76F77F2C96F88F2C96F88F2C96F88F2C9
        6F88F2C96F88F2C96F88F2C96F88F2C96F88F2C96F88F2C96F88F2C96F88F2C9
        6F88F6DCA0C7F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFEEC977F3DBA532C7DBA532C7DBA532C7DBA532C7DBA532C7DBA5
        32C7DBA532C7DBA532C7DBA532C7DBA532C7DBA532C7DBA532C7DBA735B6FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E6BCFFFAEDCFFFFAEDCFFFFAEDCFFFF9E9C4FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF3D288FFF6DA9EFFF6DA9EFFF6DA
        9EFFF2CC79FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF9E7BFFFFBF1DBFFFBF1DBFFFBF1DBFFFAEBCAFFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF5D795FFF8E4B7FFF8E4B7FFF8E4
        B7FFF3CE7FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF9E7BFFFFBF1DBFFFBF1DBFFFBF1DBFFFAEBCAFFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF5D795FFF8E4B7FFF8E4B7FFF8E4
        B7FFF3CE7FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF9E7BFFFFBF1DBFFFBF1DBFFFBF1DBFFFAEBCAFFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF5D795FFF8E4B7FFF8E4B7FFF8E4
        B7FFF3CE7FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF9E7BFFFFBF1DBFFFBF1DBFFFBF1DBFFFAEBCAFFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF5D795FFF8E4B7FFF8E4B7FFF8E4
        B7FFF3CE7FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E6BCFFFAEDCFFFFAEDCFFFFAEDCFFFF9E9C4FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF3D288FFF6DA9EFFF6DA9EFFF6DA
        9EFFF2CC79FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FF3461
        F6FF194FFFFF0032D4FF0637D7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FF3461
        F6FF194FFFFF0032D4FF0637D7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FF3461
        F6FF194FFFFF0032D4FF0637D7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FF3461
        F6FF194FFFFF0032D4FF0637D7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FF3461F6FF3461F6FF3461F6FF3461F6FF1C51
        FEFF194FFFFF0032D4FF0032D4FF0637D7FF0637D7FF0637D7FF0637D7FFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FF194FFFFF194FFFFF194FFFFF194FFFFF194F
        FFFF194FFFFF0032D4FF0032D4FF0032D4FF0032D4FF0032D4FF0032D4FFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FF194FFFFF194FFFFF194FFFFF194FFFFF194F
        FFFF194FFFFF0032D4FF0032D4FF0032D4FF0032D4FF0032D4FF0032D4FFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FF3461F6FF3461F6FF3461F6FF3461F6FF1C51
        FEFF194FFFFF0032D4FF0032D4FF0637D7FF0637D7FF0637D7FF0637D7FFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FF3461
        F6FF194FFFFF0032D4FF0637D7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FF3461
        F6FF194FFFFF0032D4FF0637D7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FF3461
        F6FF194FFFFF0032D4FF0637D7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FF3461
        F6FF194FFFFF0032D4FF0637D7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F8E5B888F8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4B7FFF8E4
        B7FFF8E4B7FFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C86FFFF1C8
        6FFFF1C86FFFF3D287C7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00}
      SkinData.SkinSection = 'CHECKBOX'
    end
    object sLabel4: TsLabel
      Left = 121
      Top = 6
      Width = 317
      Height = 38
      Align = alCustom
      Caption = 'DATA OBAT TERJUAL'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -27
      Font.Name = 'Arial Black'
      Font.Style = [fsBold]
    end
    object sLabel5: TsLabel
      Left = 120
      Top = 45
      Width = 453
      Height = 18
      Caption = 
        'Jl. Diponegoro No.40, Praya, Kabupaten Lombok Tengah, Nusa Tengg' +
        'ara Bar. 83511'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Trebuchet MS'
      Font.Style = []
    end
    object sPanel4: TsPanel
      Left = 119
      Top = 43
      Width = 727
      Height = 3
      TabOrder = 0
      SkinData.SkinSection = 'PANEL'
    end
  end
  object ds_obat_keluar: TMyDataSource
    DataSet = q_obat_keluar
    Left = 150
    Top = 312
  end
  object dlgSavedoalog: TSaveDialog
    Filter = 'Excel|*.xlsx'
    Left = 1024
    Top = 512
  end
  object ppReport_obat_terjual: TppReport
    AutoStop = False
    DataPipeline = ppDBPipeline1
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.Orientation = poLandscape
    PrinterSetup.PaperName = 'A4'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 210000
    PrinterSetup.mmPaperWidth = 297000
    PrinterSetup.PaperSize = 9
    Template.FileName = 
      'D:\RS KOTA\APP ++\10. Apotek Praya\Report\laporan harian dokter.' +
      'rtm'
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    Left = 760
    Top = 16
    Version = '15.04'
    mmColumnWidth = 0
    DataPipelineName = 'ppDBPipeline1'
    object ppHeaderBand1: TppHeaderBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 32544
      mmPrintPosition = 0
      object ppShape1: TppShape
        UserName = 'Shape1'
        mmHeight = 12700
        mmLeft = 1058
        mmTop = 20169
        mmWidth = 283369
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel1: TppLabel
        UserName = 'Label1'
        Caption = 'No . Resep'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 7673
        mmTop = 24667
        mmWidth = 15346
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel3: TppLabel
        UserName = 'Label3'
        Caption = 'Nama Obat [Jenis Racikan]'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 38629
        mmTop = 24667
        mmWidth = 38100
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel4: TppLabel
        UserName = 'Label4'
        Caption = 'Jumlah'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 86784
        mmTop = 24667
        mmWidth = 10583
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel5: TppLabel
        UserName = 'Label5'
        Caption = 'Harga'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 104775
        mmTop = 24667
        mmWidth = 8202
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel6: TppLabel
        UserName = 'Label6'
        Caption = 'Total Obat'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 121179
        mmTop = 24667
        mmWidth = 14817
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel7: TppLabel
        UserName = 'Label7'
        Caption = 'Cream'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 147902
        mmTop = 24667
        mmWidth = 9790
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel9: TppLabel
        UserName = 'Label8'
        Caption = 'Servis'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 163494
        mmTop = 27737
        mmWidth = 9525
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel10: TppLabel
        UserName = 'Label9'
        Caption = 'Listrik'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 175419
        mmTop = 27737
        mmWidth = 9525
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel11: TppLabel
        UserName = 'Label11'
        Caption = 'Ppn'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 220398
        mmTop = 24667
        mmWidth = 5556
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel12: TppLabel
        UserName = 'Label12'
        Caption = 'Nama Dokter'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 235215
        mmTop = 24667
        mmWidth = 18521
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel13: TppLabel
        UserName = 'Label13'
        Caption = 'Total'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 266701
        mmTop = 24667
        mmWidth = 7144
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel14: TppLabel
        UserName = 'Label10'
        Caption = 'Racikan'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 187418
        mmTop = 27737
        mmWidth = 11113
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel15: TppLabel
        UserName = 'Label14'
        Caption = 'Biaya - Biaya'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 179123
        mmTop = 22021
        mmWidth = 21960
        BandType = 0
        LayerName = Foreground
      end
      object ppLine45: TppLine
        UserName = 'Line12'
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 12700
        mmLeft = 30692
        mmTop = 20169
        mmWidth = 4233
        BandType = 0
        LayerName = Foreground
      end
      object ppLine46: TppLine
        UserName = 'Line46'
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 12700
        mmLeft = 80169
        mmTop = 20169
        mmWidth = 4233
        BandType = 0
        LayerName = Foreground
      end
      object ppLine47: TppLine
        UserName = 'Line47'
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 12700
        mmLeft = 100806
        mmTop = 20169
        mmWidth = 4233
        BandType = 0
        LayerName = Foreground
      end
      object ppLine48: TppLine
        UserName = 'Line48'
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 12700
        mmLeft = 115359
        mmTop = 20169
        mmWidth = 4233
        BandType = 0
        LayerName = Foreground
      end
      object ppLine49: TppLine
        UserName = 'Line49'
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 12700
        mmLeft = 140759
        mmTop = 20169
        mmWidth = 4233
        BandType = 0
        LayerName = Foreground
      end
      object ppLine50: TppLine
        UserName = 'Line50'
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 12700
        mmLeft = 161925
        mmTop = 20169
        mmWidth = 4233
        BandType = 0
        LayerName = Foreground
      end
      object ppLine51: TppLine
        UserName = 'Line501'
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 12700
        mmLeft = 215107
        mmTop = 20169
        mmWidth = 4233
        BandType = 0
        LayerName = Foreground
      end
      object ppLine52: TppLine
        UserName = 'Line52'
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 12700
        mmLeft = 230717
        mmTop = 20169
        mmWidth = 4233
        BandType = 0
        LayerName = Foreground
      end
      object ppLine53: TppLine
        UserName = 'Line53'
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 12700
        mmLeft = 258763
        mmTop = 20169
        mmWidth = 4233
        BandType = 0
        LayerName = Foreground
      end
      object ppLine54: TppLine
        UserName = 'Line17'
        Weight = 0.500000000000000000
        mmHeight = 3175
        mmLeft = 162190
        mmTop = 26784
        mmWidth = 52917
        BandType = 0
        LayerName = Foreground
      end
      object ppLine55: TppLine
        UserName = 'Line55'
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 8202
        mmLeft = 173820
        mmTop = 26784
        mmWidth = 4233
        BandType = 0
        LayerName = Foreground
      end
      object ppLine56: TppLine
        UserName = 'Line56'
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 8202
        mmLeft = 186002
        mmTop = 26723
        mmWidth = 4233
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel2: TppLabel
        UserName = 'Label2'
        Caption = 'APOTEK SEJAHTERA'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 14
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 5821
        mmLeft = 114829
        mmTop = 5578
        mmWidth = 53181
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel16: TppLabel
        UserName = 'Label16'
        Caption = 'Laporan Harian - Rincian Obat Keluar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4234
        mmLeft = 110331
        mmTop = 1058
        mmWidth = 56886
        BandType = 0
        LayerName = Foreground
      end
      object ppLine57: TppLine
        UserName = 'Line21'
        Style = lsDouble
        Weight = 0.750000000000000000
        mmHeight = 2463
        mmLeft = 1058
        mmTop = 11785
        mmWidth = 282046
        BandType = 0
        LayerName = Foreground
      end
      object ppSystemVariable1: TppSystemVariable
        UserName = 'SystemVariable1'
        DisplayFormat = 'dddd, dd/mm/yyyy'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 6
        Font.Style = []
        Transparent = True
        mmHeight = 2646
        mmLeft = 2117
        mmTop = 15610
        mmWidth = 18256
        BandType = 0
        LayerName = Foreground
      end
      object ppLabel17: TppLabel
        UserName = 'Label103'
        Caption = 'Dokter'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 202671
        mmTop = 27781
        mmWidth = 9790
        BandType = 0
        LayerName = Foreground
      end
      object ppLine8: TppLine
        UserName = 'Line8'
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 6562
        mmLeft = 199761
        mmTop = 26723
        mmWidth = 4233
        BandType = 0
        LayerName = Foreground
      end
    end
    object ppDetailBand1: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 5292
      mmPrintPosition = 0
      object ppDBText3: TppDBText
        UserName = 'DBText3'
        DataField = 'jumlah_obat_keluar'
        DataPipeline = ppDBPipeline1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'ppDBPipeline1'
        mmHeight = 3440
        mmLeft = 80169
        mmTop = 1323
        mmWidth = 6134
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText5: TppDBText
        UserName = 'DBText5'
        CharWrap = True
        DataField = 'nm_obat'
        DataPipeline = ppDBPipeline1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'ppDBPipeline1'
        mmHeight = 3704
        mmLeft = 27781
        mmTop = 709
        mmWidth = 50271
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText6: TppDBText
        UserName = 'DBText6'
        DataField = 'hrgajualbersih'
        DataPipeline = ppDBPipeline1
        DisplayFormat = ',# -'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'ppDBPipeline1'
        mmHeight = 3440
        mmLeft = 94986
        mmTop = 1323
        mmWidth = 19122
        BandType = 4
        LayerName = Foreground
      end
      object ppDBCalc1: TppDBCalc
        UserName = 'DBCalc1'
        DataPipeline = ppDBPipeline1
        DisplayFormat = '0,.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 6
        Font.Style = []
        ResetGroup = ppGroup2
        TextAlignment = taRightJustified
        Transparent = True
        DBCalcType = dcCount
        DataPipelineName = 'ppDBPipeline1'
        mmHeight = 4498
        mmLeft = 21431
        mmTop = 709
        mmWidth = 3704
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText8: TppDBText
        UserName = 'DBText8'
        DataField = 'harga_jual_cream'
        DataPipeline = ppDBPipeline1
        DisplayFormat = ',# -'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'ppDBPipeline1'
        mmHeight = 3440
        mmLeft = 141817
        mmTop = 265
        mmWidth = 17198
        BandType = 4
        LayerName = Foreground
      end
      object ppLine6: TppLine
        UserName = 'Line6'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 5358
        mmLeft = 161925
        mmTop = 0
        mmWidth = 3969
        BandType = 4
        LayerName = Foreground
      end
      object ppLine11: TppLine
        UserName = 'Line11'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 7408
        mmLeft = 173820
        mmTop = 0
        mmWidth = 3969
        BandType = 4
        LayerName = Foreground
      end
      object ppLine16: TppLine
        UserName = 'Line16'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 7408
        mmLeft = 140759
        mmTop = 0
        mmWidth = 3969
        BandType = 4
        LayerName = Foreground
      end
      object ppLine18: TppLine
        UserName = 'Line18'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 7408
        mmLeft = 186002
        mmTop = 0
        mmWidth = 3969
        BandType = 4
        LayerName = Foreground
      end
      object ppLine21: TppLine
        UserName = 'Line201'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 7408
        mmLeft = 215107
        mmTop = 0
        mmWidth = 3969
        BandType = 4
        LayerName = Foreground
      end
      object ppLine24: TppLine
        UserName = 'Line24'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 7408
        mmLeft = 230717
        mmTop = 0
        mmWidth = 3969
        BandType = 4
        LayerName = Foreground
      end
      object ppLine25: TppLine
        UserName = 'Line25'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 7408
        mmLeft = 258763
        mmTop = 0
        mmWidth = 3969
        BandType = 4
        LayerName = Foreground
      end
      object ppLine31: TppLine
        UserName = 'Line301'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 7408
        mmLeft = 115317
        mmTop = 0
        mmWidth = 3969
        BandType = 4
        LayerName = Foreground
      end
      object ppLine33: TppLine
        UserName = 'Line33'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 5182
        mmLeft = 100757
        mmTop = 360
        mmWidth = 3969
        BandType = 4
        LayerName = Foreground
      end
      object ppDBText26: TppDBText
        UserName = 'DBText26'
        DataField = 'satuanjual'
        DataPipeline = ppDBPipeline1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        DataPipelineName = 'ppDBPipeline1'
        mmHeight = 3440
        mmLeft = 88034
        mmTop = 1323
        mmWidth = 12027
        BandType = 4
        LayerName = Foreground
      end
      object ppLine37: TppLine
        UserName = 'Line37'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 5490
        mmLeft = 80169
        mmTop = 155
        mmWidth = 3969
        BandType = 4
        LayerName = Foreground
      end
      object ppLine36: TppLine
        UserName = 'Line36'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpRight
        Weight = 0.500000000000000000
        mmHeight = 7408
        mmLeft = 280400
        mmTop = 0
        mmWidth = 3969
        BandType = 4
        LayerName = Foreground
      end
      object ppLine43: TppLine
        UserName = 'Line43'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 5437
        mmLeft = 16669
        mmTop = 0
        mmWidth = 3969
        BandType = 4
        LayerName = Foreground
      end
      object ppLine42: TppLine
        UserName = 'Line42'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 7408
        mmLeft = 1058
        mmTop = 0
        mmWidth = 3969
        BandType = 4
        LayerName = Foreground
      end
      object ppLine58: TppLine
        UserName = 'Line58'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 7408
        mmLeft = 199761
        mmTop = 0
        mmWidth = 3969
        BandType = 4
        LayerName = Foreground
      end
      object ppLine61: TppLine
        UserName = 'Line61'
        Border.Color = clAqua
        Pen.Color = cl3DDkShadow
        Weight = 0.500000000000000000
        mmHeight = 1588
        mmLeft = 16874
        mmTop = 0
        mmWidth = 98425
        BandType = 4
        LayerName = Foreground
      end
      object ppLine62: TppLine
        UserName = 'Line62'
        Border.Color = clAqua
        Pen.Color = cl3DDkShadow
        Position = lpBottom
        Weight = 0.500000000000000000
        mmHeight = 1588
        mmLeft = 141023
        mmTop = 3937
        mmWidth = 21095
        BandType = 4
        LayerName = Foreground
      end
      object ppLine32: TppLine
        UserName = 'Line32'
        Border.Color = clAqua
        Pen.Color = cl3DDkShadow
        Weight = 0.500000000000000000
        mmHeight = 1628
        mmLeft = 140759
        mmTop = 0
        mmWidth = 21370
        BandType = 4
        LayerName = Foreground
      end
    end
    object ppFooterBand1: TppFooterBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 265
      mmPrintPosition = 0
      object ppLine59: TppLine
        UserName = 'Line59'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Weight = 0.500000000000000000
        mmHeight = 153
        mmLeft = 1058
        mmTop = 0
        mmWidth = 282624
        BandType = 8
        LayerName = Foreground
      end
    end
    object ppSummaryBand1: TppSummaryBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 11113
      mmPrintPosition = 0
      object ppShape2: TppShape
        UserName = 'Shape2'
        mmHeight = 9260
        mmLeft = 1058
        mmTop = 0
        mmWidth = 283324
        BandType = 7
        LayerName = Foreground
      end
      object ppLine3: TppLine
        UserName = 'Line3'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 9187
        mmLeft = 199761
        mmTop = 265
        mmWidth = 3969
        BandType = 7
        LayerName = Foreground
      end
      object ppLine60: TppLine
        UserName = 'Line60'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 9260
        mmLeft = 215226
        mmTop = 257
        mmWidth = 3969
        BandType = 7
        LayerName = Foreground
      end
      object ppLine63: TppLine
        UserName = 'Line601'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 9260
        mmLeft = 186002
        mmTop = 265
        mmWidth = 3969
        BandType = 7
        LayerName = Foreground
      end
      object ppLine64: TppLine
        UserName = 'Line64'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 9260
        mmLeft = 173820
        mmTop = 265
        mmWidth = 3969
        BandType = 7
        LayerName = Foreground
      end
      object ppLine65: TppLine
        UserName = 'Line65'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 9260
        mmLeft = 161935
        mmTop = 265
        mmWidth = 3969
        BandType = 7
        LayerName = Foreground
      end
      object ppLine66: TppLine
        UserName = 'Line66'
        Border.Color = clAppWorkSpace
        Pen.Color = clAppWorkSpace
        Position = lpLeft
        Weight = 0.500000000000000000
        mmHeight = 9260
        mmLeft = 140759
        mmTop = 265
        mmWidth = 3969
        BandType = 7
        LayerName = Foreground
      end
      object ppLabel_ppn: TppLabel
        UserName = 'Label_ppn'
        Caption = 'Cream'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3705
        mmLeft = 215746
        mmTop = 2910
        mmWidth = 12237
        BandType = 7
        LayerName = Foreground
      end
      object ppLabel_dokter: TppLabel
        UserName = 'Label_ppn1'
        Caption = 'Cream'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 201084
        mmTop = 2646
        mmWidth = 12171
        BandType = 7
        LayerName = Foreground
      end
      object ppLabel_racikan: TppLabel
        UserName = 'Label_racikan'
        Caption = 'Cream'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 186267
        mmTop = 2646
        mmWidth = 12171
        BandType = 7
        LayerName = Foreground
      end
      object ppLabel_servis: TppLabel
        UserName = 'Label_racikan1'
        Caption = 'Cream'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 160867
        mmTop = 2646
        mmWidth = 12171
        BandType = 7
        LayerName = Foreground
      end
      object ppDBCalc3: TppDBCalc
        UserName = 'DBCalc3'
        DataField = 'harga_jual_cream'
        DataPipeline = ppDBPipeline1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'ppDBPipeline1'
        mmHeight = 3728
        mmLeft = 142962
        mmTop = 2646
        mmWidth = 17198
        BandType = 7
        LayerName = Foreground
      end
      object ppLabel_total_obat: TppLabel
        UserName = 'Label_total_obat'
        Caption = 'Cream'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 127529
        mmTop = 2646
        mmWidth = 12171
        BandType = 7
        LayerName = Foreground
      end
      object ppLabel_listrik: TppLabel
        UserName = 'Label_listrik'
        Caption = 'Cream'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 172509
        mmTop = 2646
        mmWidth = 12171
        BandType = 7
        LayerName = Foreground
      end
      object ppLabel_total_harga: TppLabel
        UserName = 'Label_ppn2'
        Caption = 'Cream'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3704
        mmLeft = 256426
        mmTop = 2910
        mmWidth = 25687
        BandType = 7
        LayerName = Foreground
      end
    end
    object ppGroup1: TppGroup
      BreakName = 'no_nota'
      DataPipeline = ppDBPipeline1
      GroupFileSettings.NewFile = False
      GroupFileSettings.EmailFile = False
      KeepTogether = True
      OutlineSettings.CreateNode = True
      StartOnOddPage = False
      UserName = 'Group1'
      mmNewColumnThreshold = 0
      mmNewPageThreshold = 0
      DataPipelineName = 'ppDBPipeline1'
      NewFile = False
      object ppGroupHeaderBand1: TppGroupHeaderBand
        Background.Brush.Style = bsClear
        mmBottomOffset = 0
        mmHeight = 5292
        mmPrintPosition = 0
        object ppDBText1: TppDBText
          UserName = 'DBText1'
          DataField = 'no_nota'
          DataPipeline = ppDBPipeline1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = [fsBold]
          Transparent = True
          DataPipelineName = 'ppDBPipeline1'
          mmHeight = 3969
          mmLeft = 6615
          mmTop = 1588
          mmWidth = 103188
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppDBText9: TppDBText
          UserName = 'DBText9'
          DataField = 'biayaServis'
          DataPipeline = ppDBPipeline1
          DisplayFormat = ',# -'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = [fsBold]
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'ppDBPipeline1'
          mmHeight = 3440
          mmLeft = 162395
          mmTop = 1588
          mmWidth = 10787
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppDBText7: TppDBText
          UserName = 'DBText7'
          DataField = 'totalBersih'
          DataPipeline = ppDBPipeline1
          DisplayFormat = ',# -'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = [fsBold]
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'ppDBPipeline1'
          mmHeight = 3440
          mmLeft = 115094
          mmTop = 1588
          mmWidth = 17198
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppDBText10: TppDBText
          UserName = 'DBText10'
          DataField = 'biaya_listrik'
          DataPipeline = ppDBPipeline1
          DisplayFormat = ',# -'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = [fsBold]
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'ppDBPipeline1'
          mmHeight = 3440
          mmLeft = 175155
          mmTop = 1588
          mmWidth = 10990
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppDBText11: TppDBText
          UserName = 'DBText101'
          DataField = 'ppn'
          DataPipeline = ppDBPipeline1
          DisplayFormat = ',# -'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = [fsBold]
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'ppDBPipeline1'
          mmHeight = 3440
          mmLeft = 211667
          mmTop = 1588
          mmWidth = 17198
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppDBText12: TppDBText
          UserName = 'DBText11'
          DataField = 'nm_dokter'
          DataPipeline = ppDBPipeline1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = [fsBold]
          Transparent = True
          DataPipelineName = 'ppDBPipeline1'
          mmHeight = 3440
          mmLeft = 233363
          mmTop = 1588
          mmWidth = 23813
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppDBText13: TppDBText
          UserName = 'DBText102'
          DataField = 'totalAndDokter'
          DataPipeline = ppDBPipeline1
          DisplayFormat = ',# -'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = [fsBold]
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'ppDBPipeline1'
          mmHeight = 3440
          mmLeft = 260351
          mmTop = 1588
          mmWidth = 17198
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppDBText25: TppDBText
          UserName = 'DBText103'
          DataField = 'biaya_racikan'
          DataPipeline = ppDBPipeline1
          DisplayFormat = ',# -'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = [fsBold]
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'ppDBPipeline1'
          mmHeight = 3440
          mmLeft = 187418
          mmTop = 1614
          mmWidth = 11377
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLine4: TppLine
          UserName = 'Line4'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 161925
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLine10: TppLine
          UserName = 'Line10'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 173820
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLine14: TppLine
          UserName = 'Line14'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 140759
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLine12: TppLine
          UserName = 'Line101'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 186002
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLine19: TppLine
          UserName = 'Line19'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 215107
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLine22: TppLine
          UserName = 'Line22'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 230717
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLine27: TppLine
          UserName = 'Line27'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 258763
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLine28: TppLine
          UserName = 'Line28'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 115298
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLine39: TppLine
          UserName = 'Line39'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpRight
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 280400
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLine40: TppLine
          UserName = 'Line40'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 1058
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLine1: TppLine
          UserName = 'Line1'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Weight = 0.500000000000000000
          mmHeight = 1588
          mmLeft = 1025
          mmTop = 0
          mmWidth = 282624
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppDBText4: TppDBText
          UserName = 'DBText4'
          DataField = 'biaya_dokter'
          DataPipeline = ppDBPipeline1
          DisplayFormat = ',# -'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = [fsBold]
          TextAlignment = taRightJustified
          Transparent = True
          DataPipelineName = 'ppDBPipeline1'
          mmHeight = 3440
          mmLeft = 201348
          mmTop = 1588
          mmWidth = 12402
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
        object ppLine7: TppLine
          UserName = 'Line7'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 199761
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 0
          LayerName = Foreground
        end
      end
      object ppGroupFooterBand1: TppGroupFooterBand
        Background.Brush.Style = bsClear
        HideWhenOneDetail = False
        mmBottomOffset = 0
        mmHeight = 0
        mmPrintPosition = 0
      end
    end
    object ppGroup2: TppGroup
      BreakName = 'diskripsi'
      DataPipeline = ppDBPipeline1
      GroupFileSettings.NewFile = False
      GroupFileSettings.EmailFile = False
      KeepTogether = True
      OutlineSettings.CreateNode = True
      StartOnOddPage = False
      UserName = 'Group2'
      mmNewColumnThreshold = 0
      mmNewPageThreshold = 0
      DataPipelineName = 'ppDBPipeline1'
      NewFile = False
      object ppGroupHeaderBand2: TppGroupHeaderBand
        Background.Brush.Style = bsClear
        mmBottomOffset = 0
        mmHeight = 4498
        mmPrintPosition = 0
        object ppDBText2: TppDBText
          UserName = 'DBText2'
          DataField = 'diskripsi'
          DataPipeline = ppDBPipeline1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Name = 'Arial'
          Font.Size = 8
          Font.Style = [fsBold]
          SuppressRepeatedValues = True
          Transparent = True
          DataPipelineName = 'ppDBPipeline1'
          mmHeight = 4233
          mmLeft = 21431
          mmTop = 709
          mmWidth = 24606
          BandType = 3
          GroupNo = 1
          LayerName = Foreground
        end
        object ppLine2: TppLine
          UserName = 'Line2'
          Border.Color = clAqua
          Pen.Color = cl3DDkShadow
          Weight = 0.500000000000000000
          mmHeight = 962
          mmLeft = 1082
          mmTop = 60
          mmWidth = 114217
          BandType = 3
          GroupNo = 1
          LayerName = Foreground
        end
        object ppLine5: TppLine
          UserName = 'Line5'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 161925
          mmTop = 265
          mmWidth = 3969
          BandType = 3
          GroupNo = 1
          LayerName = Foreground
        end
        object ppLine9: TppLine
          UserName = 'Line9'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 173820
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 1
          LayerName = Foreground
        end
        object ppLine15: TppLine
          UserName = 'Line15'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 140759
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 1
          LayerName = Foreground
        end
        object ppLine13: TppLine
          UserName = 'Line13'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 186002
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 1
          LayerName = Foreground
        end
        object ppLine20: TppLine
          UserName = 'Line20'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 215107
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 1
          LayerName = Foreground
        end
        object ppLine23: TppLine
          UserName = 'Line23'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 230717
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 1
          LayerName = Foreground
        end
        object ppLine26: TppLine
          UserName = 'Line26'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 258763
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 1
          LayerName = Foreground
        end
        object ppLine30: TppLine
          UserName = 'Line30'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 115359
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 1
          LayerName = Foreground
        end
        object ppLine38: TppLine
          UserName = 'Line38'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpRight
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 280400
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 1
          LayerName = Foreground
        end
        object ppLine41: TppLine
          UserName = 'Line401'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 1058
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 1
          LayerName = Foreground
        end
        object ppLine44: TppLine
          UserName = 'Line44'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 5027
          mmLeft = 16669
          mmTop = 296
          mmWidth = 3969
          BandType = 3
          GroupNo = 1
          LayerName = Foreground
        end
        object ppLine34: TppLine
          UserName = 'Line34'
          Border.Color = clAppWorkSpace
          Pen.Color = clAppWorkSpace
          Position = lpLeft
          Weight = 0.500000000000000000
          mmHeight = 7408
          mmLeft = 199761
          mmTop = 0
          mmWidth = 3969
          BandType = 3
          GroupNo = 1
          LayerName = Foreground
        end
        object ppLine35: TppLine
          UserName = 'Line35'
          Border.Color = clAqua
          Pen.Color = cl3DDkShadow
          Pen.Width = 0
          Weight = 0.300000011920929000
          mmHeight = 1588
          mmLeft = 140759
          mmTop = 0
          mmWidth = 21287
          BandType = 3
          GroupNo = 1
          LayerName = Foreground
        end
      end
      object ppGroupFooterBand2: TppGroupFooterBand
        Background.Brush.Style = bsClear
        HideWhenOneDetail = False
        mmBottomOffset = 0
        mmHeight = 0
        mmPrintPosition = 0
      end
    end
    object ppDesignLayers1: TppDesignLayers
      object ppDesignLayer1: TppDesignLayer
        UserName = 'Foreground'
        LayerType = ltBanded
        Index = 0
      end
    end
    object ppParameterList1: TppParameterList
    end
  end
  object ppDBPipeline1: TppDBPipeline
    DataSource = ds_obat_keluar_print
    UserName = 'DBPipeline1'
    Left = 648
    Top = 32
  end
  object ds_obat_keluar_print: TMyDataSource
    DataSet = q_obat_keluar_print
    Left = 420
    Top = 380
  end
  object q_obat_keluar_print: TMyQuery
    Connection = dm.my_connection
    SQL.Strings = (
      'select * from v_obat_keluar')
    Left = 686
    Top = 434
  end
  object PrintDBGridEh1: TPrintDBGridEh
    DBGridEh = DBGridEh2
    Options = [pghFitGridToPageWidth, pghColored]
    Page.BottomMargin = 1.000000000000000000
    Page.LeftMargin = 0.500000000000000000
    Page.RightMargin = 0.500000000000000000
    Page.TopMargin = 1.000000000000000000
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -9
    PageFooter.Font.Name = 'Tahoma'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -16
    PageHeader.Font.Name = 'Tahoma'
    PageHeader.Font.Style = []
    PageHeader.LineType = pcltDoubleLine
    Title.Strings = (
      'PRINT TABLE')
    Units = MM
    Left = 856
    Top = 440
  end
  object q_obat_keluar: TMyQuery
    Connection = dm.my_connection
    SQL.Strings = (
      'select * from v_obat_keluar')
    Left = 844
    Top = 842
    object q_obat_keluarid: TLargeintField
      FieldName = 'id'
      Origin = 't_user.id'
    end
    object q_obat_keluarnm_obat: TStringField
      FieldName = 'nm_obat'
      Origin = 't_user.nm_obat'
      Size = 100
    end
    object q_obat_keluarsatuan: TStringField
      FieldName = 'satuan'
      Origin = 't_user.satuan'
      FixedChar = True
      Size = 15
    end
    object q_obat_keluarquantity_racik: TFloatField
      FieldName = 'quantity_racik'
      Origin = 't_user.quantity_racik'
    end
    object q_obat_keluarjlhper_raciik: TIntegerField
      FieldName = 'jlhper_raciik'
      Origin = 't_user.jlhper_raciik'
    end
    object q_obat_keluarsatuan_racik: TStringField
      FieldName = 'satuan_racik'
      Origin = 't_user.satuan_racik'
      Size = 50
    end
    object q_obat_keluarexpire_date: TDateField
      FieldName = 'expire_date'
      Origin = 't_user.expire_date'
    end
    object q_obat_keluartgl_jatuh_tempo: TDateField
      FieldName = 'tgl_jatuh_tempo'
      Origin = 't_user.tgl_jatuh_tempo'
    end
    object q_obat_keluarhrgper_racik: TIntegerField
      FieldName = 'hrgper_racik'
      Origin = 't_user.hrgper_racik'
    end
    object q_obat_keluarhrga_jual: TIntegerField
      FieldName = 'hrga_jual'
      Origin = 't_user.hrga_jual'
    end
    object q_obat_keluarjenis: TStringField
      FieldName = 'jenis'
      Origin = 't_user.jenis'
      FixedChar = True
      Size = 5
    end
    object q_obat_keluarjlhper_pack: TIntegerField
      FieldName = 'jlhper_pack'
      Origin = 't_user.jlhper_pack'
    end
    object q_obat_keluarsatuan_pack: TStringField
      FieldName = 'satuan_pack'
      Origin = 't_user.satuan_pack'
      Size = 100
    end
    object q_obat_keluarhrgper_pack: TIntegerField
      FieldName = 'hrgper_pack'
      Origin = 't_user.hrgper_pack'
    end
    object q_obat_keluarket: TMemoField
      FieldName = 'ket'
      Origin = 't_user.ket'
      BlobType = ftMemo
    end
    object q_obat_keluarnorec: TLargeintField
      FieldName = 'norec'
      Origin = 't_user.norec'
    end
    object q_obat_keluarno_nota: TStringField
      FieldName = 'no_nota'
      Origin = 't_user.no_nota'
      Size = 50
    end
    object q_obat_keluarnama_pengunjung: TStringField
      FieldName = 'nama_pengunjung'
      Origin = 't_user.nama_pengunjung'
      Size = 100
    end
    object q_obat_keluarid_pasien: TLargeintField
      FieldName = 'id_pasien'
      Origin = 't_user.id_pasien'
    end
    object q_obat_keluartgl_obat_keluar: TDateTimeField
      FieldName = 'tgl_obat_keluar'
      Origin = 't_user.tgl_obat_keluar'
    end
    object q_obat_keluarid_obat: TLargeintField
      FieldName = 'id_obat'
      Origin = 't_user.id_obat'
    end
    object q_obat_keluarjumlah_obat_keluar: TFloatField
      FieldName = 'jumlah_obat_keluar'
      Origin = 't_user.jumlah_obat_keluar'
    end
    object q_obat_keluarjlh_sub_racikan: TSmallintField
      FieldName = 'jlh_sub_racikan'
      Origin = 't_user.jlh_sub_racikan'
    end
    object q_obat_keluarbiaya_listrik: TIntegerField
      FieldName = 'biaya_listrik'
      Origin = 't_user.biaya_listrik'
    end
    object q_obat_keluarbiaya_admin: TIntegerField
      FieldName = 'biaya_admin'
      Origin = 't_user.biaya_admin'
    end
    object q_obat_keluarnm_dokter: TStringField
      FieldName = 'nm_dokter'
      Origin = 't_user.nm_dokter'
      Size = 150
    end
    object q_obat_keluartot_harga: TIntegerField
      FieldName = 'tot_harga'
      Origin = 't_user.tot_harga'
    end
    object q_obat_keluaruang: TIntegerField
      FieldName = 'uang'
      Origin = 't_user.uang'
    end
    object q_obat_keluarkembalian: TIntegerField
      FieldName = 'kembalian'
      Origin = 't_user.kembalian'
    end
    object q_obat_keluarinsert_at: TDateTimeField
      FieldName = 'insert_at'
      Origin = 't_user.insert_at'
    end
    object q_obat_keluarracikan: TStringField
      FieldName = 'racikan'
      Origin = 't_user.racikan'
      FixedChar = True
      Size = 1
    end
    object q_obat_keluarbiaya_dokter: TIntegerField
      FieldName = 'biaya_dokter'
      Origin = 't_user.biaya_dokter'
    end
    object q_obat_keluarid_jenis_racikan: TIntegerField
      FieldName = 'id_jenis_racikan'
      Origin = 't_user.id_jenis_racikan'
    end
    object q_obat_keluarid_user: TIntegerField
      FieldName = 'id_user'
      Origin = 't_user.id_user'
    end
    object q_obat_keluarhrgajualbersih: TIntegerField
      FieldName = 'hrgajualbersih'
      Origin = 't_user.hrgajualbersih'
    end
    object q_obat_keluarracikan1: TStringField
      FieldName = 'racikan1'
      Origin = 't_user.racikan1'
      Size = 1
    end
    object q_obat_keluarid_jenis_racikan1: TIntegerField
      FieldName = 'id_jenis_racikan1'
      Origin = 't_user.id_jenis_racikan1'
    end
    object q_obat_keluaradaRacikan: TStringField
      FieldName = 'adaRacikan'
      Origin = 't_user.adaRacikan'
      Size = 1
    end
    object q_obat_keluardiskripsi: TStringField
      FieldName = 'diskripsi'
      Origin = 't_user.diskripsi'
      Size = 59
    end
    object q_obat_keluarbiaya: TIntegerField
      FieldName = 'biaya'
      Origin = 't_user.biaya'
    end
    object q_obat_keluarusername: TStringField
      FieldName = 'username'
      Origin = 't_user.username'
      Size = 200
    end
    object q_obat_keluarbiayaServis: TIntegerField
      FieldName = 'biayaServis'
      Origin = 't_user.biayaServis'
    end
    object q_obat_keluartotalBersih: TLargeintField
      FieldName = 'totalBersih'
      Origin = 't_user.totalBersih'
    end
    object q_obat_keluarppn: TFloatField
      FieldName = 'ppn'
      Origin = 't_user.ppn'
    end
    object q_obat_keluarbiaya_racikan: TLargeintField
      FieldName = 'biaya_racikan'
      Origin = 't_user.biaya_racikan'
    end
    object q_obat_keluarsatuanjual: TStringField
      FieldName = 'satuanjual'
      Origin = 't_user.satuanjual'
      Size = 50
    end
    object q_obat_keluarquantity: TFloatField
      FieldName = 'quantity'
      Origin = 't_user.quantity'
    end
  end
  object report__Print_obat_keluar: TppReport
    AutoStop = False
    DataPipeline = print_obat_keluar
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A5'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 2540
    PrinterSetup.mmMarginLeft = 5080
    PrinterSetup.mmMarginRight = 2540
    PrinterSetup.mmMarginTop = 2540
    PrinterSetup.mmPaperHeight = 210000
    PrinterSetup.mmPaperWidth = 148000
    PrinterSetup.PaperSize = 256
    Template.FileName = 'D:\RS KOTA\APP ++\10. Apotek Praya\Report\nota Detail  mm.rtm'
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    Left = 1720
    Top = 600
    Version = '15.04'
    mmColumnWidth = 69921
    DataPipelineName = 'print_obat_keluar'
    object ppHeaderBand2: TppHeaderBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 35454
      mmPrintPosition = 0
      object ppLabel8: TppLabel
        UserName = 'Label1'
        Caption = 'Apotek'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 2910
        mmTop = 1588
        mmWidth = 10848
        BandType = 0
        LayerName = Foreground1
      end
      object ppLabel25: TppLabel
        UserName = 'Label2'
        Caption = 'APOTEK SEJAHTERA'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4234
        mmLeft = 2646
        mmTop = 5556
        mmWidth = 35189
        BandType = 0
        LayerName = Foreground1
      end
      object ppSystemVariable4: TppSystemVariable
        UserName = 'SystemVariable1'
        VarType = vtDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 20902
        mmTop = 23663
        mmWidth = 38629
        BandType = 0
        LayerName = Foreground1
      end
      object ppLabel26: TppLabel
        UserName = 'Label3'
        Caption = 'Tanggal  :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 2646
        mmTop = 23663
        mmWidth = 15081
        BandType = 0
        LayerName = Foreground1
      end
      object ppLabel27: TppLabel
        UserName = 'Label5'
        Caption = 
          'Jl. Diponegoro No.40, Praya, Kabupaten Lombok Tengah, Nusa Tengg' +
          'ara Bar. 83511'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 2646
        mmTop = 8996
        mmWidth = 129646
        BandType = 0
        LayerName = Foreground1
      end
      object ppLabel28: TppLabel
        UserName = 'Label4'
        Caption = 'Nama Obat'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 2646
        mmTop = 30427
        mmWidth = 42333
        BandType = 0
        LayerName = Foreground1
      end
      object ppLabel29: TppLabel
        UserName = 'Label7'
        Caption = 'Qty'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 59267
        mmTop = 30427
        mmWidth = 5556
        BandType = 0
        LayerName = Foreground1
      end
      object ppLabel30: TppLabel
        UserName = 'Label8'
        Caption = 'Harga Satuan'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 86254
        mmTop = 30427
        mmWidth = 23019
        BandType = 0
        LayerName = Foreground1
      end
      object ppLabel31: TppLabel
        UserName = 'Label9'
        Caption = 'Total'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4233
        mmLeft = 125148
        mmTop = 30692
        mmWidth = 8467
        BandType = 0
        LayerName = Foreground1
      end
      object ppLine17: TppLine
        UserName = 'Line1'
        Position = lpBottom
        Weight = 0.750000000000000000
        mmHeight = 1323
        mmLeft = 1588
        mmTop = 34131
        mmWidth = 137054
        BandType = 0
        LayerName = Foreground1
      end
      object ppLabel32: TppLabel
        UserName = 'Label21'
        Caption = 'No Nota : '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 2646
        mmTop = 18521
        mmWidth = 15610
        BandType = 0
        LayerName = Foreground1
      end
      object ppDBText21: TppDBText
        UserName = 'DBText21'
        DataField = 'no_nota'
        DataPipeline = print_obat_keluar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 20902
        mmTop = 18521
        mmWidth = 38629
        BandType = 0
        LayerName = Foreground1
      end
    end
    object ppDetailBand2: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 5556
      mmPrintPosition = 0
      object ppDBText14: TppDBText
        UserName = 'DBText4'
        DataField = 'harga_satuan'
        DataPipeline = print_obat_keluar
        DisplayFormat = ',0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4173
        mmLeft = 79111
        mmTop = 590
        mmWidth = 25991
        BandType = 4
        LayerName = Foreground1
      end
      object ppDBText15: TppDBText
        UserName = 'DBText5'
        DataField = 'total_harga'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4172
        mmLeft = 106098
        mmTop = 590
        mmWidth = 31485
        BandType = 4
        LayerName = Foreground1
      end
      object ppDBText16: TppDBText
        UserName = 'DBText8'
        DataField = 'jumlah_obat_keluar'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 48683
        mmTop = 590
        mmWidth = 11050
        BandType = 4
        LayerName = Foreground1
      end
      object ppDBText17: TppDBText
        UserName = 'DBText9'
        DataField = 'satuan'
        DataPipeline = print_obat_keluar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 60854
        mmTop = 590
        mmWidth = 15719
        BandType = 4
        LayerName = Foreground1
      end
      object ppDBText18: TppDBText
        UserName = 'DBText10'
        DataField = 'nama_obat'
        DataPipeline = print_obat_keluar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 2646
        mmTop = 590
        mmWidth = 43392
        BandType = 4
        LayerName = Foreground1
      end
    end
    object ppSummaryBand2: TppSummaryBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 39952
      mmPrintPosition = 0
      object ppLabel33: TppLabel
        UserName = 'Label10'
        Caption = 'Total :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 106892
        mmTop = 3514
        mmWidth = 9525
        BandType = 7
        LayerName = Foreground1
      end
      object ppLabel34: TppLabel
        UserName = 'Label6'
        Caption = 'Terima Kasih Atas kunjungan anda ...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 1588
        mmTop = 34660
        mmWidth = 57679
        BandType = 7
        LayerName = Foreground1
      end
      object ppLine29: TppLine
        UserName = 'Line2'
        Position = lpBottom
        Weight = 0.750000000000000000
        mmHeight = 467
        mmLeft = 1512
        mmTop = 0
        mmWidth = 138051
        BandType = 7
        LayerName = Foreground1
      end
      object ppLabel35: TppLabel
        UserName = 'Label101'
        Caption = 'Uang Cash :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 97367
        mmTop = 13494
        mmWidth = 19050
        BandType = 7
        LayerName = Foreground1
      end
      object ppDBText19: TppDBText
        UserName = 'DBText11'
        DataField = 'uang'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        VerticalAlignment = avCenter
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 107421
        mmTop = 13494
        mmWidth = 29369
        BandType = 7
        LayerName = Foreground1
      end
      object ppLabel36: TppLabel
        UserName = 'Label14'
        Caption = 'Kembalian :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 98161
        mmTop = 18785
        mmWidth = 18256
        BandType = 7
        LayerName = Foreground1
      end
      object ppDBText20: TppDBText
        UserName = 'DBText12'
        DataField = 'kembalian'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        VerticalAlignment = avCenter
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 107421
        mmTop = 18521
        mmWidth = 29369
        BandType = 7
        LayerName = Foreground1
      end
      object ppLabel37: TppLabel
        UserName = 'Label15'
        Caption = 'Biaya Listrik :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 8202
        mmTop = 3514
        mmWidth = 21167
        BandType = 7
        LayerName = Foreground1
      end
      object ppDBText22: TppDBText
        UserName = 'DBText15'
        DataField = 'biayaadmin'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        VerticalAlignment = avCenter
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 20902
        mmTop = 3514
        mmWidth = 29369
        BandType = 7
        LayerName = Foreground1
      end
      object ppDBCalc4: TppDBCalc
        UserName = 'DBCalc4'
        DataField = 'total_harga'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        VerticalAlignment = avCenter
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 116681
        mmTop = 3514
        mmWidth = 19579
        BandType = 7
        LayerName = Foreground1
      end
      object ppLabel38: TppLabel
        UserName = 'Label102'
        Caption = 'Total Bayar :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 96838
        mmTop = 8731
        mmWidth = 19579
        BandType = 7
        LayerName = Foreground1
      end
      object ppDBText23: TppDBText
        UserName = 'DBText16'
        DataField = 'tot_harga'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        VerticalAlignment = avCenter
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 107421
        mmTop = 8467
        mmWidth = 29369
        BandType = 7
        LayerName = Foreground1
      end
      object ppDBText24: TppDBText
        UserName = 'DBText17'
        DataField = 'biayaServis'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        VerticalAlignment = avCenter
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 20902
        mmTop = 8467
        mmWidth = 29369
        BandType = 7
        LayerName = Foreground1
      end
      object ppLabel39: TppLabel
        UserName = 'Label17'
        Caption = 'Biaya Servis :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 8467
        mmTop = 8467
        mmWidth = 21167
        BandType = 7
        LayerName = Foreground1
      end
      object ppLabel40: TppLabel
        UserName = 'Label32'
        Caption = 'Biaya Racikan :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 5292
        mmTop = 13378
        mmWidth = 24342
        BandType = 7
        LayerName = Foreground1
      end
      object ppDBCalc2: TppDBCalc
        UserName = 'DBCalc2'
        DataField = 'biayaRacik'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        VerticalAlignment = avCenter
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 30692
        mmTop = 13494
        mmWidth = 19579
        BandType = 7
        LayerName = Foreground1
      end
    end
    object ppDesignLayers2: TppDesignLayers
      object ppDesignLayer2: TppDesignLayer
        UserName = 'Foreground1'
        LayerType = ltBanded
        Index = 0
      end
    end
    object ppParameterList2: TppParameterList
    end
  end
  object print_obat_keluar: TppDBPipeline
    DataSource = ds_PrintNota
    UserName = 'print_obat_keluar'
    Left = 1720
    Top = 544
  end
  object ds_PrintNota: TMyDataSource
    DataSet = q_printNota
    Left = 1712
    Top = 456
  end
  object q_printNota: TMyQuery
    Connection = dm.my_connection
    SQL.Strings = (
      'CALL `sp_cetak_nota`('#39'N070819922302'#39',1,1000)')
    Left = 1712
    Top = 408
  end
end
