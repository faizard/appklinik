unit U_Obat;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh,
  PrnDbgeh, DB, MemDS, DBAccess, MyAccess, ppDB, ppDBPipe, ppParameter,
  ppDesignLayer, ppBands, ppVar, ppCtrls, ppPrnabl, ppClass, ppCache,
  ppComm, ppRelatv, ppProd, ppReport, GridsEh, DBAxisGridsEh, DBGridEh,
  StdCtrls, Mask, sMaskEdit, sCustomComboEdit, sToolEdit, sLabel, Buttons,
  sBitBtn, ComCtrls, acProgressBar, ExtCtrls, ToolWin, sToolBar, sPanel,
  sGroupBox, acPNG, acImage, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinsdxStatusBarPainter, dxStatusBar, sComboBox, sPageControl, sEdit, ShellAPI,ComObj;

type
  Tfrm_obat = class(TForm)
    pnl_1: TsPanel;
    sToolBar1: TsToolBar;
    btn1: TToolButton;
    img2: TImage;
    sprog_export_obatmasuk: TsProgressBar;
    pnl_2: TsPanel;
    btn_1: TsBitBtn;
    btn_refresh: TsBitBtn;
    btn_5: TsBitBtn;
    pnl_priode: TsPanel;
    lbl_1: TsLabel;
    lbl_2: TsLabel;
    sDateEdit1: TsDateEdit;
    sDateEdit2: TsDateEdit;
    btn_2: TsBitBtn;
    btn_4: TsBitBtn;
    sPanel1: TsPanel;
    lbl_3: TLabel;
    dbg_entry_obat: TDBGridEh;
    DBGridEh1: TDBGridEh;
    ppReport_obat_terjual: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppShape2: TppShape;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLine10: TppLine;
    ppLine11: TppLine;
    ppLine12: TppLine;
    ppLine13: TppLine;
    ppLine14: TppLine;
    ppLine16: TppLine;
    ppLabel12: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppLine18: TppLine;
    ppLabel15: TppLabel;
    ppLabel_priode: TppLabel;
    ppLabel20: TppLabel;
    ppLine20: TppLine;
    ppLabel21: TppLabel;
    ppLine21: TppLine;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppLine2: TppLine;
    ppLine3: TppLine;
    ppLine4: TppLine;
    ppLine1: TppLine;
    ppLine5: TppLine;
    ppLine6: TppLine;
    ppLine7: TppLine;
    ppLine8: TppLine;
    ppLine15: TppLine;
    ppDBCalc3: TppDBCalc;
    ppDBText8: TppDBText;
    ppLine19: TppLine;
    ppLine22: TppLine;
    ppDBText9: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppLabel9: TppLabel;
    ppSystemVariable1: TppSystemVariable;
    ppLabel10: TppLabel;
    ppSystemVariable2: TppSystemVariable;
    ppSummaryBand1: TppSummaryBand;
    ppShape1: TppShape;
    ppDBCalc1: TppDBCalc;
    ppDBCalc2: TppDBCalc;
    ppLabel8: TppLabel;
    ppLine9: TppLine;
    ppLabel11: TppLabel;
    ppLine17: TppLine;
    ppShape3: TppShape;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppShape4: TppShape;
    ppLabel19: TppLabel;
    ppLabel18: TppLabel;
    ppSystemVariable3: TppSystemVariable;
    ppLabel22: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    ppParameterList1: TppParameterList;
    ppDBPipeline1: TppDBPipeline;
    PrintDBGridEh1: TPrintDBGridEh;
    sImage1: TsImage;
    sLabel1: TsLabel;
    sPanel3: TsPanel;
    sLabel2: TsLabel;
    sPanel4: TsPanel;
    sLabel3: TsLabel;
    cbb_jenis: TsComboBox;
    radioGroup_quantity: TsRadioGroup;
    radioGroup_expire: TsRadioGroup;
    img1: TImage;
    img3: TImage;
    btn_detail: TsBitBtn;
    tab_kontrol: TsPageControl;
    sTabSheet1: TsTabSheet;
    sTabSheet2: TsTabSheet;
    grid_obat: TDBGridEh;
    q_data_obat_masuk: TMyQuery;
    ds_obat_masuk: TMyDataSource;
    sPanel2: TsPanel;
    cbb_obatjatuhtempo: TsComboBox;
    sLabel4: TsLabel;
    dbg_obat_jatuh_tempo: TDBGridEh;
    sTabSheet3: TsTabSheet;
    sPanel6: TsPanel;
    lbl2: TLabel;
    sPanel7: TsPanel;
    btn_7: TsBitBtn;
    dxStatusBar1: TdxStatusBar;
    dbg_jatuhtempo: TDBGridEh;
    dbg_jatuhtempoDetail: TDBGridEh;
    q_data_obat_masuk_detail: TMyQuery;
    ds_obat_masukDetail: TMyDataSource;
    sLabel5: TsLabel;
    edt_stok: TsEdit;
    dlgSave1: TSaveDialog;
    procedure FormShow(Sender: TObject);
    procedure grid_obatDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure btn_refreshClick(Sender: TObject);
    procedure cbb_jenisChange(Sender: TObject);
    procedure radioGroup_quantityClick(Sender: TObject);
    procedure btn_3Click(Sender: TObject);
    procedure radioGroup_expireClick(Sender: TObject);
    procedure btn_detailClick(Sender: TObject);
    procedure grid_obatDblClick(Sender: TObject);
    procedure cbb_obatjatuhtempoChange(Sender: TObject);
    procedure dbg_entry_obatFillSTFilterListValues(Sender: TCustomDBGridEh;
      Column: TColumnEh; Items: TStrings; var Processed: Boolean);
    procedure btn_6Click(Sender: TObject);
    procedure dbg_jatuhtempoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure dbg_jatuhtempoCellClick(Column: TColumnEh);
    procedure edt_stokKeyPress(Sender: TObject; var Key: Char);
    procedure edt_stokChange(Sender: TObject);
    procedure btn_5Click(Sender: TObject);
  private
    procedure FilterData();
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_obat: Tfrm_obat;
  

implementation

uses
  U_DM, u_bantu, U_pembayaran_tagihan, U_JlhObatTerjual, U_inputJumlahObat;

{$R *.dfm}

procedure Tfrm_obat.FilterData;
var
  sql : ShortString;
begin
   frm_obat.tab_kontrol.ActivePageIndex := 0;
  sql :='SELECT * FROM v_obat where enabled = 1 ';
        case  radioGroup_quantity.ItemIndex of
                0: sql := sql +' and quantity <>0';
                1: sql := sql +' and quantity <= 0';
                2: sql := sql +' and quantity is not null';
        end;

        case cbb_jenis.ItemIndex of
            1: sql := sql +' and jenis = "'+cream+'"';
            2: sql := sql +' and jenis = "'+nonCream+'"';
        end;

        case radioGroup_expire.ItemIndex of
            0: sql := sql +' and DATEDIFF(expire_date, CURRENT_DATE()) <=0';
            1: sql := sql +' and DATEDIFF(expire_date, CURRENT_DATE()) <=180';
        end;

        if edt_stok.Text <>'' then
        begin
          radioGroup_quantity.ItemIndex:=-1;
          sql := sql +' and quantity >= '+ edt_stok.Text;
        end;



        sql:= sql +' order by expire_date asc' ;
        GetSQL(sql,dm.q_obat);
        dxStatusBar1.Panels.Items[0].Text :='Jumlah Obat : '+IntToStr(dm.q_obat.RecordCount);

        cekobatExpire:= False;
end;

procedure Tfrm_obat.FormShow(Sender: TObject);
begin


//      if cekObatHabis then
//      begin
//        radioGroup_quantity.ItemIndex := 1;
//        radioGroup_expire.ItemIndex:=1;
//        cbb_jenis.ItemIndex:=0;
//      end;

      if cekobatExpire then
//      begin
//        radioGroup_quantity.ItemIndex:=2;
//        radioGroup_expire.ItemIndex:=1;
//        cbb_jenis.ItemIndex:=0;
//      end;

      FilterData;
      tab_kontrol.TabHeight:=1;
      tab_kontrol.TabWidth:=1;
//                          

end;

procedure Tfrm_obat.grid_obatDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
  var
      b: Byte;
begin
 if dm.q_obat.FieldByName('quantity').AsInteger <= 0 then
        grid_obat.Canvas.Brush.Color:=$00A9CCFC;

    if dm.q_obat.FieldByName('i_expire').AsInteger <=180  then
    begin
         grid_obat.Canvas.Brush.Color:=$00B7B7FF;
    end;
    if dm.q_obat.FieldByName('i_expire').AsInteger <=0  then
    begin
         grid_obat.Canvas.Brush.Color:=clRed;
         grid_obat.Canvas.Font.Color:= clWhite;
    end;



        grid_obat.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure Tfrm_obat.btn_refreshClick(Sender: TObject);
begin
  radioGroup_quantity.ItemIndex:=2;
  cbb_jenis.ItemIndex:=0;
  radioGroup_expire.ItemIndex:=2;
  FilterData;
  tab_kontrol.TabIndex:= 0;
  cbb_obatjatuhtempo.ItemIndex:=0;
  edt_stok.Text :='';
end;



procedure Tfrm_obat.cbb_jenisChange(Sender: TObject);
begin
        FilterData;
end;

procedure Tfrm_obat.radioGroup_quantityClick(Sender: TObject);
begin
        FilterData();
end;

procedure Tfrm_obat.btn_3Click(Sender: TObject);
begin
    Free;
end;

procedure Tfrm_obat.radioGroup_expireClick(Sender: TObject);
begin
      FilterData();
end;

procedure Tfrm_obat.btn_detailClick(Sender: TObject);
begin

    GetSQL('SELECT * FROM v_obat_jatuh_tempo where id_obat ='+dm.q_obatid.Text,q_data_obat_masuk);
    tab_kontrol.TabIndex:= 1;

end;

procedure Tfrm_obat.grid_obatDblClick(Sender: TObject);
begin
    btn_detailClick(Sender);
end;

procedure Tfrm_obat.cbb_obatjatuhtempoChange(Sender: TObject);
begin
    case cbb_obatjatuhtempo.ItemIndex of
        1:
        begin
            GetSQL('SELECT	jt.no_faktur,jt.namapbf, jt.tgl_fakur,jt.tgl_jatuh_tempo,count(jt.id_obat) as'
                  +' jumlahObat FROM v_obat_jatuh_tempo AS jt WHERE i_jatuhtempo <= 182 	AND ( lunas = "0" OR lunas IS NULL ) '
                  +' GROUP BY NO_FAKTUR',q_data_obat_masuk);
                  
             tab_kontrol.ActivePage := sTabSheet3;
        end
          else
            tab_kontrol.ActivePage := sTabSheet1;
    end;
end;

procedure Tfrm_obat.dbg_entry_obatFillSTFilterListValues(
  Sender: TCustomDBGridEh; Column: TColumnEh; Items: TStrings;
  var Processed: Boolean);
begin
    ShowMessage('dddd');
end;

procedure Tfrm_obat.btn_6Click(Sender: TObject);
begin
      no_faktur := q_data_obat_masuk.fieldByname('no_faktur').Text;

          if not IsFormOpen('frm_pembayaran') then
    frm_pembayaran := Tfrm_pembayaran.Create(Self) ;


      frm_pembayaran.ShowModal;
end;

procedure Tfrm_obat.dbg_jatuhtempoDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
begin
//    if q_data_obat_masuk.FieldByName('i_jatuhtempo').AsInteger <=14  then
//         dbg_obat_jatuh_tempo.Canvas.Brush.Color:=$004080FF;

    if q_data_obat_masuk_detail.FieldByName('i_jatuhtempo').AsInteger <=0  then
         dbg_jatuhtempoDetail.Canvas.Brush.Color:=$004080FF;

         dbg_jatuhtempoDetail.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure Tfrm_obat.dbg_jatuhtempoCellClick(Column: TColumnEh);
begin
    q_data_obat_masuk_detail.ParamByName('noFaktur').AsString  := dbg_obat_jatuh_tempo.Fields[0].AsString;
                             q_data_obat_masuk_detail.Execute;
//      :noFaktur
end;

procedure Tfrm_obat.edt_stokKeyPress(Sender: TObject; var Key: Char);
begin

 // #8 is Backspace
  if not (Key in [#8, '0'..'9']) then begin
    Key := #0;
    Exit;
  end;

end;

procedure Tfrm_obat.edt_stokChange(Sender: TObject);
begin
      FilterData();
end;

procedure Tfrm_obat.btn_5Click(Sender: TObject);
Var i,x:integer;
sfile:string;
begin
sprog_export_obatmasuk.Show;

if savedlg.Execute then
begin

    sprog_export_obatmasuk.Position:=0;
    XlApp  := CreateOleObject('Excel.Application');
    XlBook := XlApp.WorkBooks.Add;
    XlSheet  := XlBook.worksheets.add;
    grid_obat.Hide;
    XlSheet.cells[1,1].value := 'DATA OBAT';
   for i:=0 to grid_obat.FieldCount-1 do
   begin
      XlSheet.cells[2,i+1].value:=grid_obat.columns[i].Title.Caption;
   end;
  dm.q_obat.First;
  x:=1;
//      sprog_export_obatmasuk.Show;
      sprog_export_obatmasuk.Max:= dm.q_obat.RecordCount;

  while not dm.q_obat.Eof do
  begin
   sprog_export_obatmasuk.Position:= dm.q_obat.RecNo;
   for i:=0 to grid_obat.FieldCount-1 do
    begin
    XlSheet.cells[2+x,i+1].value:=grid_obat.Fields[i].Text;
    end;
    dm.q_obat.Next;
   inc(x);

  end;

end;
  sprog_export_obatmasuk.Hide;
  grid_obat.Show;
    XlApp.ActiveWorkbook.SaveAs(savedlg.FileName);
    XlApp.visible:=true;



end;

end.
