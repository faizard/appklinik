object frm_jlh_obat_keluar: Tfrm_jlh_obat_keluar
  Left = 611
  Top = 51
  BorderStyle = bsToolWindow
  Caption = 'Jumlah Obat'
  ClientHeight = 565
  ClientWidth = 584
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Trebuchet MS'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 584
    Height = 241
    Align = alTop
    Color = 14408667
    TabOrder = 0
    SkinData.CustomColor = True
    SkinData.SkinSection = 'PANEL'
    object lbl_11: TsLabel
      Left = 33
      Top = 79
      Width = 92
      Height = 20
      Caption = 'Nama Barang'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
    end
    object sLabel1: TsLabel
      Left = 33
      Top = 115
      Width = 61
      Height = 20
      Caption = 'Sisa Stok'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
    end
    object sLabel2: TsLabel
      Left = 33
      Top = 152
      Width = 74
      Height = 20
      Caption = 'Harga Jual'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
    end
    object sLabel8: TsLabel
      Left = 33
      Top = 187
      Width = 36
      Height = 20
      Caption = 'Jenis'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
    end
    object sLabel4: TsLabel
      Left = 33
      Top = 43
      Width = 35
      Height = 20
      Caption = 'Kode'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
    end
    object edt_nm_obat: TsEdit
      Left = 143
      Top = 79
      Width = 411
      Height = 32
      CharCase = ecUpperCase
      Color = 15066597
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object edt_stok: TsEdit
      Left = 143
      Top = 115
      Width = 411
      Height = 32
      CharCase = ecUpperCase
      Color = 15066597
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object edt_hrgajual: TsEdit
      Left = 143
      Top = 151
      Width = 411
      Height = 32
      CharCase = ecUpperCase
      Color = 15066597
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sPanel2: TsPanel
      Left = 1
      Top = 1
      Width = 582
      Height = 31
      Align = alTop
      Caption = 'DATA BARANG'
      Color = clActiveCaption
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      SkinData.CustomColor = True
      SkinData.OuterEffects.Visibility = ovAlways
      SkinData.SkinSection = 'PANEL'
    end
    object cbb_jenis: TsComboBox
      Left = 144
      Top = 187
      Width = 411
      Height = 32
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.CustomColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      ReadOnly = True
      CharCase = ecUpperCase
      Color = 15066597
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = []
      ItemHeight = 26
      ItemIndex = -1
      ParentFont = False
      TabOrder = 4
    end
    object edt_1: TsEdit
      Left = 143
      Top = 43
      Width = 411
      Height = 32
      CharCase = ecUpperCase
      Color = 15066597
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
  end
  object sPanel3: TsPanel
    Left = 0
    Top = 241
    Width = 584
    Height = 280
    Align = alTop
    TabOrder = 1
    SkinData.SkinSection = 'PANEL'
    object img3: TImage
      Left = 1
      Top = 1
      Width = 582
      Height = 33
      Align = alTop
    end
    object sLabel13: TsLabel
      Left = 82
      Top = 220
      Width = 99
      Height = 20
      Caption = 'Harga Racikan'
      FocusControl = btn_obat_simpan
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
    end
    object chk_racikan: TsCheckBox
      Left = 31
      Top = 10
      Width = 62
      Height = 26
      Caption = 'Racik'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = chk_racikanClick
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 137
      Width = 582
      Height = 72
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      SkinData.SkinSection = 'TRANSPARENT'
      object lbl_16: TsLabel
        Left = 26
        Top = 12
        Width = 101
        Height = 20
        Caption = 'JUMLAH OBAT'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
      end
      object sLabel3: TsLabel
        Left = 226
        Top = 12
        Width = 56
        Height = 20
        Caption = 'SATUAN'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
      end
      object sLabel11: TsLabel
        Left = 17
        Top = 11
        Width = 24
        Height = 13
        Caption = '*    '
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        UseSkinColor = False
      end
      object sLabel12: TsLabel
        Left = 210
        Top = 11
        Width = 24
        Height = 13
        Caption = '*    '
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        UseSkinColor = False
      end
      object edt_jlh_obat: TsEdit
        Left = 26
        Top = 34
        Width = 190
        Height = 32
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        OnChange = edt_jlh_obatChange
        OnKeyPress = edt_jlh_obatKeyPress
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object cbb_satuan: TsComboBox
        Left = 234
        Top = 34
        Width = 279
        Height = 32
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.CustomColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Style = csDropDownList
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ItemHeight = 26
        ItemIndex = -1
        ParentFont = False
        TabOrder = 1
      end
    end
    object sPanel6: TsPanel
      Left = 1
      Top = 34
      Width = 582
      Height = 103
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      SkinData.SkinSection = 'TRANSPARENT'
      object lbl1: TLabel
        Left = 292
        Top = 67
        Width = 6
        Height = 18
        Caption = '*'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        Transparent = True
      end
      object sLabel5: TsLabel
        Left = 42
        Top = 66
        Width = 105
        Height = 20
        Caption = 'JENIS RACIKAN'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
      end
      object sLabel6: TsLabel
        Left = 42
        Top = 26
        Width = 95
        Height = 20
        Caption = 'RACIKAN KE :'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
      end
      object sLabel7: TsLabel
        Left = 330
        Top = 34
        Width = 9
        Height = 20
        Caption = '#'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
      end
      object sLabel9: TsLabel
        Left = 33
        Top = 19
        Width = 24
        Height = 13
        Caption = '*    '
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        UseSkinColor = False
      end
      object sLabel10: TsLabel
        Left = 33
        Top = 67
        Width = 24
        Height = 13
        Caption = '*    '
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        UseSkinColor = False
      end
      object edt_jenisracikan: TcxExtLookupComboBox
        Left = 166
        Top = 64
        ParentFont = False
        Properties.ButtonGlyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFCFDFEE7EEF4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFBFD6494BA2166
          9C81A8C5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFAFCFD6E9BC1548CBB88B4DD175E96FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFCFD7AA3C96396C49CC0E46598
          C73070A4FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEE8CEB9DBB292D3A57FD0A07BD2
          A684ADA09874A1CCAACBE875A3CE3F7AAEDEE8F1FFFFFFFFFFFFFFFFFFFFFEFE
          E8C9AFE8C9ADF5E1CDF7E5D3F7E5D1F3DDC8DFB99BC7A79085ADD54F86BAE1EA
          F3FFFFFFFFFFFFFFFFFFFFFFFFF1DBC7EDD0B6F8E8D9F5DEC8F3D8BCF3D6BAF4
          DBC1F7E4D2DFBA9C9F9694E3ECF5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEECEB1
          F7E7D7F6E1CCF4DBC1F4DABFF3D8BCF3D7BAF4DBC1F3DEC9D2A787FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFF0CEAEF9ECDFF5DFC8F5DDC5F4DCC2F4DAC0F3
          D9BDF3D7BCF8E6D3D3A57FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4D3B4
          F9EDE1F6E1CCF5DFC9F5DEC7F4DCC3F4DBC1F4DABFF8E7D6D7AA86FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFF8DBC0F9EBDEF7E7D6F6E1CCF5E0CAF5DEC8F5
          DDC4F6E1CBF5E2D0DFB898FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCEAD7
          F8E2CCFAEEE3F7E7D6F6E2CEF6E1CBF6E3D0F9EADDECCFB4ECD3BEFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFBE4CDF9E2CDFAECDEF9EEE2F9EDE2F8
          E9DAF0D5BCEDCFB6FFFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFCEAD9FADDC2F6D6B8F4D3B4F3D4B7F5E0CDFFFFFEFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        Properties.DropDownListStyle = lsEditList
        Properties.DropDownSizeable = True
        Properties.DropDownWidth = 1000
        Properties.View = view_jenisracikanDBTableView1
        Properties.KeyFieldNames = 'id'
        Properties.ListFieldItem = view_jenisracikanDBTableView1diskripsi
        Style.BorderColor = clInfoBk
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -19
        Style.Font.Name = 'Trebuchet MS'
        Style.Font.Style = [fsBold]
        Style.ButtonStyle = btsFlat
        Style.ButtonTransparency = ebtAlways
        Style.IsFontAssigned = True
        TabOrder = 0
        Width = 371
      end
      object cbb_racikanKe: TsComboBox
        Left = 168
        Top = 27
        Width = 145
        Height = 32
        Alignment = taCenter
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Style = csDropDownList
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ItemHeight = 26
        ItemIndex = -1
        ParentFont = False
        TabOrder = 1
        OnDropDown = cbb_racikanKeExit
        OnExit = cbb_racikanKeExit
        Items.Strings = (
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9'
          '10')
      end
      object edt_jlhracikan: TsEdit
        Left = 360
        Top = 26
        Width = 177
        Height = 32
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        OnKeyPress = edt_jlh_obatKeyPress
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
    end
    object edt_2: TsEdit
      Left = 192
      Top = 224
      Width = 321
      Height = 32
      Color = 15724527
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnChange = edt_2Change
      OnKeyPress = edt_jlh_obatKeyPress
      SkinData.SkinSection = 'BAR'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
  end
  object sPanel4: TsPanel
    Left = 0
    Top = 521
    Width = 584
    Height = 40
    Align = alTop
    BevelWidth = 5
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object btn_obat_simpan: TsBitBtn
      Left = 286
      Top = 7
      Width = 161
      Height = 25
      Caption = 'Next (Enter)'
      TabOrder = 0
      OnClick = btn_obat_simpanClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFAFAFAEAEAEAEBEBEBFCFCFC0A8129037A1DDEEE
        E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F3F3C5C5C5A4A4A4A0A0A0AA
        AAAAA6A6A694949407822E42A05E1A8735D8EADCFFFFFFFFFFFFFFFFFFFFFFFF
        E5E5E5A8A8A8CECECEEDEDED2196511B9049158E43108A3B399E5D7FC09545A2
        61168631D5E9D9FFFFFFFFFFFFFFFFFFAFAFAFDEDEDEF3F3F3DBDBDB289A5A8F
        CAA88CC8A489C5A087C49D69B58481C19647A465158732CFE6D4FFFFFFFFFFFF
        B2B2B2F0F0F0DEDEDED4D4D4309E6293CDAC6EB98D6AB78865B58460B27F66B4
        8182C1973B9F5B038027FFFFFFFFFFFFB4B4B4F2F2F2E2E2E2D8D8D836A26A95
        CEAF93CDAC90CBA98FCBA773BB8F89C7A045A467098736F0F8F3FFFFFFFFFFFF
        B6B6B6F3F3F3E7E7E7DDDDDD3CA46E37A26D33A0672F9C6154AE7B90CBA94EAA
        73178E45F4F9F6FFFFFFFFFFFFFFFFFFB7B7B7F4F4F4EAEAEAE1E1E1DDDDDDE3
        E3E3DEDEDEC9C9C9359F6659B280279756A0A5A2FFFFFFFFFFFFFFFFFFFFFFFF
        B9B9B9F5F5F5EEEEEEE6E6E6E2E2E2E6E6E6E1E1E1CDCDCD3BA36D309E64DDE1
        DEA7A7A7FFFFFFFFFFFFFFFFFFFFFFFFBABABAF6F6F6EBEBEBDEDEDED6D6D6D5
        D5D5D1D1D1C2C2C2BBBBBBBFBFBFE5E5E5AAAAAAFFFFFFFFFFFFFFFFFFFFFFFF
        BCBCBCF7F7F7E7E7E7EFEFEFF6F6F6FBFBFBFAFAFAF0F0F0DEDEDEC2C2C2E6E6
        E6ABABABFFFFFFFFFFFFFFFFFFFFFFFFBEBEBEF8F8F8FEFEFEFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFBFBFBEAEAEAADADADFFFFFFFFFFFFFFFFFFFFFFFF
        CBCBCBE1E1E1FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBCFCF
        CFC8C8C8FFFFFFFFFFFFFFFFFFFFFFFFF4F4F4C6C6C6D0D0D0E8E8E8F3F3F3FD
        FDFDFCFCFCEDEDEDE0E0E0C1C1C1C0C0C0F6F6F6FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFDFDFDE8E8E8CFCFCFC3C3C3B7B7B7B7B7B7C2C2C2CCCCCCE9E9E9FEFE
        FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      SkinData.SkinSection = 'DRAGBAR'
    end
    object btn_obat_client1: TsBitBtn
      Left = 454
      Top = 7
      Width = 105
      Height = 25
      Caption = 'Batal'
      TabOrder = 1
      OnClick = btn_obat_client1Click
      SkinData.SkinSection = 'FORMTITLE'
    end
  end
  object actmgr1: TActionManager
    Left = 536
    Top = 392
    StyleName = 'XP Style'
    object act1: TAction
      Caption = 'act1'
      ShortCut = 13
      OnExecute = btn_obat_simpanClick
    end
  end
  object view_jenisracikan: TcxGridViewRepository
    Left = 356
    Top = 160
    object view_jenisracikanDBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dm.ds_jenisracikan
      DataController.KeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object view_jenisracikanDBTableView1diskripsi: TcxGridDBColumn
        Caption = 'Diskripsi'
        DataBinding.FieldName = 'diskripsi'
      end
      object view_jenisracikanDBTableView1biaya: TcxGridDBColumn
        Caption = 'Biaya'
        DataBinding.FieldName = 'biaya'
      end
    end
  end
end
