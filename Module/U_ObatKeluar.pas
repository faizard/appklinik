unit U_ObatKeluar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh,
  ppDB, ppDBPipe, ppParameter, ppDesignLayer, ppCtrls, ppBands, ppVar,
  ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport, ImgList,
  acAlphaImageList, DB, ActnList, XPStyleActnCtrls, ActnMan, DBClient,
  Provider, DBAccess, MyAccess, MemDS, ComCtrls, sStatusBar, StdCtrls,
  sButton, sCheckBox, sGroupBox, sEdit, ExtCtrls, sLabel, sPanel, GridsEh,
  DBAxisGridsEh, DBGridEh, acPNG, acImage, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinsDefaultPainters, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, Grids, DBGrids,
  Mask, sMaskEdit, sCustomComboEdit, sToolEdit, acTitleBar, Buttons,
  sSpeedButton, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxDBData, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, sComboBox, Menus;

type
  Tfrm_barang_terjual = class(TForm)
    sPanel2: TsPanel;
    pnl_1: TsPanel;
    lbl_6: TLabel;
    lbl_7: TLabel;
    edt_nm_pengunjung: TsEdit;
    sGroupBox1: TsGroupBox;
    pnl_2: TsPanel;
    lbl_3: TLabel;
    edt_uang_cash: TsEdit;
    edt_kembalian: TsEdit;
    edt_totalbayar: TsEdit;
    pnl_4: TsPanel;
    sStatusBar1: TsStatusBar;
    q_obat: TMyQuery;
    q_obatkd_obat: TIntegerField;
    q_obatnm_obat: TStringField;
    q_obatsatuan: TStringField;
    q_obathrga_jual: TIntegerField;
    ds_obat: TMyDataSource;
    prov_obat_masuk: TDataSetProvider;
    ds_obat_keluar_1: TClientDataSet;
    q_obat_masuk_dummy: TMyQuery;
    ds_1: TMyDataSource;
    actmgr1: TActionManager;
    act_hapus_entry: TAction;
    act_get_jumlah: TAction;
    act_batal: TAction;
    act_print_nota: TAction;
    q_obat_keluar: TMyQuery;
    sAlphaImageList1: TsAlphaImageList;
    report_notadetail: TppReport;
    ppParameterList1: TppParameterList;
    sPanel3: TsPanel;
    lbl_2: TLabel;
    edt_cari_obat: TsEdit;
    sButton3: TsButton;
    dbg_entry_obat: TDBGridEh;
    lbl4: TLabel;
    date_tanggal: TsDateEdit;
    sTitleBar1: TsTitleBar;
    sPanel6: TsPanel;
    btn_batal: TsSpeedButton;
    btn_simpan: TsSpeedButton;
    img5: TImage;
    lbl1: TLabel;
    lbl2: TLabel;
    pp_Print_obat_keluar: TppReport;
    ppParameterList2: TppParameterList;
    print_obat_keluar: TppDBPipeline;
    q_obat_keluar_print: TMyQuery;
    ds_obat_keluar_print: TMyDataSource;
    ppHeaderBand2: TppHeaderBand;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppSystemVariable4: TppSystemVariable;
    ppLabel25: TppLabel;
    ppLabel26: TppLabel;
    ppLabel27: TppLabel;
    ppLabel28: TppLabel;
    ppLabel29: TppLabel;
    ppLabel30: TppLabel;
    ppLine23: TppLine;
    ppDetailBand2: TppDetailBand;
    ppDBText13: TppDBText;
    ppDBText14: TppDBText;
    ppSummaryBand2: TppSummaryBand;
    ppLabel31: TppLabel;
    ppLabel34: TppLabel;
    ppLine24: TppLine;
    ppDesignLayers2: TppDesignLayers;
    ppDesignLayer2: TppDesignLayer;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppLabel13: TppLabel;
    ppDBText11: TppDBText;
    ppLabel14: TppLabel;
    ppDBText12: TppDBText;
    ppLabel15: TppLabel;
    ppDBText15: TppDBText;
    lbl3: TLabel;
    act_savetoDatabase: TAction;
    edt_1: TsEdit;
    lbl5: TLabel;
    lbl6: TLabel;
    dbg_obat1: TDBGridEh;
    ppDBCalc4: TppDBCalc;
    ppLabel16: TppLabel;
    ppDBText16: TppDBText;
    view_dokter: TcxGridViewRepository;
    view_dokterDBTableView1: TcxGridDBTableView;
    view_dokterDBTableView1nm_dokter: TcxGridDBColumn;
    lbl9: TLabel;
    edt_pot: TsEdit;
    lbl10: TLabel;
    edt_puyer: TsEdit;
    chk_cetak_nota: TsCheckBox;
    jenis_racikan: TDataSetProvider;
    dsjenisracikan: TClientDataSet;
    DBGridEh1: TDBGridEh;
    DBGridEh2: TDBGridEh;
    ds_jenisracikan: TDataSource;
    dsjenisracikanid: TIntegerField;
    dsjenisracikandiskripsi: TStringField;
    dsjenisracikanbiaya: TIntegerField;
    cbb__nm_dokter: TsComboBox;
    ppDBText17: TppDBText;
    ppLabel17: TppLabel;
    ppLabel21: TppLabel;
    ppDBText21: TppDBText;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppSystemVariable1: TppSystemVariable;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLine1: TppLine;
    ppLabel22: TppLabel;
    ppDBText22: TppDBText;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLine2: TppLine;
    ppLabel11: TppLabel;
    ppDBText6: TppDBText;
    ppLabel12: TppLabel;
    ppDBText7: TppDBText;
    ppLabel18: TppLabel;
    ppDBText18: TppDBText;
    ppDBCalc1: TppDBCalc;
    ppLabel19: TppLabel;
    ppDBText19: TppDBText;
    ppDBText20: TppDBText;
    ppLabel20: TppLabel;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    q_notadetail: TMyQuery;
    ds_notadetail: TMyDataSource;
    edt_biayaDokter: TsEdit;
    lbl8: TLabel;
    dsjenisracikanColumna: TIntegerField;
    dsjenisracikanColumnb: TIntegerField;
    ppLabel32: TppLabel;
    ppDBCalc2: TppDBCalc;
    ppDBCalc3: TppDBCalc;
    chk_jualBebas: TsCheckBox;
    edt_2: TsEdit;
    ppLine3: TppLine;
    ppLabel33: TppLabel;
    ppDBText23: TppDBText;
    ds_obat_keluar_1norec: TLargeintField;
    ds_obat_keluar_1no_nota: TStringField;
    ds_obat_keluar_1nama_pengunjung: TStringField;
    ds_obat_keluar_1id_pasien: TLargeintField;
    ds_obat_keluar_1tgl_obat_keluar: TDateTimeField;
    ds_obat_keluar_1id_obat: TLargeintField;
    ds_obat_keluar_1jumlah_obat_keluar: TFloatField;
    ds_obat_keluar_1jlh_sub_racikan: TSmallintField;
    ds_obat_keluar_1biaya_listrik: TIntegerField;
    ds_obat_keluar_1biaya_admin: TIntegerField;
    ds_obat_keluar_1nm_dokter: TStringField;
    ds_obat_keluar_1tot_harga: TIntegerField;
    ds_obat_keluar_1uang: TIntegerField;
    ds_obat_keluar_1kembalian: TIntegerField;
    ds_obat_keluar_1insert_at: TDateTimeField;
    ds_obat_keluar_1racikan: TStringField;
    ds_obat_keluar_1biaya_dokter: TIntegerField;
    ds_obat_keluar_1id_jenis_racikan: TIntegerField;
    ds_obat_keluar_1id_user: TIntegerField;
    ds_obat_keluar_1hrgajualbersih: TIntegerField;
    ds_obat_keluar_1id: TLargeintField;
    ds_obat_keluar_1enabled: TLargeintField;
    ds_obat_keluar_1nm_obat: TStringField;
    ds_obat_keluar_1satuan: TStringField;
    ds_obat_keluar_1quantity_racik: TFloatField;
    ds_obat_keluar_1jlhper_raciik: TIntegerField;
    ds_obat_keluar_1satuan_racik: TStringField;
    ds_obat_keluar_1expire_date: TDateField;
    ds_obat_keluar_1tgl_jatuh_tempo: TDateField;
    ds_obat_keluar_1hrgper_racik: TIntegerField;
    ds_obat_keluar_1hrga_jual: TIntegerField;
    ds_obat_keluar_1harga_satuan: TIntegerField;
    ds_obat_keluar_1jenis: TStringField;
    ds_obat_keluar_1jlhper_pack: TIntegerField;
    ds_obat_keluar_1satuan_pack: TStringField;
    ds_obat_keluar_1hrgper_pack: TIntegerField;
    ds_obat_keluar_1ket: TMemoField;
    ds_obat_keluar_1hrgbeli_cream: TIntegerField;
    q_obat_keluarnorec: TLargeintField;
    q_obat_keluarno_nota: TStringField;
    q_obat_keluarnama_pengunjung: TStringField;
    q_obat_keluarid_pasien: TLargeintField;
    q_obat_keluartgl_obat_keluar: TDateTimeField;
    q_obat_keluarid_obat: TLargeintField;
    q_obat_keluarjumlah_obat_keluar: TFloatField;
    q_obat_keluarjlh_sub_racikan: TSmallintField;
    q_obat_keluarbiaya_listrik: TIntegerField;
    q_obat_keluarbiaya_admin: TIntegerField;
    q_obat_keluarnm_dokter: TStringField;
    q_obat_keluartot_harga: TIntegerField;
    q_obat_keluaruang: TIntegerField;
    q_obat_keluarkembalian: TIntegerField;
    q_obat_keluarinsert_at: TDateTimeField;
    q_obat_keluarracikan: TStringField;
    q_obat_keluarbiaya_dokter: TIntegerField;
    q_obat_keluarid_jenis_racikan: TIntegerField;
    q_obat_keluarid_user: TIntegerField;
    q_obat_keluarhrgajualbersih: TIntegerField;
    q_obatquantity: TFloatField;
    ds_obat_keluar_1quantity: TFloatField;
    pmKoreksiHarga: TPopupMenu;
    K1: TMenuItem;
    procedure act_get_jumlahExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edt_uang_cashExit(Sender: TObject);
    procedure act_batalExecute(Sender: TObject);
   procedure ResetInputan();
    procedure btn_batalClick(Sender: TObject);
    procedure btn_simpanClick(Sender: TObject);
    procedure edt_cari_obatChange(Sender: TObject);
    procedure edt_cari_obatKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
       procedure getJenisRacikan;
    function getBiayaRacik() : Integer;
    procedure edt_biayaDokterChange(Sender: TObject);
    procedure ds_obat_keluar_1BeforePost(DataSet: TDataSet);
    procedure dsjenisracikanAfterPost(DataSet: TDataSet);
    procedure harusAngka(Sender: TObject; var Key: Char);
    procedure edt_uang_cashChange(Sender: TObject);
    procedure chk_jualBebasClick(Sender: TObject);
    procedure edt_biayaDokterExit(Sender: TObject);
    procedure K1Click(Sender: TObject);
    procedure ds_obat_keluar_1AfterPost(DataSet: TDataSet);
    procedure cbb__nm_dokterChange(Sender: TObject);
    procedure dbg_obat1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
  private



    uang,kembalian : Currency;

    { Private declarations }
  public
    { Public declarations }

  end;

var
  frm_barang_terjual: Tfrm_barang_terjual;

implementation

uses
  U_DM, u_bantu, U_JlhObatTerjual, Math, StrUtils;

{$R *.dfm}


function Tfrm_barang_terjual.getBiayaRacik():Integer;
var
  a : Integer;
begin
  a:=0;
//     temp_biaya_racik :=0;
     dsjenisracikan.Open;
     dsjenisracikan.First;
     while not dsjenisracikan.Eof do
     begin
      a := a + dsjenisracikanbiaya.AsInteger;
      dsjenisracikan.Next;
     end;

     Result:= a;

end;

procedure Tfrm_barang_terjual.getJenisRacikan;
begin
//  agar penambahan obat selanjutnya tidak double
//  temp_tot_bayar := temp_tot_bayar - biayaRacik;
  biayaRacik:=0;
//  jenisRacikan :='-';
  if temp_racikan = 1 then
  begin
    if  not dsjenisracikan.Locate('id',dm.q_jenisRacikanid.Text,[loPartialKey]) then
//  if temp_racikanBaru then
    begin

        dsjenisracikan.Insert;
        dsjenisracikanid.Text:= dm.q_jenisRacikanid.Text;
        dsjenisracikanColumna.AsInteger:=temp_jlh_sub_racik ;
        dsjenisracikandiskripsi.Text:= dm.q_jenisRacikandiskripsi.Text;
        dsjenisracikanbiaya.Text:= dm.q_jenisRacikanbiaya.Text;
        dsjenisracikan.Post;
        jenisRacikan := dsjenisracikandiskripsi.Text;
    end;
  end;

  biayaRacik := getBiayaRacik();

end;



procedure Tfrm_barang_terjual.ResetInputan();
begin
  
    CleanClientDs(ds_obat_keluar_1);
    edt_nm_pengunjung.Clear;
    cbb__nm_dokter.Text:='';
    date_tanggal.Date:= Now();
    act_batalExecute(Self);
    edt_uang_cash.Clear;
    edt_totalbayar.Clear;
    edt_kembalian.Clear;
    cbb__nm_dokter.SetFocus;
    temp_tot_hargaJual:=0;
    kembalian:=0;
    temp_tot_bayar :=0;
    uang:=0;
    temp_biayaPuyer:=0;
    temp_biayaPot:=0;
    adaRacikan:=0;
    edt_biayaDokter.Clear;

    biayaRacik :=0;
    jenisRacikan :='';
    racikanKe :=0;
    temp_biayaServis:=0;
    biayaAdmin:=0;
    adaListrik:=0;
    temp_biayaListrik :=0;
    jlhPerRacik :=0;
    temp_jlh_sub_racik :=0;

    temp_biayaDokter :=0;

    edt_1.Text:= IntToStr(temp_biayaServis);
    edt_2.Text:= IntToStr(temp_biayaListrik);


//    CleanClientDs(ds_obat_keluar_1);
    CleanClientDs(dsjenisracikan);
    
    

    
    
end;

procedure Tfrm_barang_terjual.act_get_jumlahExecute(Sender: TObject);
begin
  try

  
//    if not IsFormOpen('frm_jlh_obat_keluar') then
//    frm_jlh_obat_keluar := Tfrm_jlh_obat_keluar.Create(Self) ;

    if dm.q_obatquantity.AsFloat <= 0 then
    begin
        MessageError('Stok obat sudah habis ...');
        Exit;
    end;

    if ds_obat_keluar_1.Locate('norec',dm.q_obatid.Text,[loCaseInsensitive,loPartialKey]) then
    begin
          MessageError( dm.q_obatnm_obat.Text+ ' sudah ada di list ...');
          Exit;
    end;

    kd_obat:= dm.q_obatid.AsString;
    ds_obat_keluar_1.Open;
    frm_jlh_obat_keluar.ShowModal;
    if save_temp then
    begin
      getJenisRacikan;
      ds_obat_keluar_1.Append;

//      ds_obat_keluar_1satuan_pack.Text := jenisPuyer;
      ds_obat_keluar_1nama_pengunjung.Text := jenisRacikan;

      ds_obat_keluar_1id_obat.Text:= dm.q_obatid.Text;
      ds_obat_keluar_1nm_obat.Text:= dm.q_obatnm_obat.Text;
      ds_obat_keluar_1satuan.Text:= dm.q_obatsatuan.Text;
      ds_obat_keluar_1jumlah_obat_keluar.AsFloat:= temp_jlh;
      ds_obat_keluar_1quantity_racik.AsFloat:= temp_jlhracik;
      ds_obat_keluar_1satuan_racik.Text:= dm.q_obatsatuan_racik.Text;
//      ds_obat_keluar_1tot_harga.AsInteger:= temp_harga_tot;
      ds_obat_keluar_1biaya_listrik.AsInteger:= getBiayaListrik;
      ds_obat_keluar_1biaya_admin.AsInteger:=  temp_jlh_sub_racik;  //getBiayaLainnya;
      ds_obat_keluar_1tot_harga.AsCurrency:= temp_tot_hargaJual;
      ds_obat_keluar_1racikan.AsInteger:= temp_racikan;
      ds_obat_keluar_1id_pasien.AsString:=id_jenis_racikkkk;
      ds_obat_keluar_1id.AsInteger:=racikanKe;
      ds_obat_keluar_1.Post;
 

      act_batalExecute(Sender);

    end;

  except
       MessageError('Gagal Entry obat ...');
  end;

end;

procedure Tfrm_barang_terjual.FormShow(Sender: TObject);
begin


    ResetInputan;
    date_tanggal.Date:= Now();

    WindowState:= wsMaximized;
    dbg_obat1.Top:=303;
    dbg_obat1.Left:=409;

//    edt_1.Text:= FormatFloat(',#',getBiayaLainnya);
//    edt_2.Text:= FormatFloat(',#',getBiayaListrik);
    edt_1.Text:='0';
    edt_2.Text:='0';

    


    getCombo(cbb__nm_dokter,'nm_dokter','t_dokter');


//    edt_Admin.Text:= IntToStr(biayaAdmin);
//    Ribuan(edt_Admin);

//      edt_biayaAdmin.Text:= '  Biaya Admin : '+ FormatFloat(',#',biayaAdmin);
//      Ribuan(edt_biayaAdmin);

end;

procedure Tfrm_barang_terjual.edt_uang_cashExit(Sender: TObject);
begin
    if kembalian <0 then
    begin
      MessageError('Uang tidak cukup, Kami tidak melayani bon . :)');
      Exit;
    end;

end;

procedure Tfrm_barang_terjual.act_batalExecute(Sender: TObject);
begin
//    FormShow(Sender);
    dbg_obat1.Hide;
    edt_cari_obat.Clear;
    edt_cari_obat.SetFocus;
//    ResetInputan();
end;

procedure Tfrm_barang_terjual.btn_batalClick(Sender: TObject);
begin
  if Application.MessageBox('Reset inputan ...','Reset',MB_ICONWARNING + MB_YESNO) = mrYes then
  begin
    ResetInputan();
    biayaAdmin := getBiayaAdmin;
  end;


end;

procedure Tfrm_barang_terjual.btn_simpanClick(Sender: TObject);
begin
  try
    if  StrToInt(FormatDateTime('yyyy',date_tanggal.Date)) < 2000 then
    begin
      MessageError('Isi tanggal ...');
      Exit;
    end;

    if cbb__nm_dokter.Text ='' then
    begin
      MessageError('Pilih dokter ...');
      Exit;
    end
      else
        begin
//          if dm.q_dokterdranak.AsInteger = 1 then
//          begin
//              if  (edt_biayaDokter.Text='') then
//              begin
//                MessageError('Input biaya untuk dokter ...!');
//          //      edt_biayaDokter.SetFocus;
//                Exit;
//              end
//          end;
        end;

    if edt_uang_cash.Text='' then
    begin
      MessageError('Silahkan isi uang cash ..');
      Exit;
    end;

    if (temp_tot_bayar > HapusFormat(edt_uang_cash)) or (edt_uang_cash.Text='') then
//    if (kembalian <0) or (edt_uang_cash.Text='') then
    begin
      MessageError('Uang tidak cukup, Kami tidak melayani bon . :)');
      edt_uang_cash.SetFocus;
      Exit;
    end;

    if cbb__nm_dokter.ItemIndex = -1 then
    begin
        dm.q_dokter.Insert;
        dm.q_dokternm_dokter.Text:= cbb__nm_dokter.Text;
        dm.q_dokter.Post;
    end;

    if Application.MessageBox('Pastikan inputan sudah di cek kembali ...','Save',MB_ICONQUESTION + MB_YESNO) = mrNo then Exit;

    berhasil_save:= False;
    ds_obat_keluar_1.Open;
    q_obat_keluar.Open;
    ds_obat_keluar_1.First;

    no_nota:= getRandom();
    startTransaksi();


    while not ds_obat_keluar_1.Eof do
    begin
//        if ds_obat_keluar_1racikan.AsInteger = 1 then
//        begin
//
//        end
//          else
//            begin
        q_obat_keluar.Insert;
        q_obat_keluarno_nota.Text:= no_nota;
        q_obat_keluarnama_pengunjung.Text:= edt_nm_pengunjung.Text;
        q_obat_keluarid_obat.Text:= ds_obat_keluar_1id_obat.Text;
        q_obat_keluartgl_obat_keluar.AsDateTime:= date_tanggal.Date;
        if ds_obat_keluar_1racikan.AsInteger = 1 then
        begin
              q_obat_keluarbiaya_listrik.AsInteger:= temp_biayaListrik; //ds_obat_keluar_1biaya_listrik.Text;
              q_obat_keluarjlh_sub_racikan.Text:= ds_obat_keluar_1biaya_admin.Text;
              q_obat_keluarjumlah_obat_keluar.AsFloat:= ds_obat_keluar_1quantity_racik.AsFloat;
        end
            else
                q_obat_keluarjumlah_obat_keluar.AsFloat:= ds_obat_keluar_1jumlah_obat_keluar.AsFloat;
                
        q_obat_keluarid_user.Text:= kd_user;
//        q_obat_keluarid_user.Text:= '1';
        q_obat_keluarracikan.Text:= ds_obat_keluar_1racikan.Text;
        q_obat_keluar.FieldByName('nm_dokter').Text:= cbb__nm_dokter.Text;

        q_obat_keluartot_harga.AsCurrency:= bulatkanKe500(temp_tot_bayar);
        q_obat_keluar.FieldByName('uang').AsCurrency:= HapusFormat(edt_uang_cash);
        q_obat_keluar.FieldByName('biaya_dokter').AsInteger:=temp_biayaDokter;
        q_obat_keluar.FieldByName('kembalian').AsCurrency:= kembalian;
        q_obat_keluar.FieldByName('id_jenis_racikan').AsString:= ds_obat_keluar_1id_pasien.AsString;

        q_obat_keluarhrgajualbersih.AsInteger:= ds_obat_keluar_1tot_harga.AsInteger;


        q_obat_keluar.Post;
        
//            end;

         ds_obat_keluar_1.Next;
    end;

    berhasil_save:= True;
  except  on E : Exception do begin

      berhasil_save:= False;
      errorMess := e.Message;
    end;
  end;

     if berhasil_save then
        begin
          MessageInfo('Sukses simpan data ke database ...');
          CommitTransaksi();


          if chk_cetak_nota.Checked then
          begin
              q_obat_keluar_print.SQL.Text:= 'CALL `sp_cetak_nota`("'+no_nota+'",'+IntToStr(adaRacikan)+','+IntToStr(getBiayaRacik)+')';
              q_obat_keluar_print.Open;
              pp_Print_obat_keluar.PrintReport;

//              q_obat_keluar_print.SQL.Text:= 'CALL `sp_cetak_nota_detail`("'+no_nota+'",'+IntToStr(getBiayaRacik)+')';
//              q_obat_keluar_print.Open;
//              pp_Print_obat_keluar.PrintReport;

          end;

          FormShow(Sender);
          biayaAdmin := getBiayaAdmin;
        end
          else
          begin
               MessageError('Gagal simpan data ke database ...' + errorMess);
               RollbackTransaksi();
          end;


end;

procedure Tfrm_barang_terjual.edt_cari_obatChange(Sender: TObject);
begin
    with   dbg_obat1 do
    begin
      Top:=224;
      Left:=200;
      Width:=1000;
      Height:= 400;
      Show
    end;
    GetSQL('select * from v_obat where nm_obat like "%'+edt_cari_obat.Text+'%" limit 20',dm.q_obat);

    if edt_cari_obat.Text ='' then   dbg_obat1.Hide



end;

procedure Tfrm_barang_terjual.edt_cari_obatKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
      if key = VK_DOWN then
      begin
        dbg_obat1.SetFocus;
      end;
end;

procedure Tfrm_barang_terjual.edt_biayaDokterChange(Sender: TObject);
begin
//      Ribuan(TsEdit(Sender));

end;

procedure Tfrm_barang_terjual.ds_obat_keluar_1BeforePost(
  DataSet: TDataSet);
begin
    ds_obat_keluar_1norec.AsInteger :=  RandomRange(1000000, MaxInt);
end;

procedure Tfrm_barang_terjual.dsjenisracikanAfterPost(DataSet: TDataSet);
begin
      temp_tot_bayar := temp_tot_bayar + ( dsjenisracikanColumna.AsInteger * dsjenisracikanbiaya.AsInteger  );
end;

procedure Tfrm_barang_terjual.harusAngka(Sender: TObject;
  var Key: Char);
begin

     // #8 is Backspace
  if not (Key in [#8, '0'..'9']) then begin
    Key := #0;
    Exit;
  end;

end;

procedure Tfrm_barang_terjual.edt_uang_cashChange(Sender: TObject);
begin
    if TsEdit(Sender).Text ='' then Exit;
    uang:= HapusFormat(TsEdit(Sender));
    kembalian:= uang - bulatkanKe500(temp_tot_bayar);

    Ribuan(TsEdit(Sender));

    edt_kembalian.Text:= CurrToStr(kembalian);
    Ribuan(edt_kembalian);

end;

procedure Tfrm_barang_terjual.chk_jualBebasClick(Sender: TObject);
begin
    if dsjenisracikan.RecordCount > 0 then
    begin
//      MessageError('Terdapat obat yang di racik, jadi');
        chk_jualBebas.Checked := False;
        Exit;
    end;

    if not chk_jualBebas.Checked then
        temp_biayaListrik := getBiayaListrik()
        else
          temp_biayaListrik :=0;

      edt_2.Text:= IntToStr(temp_biayaListrik);


end;

procedure Tfrm_barang_terjual.edt_biayaDokterExit(Sender: TObject);
begin
      if edt_biayaDokter.Text <> '' then temp_biayaDokter := HapusFormat(edt_biayaDokter);
end;

procedure Tfrm_barang_terjual.K1Click(Sender: TObject);
var
    harga : ShortString;
begin
  temp_tot_bayar :=  temp_tot_bayar - ds_obat_keluar_1tot_harga.AsCurrency ;

  if ds_obat_keluar_1racikan.AsInteger = 1 then
    temp_biayaServis := temp_biayaServis - 1000  ;

 harga := InputBox('Action','Koreksi Harga ',ds_obat_keluar_1tot_harga.AsString);

  ds_obat_keluar_1.Edit;
  ds_obat_keluar_1tot_harga.AsInteger := StrToInt( harga);
  ds_obat_keluar_1.Post;

   ShowMessage(CurrToStr(temp_tot_bayar));


end;

procedure Tfrm_barang_terjual.ds_obat_keluar_1AfterPost(DataSet: TDataSet);
begin
    adaRacikan := adaRacikan + ds_obat_keluar_1racikan.AsInteger;

//    ShowMessage(ds_obat_keluar_1racikan.AsString);
//    ShowMessage(IntToStr(biayaAdmin));
    if ds_obat_keluar_1racikan.AsInteger = 1 then
    begin
      if  temp_biayaServis < 5000 then
          temp_biayaServis := temp_biayaServis + 1000;

     if  (adaListrik = 0)  then
     begin

      temp_tot_bayar:= temp_tot_bayar + ds_obat_keluar_1tot_harga.AsInteger + temp_biayaListrik  + 1000;
//        edt_2.Text:= IntToStr(getBiayaLainnya1)  ;
     end
        else
        temp_tot_bayar:= temp_tot_bayar + ds_obat_keluar_1tot_harga.AsInteger  + 1000;
    end
        else
           temp_tot_bayar:= temp_tot_bayar + ds_obat_keluar_1tot_harga.AsInteger;


//           ShowMessage(IntToStr(adaRacikan));
      if adaRacikan > 0 then
      begin
          edt_1.Text:= IntToStr(temp_biayaServis)  ;
         if not chk_jualBebas.Checked then temp_biayaListrik:= getBiayaListrik
         else
          begin
            temp_biayaListrik :=0;
          end;
          edt_2.Text := IntToStr(temp_biayaListrik);
      end;


//     ShowMessage(CurrToStr(temp_tot_bayar) +' '+IntToStr(biayaAdmin) +' '+IntToStr(biayaRacik));

     edt_totalbayar.Text:= CurrToStr( bulatkanKe500(temp_tot_bayar) );
//     edt_uang_cash.Text:= CurrToStr(temp_tot_bayar);

//     edt_Total.Text:= CurrToStr(temp_tot_bayar - biayaAdmin);
//     Ribuan(edt_Total);
     Ribuan(edt_totalbayar);
     btn_simpan.Enabled:= True;
     biayaAdmin:=0;
     adaListrik := temp_biayaListrik;

     edt_puyer.Text:= IntToStr(temp_biayaPuyer);
     Ribuan(edt_puyer);

     edt_pot.Text:= IntToStr(temp_biayaPot);
     Ribuan(edt_pot);
end;

procedure Tfrm_barang_terjual.cbb__nm_dokterChange(Sender: TObject);
begin
      edt_biayaDokter.Text:='0';
end;

procedure Tfrm_barang_terjual.dbg_obat1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
       if dm.q_obat.FieldByName('quantity').AsInteger <= 0 then
        dbg_obat1.Canvas.Brush.Color:=$00A9CCFC;

//        dbg_obat1.DefaultDrawColumnCell(Rect, DataCol, TGridDrawState );
end;

end.
