object frm_barang_terjual: Tfrm_barang_terjual
  Left = -8
  Top = -8
  Width = 1936
  Height = 1096
  Color = clMedGray
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Trebuchet MS'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object lbl3: TLabel
    Left = 583
    Top = 33
    Width = 64
    Height = 18
    Caption = 'Uang Cash'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Trebuchet MS'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object sPanel2: TsPanel
    Left = 0
    Top = 0
    Width = 1920
    Height = 1057
    Align = alClient
    Caption = 'sPanel2'
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object pnl_1: TsPanel
      Left = 1
      Top = 60
      Width = 1918
      Height = 117
      Align = alTop
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -24
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      SkinData.SkinSection = 'SELECTION'
      object lbl_6: TLabel
        Left = 66
        Top = 39
        Width = 128
        Height = 20
        Caption = 'Nama Pengunjung'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object lbl_7: TLabel
        Left = 116
        Top = 10
        Width = 91
        Height = 20
        Caption = 'Nama Dokter'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object lbl4: TLabel
        Left = 158
        Top = 71
        Width = 52
        Height = 20
        Caption = 'Tanggal'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object lbl8: TLabel
        Left = 708
        Top = 10
        Width = 23
        Height = 20
        Caption = 'IDR'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object edt_nm_pengunjung: TsEdit
        Left = 248
        Top = 39
        Width = 681
        Height = 28
        CharCase = ecUpperCase
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object sGroupBox1: TsGroupBox
        Left = 1586
        Top = 1
        Width = 331
        Height = 115
        Align = alRight
        Caption = 'Nota'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        SkinData.SkinSection = 'GROUPBOX'
        object lbl5: TLabel
          Left = 15
          Top = 64
          Width = 67
          Height = 18
          Caption = 'Biaya Servis'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Trebuchet MS'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
        end
        object lbl6: TLabel
          Left = 15
          Top = 19
          Width = 71
          Height = 18
          Caption = 'Biaya Listrik'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Trebuchet MS'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
        end
        object lbl9: TLabel
          Left = 15
          Top = 126
          Width = 18
          Height = 18
          Caption = 'Pot'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Trebuchet MS'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          Visible = False
        end
        object lbl10: TLabel
          Left = 15
          Top = 153
          Width = 33
          Height = 18
          Caption = 'Puyer'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Trebuchet MS'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          Visible = False
        end
        object edt_1: TsEdit
          Left = 16
          Top = 82
          Width = 289
          Height = 26
          Color = 14408667
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Trebuchet MS'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          Text = '\'
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'Tahoma'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object edt_pot: TsEdit
          Left = 104
          Top = 126
          Width = 210
          Height = 26
          Color = 14408667
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Trebuchet MS'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          Visible = False
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'Tahoma'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object edt_puyer: TsEdit
          Left = 104
          Top = 153
          Width = 210
          Height = 26
          Color = 14408667
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Trebuchet MS'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          Visible = False
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'Tahoma'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object edt_2: TsEdit
          Left = 16
          Top = 35
          Width = 289
          Height = 26
          Color = 14408667
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Trebuchet MS'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          Text = '\'
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'Tahoma'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
      end
      object date_tanggal: TsDateEdit
        Left = 248
        Top = 71
        Width = 433
        Height = 30
        AutoSize = False
        Color = clWhite
        EditMask = '!99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        Text = '  /  /    '
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
      end
      object cbb__nm_dokter: TsComboBox
        Left = 248
        Top = 8
        Width = 449
        Height = 28
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        CharCase = ecUpperCase
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ItemHeight = 22
        ItemIndex = -1
        ParentFont = False
        TabOrder = 3
        OnChange = cbb__nm_dokterChange
      end
      object edt_biayaDokter: TsEdit
        Left = 752
        Top = 8
        Width = 177
        Height = 28
        CharCase = ecUpperCase
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
        OnChange = edt_biayaDokterChange
        OnExit = edt_biayaDokterExit
        OnKeyPress = harusAngka
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
    end
    object pnl_2: TsPanel
      Left = 1
      Top = 889
      Width = 1918
      Height = 148
      Align = alBottom
      BorderWidth = 5
      TabOrder = 1
      SkinData.SkinSection = 'PANEL'
      object lbl_3: TLabel
        Left = 311
        Top = 21
        Width = 84
        Height = 18
        Caption = 'TOTAL HARGA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object lbl1: TLabel
        Left = 903
        Top = 14
        Width = 64
        Height = 18
        Caption = 'Uang Cash'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object lbl2: TLabel
        Left = 903
        Top = 46
        Width = 64
        Height = 18
        Caption = 'Kembalian'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object edt_uang_cash: TsEdit
        Left = 984
        Top = 7
        Width = 345
        Height = 35
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        OnChange = edt_uang_cashChange
        OnExit = edt_uang_cashExit
        OnKeyPress = harusAngka
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object edt_kembalian: TsEdit
        Left = 984
        Top = 43
        Width = 345
        Height = 35
        Color = 14408667
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object edt_totalbayar: TsEdit
        Left = 311
        Top = 39
        Width = 490
        Height = 35
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        Text = '20.000'
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object sPanel6: TsPanel
        Left = 6
        Top = 101
        Width = 1906
        Height = 41
        Align = alBottom
        BorderWidth = 4
        TabOrder = 3
        SkinData.SkinSection = 'PANEL'
        object btn_batal: TsSpeedButton
          Left = 235
          Top = 5
          Width = 143
          Height = 31
          Caption = 'Reset'
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCECDFAF9F9FEFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFF8F8FEC5C5F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            D1D0FB4E4BF2413FEDF9F9FEFFFFFFFFFFFFFFFFFFFFFFFFF8F8FE2624E4302E
            EAC6C5F8FFFFFFFFFFFFFFFFFFD3D2FC5755F56260FA5754F64240EDF9F9FEFF
            FFFFFFFFFFF8F8FE2D2CE6403EF14B49F6302EEAC6C5F8FFFFFFFFFFFFE3E2FD
            5A57F66461FA706FFF5855F64341EEF9F9FEF9F9FE3633E94644F26261FF4947
            F42E2CE9DAD9FAFFFFFFFFFFFFFFFFFFE3E3FD5A58F66562FA7370FF5957F644
            42EE3F3DEC4F4CF46766FF4F4DF53533EBDBDAFBFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFE3E3FD5B59F66663FA7371FF726FFF6F6DFF6D6BFF5654F73E3CEEDCDC
            FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3E3FD5C5AF77875FF58
            55FF5653FF716FFF4745F0DEDDFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFAFAFF5D5AF67C78FF5D5AFF5A57FF7573FF4643EFF9F9FEFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFF6764F96F6CFB7F7DFF7D
            7AFF7B78FF7876FF5D5BF74845EFF9F9FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FBFAFF6F6CFC7673FD8581FF7572FC6361F85F5CF76C69FA7A78FF5F5CF74946
            EFF9F9FEFFFFFFFFFFFFFFFFFFFBFBFF7471FE7C79FE8986FF7B78FD6B68FBE5
            E4FEE4E3FE605DF86D6BFA7C79FF605EF74A47F0FBFBFFFFFFFFFFFFFFEEEEFF
            7976FF807DFF807DFE7370FDE6E6FEFFFFFFFFFFFFE4E4FE615EF86E6CFB7D7B
            FF615EF8B0AEF8FEFEFFFFFFFFFFFFFFEEEEFF7976FF7875FEE7E7FFFFFFFFFF
            FFFFFFFFFFFFFFFFE4E4FE6360F86967F98E8CF7E3E2FDFFFFFFFFFFFFFFFFFF
            FFFFFFEEEEFFE8E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4E4FEB8B7
            FCD6D6FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9FFFFFFFFFFFFFFFFFFFF}
          OnClick = btn_batalClick
          Align = alLeft
          SkinData.CustomColor = True
          SkinData.SkinSection = 'ALPHACOMBOBOX'
          SkinData.ColorTone = 8553215
        end
        object btn_simpan: TsSpeedButton
          Left = 5
          Top = 5
          Width = 212
          Height = 31
          Caption = 'Simpan Ke Database (F9)'
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Trebuchet MS'
          Font.Style = [fsBold]
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFC98A61C28357D38A67E18E6FDC8C6BDA8A6CD789
            6DCD8A6BAA6C43A55E2DFFFFFFFFFFFFFFFFFFFFFFFFFAFAFAEAEAEAC58254EF
            CEB9DDFFFF86EEC7A1F4D7A1F6D78BEEC7E0FFFFDDA184AA693DFFFFFFF3F3F3
            C5C5C5A4A4A4A0A0A0AAAAAAC27E50EFB599EAF3E850BE836EC99770C99853BE
            83E4F4E9DD9B7AA96839E5E5E5A8A8A8CECECEEDEDEDF4F4F4F5F5F5C38053EA
            B596F3F3EAEDF1E6EFF1E6EFF0E6EDF1E5F3F5EDD59B78AF6F43AFAFAFDEDEDE
            F3F3F3DBDBDBD2D2D2DBDBDBC98A60E6B491E2A680E1A680DEA27CDCA07ADB9E
            78D99D76D49972BA7D56B2B2B2F0F0F0DEDEDED4D4D4D2D2D2DBDBDBCA8C64EA
            B798DDA47DDDA57FDBA27BD99F79D99F78D89E77D89D77BE835CB4B4B4F2F2F2
            E2E2E2D8D8D8D5D5D5DCDCDCC8875CEFBEA0FDFCFAFEFCFBFEFDFDFEFDFCFDFB
            FAFDFCFBDDA784C07E52B6B6B6F3F3F3E7E7E7DDDDDDD9D9D9E0E0E0C7855AEF
            BF9DFFFFFFCC926DFFFFFFFFFFFFFFFBF7FFF8F1E4AE8BC78960B7B7B7F4F4F4
            EAEAEAE1E1E1DDDDDDE3E3E3CC8C64F3CDAFFFFFFFE3C7B2FFFFFFFFFFFFFFFF
            FFFFFFFFEABEA0C9885FB9B9B9F5F5F5EEEEEEE6E6E6E2E2E2E6E6E6D4966DD4
            9D7AD09770D6A381CD8D67CD8F68D09974D19872C88A61EDDCD0BABABAF6F6F6
            EBEBEBDEDEDED6D6D6D5D5D5D1D1D1C2C2C2BBBBBBBFBFBFE5E5E5AAAAAAFFFF
            FFFFFFFFFFFFFFFFFFFFBCBCBCF7F7F7E7E7E7EFEFEFF6F6F6FBFBFBFAFAFAF0
            F0F0DEDEDEC2C2C2E6E6E6ABABABFFFFFFFFFFFFFFFFFFFFFFFFBEBEBEF8F8F8
            FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBEAEAEAADADADFFFF
            FFFFFFFFFFFFFFFFFFFFCBCBCBE1E1E1FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFBFBFBCFCFCFC8C8C8FFFFFFFFFFFFFFFFFFFFFFFFF4F4F4C6C6C6
            D0D0D0E8E8E8F3F3F3FDFDFDFCFCFCEDEDEDE0E0E0C1C1C1C0C0C0F6F6F6FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDE8E8E8CFCFCFC3C3C3B7B7B7B7B7B7C2
            C2C2CCCCCCE9E9E9FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          ParentFont = False
          OnClick = btn_simpanClick
          Align = alLeft
          SkinData.SkinSection = 'DRAGBAR'
        end
        object img5: TImage
          Left = 217
          Top = 5
          Width = 18
          Height = 31
          Align = alLeft
        end
      end
      object chk_cetak_nota: TsCheckBox
        Left = 455
        Top = 6
        Width = 132
        Height = 24
        Caption = 'Selalu Cetak Nota'
        Checked = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        State = cbChecked
        TabOrder = 4
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object DBGridEh2: TDBGridEh
        Left = 6
        Top = 6
        Width = 259
        Height = 95
        Align = alLeft
        Color = 15854306
        DataSource = ds_jenisracikan
        DynProps = <>
        Flat = True
        IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
        TabOrder = 5
        Columns = <
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'id'
            Footers = <>
            Title.Alignment = taCenter
            Title.Caption = 'Id'
            Title.Font.Charset = ANSI_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'Trebuchet MS'
            Title.Font.Style = [fsBold]
            Visible = False
            Width = 40
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'diskripsi'
            Footers = <>
            Title.Alignment = taCenter
            Title.Caption = 'Deskripsi'
            Title.Font.Charset = ANSI_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'Trebuchet MS'
            Title.Font.Style = [fsBold]
            Width = 89
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'Column a'
            Footers = <>
            Title.Alignment = taCenter
            Title.Caption = 'Jlh'
            Title.Font.Charset = ANSI_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'Trebuchet MS'
            Title.Font.Style = [fsBold]
            Width = 44
          end
          item
            DisplayFormat = ',#'
            DynProps = <>
            EditButtons = <>
            FieldName = 'biaya'
            Footers = <>
            Title.Alignment = taCenter
            Title.Caption = 'Biaya'
            Title.Font.Charset = ANSI_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'Trebuchet MS'
            Title.Font.Style = [fsBold]
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object pnl_4: TsPanel
      Left = 1
      Top = 177
      Width = 1918
      Height = 53
      Align = alTop
      BevelWidth = 6
      TabOrder = 2
      SkinData.SkinSection = 'PANEL'
      object lbl_2: TLabel
        Left = 44
        Top = 13
        Width = 145
        Height = 22
        Caption = 'KODE / NAMA OBAT'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object edt_cari_obat: TsEdit
        Left = 200
        Top = 9
        Width = 689
        Height = 35
        CharCase = ecUpperCase
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Trebuchet MS'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnChange = edt_cari_obatChange
        OnKeyUp = edt_cari_obatKeyUp
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object sButton3: TsButton
        Left = 813
        Top = 12
        Width = 73
        Height = 29
        Action = act_get_jumlah
        TabOrder = 1
        SkinData.SkinSection = 'BUTTON'
      end
      object chk_jualBebas: TsCheckBox
        Left = 912
        Top = 16
        Width = 169
        Height = 24
        Caption = 'Jual Bebas'
        AutoSize = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Trebuchet MS'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        OnClick = chk_jualBebasClick
        SkinData.SkinSection = 'SELECTION'
        ImgChecked = 0
        ImgUnchecked = 0
      end
    end
    object sStatusBar1: TsStatusBar
      Left = 1
      Top = 1037
      Width = 1918
      Height = 19
      Panels = <
        item
          Width = 200
        end
        item
          Text = 'Pastikan data sudah sesuai, sebelum anda simpan ke database ...'
          Width = 50
        end>
      SkinData.SkinSection = 'STATUSBAR'
    end
    object sPanel3: TsPanel
      Left = 1
      Top = 1
      Width = 1918
      Height = 59
      Align = alTop
      Caption = 'ENTRY OBAT TERJUAL'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -27
      Font.Name = 'Trebuchet MS'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      SkinData.SkinSection = 'HINT'
    end
    object dbg_entry_obat: TDBGridEh
      Left = 1
      Top = 230
      Width = 1918
      Height = 659
      Align = alClient
      DataSource = ds_1
      DynProps = <>
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Trebuchet MS'
      Font.Style = []
      IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
      ParentFont = False
      PopupMenu = pmKoreksiHarga
      SearchPanel.Enabled = True
      TabOrder = 5
      TitleParams.MultiTitle = True
      Columns = <
        item
          Alignment = taCenter
          Color = 15854306
          DynProps = <>
          EditButtons = <>
          FieldName = 'nama_pengunjung'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          HideDuplicates = True
          Title.Caption = 'Jenis Racikan'
          Title.Color = 15259850
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 175
        end
        item
          Alignment = taCenter
          Color = 15854306
          DisplayFormat = ',#'
          DynProps = <>
          EditButtons = <>
          FieldName = 'biaya_admin'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          HideDuplicates = True
          Title.Caption = 'Jlh Racikan'
          Title.Color = 15259850
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 71
        end
        item
          Alignment = taCenter
          Color = 15854306
          DynProps = <>
          EditButtons = <>
          FieldName = 'id'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          HideDuplicates = True
          Title.Caption = 'Racikan Ke'
          Title.Color = 15259850
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 82
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'id_obat'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Kd Obat'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'nm_obat'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Nama Barang'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 396
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'jumlah_obat_keluar'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Non Racikan | Jumlah'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 74
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'satuan'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Non Racikan | Satuan'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
        end
        item
          Color = 15854306
          DynProps = <>
          EditButtons = <>
          FieldName = 'quantity_racik'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Per Racikan | Jlh'
          Title.Color = 15259850
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Title.TitleButton = True
          Width = 61
        end
        item
          Color = 15854306
          DynProps = <>
          EditButtons = <>
          FieldName = 'satuan_racik'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Per Racikan | Satuan'
          Title.Color = 15259850
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 109
        end
        item
          DisplayFormat = ',#'
          DynProps = <>
          EditButtons = <>
          FieldName = 'biaya_listrik'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Biaya Lain - Lain | Listik'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Visible = False
          Width = 130
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'jenis'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Jenis'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Visible = False
          Width = 81
        end
        item
          DisplayFormat = '#,#'
          DynProps = <>
          EditButtons = <>
          FieldName = 'tot_harga'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Total Harga'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 186
        end
        item
          Alignment = taCenter
          Color = 15854306
          DynProps = <>
          EditButtons = <>
          FieldName = 'racikan'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Racikan'
          Title.Color = 15259850
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Visible = False
          Width = 81
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'id_pasien'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'id_jenis_racik'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Visible = False
          Width = 202
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'hrga_jual'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Harga Jual'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Width = 100
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'jlhper_pack'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Visible = False
        end
        item
          Alignment = taCenter
          DynProps = <>
          EditButtons = <>
          FieldName = 'norec'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          Footers = <>
          Title.Caption = 'Kode Barang'
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Trebuchet MS'
          Title.Font.Style = [fsBold]
          Visible = False
          Width = 107
        end>
      object RowDetailData: TRowDetailPanelControlEh
        object DBGridEh1: TDBGridEh
          Left = -1496
          Top = -470
          Width = 1496
          Height = 470
          Align = alCustom
          DataSource = ds_1
          DynProps = <>
          Flat = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Trebuchet MS'
          Font.Style = []
          IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
          ParentFont = False
          SearchPanel.Enabled = True
          TabOrder = 0
          TitleParams.MultiTitle = True
          Columns = <
            item
              Alignment = taCenter
              DynProps = <>
              EditButtons = <>
              FieldName = 'norec'
              Footers = <>
              Title.Caption = 'Kode Barang'
              Width = 108
            end
            item
              DynProps = <>
              EditButtons = <>
              FieldName = 'nm_obat'
              Footers = <>
              Title.Caption = 'Nama Barang'
              Width = 448
            end
            item
              DynProps = <>
              EditButtons = <>
              FieldName = 'jumlah_obat_keluar'
              Footers = <>
              Title.Caption = 'Jumlah'
            end
            item
              DynProps = <>
              EditButtons = <>
              FieldName = 'satuan'
              Footers = <>
              Title.Caption = 'Satuan'
            end
            item
              DynProps = <>
              EditButtons = <>
              FieldName = 'jlhper_raciik'
              Footers = <>
              Title.Caption = 'Per Racikan | Jumlah '
              Width = 113
            end
            item
              DynProps = <>
              EditButtons = <>
              FieldName = 'satuan_racik'
              Footers = <>
              Title.Caption = 'Per Racikan | Satuan'
              Width = 141
            end
            item
              DisplayFormat = ',#'
              DynProps = <>
              EditButtons = <>
              FieldName = 'biaya_listrik'
              Footers = <>
              Title.Caption = 'Biaya Lain - Lain | Listik'
              Visible = False
              Width = 130
            end
            item
              DisplayFormat = ',#'
              DynProps = <>
              EditButtons = <>
              FieldName = 'biaya_admin'
              Footers = <>
              Title.Caption = 'Biaya Lain - Lain | Admin'
              Visible = False
              Width = 103
            end
            item
              DynProps = <>
              EditButtons = <>
              FieldName = 'jenis'
              Footers = <>
              Title.Caption = 'Jenis'
              Width = 73
            end
            item
              DisplayFormat = ',#'
              DynProps = <>
              EditButtons = <>
              FieldName = 'tot_harga'
              Footers = <>
              Title.Caption = 'Total harga'
              Width = 186
            end
            item
              DynProps = <>
              EditButtons = <>
              FieldName = 'racikan'
              Footers = <>
              Title.Caption = 'Racikan'
              Width = 109
            end
            item
              DynProps = <>
              EditButtons = <>
              FieldName = 'satuan_pack'
              Footers = <>
              Title.Caption = 'Puyer'
              Width = 202
            end
            item
              DynProps = <>
              EditButtons = <>
              FieldName = 'nama_pengunjung'
              Footers = <>
              Title.Caption = 'Pot'
              Width = 175
            end
            item
              DynProps = <>
              EditButtons = <>
              FieldName = 'hrga_jual'
              Footers = <>
            end
            item
              DynProps = <>
              EditButtons = <>
              FieldName = 'jlhper_pack'
              Footers = <>
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
      end
    end
  end
  object dbg_obat1: TDBGridEh
    Left = 608
    Top = 344
    Width = 345
    Height = 161
    Align = alCustom
    DataSource = dm.ds_obat
    DrawMemoText = True
    DynProps = <>
    Flat = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Trebuchet MS'
    Font.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
    ParentFont = False
    SearchPanel.FilterOnTyping = True
    TabOrder = 1
    TitleParams.MultiTitle = True
    VertScrollBar.VisibleMode = sbNeverShowEh
    Visible = False
    OnDblClick = act_get_jumlahExecute
    OnDrawColumnCell = dbg_obat1DrawColumnCell
    Columns = <
      item
        Alignment = taCenter
        DynProps = <>
        EditButtons = <>
        FieldName = 'id'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = []
        Footers = <>
        Title.Caption = 'Kode Obat'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 95
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'nm_obat'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = []
        Footers = <>
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 318
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'quantity'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = []
        Footers = <>
        Title.Caption = 'Quantity / Stok'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 115
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'satuan'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = []
        Footers = <>
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'quantity_racik'
        Footers = <>
        Title.Caption = '-'
        Width = 52
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'satuan_racik'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = []
        Footers = <>
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 154
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'hrga_jual'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = []
        Footers = <>
        Title.Caption = 'Harga Jual'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'jenis'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Trebuchet MS'
        Font.Style = []
        Footers = <>
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Trebuchet MS'
        Title.Font.Style = [fsBold]
        Width = 83
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object q_obat: TMyQuery
    Connection = dm.my_connection
    SQL.Strings = (
      'select * from t_obat')
    Left = 48
    Top = 312
    object q_obatkd_obat: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'kd_obat'
      Origin = 't_obat.kd_obat'
    end
    object q_obatnm_obat: TStringField
      FieldName = 'nm_obat'
      Origin = 't_obat.nm_obat'
      Size = 100
    end
    object q_obatsatuan: TStringField
      FieldName = 'satuan'
      Origin = 't_obat.satuan'
      FixedChar = True
      Size = 15
    end
    object q_obathrga_jual: TIntegerField
      FieldName = 'hrga_jual'
      Origin = 't_obat.hrga_jual'
    end
    object q_obatquantity: TFloatField
      FieldName = 'quantity'
    end
  end
  object ds_obat: TMyDataSource
    DataSet = q_obat
    Left = 8
    Top = 312
  end
  object prov_obat_masuk: TDataSetProvider
    DataSet = q_obat_masuk_dummy
    Left = 112
    Top = 424
  end
  object ds_obat_keluar_1: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    ProviderName = 'prov_obat_masuk'
    BeforePost = ds_obat_keluar_1BeforePost
    AfterPost = ds_obat_keluar_1AfterPost
    Left = 120
    Top = 480
    object ds_obat_keluar_1norec: TLargeintField
      FieldName = 'norec'
      Origin = 't_obat_keluar.norec'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object ds_obat_keluar_1no_nota: TStringField
      FieldName = 'no_nota'
      Origin = 't_obat_keluar.no_nota'
      Size = 50
    end
    object ds_obat_keluar_1nama_pengunjung: TStringField
      FieldName = 'nama_pengunjung'
      Origin = 't_obat_keluar.nama_pengunjung'
      Size = 100
    end
    object ds_obat_keluar_1id_pasien: TLargeintField
      FieldName = 'id_pasien'
      Origin = 't_obat_keluar.id_pasien'
    end
    object ds_obat_keluar_1tgl_obat_keluar: TDateTimeField
      FieldName = 'tgl_obat_keluar'
      Origin = 't_obat_keluar.tgl_obat_keluar'
    end
    object ds_obat_keluar_1id_obat: TLargeintField
      FieldName = 'id_obat'
      Origin = 't_obat_keluar.id_obat'
    end
    object ds_obat_keluar_1jumlah_obat_keluar: TFloatField
      FieldName = 'jumlah_obat_keluar'
      Origin = 't_obat_keluar.jumlah_obat_keluar'
    end
    object ds_obat_keluar_1jlh_sub_racikan: TSmallintField
      FieldName = 'jlh_sub_racikan'
      Origin = 't_obat_keluar.jlh_sub_racikan'
    end
    object ds_obat_keluar_1biaya_listrik: TIntegerField
      FieldName = 'biaya_listrik'
      Origin = 't_obat_keluar.biaya_listrik'
    end
    object ds_obat_keluar_1biaya_admin: TIntegerField
      FieldName = 'biaya_admin'
      Origin = 't_obat_keluar.biaya_admin'
    end
    object ds_obat_keluar_1nm_dokter: TStringField
      FieldName = 'nm_dokter'
      Origin = 't_obat_keluar.nm_dokter'
      Size = 150
    end
    object ds_obat_keluar_1tot_harga: TIntegerField
      FieldName = 'tot_harga'
      Origin = 't_obat_keluar.tot_harga'
    end
    object ds_obat_keluar_1uang: TIntegerField
      FieldName = 'uang'
      Origin = 't_obat_keluar.uang'
    end
    object ds_obat_keluar_1kembalian: TIntegerField
      FieldName = 'kembalian'
      Origin = 't_obat_keluar.kembalian'
    end
    object ds_obat_keluar_1insert_at: TDateTimeField
      FieldName = 'insert_at'
      Origin = 't_obat_keluar.insert_at'
    end
    object ds_obat_keluar_1racikan: TStringField
      FieldName = 'racikan'
      Origin = 't_obat_keluar.racikan'
      FixedChar = True
      Size = 1
    end
    object ds_obat_keluar_1biaya_dokter: TIntegerField
      FieldName = 'biaya_dokter'
      Origin = 't_obat_keluar.biaya_dokter'
    end
    object ds_obat_keluar_1id_jenis_racikan: TIntegerField
      FieldName = 'id_jenis_racikan'
      Origin = 't_obat_keluar.id_jenis_racikan'
    end
    object ds_obat_keluar_1id_user: TIntegerField
      FieldName = 'id_user'
      Origin = 't_obat_keluar.id_user'
    end
    object ds_obat_keluar_1hrgajualbersih: TIntegerField
      FieldName = 'hrgajualbersih'
      Origin = 't_obat_keluar.hrgajualbersih'
    end
    object ds_obat_keluar_1id: TLargeintField
      FieldName = 'id'
      Origin = 't_obat.id'
    end
    object ds_obat_keluar_1enabled: TLargeintField
      FieldName = 'enabled'
      Origin = 't_obat.enabled'
    end
    object ds_obat_keluar_1nm_obat: TStringField
      FieldName = 'nm_obat'
      Origin = 't_obat.nm_obat'
      Size = 100
    end
    object ds_obat_keluar_1satuan: TStringField
      FieldName = 'satuan'
      Origin = 't_obat.satuan'
      FixedChar = True
      Size = 15
    end
    object ds_obat_keluar_1quantity_racik: TFloatField
      FieldName = 'quantity_racik'
      Origin = 't_obat.quantity_racik'
    end
    object ds_obat_keluar_1jlhper_raciik: TIntegerField
      FieldName = 'jlhper_raciik'
      Origin = 't_obat.jlhper_raciik'
    end
    object ds_obat_keluar_1satuan_racik: TStringField
      FieldName = 'satuan_racik'
      Origin = 't_obat.satuan_racik'
      Size = 50
    end
    object ds_obat_keluar_1expire_date: TDateField
      FieldName = 'expire_date'
      Origin = 't_obat.expire_date'
    end
    object ds_obat_keluar_1tgl_jatuh_tempo: TDateField
      FieldName = 'tgl_jatuh_tempo'
      Origin = 't_obat.tgl_jatuh_tempo'
    end
    object ds_obat_keluar_1hrgper_racik: TIntegerField
      FieldName = 'hrgper_racik'
      Origin = 't_obat.hrgper_racik'
    end
    object ds_obat_keluar_1hrga_jual: TIntegerField
      FieldName = 'hrga_jual'
      Origin = 't_obat.hrga_jual'
    end
    object ds_obat_keluar_1harga_satuan: TIntegerField
      FieldName = 'harga_satuan'
      Origin = 't_obat.harga_satuan'
    end
    object ds_obat_keluar_1jenis: TStringField
      FieldName = 'jenis'
      Origin = 't_obat.jenis'
      FixedChar = True
      Size = 5
    end
    object ds_obat_keluar_1jlhper_pack: TIntegerField
      FieldName = 'jlhper_pack'
      Origin = 't_obat.jlhper_pack'
    end
    object ds_obat_keluar_1satuan_pack: TStringField
      FieldName = 'satuan_pack'
      Origin = 't_obat.satuan_pack'
      Size = 100
    end
    object ds_obat_keluar_1hrgper_pack: TIntegerField
      FieldName = 'hrgper_pack'
      Origin = 't_obat.hrgper_pack'
    end
    object ds_obat_keluar_1ket: TMemoField
      FieldName = 'ket'
      Origin = 't_obat.ket'
      BlobType = ftMemo
    end
    object ds_obat_keluar_1hrgbeli_cream: TIntegerField
      FieldName = 'hrgbeli_cream'
      Origin = 't_obat.hrgbeli_cream'
    end
    object ds_obat_keluar_1quantity: TFloatField
      FieldName = 'quantity'
      Origin = 't_obat.quantity'
    end
  end
  object q_obat_masuk_dummy: TMyQuery
    Connection = dm.my_connection
    SQL.Strings = (
      'SELECT '
      't_obat_keluar.*,'
      't_obat.* FROM '
      't_obat, t_obat_keluar'
      'LIMIT 1')
    Active = True
    Left = 80
    Top = 584
  end
  object ds_1: TMyDataSource
    DataSet = ds_obat_keluar_1
    Left = 640
    Top = 512
  end
  object actmgr1: TActionManager
    Left = 24
    Top = 424
    StyleName = 'XP Style'
    object act_hapus_entry: TAction
      Caption = 'act_hapus_entry'
      ShortCut = 46
    end
    object act_get_jumlah: TAction
      Caption = 'Entry (F1)'
      ShortCut = 112
      OnExecute = act_get_jumlahExecute
    end
    object act_batal: TAction
      Caption = 'Batal (Esc)'
      ShortCut = 27
      OnExecute = act_batalExecute
    end
    object act_print_nota: TAction
      Caption = 'act_print_nota'
      ShortCut = 16464
    end
    object act_savetoDatabase: TAction
      Caption = 'act_savetoDatabase'
      ShortCut = 120
      OnExecute = btn_simpanClick
    end
  end
  object q_obat_keluar: TMyQuery
    Connection = dm.my_connection
    SQL.Strings = (
      'select * from t_obat_keluar limit 1')
    Active = True
    Left = 24
    Top = 480
    object q_obat_keluarnorec: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'norec'
      Origin = 't_obat_keluar.norec'
    end
    object q_obat_keluarno_nota: TStringField
      FieldName = 'no_nota'
      Origin = 't_obat_keluar.no_nota'
      Size = 50
    end
    object q_obat_keluarnama_pengunjung: TStringField
      FieldName = 'nama_pengunjung'
      Origin = 't_obat_keluar.nama_pengunjung'
      Size = 100
    end
    object q_obat_keluarid_pasien: TLargeintField
      FieldName = 'id_pasien'
      Origin = 't_obat_keluar.id_pasien'
    end
    object q_obat_keluartgl_obat_keluar: TDateTimeField
      FieldName = 'tgl_obat_keluar'
      Origin = 't_obat_keluar.tgl_obat_keluar'
    end
    object q_obat_keluarid_obat: TLargeintField
      FieldName = 'id_obat'
      Origin = 't_obat_keluar.id_obat'
    end
    object q_obat_keluarjumlah_obat_keluar: TFloatField
      FieldName = 'jumlah_obat_keluar'
      Origin = 't_obat_keluar.jumlah_obat_keluar'
    end
    object q_obat_keluarjlh_sub_racikan: TSmallintField
      FieldName = 'jlh_sub_racikan'
      Origin = 't_obat_keluar.jlh_sub_racikan'
    end
    object q_obat_keluarbiaya_listrik: TIntegerField
      FieldName = 'biaya_listrik'
      Origin = 't_obat_keluar.biaya_listrik'
    end
    object q_obat_keluarbiaya_admin: TIntegerField
      FieldName = 'biaya_admin'
      Origin = 't_obat_keluar.biaya_admin'
    end
    object q_obat_keluarnm_dokter: TStringField
      FieldName = 'nm_dokter'
      Origin = 't_obat_keluar.nm_dokter'
      Size = 150
    end
    object q_obat_keluartot_harga: TIntegerField
      FieldName = 'tot_harga'
      Origin = 't_obat_keluar.tot_harga'
    end
    object q_obat_keluaruang: TIntegerField
      FieldName = 'uang'
      Origin = 't_obat_keluar.uang'
    end
    object q_obat_keluarkembalian: TIntegerField
      FieldName = 'kembalian'
      Origin = 't_obat_keluar.kembalian'
    end
    object q_obat_keluarinsert_at: TDateTimeField
      FieldName = 'insert_at'
      Origin = 't_obat_keluar.insert_at'
    end
    object q_obat_keluarracikan: TStringField
      FieldName = 'racikan'
      Origin = 't_obat_keluar.racikan'
      FixedChar = True
      Size = 1
    end
    object q_obat_keluarbiaya_dokter: TIntegerField
      FieldName = 'biaya_dokter'
      Origin = 't_obat_keluar.biaya_dokter'
    end
    object q_obat_keluarid_jenis_racikan: TIntegerField
      FieldName = 'id_jenis_racikan'
      Origin = 't_obat_keluar.id_jenis_racikan'
    end
    object q_obat_keluarid_user: TIntegerField
      FieldName = 'id_user'
      Origin = 't_obat_keluar.id_user'
    end
    object q_obat_keluarhrgajualbersih: TIntegerField
      FieldName = 'hrgajualbersih'
      Origin = 't_obat_keluar.hrgajualbersih'
    end
  end
  object sAlphaImageList1: TsAlphaImageList
    Items = <
      item
        ImageFormat = ifPNG
        ImageName = 'add'
        ImgData = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          61000000017352474200AECE1CE90000000467414D410000B18F0BFC61050000
          00097048597300000EC400000EC401952B0E1B0000025D49444154384FA593EB
          4FD25118C7FDA36A6DD54ACBAD5EB4B53993C2CD5AA426E5A58B8A89E48D0245
          372F80F332BB48852C36051154529412076A2A2E2F9066653ACB32B7DE7EFAFD
          D884A8E99B9EED6CE7ECECFBF93EE7799E93C07F46C2DFFAEFBFBEE20DB9D00F
          57A276145061CF45EB523030DFC3D6EEC63F767180C5CD191A5FA9783AD5847F
          7D94E0D634539B13F487ADA89C37297E99CDE4EA781C240A10C5B503850C84AC
          ACFE0C31F4A98FAEE5563A17F55857BAF06E0CA3F528C87C2C61E2BD370A8900
          C4B44567C79299D0CE02ED0B0D344E3EA42E50853E584373504BDD6C258E552B
          4AA79CAC27523676D6239008606CD98961BC9A77DBB38263334D735A343E55D4
          A57A5241E9A89CB2F13CEC2B16D23A9279E67B1403340F57D017B608A99AA8F5
          97A39E284531941B05E458A4140D5EE6B62B9D96991A1ABCF7B9D22189012A85
          4ABBD7FA30CCD708E2927D1B9BDF7F91B291EB9883ED9CAD3F1103A87A72E80D
          BF403BADA43C70F74080D29D4577B08DD3B54763004D7F11C68016FD9C8692D7
          72F247AE72AD3B350A129DC575CB29C510A846E7BE47AAFE5C0CE00C5AC9EE4E
          C11636A3189151E0CE88BC792F4471AE5DC21D473AB62513A7EA8FD0EA31C400
          5BBB9B145AB28416E5D0B3FC3C5A3051986717972896D2BBD8C50DF325E1FD49
          7CDC5E8B01C49D7FE50DB2CE548A6D32EC02C418788072309352970CA35F8D4D
          10CBCD128EAB0F3138EF8A1FA4BD932F3C2640D238DF9248BD4789E9AD1ED3B4
          1E8DBB9824DD61CEE812E3C4D141FAB3EC5F7E7CC6E4EB24A3ED8290EA499275
          C748110AD6E631B2F6EDC3C19F69DFFE1D70F11B6D79504FF1E7C27C00000000
          49454E44AE426082}
      end
      item
        ImageFormat = ifPNG
        ImageName = 'cross'
        ImgData = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          61000000017352474200AECE1CE90000000467414D410000B18F0BFC61050000
          00097048597300000EC400000EC401952B0E1B000001FA49444154384F95934D
          4F13511486F931C6151213A22BE59BD68ACA42A05028F603A9846229B5A4501B
          74A18989F1A71877FA07EA4EE3474222A20B5B8BA8B44860667AA7731FEE8C40
          9DB43561929B39937B9F77CE7BCEB96D9CF6A9565D449BEBAB5C86AD1F2D2565
          B148EDE52BACF5F5933375011B5E5E41A6334A64AB41C4868D50946A6C0EFDE9
          33AC6F05E7CC5F812398DC03A45AD6620A592A9D8838F07418337E17B2392736
          9EBF00298F044C1399BC875CB90F8F9F6065B29877E691C5EFC882828321C4FC
          02A87D7D3AC4AEF72A28A69E811D09935A3C81955A86878FA82DA531C251F4A9
          5B88B93864569DB8E2F121FF29A4BB8842509D89A95413905B83D59C02B3AA36
          19F4C92095C12B2ED89DC1B16325A205820A5476ECB73F00A934957E4F03DC54
          C02A14D00353607763741C6E8E4122C9EEA097DA97AF0DDD7159B0616D621211
          9D85644A814BB0A0ECA802EA2363942F75616E6E361F24071E0F2022B741B551
          F34F50EE1BE0F7E56EF6AF0DC36C8C83EBC3FCEABC80B9F1D93D48F65068CAAB
          08CFA8BF2EA2A9D4CBBDFD8E677B6D775E646FC003E108FB5E1FDBED1D884F1B
          F536D63E7CE460D48F0845D046FCECF4F4B90A260D8352FB39FE74F5A0F986F8
          D9711E23FFDA3D07E6BBF7EC0DDD60A7BBB769B56D91E299B32EB8A10BE2CDDB
          A6F0B1615BC4C8E7FF731B5BDEC3D61B872D2C7BFF6B936CF50000000049454E
          44AE426082}
      end
      item
        ImageFormat = ifPNG
        ImageName = 'delete'
        ImgData = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          61000000017352474200AECE1CE90000000467414D410000B18F0BFC61050000
          00097048597300000EC400000EC401952B0E1B0000024249444154384FA5D3ED
          4BD35114C071FFA67C53F4E0FB3475CCE1722B6B6E990B9998B9296E98665062
          A821113E5B662E1B5119ED855AAECC5233483498896EB52773FED43DAB7C7383
          FCB5345FD48503F7BE389F7339F7DC14FE73A5FC99BFB9BC8C30F80287A98A05
          6D11F3DA42160C97597E6A21EAF5EC29970484A6A771180CAC343710191D22FA
          7992E8848DC0B37E1C3A0DB3C505AC7E184B42768178F2924EC7FA403FDBF35F
          88591F13EB6924D67693CD87AD6CBDB3E2ADD4F1F1BC14FFB86D174900F16B3B
          F506361EF4B06D9F2176F73A42A389B5860A22B74D449B8C44EAAF1019ECC359
          AC62AA3097B0C795401280F07C105F4D155B3353898AE16623FE1B65846A2F25
          C5864145C4728F4F99C75934778A8073A76141CB7D62E63684FA727E9C95EE1B
          EBE52A020DD5784D7AC6B53211982F2E22FAD242ACD984704DF7D787F5CA33F0
          179D61ADAD85919C632260BFA8266CEE2252A72374B5F040C0A751B0D6DAC290
          ECB0087CD597E2AFAF26DC6864A35449BCD2BE919789505389A3B20C9B3A5D04
          7C4F06B0CB4E1232772314C8F06B247B00576E3AAED312823D1DD8B252B1F7B6
          8840D4E761A6448D537B8EE0A3AE44037D8AAC04E2929FC225DB49966511E86E
          67365FCAB0328DA0DB2102F1DDEAFBB74CE567B3A85610ECED62B5CE88579587
          5B29C75F5B45A0B39D39653656492AEE37D6E441FA755A191F654293C3A4E428
          9E8A32FC4DB712F14D5FC2AB8C430C29D292927707E9F7B687DDDF59EAEF604C
          2B65447E8261E9115E5F48C7DE7787A06BE9E0CFF42F3FFB279BD5512A825470
          240000000049454E44AE426082}
      end
      item
        ImageFormat = ifPNG
        ImageName = 'disk'
        ImgData = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          61000000017352474200AECE1CE90000000467414D410000B18F0BFC61050000
          00097048597300000EC400000EC401952B0E1B0000029F49444154384F8D92D9
          4F135114C6E74F12EA863E1889CF3EF96A6469D98D424156A3468D1A5B090623
          8BA050A16C0A22600A2D0829A1AD140B06428142A708A5405B28D0295DFC3CF7
          5679249DE4979B99B9DF77BE7BCE15DA0C22D4DD76DC7C6E466AF177A4DCD6E3
          7CFE30CEE511B9712EE68FE0127D4F2918C635E528142A13F45637D8233CF9B8
          8059679090108AFC8144048F63D83B8A627B2F0C6F20C2DFA5700C3BFB11AC79
          4230CCEE22B7C68A3EA30BC28D4793985D0BE241A707C130896963E64B137C07
          116C9320108C72E37D5A5DBBC7587687A0B3F9A0B7ED2255A987705539061B19
          DCD76EF14D0C66E0D90FC37718E19559822D4AE3D80A6179534279AB8889793F
          1D6F08023BF3B4E3880CDC7C1343A132C343EB41288610A5F21F46293AAB2EC1
          BE21A1B4C5898E7137CE660F42600D33AF1CA2AADD0D912232D6BDC7543DCAAB
          1F48316C78C327E2C58D20EEBD5F85C6F01BB2ACAF1058B7A7EC64D0B6C9A3CB
          A97A96DA8CFC9A1F282072AA2D90D377F6EF3FCAC62534EB44C814FD64406332
          2E0650A159A7B311B456685CA8686588286B11A9E21A4A9A57A16C72A0B07119
          CA772BA81F704026EF8B1B8CCF07E262222E24C884894B3F3879E4E226322061
          51C31261476DEF1292333FC70D0C737B2476FD433CA9CEC554B9A4D9C1C58524
          BC5BB7883B750B5077CD2339A38719E8306CF3A3BCC5C5E79D08056F7EE145FB
          1C92D2BB205CA0267E9BF1F3B8128DECD958FAA9B03B91FBFA271EB7CE2029AD
          930C688C83D33E4A1037185F193D1566905D6DC5C3A66932D032031DFA2D5E32
          70269C40AEB2A0B2C14C06ED10AE574DA06772073DC6ED84CECF7A54596F42D9
          DB295CC9A326BEEAA6AED65A915AA4A7AB39440CF01B26537C41B2BC9746F509
          4919DDD4B04EAAD88133B7B4BCF2E59C2EA43D1DC15F3C7421360609534C0000
          000049454E44AE426082}
      end>
    Left = 16
    Top = 176
    Bitmap = {}
  end
  object report_notadetail: TppReport
    AutoStop = False
    DataPipeline = print_obat_keluar
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A5'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 2540
    PrinterSetup.mmMarginLeft = 5080
    PrinterSetup.mmMarginRight = 2540
    PrinterSetup.mmMarginTop = 2540
    PrinterSetup.mmPaperHeight = 210000
    PrinterSetup.mmPaperWidth = 148000
    PrinterSetup.PaperSize = 11
    Template.FileName = 'D:\RS KOTA\APP ++\10. Apotek Praya\Report\nota Detail.rtm'
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    Left = 56
    Top = 80
    Version = '15.04'
    mmColumnWidth = 69921
    DataPipelineName = 'print_obat_keluar'
    object ppHeaderBand1: TppHeaderBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 35454
      mmPrintPosition = 0
      object ppLabel1: TppLabel
        UserName = 'Label1'
        Caption = 'Apotek'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 2910
        mmTop = 4233
        mmWidth = 10848
        BandType = 0
        LayerName = BandLayer2
      end
      object ppLabel2: TppLabel
        UserName = 'Label2'
        Caption = 'APOTEK PRAYA '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 2646
        mmTop = 8202
        mmWidth = 27781
        BandType = 0
        LayerName = BandLayer2
      end
      object ppSystemVariable1: TppSystemVariable
        UserName = 'SystemVariable1'
        VarType = vtDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 20902
        mmTop = 22490
        mmWidth = 38629
        BandType = 0
        LayerName = BandLayer2
      end
      object ppLabel3: TppLabel
        UserName = 'Label3'
        Caption = 'Tanggal  :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 2646
        mmTop = 22225
        mmWidth = 15081
        BandType = 0
        LayerName = BandLayer2
      end
      object ppLabel4: TppLabel
        UserName = 'Label5'
        Caption = 'Jln. Bungkarno  No 3. Pagutan Kota Mataram'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 2646
        mmTop = 11642
        mmWidth = 69586
        BandType = 0
        LayerName = BandLayer2
      end
      object ppLabel5: TppLabel
        UserName = 'Label4'
        Caption = 'Nama Obat'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 2646
        mmTop = 28575
        mmWidth = 42333
        BandType = 0
        LayerName = BandLayer2
      end
      object ppLabel6: TppLabel
        UserName = 'Label7'
        Caption = 'Qty'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 59267
        mmTop = 28575
        mmWidth = 8202
        BandType = 0
        LayerName = BandLayer2
      end
      object ppLabel7: TppLabel
        UserName = 'Label8'
        Caption = 'Harga Satuan'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 86254
        mmTop = 28575
        mmWidth = 23019
        BandType = 0
        LayerName = BandLayer2
      end
      object ppLabel8: TppLabel
        UserName = 'Label9'
        Caption = 'Total'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4233
        mmLeft = 125148
        mmTop = 28840
        mmWidth = 8467
        BandType = 0
        LayerName = BandLayer2
      end
      object ppLine1: TppLine
        UserName = 'Line1'
        Position = lpBottom
        Weight = 0.750000000000000000
        mmHeight = 1323
        mmLeft = 1588
        mmTop = 32279
        mmWidth = 137054
        BandType = 0
        LayerName = BandLayer2
      end
      object ppLabel22: TppLabel
        UserName = 'Label22'
        Caption = 'No Nota  : '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 2646
        mmTop = 17463
        mmWidth = 16669
        BandType = 0
        LayerName = BandLayer2
      end
      object ppDBText22: TppDBText
        UserName = 'DBText101'
        DataField = 'no_nota'
        DataPipeline = print_obat_keluar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 20902
        mmTop = 17429
        mmWidth = 43392
        BandType = 0
        LayerName = BandLayer2
      end
    end
    object ppDetailBand1: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 5556
      mmPrintPosition = 0
      object ppDBText1: TppDBText
        UserName = 'DBText4'
        DataField = 'harga_satuan'
        DataPipeline = print_obat_keluar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4173
        mmLeft = 79111
        mmTop = 590
        mmWidth = 25991
        BandType = 4
        LayerName = BandLayer2
      end
      object ppDBText2: TppDBText
        UserName = 'DBText5'
        DataField = 'total_harga'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4172
        mmLeft = 106098
        mmTop = 590
        mmWidth = 31485
        BandType = 4
        LayerName = BandLayer2
      end
      object ppDBText3: TppDBText
        UserName = 'DBText8'
        DataField = 'jumlah_obat_keluar'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 48683
        mmTop = 590
        mmWidth = 11050
        BandType = 4
        LayerName = BandLayer2
      end
      object ppDBText4: TppDBText
        UserName = 'DBText9'
        DataField = 'satuan'
        DataPipeline = print_obat_keluar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 60854
        mmTop = 590
        mmWidth = 15719
        BandType = 4
        LayerName = BandLayer2
      end
      object ppDBText5: TppDBText
        UserName = 'DBText10'
        DataField = 'nama_obat'
        DataPipeline = print_obat_keluar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 2646
        mmTop = 590
        mmWidth = 43392
        BandType = 4
        LayerName = BandLayer2
      end
    end
    object ppSummaryBand1: TppSummaryBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 52123
      mmPrintPosition = 0
      object ppLabel9: TppLabel
        UserName = 'Label10'
        Caption = 'Total :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 96309
        mmTop = 2646
        mmWidth = 9525
        BandType = 7
        LayerName = BandLayer2
      end
      object ppLabel10: TppLabel
        UserName = 'Label6'
        Caption = 'Terima Kasih Atas kunjungan anda ...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 1588
        mmTop = 34660
        mmWidth = 57679
        BandType = 7
        LayerName = BandLayer2
      end
      object ppLine2: TppLine
        UserName = 'Line2'
        Position = lpBottom
        Weight = 0.750000000000000000
        mmHeight = 467
        mmLeft = 1512
        mmTop = 0
        mmWidth = 138051
        BandType = 7
        LayerName = BandLayer2
      end
      object ppLabel11: TppLabel
        UserName = 'Label101'
        Caption = 'Uang Cash :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 86024
        mmTop = 22225
        mmWidth = 19050
        BandType = 7
        LayerName = BandLayer2
      end
      object ppDBText6: TppDBText
        UserName = 'DBText11'
        DataField = 'uang'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 105834
        mmTop = 22225
        mmWidth = 31750
        BandType = 7
        LayerName = BandLayer2
      end
      object ppLabel12: TppLabel
        UserName = 'Label14'
        Caption = 'Kembalian :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 86892
        mmTop = 27558
        mmWidth = 18256
        BandType = 7
        LayerName = BandLayer2
      end
      object ppDBText7: TppDBText
        UserName = 'DBText12'
        DataField = 'kembalian'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 105834
        mmTop = 27252
        mmWidth = 31750
        BandType = 7
        LayerName = BandLayer2
      end
      object ppLabel18: TppLabel
        UserName = 'Label15'
        Caption = 'Biaya Admin :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 83873
        mmTop = 7408
        mmWidth = 21696
        BandType = 7
        LayerName = BandLayer2
      end
      object ppDBText18: TppDBText
        UserName = 'DBText15'
        DataField = 'biayaadmin'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 106098
        mmTop = 7456
        mmWidth = 31750
        BandType = 7
        LayerName = BandLayer2
      end
      object ppDBCalc1: TppDBCalc
        UserName = 'DBCalc4'
        DataField = 'total_harga'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 106098
        mmTop = 2646
        mmWidth = 31750
        BandType = 7
        LayerName = BandLayer2
      end
      object ppLabel19: TppLabel
        UserName = 'Label102'
        Caption = 'Total Bayar :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 85461
        mmTop = 17198
        mmWidth = 19579
        BandType = 7
        LayerName = BandLayer2
      end
      object ppDBText19: TppDBText
        UserName = 'DBText16'
        DataField = 'tot_harga'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 105834
        mmTop = 17198
        mmWidth = 31750
        BandType = 7
        LayerName = BandLayer2
      end
      object ppDBText20: TppDBText
        UserName = 'DBText17'
        DataField = 'biayaRacik'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 105569
        mmTop = 12171
        mmWidth = 31750
        BandType = 7
        LayerName = BandLayer2
      end
      object ppLabel20: TppLabel
        UserName = 'Label17'
        Caption = 'Biaya Racikan :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 80698
        mmTop = 12171
        mmWidth = 24342
        BandType = 7
        LayerName = BandLayer2
      end
    end
    object ppDesignLayers1: TppDesignLayers
      object ppDesignLayer1: TppDesignLayer
        UserName = 'BandLayer2'
        LayerType = ltBanded
        Index = 0
      end
    end
    object ppParameterList1: TppParameterList
    end
  end
  object sTitleBar1: TsTitleBar
    Items = <
      item
        Caption = 'Simpan Ke Database'
        FontData.Font.Charset = DEFAULT_CHARSET
        FontData.Font.Color = clWindowText
        FontData.Font.Height = -11
        FontData.Font.Name = 'Tahoma'
        FontData.Font.Style = []
        Height = 21
        Index = 0
        Name = 'TacTitleBarItem'
        ShowHint = False
        Width = 113
      end>
    ShowCaption = False
    Left = 1202
    Top = 185
  end
  object pp_Print_obat_keluar: TppReport
    AutoStop = False
    DataPipeline = print_obat_keluar
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A5'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 2540
    PrinterSetup.mmMarginLeft = 5080
    PrinterSetup.mmMarginRight = 2540
    PrinterSetup.mmMarginTop = 2540
    PrinterSetup.mmPaperHeight = 210000
    PrinterSetup.mmPaperWidth = 148000
    PrinterSetup.PaperSize = 11
    Template.FileName = 'D:\RS KOTA\APP ++\10. Apotek Praya\Report\nota Detail  mm.rtm'
    ArchiveFileName = '($MyDocuments)\ReportArchive.raf'
    DeviceType = 'Screen'
    DefaultFileDeviceType = 'PDF'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OpenFile = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    ThumbnailSettings.Enabled = True
    ThumbnailSettings.Visible = True
    ThumbnailSettings.DeadSpace = 30
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.EncryptSettings.KeyLength = kl40Bit
    PDFSettings.FontEncoding = feAnsi
    PDFSettings.ImageCompressionLevel = 25
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextFileName = '($MyDocuments)\Report.pdf'
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    XLSSettings.AppName = 'ReportBuilder'
    XLSSettings.Author = 'ReportBuilder'
    XLSSettings.Subject = 'Report'
    XLSSettings.Title = 'Report'
    Left = 88
    Top = 824
    Version = '15.04'
    mmColumnWidth = 69921
    DataPipelineName = 'print_obat_keluar'
    object ppHeaderBand2: TppHeaderBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 35454
      mmPrintPosition = 0
      object ppLabel23: TppLabel
        UserName = 'Label1'
        Caption = 'Apotek'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 2910
        mmTop = 1588
        mmWidth = 10848
        BandType = 0
        LayerName = Foreground1
      end
      object ppLabel24: TppLabel
        UserName = 'Label2'
        Caption = 'APOTEK SEJAHTERA'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4234
        mmLeft = 2646
        mmTop = 5556
        mmWidth = 35189
        BandType = 0
        LayerName = Foreground1
      end
      object ppSystemVariable4: TppSystemVariable
        UserName = 'SystemVariable1'
        VarType = vtDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 20902
        mmTop = 23663
        mmWidth = 38629
        BandType = 0
        LayerName = Foreground1
      end
      object ppLabel25: TppLabel
        UserName = 'Label3'
        Caption = 'Tanggal  :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 2646
        mmTop = 23663
        mmWidth = 15081
        BandType = 0
        LayerName = Foreground1
      end
      object ppLabel26: TppLabel
        UserName = 'Label5'
        Caption = 
          'Jl. Diponegoro No.40, Praya, Kabupaten Lombok Tengah, Nusa Tengg' +
          'ara Bar. 83511'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 2646
        mmTop = 10848
        mmWidth = 107421
        BandType = 0
        LayerName = Foreground1
      end
      object ppLabel27: TppLabel
        UserName = 'Label4'
        Caption = 'Nama Obat'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 2646
        mmTop = 30427
        mmWidth = 42333
        BandType = 0
        LayerName = Foreground1
      end
      object ppLabel28: TppLabel
        UserName = 'Label7'
        Caption = 'Qty'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 59267
        mmTop = 30427
        mmWidth = 5556
        BandType = 0
        LayerName = Foreground1
      end
      object ppLabel29: TppLabel
        UserName = 'Label8'
        Caption = 'Harga Satuan'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 86254
        mmTop = 30427
        mmWidth = 23019
        BandType = 0
        LayerName = Foreground1
      end
      object ppLabel30: TppLabel
        UserName = 'Label9'
        Caption = 'Total'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4233
        mmLeft = 125148
        mmTop = 30692
        mmWidth = 8467
        BandType = 0
        LayerName = Foreground1
      end
      object ppLine23: TppLine
        UserName = 'Line1'
        Position = lpBottom
        Weight = 0.750000000000000000
        mmHeight = 1323
        mmLeft = 1588
        mmTop = 34131
        mmWidth = 137054
        BandType = 0
        LayerName = Foreground1
      end
      object ppLabel21: TppLabel
        UserName = 'Label21'
        Caption = 'No Nota : '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 2646
        mmTop = 18521
        mmWidth = 15610
        BandType = 0
        LayerName = Foreground1
      end
      object ppDBText21: TppDBText
        UserName = 'DBText21'
        DataField = 'no_nota'
        DataPipeline = print_obat_keluar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 20902
        mmTop = 18521
        mmWidth = 38629
        BandType = 0
        LayerName = Foreground1
      end
      object ppLine3: TppLine
        UserName = 'Line3'
        Style = lsDouble
        Weight = 0.750000000000000000
        mmHeight = 1989
        mmLeft = 2910
        mmTop = 9745
        mmWidth = 135467
        BandType = 0
        LayerName = Foreground1
      end
    end
    object ppDetailBand2: TppDetailBand
      Background1.Brush.Style = bsClear
      Background2.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 5556
      mmPrintPosition = 0
      object ppDBText13: TppDBText
        UserName = 'DBText4'
        DataField = 'harga_satuan'
        DataPipeline = print_obat_keluar
        DisplayFormat = ',0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4173
        mmLeft = 79111
        mmTop = 590
        mmWidth = 25991
        BandType = 4
        LayerName = Foreground1
      end
      object ppDBText14: TppDBText
        UserName = 'DBText5'
        DataField = 'total_harga'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4172
        mmLeft = 106098
        mmTop = 590
        mmWidth = 28901
        BandType = 4
        LayerName = Foreground1
      end
      object ppDBText8: TppDBText
        UserName = 'DBText8'
        DataField = 'jumlah_obat_keluar'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 48683
        mmTop = 590
        mmWidth = 11050
        BandType = 4
        LayerName = Foreground1
      end
      object ppDBText9: TppDBText
        UserName = 'DBText9'
        DataField = 'satuan'
        DataPipeline = print_obat_keluar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 60854
        mmTop = 590
        mmWidth = 15719
        BandType = 4
        LayerName = Foreground1
      end
      object ppDBText10: TppDBText
        UserName = 'DBText10'
        DataField = 'nama_obat'
        DataPipeline = print_obat_keluar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 5292
        mmTop = 590
        mmWidth = 41926
        BandType = 4
        LayerName = Foreground1
      end
      object ppDBCalc3: TppDBCalc
        UserName = 'DBCalc1'
        DataPipeline = print_obat_keluar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 6
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4498
        mmLeft = 0
        mmTop = 529
        mmWidth = 4882
        BandType = 4
        LayerName = Foreground1
      end
    end
    object ppSummaryBand2: TppSummaryBand
      Background.Brush.Style = bsClear
      mmBottomOffset = 0
      mmHeight = 39952
      mmPrintPosition = 0
      object ppLabel31: TppLabel
        UserName = 'Label10'
        Caption = 'Total :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 106892
        mmTop = 3514
        mmWidth = 9525
        BandType = 7
        LayerName = Foreground1
      end
      object ppLabel34: TppLabel
        UserName = 'Label6'
        Caption = 'Terima Kasih Atas kunjungan anda ...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 1588
        mmTop = 34660
        mmWidth = 57679
        BandType = 7
        LayerName = Foreground1
      end
      object ppLine24: TppLine
        UserName = 'Line2'
        Position = lpBottom
        Weight = 0.750000000000000000
        mmHeight = 467
        mmLeft = 1512
        mmTop = 0
        mmWidth = 138051
        BandType = 7
        LayerName = Foreground1
      end
      object ppLabel13: TppLabel
        UserName = 'Label101'
        Caption = 'Cash :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 106363
        mmTop = 13494
        mmWidth = 10054
        BandType = 7
        LayerName = Foreground1
      end
      object ppDBText11: TppDBText
        UserName = 'DBText11'
        DataField = 'uang'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        VerticalAlignment = avCenter
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 107421
        mmTop = 13494
        mmWidth = 29369
        BandType = 7
        LayerName = Foreground1
      end
      object ppLabel14: TppLabel
        UserName = 'Label14'
        Caption = 'Kembalian :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 98161
        mmTop = 18785
        mmWidth = 18256
        BandType = 7
        LayerName = Foreground1
      end
      object ppDBText12: TppDBText
        UserName = 'DBText12'
        DataField = 'kembalian'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        VerticalAlignment = avCenter
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 107421
        mmTop = 18521
        mmWidth = 29369
        BandType = 7
        LayerName = Foreground1
      end
      object ppLabel15: TppLabel
        UserName = 'Label15'
        Caption = 'Biaya Listrik :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 8202
        mmTop = 3514
        mmWidth = 21167
        BandType = 7
        LayerName = Foreground1
      end
      object ppDBText15: TppDBText
        UserName = 'DBText15'
        DataField = 'biayaadmin'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        VerticalAlignment = avCenter
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 20902
        mmTop = 3514
        mmWidth = 29369
        BandType = 7
        LayerName = Foreground1
      end
      object ppDBCalc4: TppDBCalc
        UserName = 'DBCalc4'
        DataField = 'total_harga'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        VerticalAlignment = avCenter
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 116681
        mmTop = 3514
        mmWidth = 19579
        BandType = 7
        LayerName = Foreground1
      end
      object ppLabel16: TppLabel
        UserName = 'Label102'
        Caption = 'Total Bayar :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 96838
        mmTop = 8731
        mmWidth = 19579
        BandType = 7
        LayerName = Foreground1
      end
      object ppDBText16: TppDBText
        UserName = 'DBText16'
        DataField = 'tot_harga'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        VerticalAlignment = avCenter
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 107421
        mmTop = 8467
        mmWidth = 29369
        BandType = 7
        LayerName = Foreground1
      end
      object ppDBText17: TppDBText
        UserName = 'DBText17'
        DataField = 'biayaServis'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        VerticalAlignment = avCenter
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 20902
        mmTop = 8467
        mmWidth = 29369
        BandType = 7
        LayerName = Foreground1
      end
      object ppLabel17: TppLabel
        UserName = 'Label17'
        Caption = 'Biaya Servis :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 8467
        mmTop = 8467
        mmWidth = 21167
        BandType = 7
        LayerName = Foreground1
      end
      object ppLabel32: TppLabel
        UserName = 'Label32'
        Caption = 'Biaya Racikan :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 5292
        mmTop = 13378
        mmWidth = 24342
        BandType = 7
        LayerName = Foreground1
      end
      object ppDBCalc2: TppDBCalc
        UserName = 'DBCalc2'
        DataField = 'biayaRacik'
        DataPipeline = print_obat_keluar
        DisplayFormat = ', 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        VerticalAlignment = avCenter
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 30692
        mmTop = 13494
        mmWidth = 19579
        BandType = 7
        LayerName = Foreground1
      end
      object ppLabel33: TppLabel
        UserName = 'Label33'
        Caption = 'Admin : '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 1588
        mmTop = 29369
        mmWidth = 12964
        BandType = 7
        LayerName = Foreground1
      end
      object ppDBText23: TppDBText
        UserName = 'DBText102'
        DataField = 'nama_obat'
        DataPipeline = print_obat_keluar
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'print_obat_keluar'
        mmHeight = 4233
        mmLeft = 15875
        mmTop = 29369
        mmWidth = 41804
        BandType = 7
        LayerName = Foreground1
      end
    end
    object ppDesignLayers2: TppDesignLayers
      object ppDesignLayer2: TppDesignLayer
        UserName = 'Foreground1'
        LayerType = ltBanded
        Index = 0
      end
    end
    object ppParameterList2: TppParameterList
    end
  end
  object print_obat_keluar: TppDBPipeline
    DataSource = ds_obat_keluar_print
    UserName = 'print_obat_keluar'
    Left = 88
    Top = 768
    object print_obat_keluarppField1: TppField
      FieldAlias = 'no_nota'
      FieldName = 'no_nota'
      FieldLength = 50
      DisplayWidth = 50
      Position = 0
    end
    object print_obat_keluarppField2: TppField
      FieldAlias = 'nama_obat'
      FieldName = 'nama_obat'
      FieldLength = 100
      DisplayWidth = 100
      Position = 1
    end
    object print_obat_keluarppField3: TppField
      Alignment = taRightJustify
      FieldAlias = 'jumlah_obat_keluar'
      FieldName = 'jumlah_obat_keluar'
      FieldLength = 0
      DataType = dtDouble
      DisplayWidth = 10
      Position = 2
    end
    object print_obat_keluarppField4: TppField
      FieldAlias = 'satuan'
      FieldName = 'satuan'
      FieldLength = 15
      DisplayWidth = 15
      Position = 3
    end
    object print_obat_keluarppField5: TppField
      Alignment = taRightJustify
      FieldAlias = 'harga_satuan'
      FieldName = 'harga_satuan'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 4
    end
    object print_obat_keluarppField6: TppField
      Alignment = taRightJustify
      FieldAlias = 'biayaPerRacikan'
      FieldName = 'biayaPerRacikan'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 5
    end
    object print_obat_keluarppField7: TppField
      Alignment = taRightJustify
      FieldAlias = 'total_harga'
      FieldName = 'total_harga'
      FieldLength = 0
      DataType = dtDouble
      DisplayWidth = 10
      Position = 6
    end
    object print_obat_keluarppField8: TppField
      Alignment = taRightJustify
      FieldAlias = 'uang'
      FieldName = 'uang'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 7
    end
    object print_obat_keluarppField9: TppField
      FieldAlias = 'kembalian'
      FieldName = 'kembalian'
      FieldLength = 0
      DataType = dtLargeInt
      DisplayWidth = 15
      Position = 8
    end
    object print_obat_keluarppField10: TppField
      Alignment = taRightJustify
      FieldAlias = 'biaya_listrik'
      FieldName = 'biaya_listrik'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 9
    end
    object print_obat_keluarppField11: TppField
      Alignment = taRightJustify
      FieldAlias = 'biayaadmin'
      FieldName = 'biayaadmin'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 10
    end
    object print_obat_keluarppField12: TppField
      Alignment = taRightJustify
      FieldAlias = 'tot_harga'
      FieldName = 'tot_harga'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 11
    end
    object print_obat_keluarppField13: TppField
      FieldAlias = 'biayaRacik'
      FieldName = 'biayaRacik'
      FieldLength = 0
      DataType = dtLargeInt
      DisplayWidth = 15
      Position = 12
    end
    object print_obat_keluarppField14: TppField
      Alignment = taRightJustify
      FieldAlias = 'biayaServis'
      FieldName = 'biayaServis'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 13
    end
  end
  object q_obat_keluar_print: TMyQuery
    Connection = dm.my_connection
    SQL.Strings = (
      'CALL `sp_cetak_nota`('#39'N070819922302'#39',1,1000)')
    Active = True
    Left = 80
    Top = 632
  end
  object ds_obat_keluar_print: TMyDataSource
    DataSet = q_obat_keluar_print
    Left = 80
    Top = 680
  end
  object view_dokter: TcxGridViewRepository
    Left = 92
    Top = 112
    object view_dokterDBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dm.ds_dokter
      DataController.Filter.Active = True
      DataController.Filter.TranslateBetween = True
      DataController.KeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.GroupByBox = False
      object view_dokterDBTableView1nm_dokter: TcxGridDBColumn
        Caption = 'Nama Dokter'
        DataBinding.FieldName = 'nm_dokter'
        Options.GroupFooters = False
        Options.Grouping = False
        Width = 1000
      end
    end
  end
  object jenis_racikan: TDataSetProvider
    DataSet = dm.q_jenisRacikan
    Left = 576
    Top = 696
  end
  object dsjenisracikan: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    ProviderName = 'jenis_racikan'
    AfterPost = dsjenisracikanAfterPost
    Left = 552
    Top = 632
    object dsjenisracikanid: TIntegerField
      FieldName = 'id'
      Origin = 't_jenis_racikan.id'
    end
    object dsjenisracikandiskripsi: TStringField
      FieldName = 'diskripsi'
      Origin = 't_jenis_racikan.diskripsi'
      Size = 50
    end
    object dsjenisracikanbiaya: TIntegerField
      FieldName = 'biaya'
      Origin = 't_jenis_racikan.biaya'
    end
    object dsjenisracikanColumna: TIntegerField
      FieldName = 'Column a'
      Origin = 't_jenis_racikan.Column a'
    end
    object dsjenisracikanColumnb: TIntegerField
      FieldName = 'Column b'
      Origin = 't_jenis_racikan.Column b'
    end
  end
  object ds_jenisracikan: TDataSource
    DataSet = dsjenisracikan
    Left = 384
    Top = 648
  end
  object q_notadetail: TMyQuery
    Connection = dm.my_connection
    SQL.Strings = (
      'CALL `sp_cetak_nota`('#39'N070819922302'#39',1,1000)')
    Active = True
    Left = 136
    Top = 176
  end
  object ds_notadetail: TMyDataSource
    DataSet = q_notadetail
    Left = 136
    Top = 224
  end
  object pmKoreksiHarga: TPopupMenu
    Left = 1777
    Top = 224
    object K1: TMenuItem
      Caption = 'Koreksi Harga'
      OnClick = K1Click
    end
  end
end
