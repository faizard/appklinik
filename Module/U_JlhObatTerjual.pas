unit U_JlhObatTerjual;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sComboBox, sEdit, sLabel, ExtCtrls, sPanel, sCheckBox,
  Buttons, sBitBtn, ActnList, XPStyleActnCtrls, ActnMan, sGroupBox,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinsDefaultPainters, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBExtLookupComboBox, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid;

type
  Tfrm_jlh_obat_keluar = class(TForm)
    sPanel1: TsPanel;
    lbl_11: TsLabel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sLabel8: TsLabel;
    edt_nm_obat: TsEdit;
    edt_stok: TsEdit;
    edt_hrgajual: TsEdit;
    sPanel2: TsPanel;
    cbb_jenis: TsComboBox;
    sPanel3: TsPanel;
    sLabel4: TsLabel;
    edt_1: TsEdit;
    actmgr1: TActionManager;
    act1: TAction;
    sPanel4: TsPanel;
    view_jenisracikan: TcxGridViewRepository;
    view_jenisracikanDBTableView1: TcxGridDBTableView;
    view_jenisracikanDBTableView1diskripsi: TcxGridDBColumn;
    view_jenisracikanDBTableView1biaya: TcxGridDBColumn;
    chk_racikan: TsCheckBox;
    img3: TImage;
    sPanel5: TsPanel;
    lbl_16: TsLabel;
    sLabel3: TsLabel;
    sLabel11: TsLabel;
    sLabel12: TsLabel;
    edt_jlh_obat: TsEdit;
    cbb_satuan: TsComboBox;
    btn_obat_simpan: TsBitBtn;
    btn_obat_client1: TsBitBtn;
    sPanel6: TsPanel;
    lbl1: TLabel;
    sLabel5: TsLabel;
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    sLabel9: TsLabel;
    sLabel10: TsLabel;
    edt_jenisracikan: TcxExtLookupComboBox;
    cbb_racikanKe: TsComboBox;
    edt_jlhracikan: TsEdit;
    edt_2: TsEdit;
    sLabel13: TsLabel;
    procedure FormShow(Sender: TObject);
    procedure chk_racikanClick(Sender: TObject);
    procedure btn_obat_client1Click(Sender: TObject);
    procedure btn_obat_simpanClick(Sender: TObject);
    procedure edt_jlh_obatKeyPress(Sender: TObject; var Key: Char);
    procedure chk_potClick(Sender: TObject);
    procedure chk_puyerClick(Sender: TObject);
    procedure cbb_racikanKeExit(Sender: TObject);
    function cekRacikanBaru():Boolean;
    procedure edt_jlh_obatChange(Sender: TObject);
    procedure edt_2Change(Sender: TObject);
    procedure cekharga();

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_jlh_obat_keluar: Tfrm_jlh_obat_keluar;
  racik : String[1];

implementation

uses U_DM, u_bantu;

{$R *.dfm}




function Tfrm_jlh_obat_keluar.cekRacikanBaru():Boolean;
begin

//  ShowMessage(IntToStr(racikanKe) +enter+jenisRacikan);
//    if
//    (temp_jlh_sub_racik <> HapusFormat(edt_jlhracikan))
//    or
//    (racikanKe <> StrToInt(cbb_racikanKe.Text))
//    or ( jenisRacikan <> dm.q_jenisRacikandiskripsi.Text)
//    or (racikanKe=0) or (jenisRacikan='') then
//    begin
//      Result := true;
//      temp_racikanBaru:= true;
//    end;

    temp_racikanBaru:= true;


end;

procedure Tfrm_jlh_obat_keluar.FormShow(Sender: TObject);
begin
//    ShowMessage(oldIdJenis);
//    edt_nm_dokter.Properties.View := nil;
//     if oldIdJenis <>'' then
//     begin
//      chk_racikan.Checked:= True;
//      dm.q_jenisRacikan.Locate('id',oldIdJenis,[loCaseInsensitive,loPartialKey]);
//     end
//      else
//      begin
//          chk_racikan.Checked:= False;
//          edt_nm_dokter.Properties.View := view_jenisracikanDBTableView1;
//
//      end;
//
    temp_racikanBaru:= False;
    
    save_temp := false;
    dm.q_jenisRacikan.Close;
    dm.q_jenisRacikan.Open;

    racik:='0';
    id_jenis_racikkkk := '0';
    dm.q_jenisRacikan.First;
    edt_jlh_obat.Clear;
    edt_nm_obat.Text := dm.q_obatnm_obat.Text;
    edt_hrgajual.Text := FormatFloat(',#',dm.q_obathrga_jual.AsInteger);
    edt_stok.Text := dm.q_obatquantity.Text +' '+dm.q_obatsatuan.Text;
    edt_1.Text:=  dm.q_obatid.Text;
    cbb_satuan.Clear;
    cbb_satuan.Items.Add(dm.q_obatsatuan.Text);
    cbb_satuan.ItemIndex:=0;
    cbb_jenis.Text:= dm.q_obatjenis.Text;

    chk_racikan.Checked:= False;
    edt_jenisracikan.Visible := False;
//
//      if UpperCase(dm.q_obatjenis.Text) = cream then
//        type_boolean := True
//          else
//            type_boolean:= False;

//    chk_racikan.Enabled := type_boolean;
    chk_racikan.Checked := true;

//    chk_racikanClick

//    edt_jlhracikan.Text:= IntToStr(jlhPerRacik);
    if temp_jlh_sub_racik = 0 then  edt_jlhracikan.Text:='0' else edt_jlhracikan.Text:= IntToStr(temp_jlh_sub_racik);
   if racikanKe =0 then  cbb_racikanKe.Text:='0' else cbb_racikanKe.Text:= IntToStr(racikanKe);



//    chk_pot.Checked:= False;
//    chk_puyer.Checked:= False;
    jenisPot:='';
    jenisPuyer:='';
    edt_jlhracikan.Enabled:= false;
    edt_jenisracikan.Enabled:= false;

    getComboQuery(cbb_satuan,'satuan','CALL `getSatuan`("'+dm.q_obatid.Text+'")');
    if jenisRacikan <> '' then  GetSQL('SELECT * FROM t_jenis_racikan where diskripsi = "'+jenisRacikan+'"',dm.q_jenisRacikan);
    edt_jlh_obat.SetFocus;
    chk_racikanClick(Sender);

end;


procedure Tfrm_jlh_obat_keluar.chk_racikanClick(Sender: TObject);
begin
  cbb_satuan.Clear;
  if  chk_racikan.Checked then
  begin
       cbb_satuan.Items.Add(dm.q_obatsatuan_racik.AsString);
       racik :='1';
       dm.q_jenisRacikan.Open;
       edt_jenisracikan.Visible := True;

       sPanel6.Show;
       edt_jenisracikan.ItemIndex:=0;

  end
    else
    begin
      sPanel6.Hide;
      getComboQuery(cbb_satuan,'satuan','CALL `getSatuan`("'+dm.q_obatid.Text+'")');
//      cbb_satuan.Items.Add(dm.q_obatsatuan.Text);
      racik :='0';
      dm.q_jenisRacikan.Close;
      edt_jenisracikan.Visible := False;
    end;

    cbb_satuan.ItemIndex:=0;
    edt_jlh_obat.SetFocus;
    dm.q_jenisRacikan.Open;
    

end;

procedure Tfrm_jlh_obat_keluar.btn_obat_client1Click(Sender: TObject);
begin
     save_temp := False;
    Close;
end;

procedure Tfrm_jlh_obat_keluar.cekharga();
var
  hrgperracik : Integer;
begin
  temp_jlhracik:=0;
  temp_tot_hargaJual :=0;
  temp_jlh:=0;
  hrgperracik:=0;

  if ( (edt_jenisracikan.Text = '-') or (edt_jlh_obat.Text = '') or (edt_jlhracikan.Text = '') or (edt_jlhracikan.Text = '0')) and chk_racikan.Checked then
  begin
    MessageError('Lengkapi data .!');
    save_temp:= False;
    Exit;
  end;

  oldIdJenis := dm.q_jenisRacikanid.Text;

//  dm.q_jenisRacikan.Locate('')
//   ShowMessage(racik);
   case cbb_satuan.ItemIndex of
     0: temp_jlh:= StrToFloat(edt_jlh_obat.Text); //HapusFormat(edt_jlh_obat);
     1: temp_jlh:= HapusFormat(edt_jlh_obat) * dm.q_obatjlhper_pack.AsInteger;
    else
           temp_jlh:= HapusFormat(edt_jlh_obat);
   end;

     if  chk_racikan.Checked then racik := '12' else racik :='0';
     if not CekStokObat( temp_jlh ,dm.q_obatid.AsString,racik) then Exit;



    if chk_racikan.Checked then
    begin
      hrgperracik:= Round(dm.q_obathrga_jual.AsInteger / dm.q_obatjlhper_raciik.AsInteger);
      temp_jlhracik := StrToFloat(edt_jlh_obat.Text); //HapusFormat(edt_jlh_obat);
//      ShowMessage( IntToStr(hrgperracik) +' * '+ FloatToStr(temp_jlhracik) );
      temp_tot_hargaJual := hrgperracik * temp_jlhracik;

      edt_2.Text:= CurrToStr(temp_tot_hargaJual);




//      ShowMessage( CurrToStr(temp_tot_hargaJual) );


//      dm.q_jenisRacikan.Locate('id',oldIdJenis,[loCaseInsensitive,loPartialKey]);
      jenisRacikan := dm.q_jenisRacikandiskripsi.Text;
      id_jenis_racikkkk :=  dm.q_jenisRacikanid.Text;
      temp_racikan:=1;
      temp_jlh :=0;
      jlhPerRacik:= HapusFormat(edt_jlhracikan);
      racikanKe :=StrToInt(cbb_racikanKe.Text);
      temp_biayaListrik := getBiayaListrik;
    end
      else
      begin
          //        temp_jlh :=HapusFormat(edt_jlh_obat);
          temp_tot_hargaJual := dm.q_obathrga_jual.AsInteger * temp_jlh;
          temp_racikan:=0;
          jenisRacikan:='-';
          jlhPerRacik:= 0;
          racikanKe :=0;

      end;

//    if radioG_puyer.ItemIndex >=0 then
//    begin
//       if temp_biayaPuyer >=0 then
//        temp_biayaPuyer := getBiayaPuyer(radioG_puyer.ItemIndex + 1);
//
//    end;
//
//    if radioG_pot.ItemIndex >=0 then
//    begin
//       case radioG_puyer.ItemIndex of
//           0:
//       end;
//       if temp_biayaPot >=0 then
//        temp_biayaPot := getBiayaPot(radioG_pot.ItemIndex + 1);
//    end;

     cekRacikanBaru();

//    id_jenis_racikkkk := dm.q_jenisRacikanid.Text;


//    jenisRacikan := edt_jenisracikan.Text;

    temp_jlh_sub_racik := jlhPerRacik;


end;

procedure Tfrm_jlh_obat_keluar.btn_obat_simpanClick(Sender: TObject);
begin
    if ( (edt_jenisracikan.Text = '-') or (edt_jlh_obat.Text = '') or (edt_jlhracikan.Text = '') or (edt_jlhracikan.Text = '0')) and chk_racikan.Checked then
    begin
      MessageError('Lengkapi data .!');
      save_temp:= False;
    end
      else
        save_temp:= true;


    Close;
end;

procedure Tfrm_jlh_obat_keluar.edt_jlh_obatKeyPress(Sender: TObject;
  var Key: Char);
begin

 // #8 is Backspace
  if not (Key in [#8,',','.', '0'..'9']) then begin
    Key := #0;
    Exit;
  end;
end;

procedure Tfrm_jlh_obat_keluar.chk_potClick(Sender: TObject);
begin
//
//    if     chk_pot.Checked then
//    begin
//      radioG_pot.Enabled := True;
//      chk_puyer.Checked := False;
////      radioG_puyer.ItemIndex:=-1;
//    end
//
//        else
//        begin
//          radioG_pot.ItemIndex:=-1;
//          radioG_pot.Enabled:= False;
//        end;

    
end;

procedure Tfrm_jlh_obat_keluar.chk_puyerClick(Sender: TObject);
begin
//    if     chk_puyer.Checked then
//    begin
//      radioG_puyer.Enabled := True;
//      chk_pot.Checked := False;
////      radioG_puyer.ItemIndex:=-1;
//    end
//
//        else
//        begin
//          radioG_puyer.ItemIndex:=-1;
//          radioG_puyer.Enabled:= False;
//        end;

end;

procedure Tfrm_jlh_obat_keluar.cbb_racikanKeExit(Sender: TObject);
begin
    GetSQL('SELECT * FROM t_jenis_racikan',dm.q_jenisRacikan);
    edt_jlhracikan.Enabled:= True;
    edt_jenisracikan.Enabled:= True;
end;

procedure Tfrm_jlh_obat_keluar.edt_jlh_obatChange(Sender: TObject);
begin
    if edt_jlh_obat.Text ='' then Exit;
    cekharga();
end;

procedure Tfrm_jlh_obat_keluar.edt_2Change(Sender: TObject);
begin
    if edt_2.Text = '' then Exit;
    temp_tot_hargaJual := StrToCurr( edt_2.Text );
end;

end.
