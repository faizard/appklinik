unit U_data_obat_keluar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, DB,
  DBAccess, MyAccess, MemDS, ComCtrls, acProgressBar, ExtCtrls, StdCtrls,
  Buttons, sBitBtn, ToolWin, sToolBar, sPanel, GridsEh, DBAxisGridsEh,
  DBGridEh, ppCtrls, ppBands, ppDB, ppPrnabl, ppClass, ppCache,
  ppDesignLayer, ppParameter, ppDBPipe, ppComm, ppRelatv, ppProd, ppReport,
  ppVar, Mask, sMaskEdit, sCustomComboEdit, sToolEdit, sLabel, PrnDbgeh,      u_Thread,
  Grids, DBGrids, sComboBox, acPNG, acImage;

type
  Tfrm_obat_keluar = class(TForm)
    sToolBar1: TsToolBar;
    btn1: TToolButton;
    img2: TImage;
    sprog_export_obatmasuk: TsProgressBar;
    ds_obat_keluar: TMyDataSource;
    dlgSavedoalog: TSaveDialog;
    ppReport_obat_terjual: TppReport;
    ppDBPipeline1: TppDBPipeline;
    ppParameterList1: TppParameterList;
    pnl_2: TsPanel;
    btn_1: TsBitBtn;
    btn_refresh: TsBitBtn;
    btn_5: TsBitBtn;
    pnl_priode: TsPanel;
    lbl_1: TsLabel;
    lbl_2: TsLabel;
    sDateEdit1: TsDateEdit;
    sDateEdit2: TsDateEdit;
    btn_2: TsBitBtn;
    ds_obat_keluar_print: TMyDataSource;
    q_obat_keluar_print: TMyQuery;
    PrintDBGridEh1: TPrintDBGridEh;
    btn_4: TsBitBtn;
    sPanel1: TsPanel;
    lbl_3: TLabel;
    sPanel2: TsPanel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    stglAwal: TsDateEdit;
    stglAkhir: TsDateEdit;
    q_obat_keluar: TMyQuery;
    DBGridEh2: TDBGridEh;
    cbb_jenis: TsComboBox;
    sLabel3: TsLabel;
    DBGridEh3: TDBGridEh;
    sPanel3: TsPanel;
    sImage1: TsImage;
    sLabel4: TsLabel;
    sLabel5: TsLabel;
    sPanel4: TsPanel;
    report__Print_obat_keluar: TppReport;
    ppHeaderBand2: TppHeaderBand;
    ppLabel8: TppLabel;
    ppLabel25: TppLabel;
    ppSystemVariable4: TppSystemVariable;
    ppLabel26: TppLabel;
    ppLabel27: TppLabel;
    ppLabel28: TppLabel;
    ppLabel29: TppLabel;
    ppLabel30: TppLabel;
    ppLabel31: TppLabel;
    ppLine17: TppLine;
    ppLabel32: TppLabel;
    ppDBText21: TppDBText;
    ppDetailBand2: TppDetailBand;
    ppDBText14: TppDBText;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    ppDBText17: TppDBText;
    ppDBText18: TppDBText;
    ppSummaryBand2: TppSummaryBand;
    ppLabel33: TppLabel;
    ppLabel34: TppLabel;
    ppLine29: TppLine;
    ppLabel35: TppLabel;
    ppDBText19: TppDBText;
    ppLabel36: TppLabel;
    ppDBText20: TppDBText;
    ppLabel37: TppLabel;
    ppDBText22: TppDBText;
    ppDBCalc4: TppDBCalc;
    ppLabel38: TppLabel;
    ppDBText23: TppDBText;
    ppDBText24: TppDBText;
    ppLabel39: TppLabel;
    ppLabel40: TppLabel;
    ppDBCalc2: TppDBCalc;
    ppDesignLayers2: TppDesignLayers;
    ppDesignLayer2: TppDesignLayer;
    ppParameterList2: TppParameterList;
    print_obat_keluar: TppDBPipeline;
    ds_PrintNota: TMyDataSource;
    q_printNota: TMyQuery;
    ppHeaderBand1: TppHeaderBand;
    ppShape1: TppShape;
    ppLabel1: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppLabel12: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppLine45: TppLine;
    ppLine46: TppLine;
    ppLine47: TppLine;
    ppLine48: TppLine;
    ppLine49: TppLine;
    ppLine50: TppLine;
    ppLine51: TppLine;
    ppLine52: TppLine;
    ppLine53: TppLine;
    ppLine54: TppLine;
    ppLine55: TppLine;
    ppLine56: TppLine;
    ppLabel2: TppLabel;
    ppLabel16: TppLabel;
    ppLine57: TppLine;
    ppSystemVariable1: TppSystemVariable;
    ppLabel17: TppLabel;
    ppLine8: TppLine;
    ppDetailBand1: TppDetailBand;
    ppDBText3: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBCalc1: TppDBCalc;
    ppDBText8: TppDBText;
    ppLine6: TppLine;
    ppLine11: TppLine;
    ppLine16: TppLine;
    ppLine18: TppLine;
    ppLine21: TppLine;
    ppLine24: TppLine;
    ppLine25: TppLine;
    ppLine31: TppLine;
    ppLine33: TppLine;
    ppDBText26: TppDBText;
    ppLine37: TppLine;
    ppLine36: TppLine;
    ppLine43: TppLine;
    ppLine42: TppLine;
    ppLine58: TppLine;
    ppFooterBand1: TppFooterBand;
    ppSummaryBand1: TppSummaryBand;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppDBText1: TppDBText;
    ppDBText9: TppDBText;
    ppDBText7: TppDBText;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppDBText25: TppDBText;
    ppLine4: TppLine;
    ppLine10: TppLine;
    ppLine14: TppLine;
    ppLine12: TppLine;
    ppLine19: TppLine;
    ppLine22: TppLine;
    ppLine27: TppLine;
    ppLine28: TppLine;
    ppLine39: TppLine;
    ppLine40: TppLine;
    ppLine1: TppLine;
    ppDBText4: TppDBText;
    ppLine7: TppLine;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppDBText2: TppDBText;
    ppLine2: TppLine;
    ppLine5: TppLine;
    ppLine9: TppLine;
    ppLine15: TppLine;
    ppLine13: TppLine;
    ppLine20: TppLine;
    ppLine23: TppLine;
    ppLine26: TppLine;
    ppLine30: TppLine;
    ppLine38: TppLine;
    ppLine41: TppLine;
    ppLine44: TppLine;
    ppLine34: TppLine;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppDesignLayers1: TppDesignLayers;
    ppDesignLayer1: TppDesignLayer;
    ppLine59: TppLine;
    ppLine61: TppLine;
    ppLine62: TppLine;
    sBitBtn1: TsBitBtn;
    ppShape2: TppShape;
    ppLine3: TppLine;
    ppLine60: TppLine;
    ppLine63: TppLine;
    ppLine64: TppLine;
    ppLine65: TppLine;
    ppLine66: TppLine;
    ppLine35: TppLine;
    ppLine32: TppLine;
    ppLabel_ppn: TppLabel;
    ppLabel_dokter: TppLabel;
    ppLabel_racikan: TppLabel;
    ppLabel_servis: TppLabel;
    ppDBCalc3: TppDBCalc;
    ppLabel_total_obat: TppLabel;
    ppLabel_listrik: TppLabel;
    ppLabel_total_harga: TppLabel;
    q_obat_keluarid: TLargeintField;
    q_obat_keluarnm_obat: TStringField;
    q_obat_keluarquantity: TFloatField;
    q_obat_keluarsatuan: TStringField;
    q_obat_keluarquantity_racik: TFloatField;
    q_obat_keluarjlhper_raciik: TIntegerField;
    q_obat_keluarsatuan_racik: TStringField;
    q_obat_keluarexpire_date: TDateField;
    q_obat_keluartgl_jatuh_tempo: TDateField;
    q_obat_keluarhrgper_racik: TIntegerField;
    q_obat_keluarhrga_jual: TIntegerField;
    q_obat_keluarjenis: TStringField;
    q_obat_keluarjlhper_pack: TIntegerField;
    q_obat_keluarsatuan_pack: TStringField;
    q_obat_keluarhrgper_pack: TIntegerField;
    q_obat_keluarket: TMemoField;
    q_obat_keluarnorec: TLargeintField;
    q_obat_keluarno_nota: TStringField;
    q_obat_keluarnama_pengunjung: TStringField;
    q_obat_keluarid_pasien: TLargeintField;
    q_obat_keluartgl_obat_keluar: TDateTimeField;
    q_obat_keluarid_obat: TLargeintField;
    q_obat_keluarjumlah_obat_keluar: TFloatField;
    q_obat_keluarjlh_sub_racikan: TSmallintField;
    q_obat_keluarbiaya_listrik: TIntegerField;
    q_obat_keluarbiaya_admin: TIntegerField;
    q_obat_keluarnm_dokter: TStringField;
    q_obat_keluartot_harga: TIntegerField;
    q_obat_keluaruang: TIntegerField;
    q_obat_keluarkembalian: TIntegerField;
    q_obat_keluarinsert_at: TDateTimeField;
    q_obat_keluarracikan: TStringField;
    q_obat_keluarbiaya_dokter: TIntegerField;
    q_obat_keluarid_jenis_racikan: TIntegerField;
    q_obat_keluarid_user: TIntegerField;
    q_obat_keluarhrgajualbersih: TIntegerField;
    q_obat_keluarracikan1: TStringField;
    q_obat_keluarid_jenis_racikan1: TIntegerField;
    q_obat_keluaradaRacikan: TStringField;
    q_obat_keluardiskripsi: TStringField;
    q_obat_keluarbiaya: TIntegerField;
    q_obat_keluarusername: TStringField;
    q_obat_keluarbiayaServis: TIntegerField;
    q_obat_keluartotalBersih: TLargeintField;
    q_obat_keluarppn: TFloatField;
    q_obat_keluarbiaya_racikan: TLargeintField;
    procedure btn_3Click(Sender: TObject);
    procedure btn_1Click(Sender: TObject);
    procedure btn_2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btn_4Click(Sender: TObject);
    procedure stglAwalChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btn_refreshClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbb_jenisChange(Sender: TObject);
    procedure DBGridEh2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure btn_5Click(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
  private
              XlApp, XlBook, XlSheet, XlSheets, Range,chat : Variant; // Excel 97
      WApp, Word : Variant; // Word 97
       thred: ThreadSql;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_obat_keluar: Tfrm_obat_keluar;

implementation

uses ComObj, StrUtils, U_DM, DateUtils, u_bantu, U_ObatKeluar;

{$R *.dfm}

procedure Tfrm_obat_keluar.btn_3Click(Sender: TObject);
begin
    Close;
end;



procedure Tfrm_obat_keluar.btn_1Click(Sender: TObject);
begin
    CreateADO;
    ado.SQL.Text :='SELECT SUM(ppn) AS ppn FROM ( SELECT ppn FROM v_obat_keluar WHERE tgl_obat_keluar BETWEEN "'+FormatDateTime('yyyy-mm-dd',stglAwal.Date)
      +'" AND "'+FormatDateTime('yyyy-mm-dd',stglAkhir.Date)+'" GROUP by no_nota  ) as f';

    ado.Open;
    ppLabel_ppn.Caption:= FormatFloat('#,0',ado.fieldByName('ppn').AsInteger);


    ado.SQL.Text :='SELECT SUM(biaya_dokter) AS biaya_dokter FROM ( SELECT biaya_dokter FROM v_obat_keluar WHERE tgl_obat_keluar BETWEEN "'+FormatDateTime('yyyy-mm-dd',stglAwal.Date)
      +'" AND "'+FormatDateTime('yyyy-mm-dd',stglAkhir.Date)+'" GROUP by no_nota  ) as f';
    ado.Open;
    ppLabel_dokter.Caption:= FormatFloat('#,0',ado.fieldByName('biaya_dokter').AsInteger);

    ado.SQL.Text :='SELECT SUM(biaya_racikan) AS biaya_racikan FROM ( SELECT biaya_racikan FROM v_obat_keluar WHERE tgl_obat_keluar BETWEEN "'+FormatDateTime('yyyy-mm-dd',stglAwal.Date)
      +'" AND "'+FormatDateTime('yyyy-mm-dd',stglAkhir.Date)+'" GROUP by no_nota  ) as f';
    ado.Open;
    ppLabel_racikan.Caption:= FormatFloat('#,0',ado.fieldByName('biaya_racikan').AsInteger);


    ado.SQL.Text :='SELECT SUM(biayaServis) AS biayaServis FROM ( SELECT biayaServis FROM v_obat_keluar WHERE tgl_obat_keluar BETWEEN "'+FormatDateTime('yyyy-mm-dd',stglAwal.Date)
      +'" AND "'+FormatDateTime('yyyy-mm-dd',stglAkhir.Date)+'" GROUP by no_nota  ) as f';
    ado.Open;
    ppLabel_servis.Caption:= FormatFloat('#,0',ado.fieldByName('biayaServis').AsInteger);

    ado.SQL.Text :='SELECT SUM(totalBersih) AS totalBersih FROM ( SELECT totalBersih FROM v_obat_keluar WHERE tgl_obat_keluar BETWEEN "'+FormatDateTime('yyyy-mm-dd',stglAwal.Date)
      +'" AND "'+FormatDateTime('yyyy-mm-dd',stglAkhir.Date)+'" GROUP by no_nota  ) as f';
    ado.Open;
    ppLabel_total_obat.Caption:= FormatFloat('#,0',ado.fieldByName('totalBersih').AsInteger);

    ado.SQL.Text :='SELECT SUM(biaya_listrik) AS biaya_listrik FROM ( SELECT biaya_listrik FROM v_obat_keluar WHERE tgl_obat_keluar BETWEEN "'+FormatDateTime('yyyy-mm-dd',stglAwal.Date)
      +'" AND "'+FormatDateTime('yyyy-mm-dd',stglAkhir.Date)+'" GROUP by no_nota  ) as f';
    ado.Open;
    ppLabel_listrik.Caption:= FormatFloat('#,0',ado.fieldByName('biaya_listrik').AsInteger);


    ado.SQL.Text :='SELECT SUM(tot_harga) AS tot_harga FROM ( SELECT tot_harga FROM v_obat_keluar WHERE tgl_obat_keluar BETWEEN "'+FormatDateTime('yyyy-mm-dd',stglAwal.Date)
      +'" AND "'+FormatDateTime('yyyy-mm-dd',stglAkhir.Date)+'" GROUP by no_nota  ) as f';
    ado.Open;
    ppLabel_total_harga.Caption:= FormatFloat('#,0',ado.fieldByName('tot_harga').AsInteger);


//   biaya_racikan    tot_harga
//                           biaya_listrik

    ppReport_obat_terjual.Print;
end;

procedure Tfrm_obat_keluar.btn_2Click(Sender: TObject);
var
  tgl_awal, tgl_akhir : ShortString;
begin
  tgl_awal:= FormatDateTime('yyyy-mm-dd', sDateEdit1.Date);
  tgl_akhir:= FormatDateTime('yyyy-mm-dd', sDateEdit2.Date);

  q_obat_keluar_print.SQL.Text:= 'SELECT * FROM v_obat_keluar WHERE  tgl_obat_keluar BETWEEN "'+tgl_awal+'00:00:00" AND "'+tgl_akhir+' 23:59:00"' ;
  q_obat_keluar_print.Open;

  if q_obat_keluar_print.IsEmpty then
  begin
    Application.MessageBox('Data obat keluar tidak ditemukan ...!','Ups',MB_ICONSTOP);
    btn_refreshClick(Sender);
    Exit;
  end;
//    ppLabel_priode.Caption:= sDateEdit1.Text +'   s/d   '+ sDateEdit2.Text;
     pnl_priode.Hide;
    ppReport_obat_terjual.Print;
end;



procedure Tfrm_obat_keluar.FormShow(Sender: TObject);
begin
    btn_refreshClick(Sender);
end;

procedure Tfrm_obat_keluar.btn_4Click(Sender: TObject);
begin
     PrintDBGridEh1.Preview;
end;

procedure Tfrm_obat_keluar.stglAwalChange(Sender: TObject);
begin
      btn_refreshClick(Sender);
end;

procedure Tfrm_obat_keluar.FormCreate(Sender: TObject);
begin
        stglAwal.Date:=StartOfTheMonth(Now);
        stglAkhir.Date:=Now();
        WindowState:= wsMaximized;
end;

procedure Tfrm_obat_keluar.btn_refreshClick(Sender: TObject);
var
  tglAwal, tglAkhir,jenis  : String[50];
begin
  tglAwal := FormatDateTime('yyyy-mm-dd', stglAwal.Date);
   tglAkhir := FormatDateTime('yyyy-mm-dd', stglAkhir.Date);

   string2:= 'SELECT * FROM v_obat_keluar WHERE v_obat_keluar.tgl_obat_keluar BETWEEN "'+tglAwal+'" AND "'+tglAkhir+'"';

         case cbb_jenis.ItemIndex of
            1: string2 :=string2+ ' and jenis = "'+cream+'"';
            2: string2 :=string2 +' and jenis = "'+nonCream+'"';
         end;
       cbb_jenis.ItemIndex:=0;  
      GetSQL(string2 ,q_obat_keluar);
      GetSQL(string2 ,q_obat_keluar_print);

end;

procedure Tfrm_obat_keluar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
      Free;
end;

procedure Tfrm_obat_keluar.cbb_jenisChange(Sender: TObject);
begin
    btn_refreshClick(Sender);
end;

procedure Tfrm_obat_keluar.DBGridEh2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin

//    if q_obat_keluar.FieldByName('racikan').AsInteger =1  then
//    begin
//         DBGridEh2.Canvas.Brush.Color:=$00FDF8E3;
//    end;
end;

procedure Tfrm_obat_keluar.btn_5Click(Sender: TObject);
Var i,x:integer;
sfile:string;
begin
try
    sprog_export_obatmasuk.Show;

    if dlgSavedoalog.Execute then
    begin

        sprog_export_obatmasuk.Position:=0;
        XlApp  := CreateOleObject('Excel.Application');
        XlBook := XlApp.WorkBooks.Add;
        XlSheet  := XlBook.worksheets.add;
        DBGridEh2.Hide;
        XlSheet.cells[1,1].value := 'DATA OBAT KELUAR';
       for i:=0 to DBGridEh2.FieldCount-1 do
       begin
          XlSheet.cells[2,i+1].value:=DBGridEh2.columns[i].Title.Caption;
       end;
      q_obat_keluar.First;
      x:=1;
    //      sprog_export_obatmasuk.Show;
          sprog_export_obatmasuk.Max:= q_obat_keluar.RecordCount;

      while not q_obat_keluar.Eof do
      begin
       sprog_export_obatmasuk.Position:= q_obat_keluar.RecNo;
       for i:=0 to DBGridEh2.FieldCount-1 do
        begin
        XlSheet.cells[2+x,i+1].value:=DBGridEh2.Fields[i].Text;
        end;
        q_obat_keluar.Next;
       inc(x);

      end;

    end;
      sprog_export_obatmasuk.Hide;
      DBGridEh2.Show;
        XlApp.ActiveWorkbook.SaveAs(dlgSavedoalog.FileName);
        XlApp.visible:=true;

   except Exit;
   end;

end;

procedure Tfrm_obat_keluar.sBitBtn1Click(Sender: TObject);
begin
         q_printNota.SQL.Text:= 'CALL `sp_cetak_nota`("'+q_obat_keluarno_nota.Text+'",'+q_obat_keluaradaRacikan.Text+','+q_obat_keluarbiaya_racikan.Text+')';
      q_printNota.Open;
      report__Print_obat_keluar.PrintReport;
end;

end.
