unit U_koneksi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sLabel, ExtCtrls, sPanel, sButton;

type
  Tfrm_koneksi = class(TForm)
    pnl_1: TsPanel;
    lbl_1: TsLabel;
    edt_server: TsEdit;
    lbl_2: TsLabel;
    edt_user: TsEdit;
    lbl_3: TsLabel;
    edt_pass: TsEdit;
    lbl_4: TsLabel;
    edt_port: TsEdit;
    lbl_5: TsLabel;
    edt_database: TsEdit;
    pnl_2: TsPanel;
    sButton1: TsButton;
    sButton2: TsButton;
    sButton3: TsButton;
    procedure sButton2Click(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_koneksi: Tfrm_koneksi;
  konek: Boolean;

implementation

uses
  u_bantu, U_DM, MyAccess, DBAccess, MenuUtama;

{$R *.dfm}

procedure Tfrm_koneksi.sButton1Click(Sender: TObject);
begin
  try
    with dm.my_connection do
    begin
      Server := edt_server.Text;
      Username := edt_user.Text;
      Password := edt_pass.Text;
      Port := StrToInt( edt_port.Text);
      Database := edt_database.Text;
      Connect;
    end;
    konek:= True;
    MessageInfo('Database berhasil terkoneksi ...');
  except
    MessageError('Gagal koneksi database ...');
    konek:= False;
  end;
end;

procedure Tfrm_koneksi.sButton2Click(Sender: TObject);
var MyText: TStringList;
begin

  sButton1Click(Sender);
  if konek = True then
  begin
    try
      MyText:= TStringlist.create;
      MyText.Add(edt_server.Text);
      MyText.Add(edt_user.Text);
      MyText.Add(edt_pass.Text);
      MyText.Add(edt_database.Text);
      MyText.Add(edt_port.Text);
      MyText.SaveToFile('appklinik.dll');
    finally
      MyText.Free
    end; {try}
  
    if not IsFormOpen('frm_menu_utama') then
      frm_menu_utama := tfrm_menu_utama.Create(Self) ;
    Hide;
    frm_menu_utama.Show;
  end;


end;



procedure Tfrm_koneksi.sButton3Click(Sender: TObject);
begin
    Close;
end;

procedure Tfrm_koneksi.FormCreate(Sender: TObject);
begin
    konek:= False;
end;

end.
