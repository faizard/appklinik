unit U_inputJumlahObat;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, sEdit, sLabel, ExtCtrls, sPanel,
  ComCtrls, sPageControl, sComboBox, sGroupBox, Mask, sMaskEdit,
  sCustomComboEdit, sToolEdit;

type
  Tfrm_input_jlhObat = class(TForm)
    sPanel1: TsPanel;
    lbl_11: TsLabel;
    edt_nm_obat: TsEdit;
    sLabel1: TsLabel;
    edt_stok: TsEdit;
    sPanel2: TsPanel;
    sPanel3: TsPanel;
    btn_obat_client: TsBitBtn;
    btn_obat_client1: TsBitBtn;
    pnl1: TPanel;
    sDateEdit1: TsDateEdit;
    lbl1: TLabel;
    sLabel8: TsLabel;
    cbb_jenis: TsComboBox;
    cbb_satuan: TsComboBox;
    sLabel9: TsLabel;
    sGroupBox1: TsGroupBox;
    sLabel4: TsLabel;
    sLabel5: TsLabel;
    sLabel6: TsLabel;
    edt_1: TsEdit;
    cbb_satuanPack: TsComboBox;
    edt_jlh_pack: TsEdit;
    edt_2: TsEdit;
    sLabel16: TsLabel;
    sLabel17: TsLabel;
    sLabel18: TsLabel;
    sLabel19: TsLabel;
    sLabel20: TsLabel;
    sLabel21: TsLabel;
    sPanel4: TsPanel;
    lbl_16: TsLabel;
    lbl_8: TsLabel;
    sLabel3: TsLabel;
    sLabel7: TsLabel;
    sLabel2: TsLabel;
    sLabel12: TsLabel;
    sLabel13: TsLabel;
    edt_jlh_obat: TsEdit;
    edt_tot_harga: TsEdit;
    edt_hrgBeli: TsEdit;
    edt_bath: TsEdit;
    sGroupBox2: TsGroupBox;
    sLabel10: TsLabel;
    sLabel11: TsLabel;
    lbl8: TLabel;
    sLabel14: TsLabel;
    sLabel15: TsLabel;
    cbb_satuanracik: TsComboBox;
    edt_jlhperRacik: TsEdit;
    edt_hrgajual: TsEdit;
    cbb_satuanMasuk: TsComboBox;
    sEdit1: TsEdit;
    sLabel22: TsLabel;
    sEdit2: TsEdit;
    procedure FormShow(Sender: TObject);
    procedure edt_jlh_obatKeyPress(Sender: TObject; var Key: Char);
    procedure edt_tot_hargaChange(Sender: TObject);
    procedure edt_hrgBeliChange(Sender: TObject);
    procedure btn_obat_clientClick(Sender: TObject);
    procedure edt_JualChange(Sender: TObject);
    procedure edt_hrgajualChange(Sender: TObject);
    procedure cbb_jenisChange(Sender: TObject);
    procedure btn_obat_client1Click(Sender: TObject);
    procedure edt_jlh_packChange(Sender: TObject);
    procedure cbb_satuanChange(Sender: TObject);
    procedure cbb_satuanMasukChange(Sender: TObject);
    procedure cbb_satuanMasukEnter(Sender: TObject);
    procedure edt_tot_hargaExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure ObatBaru;
    procedure ObatLama;
    procedure ClearAllEditFields;
    function Validasi():BOOL;
//    procedure getCbbSatuan();
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_input_jlhObat: Tfrm_input_jlhObat;
//  baru : Boolean;

implementation

uses
  U_Obat, U_DM, u_bantu;

{$R *.dfm}




function Tfrm_input_jlhObat.Validasi: BOOL;
begin
  Result:= true ;
    if
    (edt_nm_obat.Text='') or
    (edt_hrgajual.Text='') or
    (edt_jlh_obat.Text='') or
    (cbb_jenis.ItemIndex =-1) or
//    (cbb_satuanracik.ItemIndex =-1) or
//    (edt_jlhperRacik.Text='') or
    (cbb_jenis.Text ='') or
//    (cbb_satuan.ItemIndex = -1) or
    (cbb_satuan.Text ='') or
//    (edt_jlhperRacik.Text ='') or
    (edt_tot_harga.Text='')
     then
    Result:= False

//    (edt_jlh_obat.Text='') or
end;

procedure Tfrm_input_jlhObat.ClearAllEditFields;
begin
    edt_jlh_obat.Clear;
    edt_tot_harga.Clear;
    edt_hrgajual.Clear;
    //        edt_Jual.Clear;
    edt_jlh_pack.Clear;
    sDateEdit1.Text:= '  /  /    ';
    edt_nm_obat.Clear;
    cbb_jenis.ItemIndex:=-1;
    edt_stok.Clear;
    edt_hrgBeli.Clear;
    edt_hrgajual.Clear;
    cbb_jenis.Text:='';
//    date_jatuhtempo.Text:='  /  /    ';
    edt_bath.Clear;
    sEdit1.Text:='0';
    sEdit2.Clear;
    edt_jlh_obat.SetFocus;
    cbb_satuan.Text:='';
    edt_1.Text:='';

     
end;


procedure Tfrm_input_jlhObat.ObatBaru;
begin
    baru := True;
    edt_nm_obat.ReadOnly:= false;
    edt_nm_obat.Color:= clWhite ;

    edt_stok.Text:= '0';


    edt_hrgajual.ReadOnly:= false;
    edt_hrgajual.Color:= clWhite ;


    cbb_jenis.ReadOnly:= false;
    cbb_jenis.Color:= clWhite ;

    edt_1.ReadOnly:= false;
    edt_1.Color:= clWhite ;

    cbb_satuanPack.ReadOnly:= false;
    cbb_satuanPack.Color:= clWhite ;

    cbb_satuan.ReadOnly:= false;
    cbb_satuan.Color:= clWhite ;

    cbb_satuanracik.ReadOnly:= false;
    cbb_satuanracik.Color:= clWhite ;

    edt_jlhperRacik.ReadOnly:= false;
    edt_jlhperRacik.Color:= clWhite ;


    edt_hrgBeli.ReadOnly:= false;
    edt_hrgBeli.Color:= clWhite ;

    edt_2.Text:= '';
//    cbb_satuanMasuk.Clear;
//    cbb_satuanMasuk.ReadOnly:= True;
//    cbb_satuanMasuk.Color:= $00E5E5E5 ;


end;

procedure Tfrm_input_jlhObat.ObatLama;
begin

    sEdit2.PasswordChar :=#32;
    edt_hrgBeli.PasswordChar:= #32;

    edt_hrgBeli.ReadOnly:= True;
    edt_hrgBeli.Color:= $00E5E5E5 ;

    
    baru := False;

    edt_nm_obat.ReadOnly:= True;
    edt_nm_obat.Color:= $00E5E5E5 ;

    edt_stok.Text:= '0';

    edt_hrgajual.ReadOnly:= True;
    edt_hrgajual.Color:= $00E5E5E5 ;

    cbb_jenis.ReadOnly:= True;
    cbb_jenis.Color:= $00E5E5E5 ;

    edt_1.ReadOnly:= True;
    edt_1.Color:= $00E5E5E5 ;

    cbb_satuanPack.ReadOnly:= True;
    cbb_satuanPack.Color:= $00E5E5E5 ;

    cbb_satuan.ReadOnly:= True;
    cbb_satuan.Color:= $00E5E5E5 ;

    cbb_satuanracik.ReadOnly:= True;
    cbb_satuanracik.Color:= $00E5E5E5 ;

    edt_jlhperRacik.ReadOnly:= True;
    edt_jlhperRacik.Color:= $00E5E5E5 ;

   case cbb_jenis.ItemIndex of
        0:
        begin
         sPanel2.Caption:='DATA CREAM';
         temp_jenis:= cream;
        end;
        1:begin
            sPanel2.Caption:='DATA OBAT';
            temp_jenis:= nonCream;
          end;
   end;

  if UpperCase(dm.q_obatjenis.Text) = cream then
//        tabKontrol.ActivePage:= tabCream
        cbb_jenis.ItemIndex:=0
  else
        cbb_jenis.ItemIndex:= 1;

    edt_nm_obat.Text := dm.q_obatnm_obat.Text;
    edt_hrgajual.Text := FormatFloat(',#',dm.q_obathrga_jual.AsInteger);
    edt_stok.Text := dm.q_obatquantity.Text +' '+dm.q_obatsatuan.Text;
    edt_1.Text:=  dm.q_obatjlhper_pack.Text;
    cbb_satuanPack.Text:= dm.q_obatsatuan_pack.Text;
    //      cbb_satuan.Text:= dm.q_obatsatuan.Text;

    cbb_satuan.Text:= dm.q_obatsatuan.Text;
    edt_2.Text:= dm.q_obatsatuan.Text;
    cbb_satuanMasuk.Text:= dm.q_obatsatuan.Text;
    cbb_satuanracik.Text:= dm.q_obatsatuan_racik.Text;
    edt_jlhperRacik.Text:= dm.q_obatjlhper_raciik.Text;

    cbb_satuanMasuk.Clear;
    getComboQuery(cbb_satuanMasuk,'satuan','CALL `getSatuan`("'+dm.q_obatid.Text+'")');
    cbb_satuanMasuk.ReadOnly:= false;
    cbb_satuanMasuk.Color:= clWhite ;



      



end;

procedure Tfrm_input_jlhObat.FormShow(Sender: TObject);
begin
   save_temp:= False;
   ClearAllEditFields();

   edt_1.Text:='1';



   getCombo(cbb_satuanPack,'satuan','t_satuan');
   getCombo(cbb_satuan,'satuan','t_satuan');
   getCombo(cbb_satuanracik,'satuan','t_satuan');

//   getCombo(cbb_jenis,'jenis','t_jenis');
  cbb_jenis.Clear ;
  cbb_jenis.Items.Add(cream);
  cbb_jenis.Items.Add(nonCream);


  if status_barang = True then
  begin
    ObatBaru;
    edt_nm_obat.SetFocus;
    sEdit2.PasswordChar :=#0;
    edt_hrgBeli.PasswordChar:= #0;
    Exit;
  end;

    ObatLama;
     edt_jlhperRacik.Text:='1';







end;

procedure Tfrm_input_jlhObat.edt_jlh_obatKeyPress(Sender: TObject;
  var Key: Char);
begin

 // #8 is Backspace
  if not (Key in [#8, '0'..'9']) then begin
    Key := #0;
    Exit;
  end;

end;

procedure Tfrm_input_jlhObat.edt_tot_hargaChange(Sender: TObject);
var jlhPack, jlhsatuan : Integer;
begin
  if (edt_jlh_obat.Text ='')
    or (edt_tot_harga.Text='')
  then Exit;
      Ribuan(edt_tot_harga);

    if baru then  jlhPack := HapusFormat(edt_1) else jlhPack := dm.q_obatjlhper_pack.AsInteger;
//    ShowMessage(IntToStr(cbb_satuanMasuk.ItemIndex));

    temp_tothargaBeli :=  HapusFormat(edt_tot_harga);


    case cbb_satuanMasuk.ItemIndex of
        0 :
        begin
            temp_jlh:=    (jlhPack *   HapusFormat(edt_jlh_obat) );
            temp_harga_beli_satuan := Round (  HapusFormat(edt_tot_harga) / temp_jlh );
            temp_harga_beli_pack :=  Round(temp_tothargaBeli / HapusFormat(edt_jlh_obat) );
            sEdit2.Text:= IntToStr(temp_harga_beli_pack);
        end;

        1 :
        begin
//              temp_harga_beli_satuan := Round (  HapusFormat(edt_tot_harga) / (jlhPack *   HapusFormat(edt_jlh_obat) ) );
              temp_jlh:= HapusFormat(edt_jlh_obat);
              temp_harga_beli_pack :=  Round( temp_tothargaBeli /temp_jlh );
              temp_harga_beli_satuan :=   temp_harga_beli_pack;
              sEdit2.Text:= '0';

        end;
        else
        begin
//           jlhsatuan:= HapusFormat(edt_jlh_obat);
//           temp_jlh := jlhsatuan;
        end;
    end;

//       temp_harga_beli_satuan := HapusFormat(edt_tot_harga) * (jlhPack *   HapusFormat(edt_jlh_obat));






      edt_hrgBeli.Text:= IntToStr(temp_harga_beli_satuan)  ;


    temp_harga_tot:= HapusFormat(edt_tot_harga);
//    temp_harga_beli := round(temp_harga_beli_satuan);
//    edt_hrgBeli.Text:= IntToStr(Round(temp_harga_beli));
end;

procedure Tfrm_input_jlhObat.edt_hrgBeliChange(Sender: TObject);
begin
         Ribuan(TsEdit(Sender));

//         temp_harga_tot:= temp_jlh * HapusFormat(TsEdit(Sender));
//         edt_tot_harga.Text:= IntToStr(temp_harga_tot)    ;
end;

procedure Tfrm_input_jlhObat.btn_obat_clientClick(Sender: TObject);
begin

  if not Validasi then Exit;

  if temp_harga_beli > HapusFormat(edt_hrgajual) then
  begin
    temp_harga_beli := Round(temp_harga_tot /temp_jlh);
    MessageError('Harga BELI tidak boleh lebih tinggi dari harga JUAL ...!');
    Exit;
  end;




    temp_tglexpire:= sDateEdit1.Date;
    temp_bath:= edt_bath.Text;
    temp_namabarang:= edt_nm_obat.Text;
    temp_satuan:= cbb_satuan.Text;
    temp_harga_jual:= HapusFormat(edt_hrgajual);
    temp_jenis := cbb_jenis.Text;
    if edt_1.Text ='' then edt_1.Text:='0';
    temp_jlh_per_pack:= StrToInt(edt_1.Text);
    temp_satuan_pack := cbb_satuanPack.Text;
    temp_harga_beli:= HapusFormat(edt_hrgBeli);
    temp_diskon := HapusFormat(sEdit1);
    temp_diskonHarga:= Round (temp_tothargaBeli * (temp_diskon/100));
    temp_harga_tot := Round(temp_tothargaBeli) - temp_diskonHarga ;
//    temp_tgl_jatuh_tempo := date_jatuhtempo.Date;

//    if temp_jenis = cream then
//    begin
        if edt_jlhperRacik.Text ='' then edt_jlhperRacik.Text:='0';
        temp_jlh_per_racik:= StrToInt(edt_jlhperRacik.Text);
        temp_satuan_racik := cbb_satuanracik.Text;
//    end;


    
    save_temp := True;
    Close;
end;

procedure Tfrm_input_jlhObat.edt_JualChange(Sender: TObject);
begin
//  if TsEdit(Sender).Text ='' then Exit;
//      Ribuan(TsEdit(Sender));
//      temp_harga_jual:= HapusFormat(edt_Jual);
end;


procedure Tfrm_input_jlhObat.edt_hrgajualChange(Sender: TObject);
begin
    Ribuan(TsEdit(Sender));
end;

procedure Tfrm_input_jlhObat.cbb_jenisChange(Sender: TObject);
begin
   case cbb_jenis.ItemIndex of
        0:
        begin
            sPanel2.Caption:='DATA '+UpperCase(cream);
//            cbb_satuanracik.ReadOnly:= False;
//            cbb_satuanracik.Color:= clWhite;
//            edt_jlhperRacik.ReadOnly:= false;
//            edt_jlhperRacik.Color:= clWhite ;
//            cbb_satuanracik.Text:='GRAM';
//            edt_jlhperRacik.Clear;
            edt_jlhperRacik.Text:='1';
        end;
        1:begin
              sPanel2.Caption:='DATA '+UpperCase(nonCream);
//              cbb_satuanracik.ReadOnly:= True;
//              cbb_satuanracik.Color:= $00E5E5E5 ;
//              edt_jlhperRacik.ReadOnly:= True;
//              edt_jlhperRacik.Color:= $00E5E5E5 ;
//              cbb_satuanracik.Text:='-';
              edt_jlhperRacik.Text:='1';

          end;
   end;
end;

procedure Tfrm_input_jlhObat.btn_obat_client1Click(Sender: TObject);
begin
    Close;
end;

procedure Tfrm_input_jlhObat.edt_jlh_packChange(Sender: TObject);
begin
    if edt_jlh_pack.Text = '' then Exit;
      edt_jlh_obat.Text:= IntToStr(dm.q_obatjlhper_pack.AsInteger * StrToInt(edt_jlh_pack.Text));    
end;

procedure Tfrm_input_jlhObat.cbb_satuanChange(Sender: TObject);
begin
    edt_2.Text:= cbb_satuan.Text;
//    cbb_satuanMasuk.Text:= cbb_satuan.Text;
    sLabel2.Caption:= 'Harga Jual / '+ cbb_satuan.Text;
    cbb_satuanracik.ItemIndex := cbb_satuan.ItemIndex;
    cbb_satuanPack.ItemIndex := cbb_satuan.ItemIndex;
    edt_jlhperRacik.Text:='1';
end;

procedure Tfrm_input_jlhObat.cbb_satuanMasukChange(Sender: TObject);
begin
    edt_tot_harga.Clear;
    edt_hrgBeli.Clear;
//    ShowMessage(IntToStr( cbb_satuanMasuk.ItemIndex));
end;

procedure Tfrm_input_jlhObat.cbb_satuanMasukEnter(Sender: TObject);
begin
  if baru = true then begin
      cbb_satuanMasuk.Clear;

      cbb_satuanMasuk.Items.Add(' '+cbb_satuanPack.Text);
      cbb_satuanMasuk.Items.Add('-'+cbb_satuan.Text);

     end;
end;

procedure Tfrm_input_jlhObat.edt_tot_hargaExit(Sender: TObject);
begin
//      ShowMessage(IntToStr(temp_jlh));
end;

procedure Tfrm_input_jlhObat.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    id_obat:='';
end;

end.
