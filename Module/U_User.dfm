object frm_user: Tfrm_user
  Left = 943
  Top = 146
  BorderStyle = bsToolWindow
  Caption = 'Form User'
  ClientHeight = 809
  ClientWidth = 858
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Trebuchet MS'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 16
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 858
    Height = 68
    Align = alTop
    Caption = 'AKSES USER'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Trebuchet MS'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    SkinData.SkinSection = 'HINT'
  end
  object sPanel2: TsPanel
    Left = 0
    Top = 68
    Width = 858
    Height = 329
    Align = alTop
    TabOrder = 1
    Visible = False
    SkinData.SkinSection = 'PANEL'
    object Label1: TLabel
      Left = 36
      Top = 24
      Width = 49
      Height = 16
      Caption = 'Username'
      FocusControl = dbedtusername
      Transparent = True
    end
    object Label2: TLabel
      Left = 36
      Top = 54
      Width = 46
      Height = 16
      Caption = 'Password'
      FocusControl = dbedtpassword
      Transparent = True
    end
    object Label3: TLabel
      Left = 36
      Top = 102
      Width = 65
      Height = 16
      Caption = 'Jenis Kelamin'
      Transparent = True
    end
    object Label4: TLabel
      Left = 36
      Top = 132
      Width = 29
      Height = 16
      Caption = 'No HP'
      FocusControl = dbedtno_hp
      Transparent = True
    end
    object Label5: TLabel
      Left = 36
      Top = 162
      Width = 33
      Height = 16
      Caption = 'Alamat'
      FocusControl = dbmmoalamat
      Transparent = True
    end
    object Label6: TLabel
      Left = 36
      Top = 258
      Width = 32
      Height = 16
      Caption = 'Status'
      FocusControl = dbmmoalamat
      Transparent = True
    end
    object Label7: TLabel
      Left = 36
      Top = 78
      Width = 70
      Height = 16
      Caption = 'Password Lagi'
      Transparent = True
    end
    object dbedtusername: TDBEdit
      Left = 132
      Top = 24
      Width = 682
      Height = 26
      DataField = 'username'
      DataSource = dm.ds_user
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Trebuchet MS'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object dbedtpassword: TDBEdit
      Left = 132
      Top = 54
      Width = 681
      Height = 22
      DataField = 'password'
      DataSource = dm.ds_user
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Wingdings 2'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'u'
      TabOrder = 1
    end
    object dbedtno_hp: TDBEdit
      Left = 132
      Top = 132
      Width = 196
      Height = 26
      DataField = 'no_hp'
      DataSource = dm.ds_user
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Trebuchet MS'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
    end
    object dbmmoalamat: TDBMemo
      Left = 132
      Top = 162
      Width = 682
      Height = 89
      DataField = 'alamat'
      DataSource = dm.ds_user
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Trebuchet MS'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
    end
    object sButton1: TsButton
      Left = 354
      Top = 258
      Width = 75
      Height = 25
      Caption = 'Simpan'
      TabOrder = 7
      OnClick = sButton1Click
      SkinData.SkinSection = 'BUTTON'
      Images = dm.ilimages
      ImageIndex = 50
    end
    object sButton2: TsButton
      Left = 438
      Top = 258
      Width = 91
      Height = 25
      Caption = 'Batal'
      TabOrder = 8
      OnClick = sButton2Click
      SkinData.SkinSection = 'BUTTON'
      Images = dm.ilimages
      ImageIndex = 47
    end
    object dbcbbstatus: TDBComboBox
      Left = 132
      Top = 258
      Width = 187
      Height = 24
      DataField = 'status'
      DataSource = dm.ds_user
      ItemHeight = 16
      Items.Strings = (
        'Operator'
        'Admin')
      TabOrder = 6
    end
    object edt1: TEdit
      Left = 132
      Top = 78
      Width = 682
      Height = 22
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Wingdings 2'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'u'
      TabOrder = 2
    end
    object dbcbbstatus1: TDBComboBox
      Left = 132
      Top = 102
      Width = 187
      Height = 24
      DataField = 'jk'
      DataSource = dm.ds_user
      ItemHeight = 16
      Items.Strings = (
        'Laki - Laki'
        'Perampuan')
      TabOrder = 3
    end
  end
  object sPanel3: TsPanel
    Left = 0
    Top = 397
    Width = 858
    Height = 56
    Align = alTop
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object btn_3: TsBitBtn
      Left = 230
      Top = 14
      Width = 98
      Height = 32
      Caption = 'Hapus'
      TabOrder = 0
      OnClick = btn_3Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCECDFAF9F9FEFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFF8F8FEC5C5F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        D1D0FB4E4BF2413FEDF9F9FEFFFFFFFFFFFFFFFFFFFFFFFFF8F8FE2624E4302E
        EAC6C5F8FFFFFFFFFFFFFFFFFFD3D2FC5755F56260FA5754F64240EDF9F9FEFF
        FFFFFFFFFFF8F8FE2D2CE6403EF14B49F6302EEAC6C5F8FFFFFFFFFFFFE3E2FD
        5A57F66461FA706FFF5855F64341EEF9F9FEF9F9FE3633E94644F26261FF4947
        F42E2CE9DAD9FAFFFFFFFFFFFFFFFFFFE3E3FD5A58F66562FA7370FF5957F644
        42EE3F3DEC4F4CF46766FF4F4DF53533EBDBDAFBFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFE3E3FD5B59F66663FA7371FF726FFF6F6DFF6D6BFF5654F73E3CEEDCDC
        FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3E3FD5C5AF77875FF58
        55FF5653FF716FFF4745F0DEDDFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFAFAFF5D5AF67C78FF5D5AFF5A57FF7573FF4643EFF9F9FEFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFF6764F96F6CFB7F7DFF7D
        7AFF7B78FF7876FF5D5BF74845EFF9F9FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FBFAFF6F6CFC7673FD8581FF7572FC6361F85F5CF76C69FA7A78FF5F5CF74946
        EFF9F9FEFFFFFFFFFFFFFFFFFFFBFBFF7471FE7C79FE8986FF7B78FD6B68FBE5
        E4FEE4E3FE605DF86D6BFA7C79FF605EF74A47F0FBFBFFFFFFFFFFFFFFEEEEFF
        7976FF807DFF807DFE7370FDE6E6FEFFFFFFFFFFFFE4E4FE615EF86E6CFB7D7B
        FF615EF8B0AEF8FEFEFFFFFFFFFFFFFFEEEEFF7976FF7875FEE7E7FFFFFFFFFF
        FFFFFFFFFFFFFFFFE4E4FE6360F86967F98E8CF7E3E2FDFFFFFFFFFFFFFFFFFF
        FFFFFFEEEEFFE8E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4E4FEB8B7
        FCD6D6FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9FFFFFFFFFFFFFFFFFFFF}
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
    end
    object btn_refresh: TsBitBtn
      Left = 138
      Top = 14
      Width = 83
      Height = 32
      Caption = 'Refresh'
      TabOrder = 1
      OnClick = btn_refreshClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFDFEFD9FC2A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8ABB8F5D9C63FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF9ACAA065AF6D60A9673C8A4336823D317A363C7F426396679EBB
        A0E5EDE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAAD8AF72BC7B95D19E93CF9B8E
        CD9589CA9084C78A79BD8064AC6B4A915067966BCDDCCEFFFFFFFFFFFFFFFFFF
        FFFFFFA8DAAF78C3829DD7A69AD4A396D29E91CF998CCC9487CA8F79C1817DC3
        845CA36269986BE6EDE6FFFFFFFFFFFFFFFFFFFFFFFFA4D9AA7AC78476C18053
        AA5D4DA256489A5062AB6A82C28A86C98E81C5884F9655A0BEA2FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF9DD7A47AC683FFFFFFFFFFFFFFFFFF9BC99F5AA16185C5
        8D87C98E6EB275689C6CB8D7BBB6D4B8B3D1B6B1CEB4AFCBB1FDFEFDB4E1BAFF
        FFFFFFFFFFFFFFFFFFFFFFA3CEA7539F5A47944E3F8A46468B4D5CA9634B9B53
        47944E488F4E97BD9AFFFFFFFFFFFFFFFFFFFFFFFF92B193FCFDFCBEDFC1BBDC
        BFB9D9BCB7D6BAB5D2B784C28B7FC2888CCC9482C38953985990BA94FFFFFFFF
        FFFFFFFFFF49804C739C75FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7DEBB74BE7D
        97D2A093CF9B85C78C5DA66438853F337D392D7532488F4E448A497EA480FFFF
        FFFFFFFFFFFFFFFFFFFFEDF7EE8ECC9584C98D9AD4A38ECE9791CF998CCC9487
        CA8F82C58A7DC38478C07E468C4B87AB88FFFFFFFFFFFFFFFFFFFFFFFFDCF0DE
        8FCE9678C28288CA9193D09B94D19D8FCF988BCB9386C98E7FC3864D94538FB3
        91FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8EEB8E1BD89C89063B36B4FA5594A
        9D5244954C5FA7675AA1618CB68FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF67AA6D8ABB8FFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFABD3AFFDFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
    end
    object btn_4: TsBitBtn
      Left = 16
      Top = 14
      Width = 116
      Height = 32
      Caption = 'Tambah'
      TabOrder = 2
      OnClick = btn_4Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5F0E79FC8A5559B5E3F
        8E483B8C444C955297C19BE1EDE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFC3DDC8569F6340984F7CC18E95D0A595CFA577BD88358C41408C47B9D5
        BBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6DFCB549F6363B377A7DBB486CC9765
        BB7C63B97B85CB97A4D9B357A96A34853CB9D5BBFFFFFFFFFFFFFFFFFFE9F3EB
        66AB7569B87CA7DBB15FBB765BB97258B76F58B46E57B46E5AB673A4D9B259A9
        6B418E48E2EEE3FFFFFFFFFFFFAED4B852AA67A9DDB363C0785EBD705FBB76FF
        FFFFFFFFFF58B76F57B46D5BB673A5DAB3378E4296C19AFFFFFFFFFFFF76B788
        89CC9788D3956AC57962C06F54AA64FFFFFFFFFFFF58B76F58B76F5AB87184CC
        967ABD8C4C9554FFFFFFFFFFFF69B17EA8DDB27CCF8974CC80FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF58B76F66BD7C9BD4AA3A8B43FFFFFFFFFFFF6DB482
        B5E2BD8AD59679C985FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF58B76F68C0
        7D9CD4A93E8E48FFFFFFFFFFFF81BF94ABDDB5A5DFAE80CB8B7BC9856DBC78FF
        FFFFFFFFFF5AAB695FBB765BB9728AD1987FC491579D60FFFFFFFFFFFFB8DBC3
        84C796D2EED794D99F89D3937EC888FFFFFFFFFFFF78CD846AC27B6EC77DABDF
        B4449D56A0C8A6FFFFFFFFFFFFECF6EF7EBE92A9DAB6D8F1DC91D89C87CD9283
        CC8D8AD49589D49482D28DAEE0B66AB87C5AA266E6F1E8FFFFFFFFFFFFFFFFFF
        D1E9D975BA8BAEDCBADCF2E0B5E4BC9ADBA495D99FA4DFAEBFE8C478C18957A1
        65C4DEC9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD1E9D97EBE9293CEA3C2E6CBCF
        EBD4C9E9CEAEDDB76CB87E67AD77C7E0CDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFECF6EFB9DCC482BF9570B6856DB48178B989B1D5BAE8F3EBFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
    end
    object sBitBtn4: TsBitBtn
      Left = 338
      Top = 14
      Width = 98
      Height = 32
      Caption = 'Edit'
      TabOrder = 3
      OnClick = sBitBtn4Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 4
      Images = dm.ilimages
      Reflected = True
    end
  end
  object dbg_entry_obat: TDBGridEh
    Left = 0
    Top = 453
    Width = 858
    Height = 356
    Align = alClient
    DataSource = dm.ds_user
    DrawMemoText = True
    DynProps = <>
    Flat = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Trebuchet MS'
    Font.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
    ParentFont = False
    SearchPanel.Enabled = True
    SearchPanel.FilterOnTyping = True
    TabOrder = 3
    TitleParams.MultiTitle = True
    OnTitleClick = dbg_entry_obatTitleClick
    Columns = <
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'id'
        Footers = <>
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'username'
        Footers = <>
        Width = 206
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'jk'
        Footers = <>
        Width = 65
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'no_hp'
        Footers = <>
        Width = 118
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'alamat'
        Footers = <>
        Width = 125
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'status'
        Footers = <>
        Width = 189
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object XPManifest1: TXPManifest
    Left = 536
    Top = 244
  end
end
